import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, forwardRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { Ng4LoadingSpinnerModule } from 'ngx-spinner';
import { NgxSpinnerModule } from 'ngx-spinner';
import 'hammerjs';
import localeRu from '@angular/common/locales/ru';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { registerLocaleData } from '@angular/common';
import { MatPaginatorIntl } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatPaginatorIntlRus } from './shared/services/global.service';
import { SystemModule } from './system/system.module';
import { AuthModule } from './auth/auth.module';
import { AuthGuard } from './shared/services/auth.guard';

registerLocaleData(localeRu);

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    // Ng4LoadingSpinnerModule.forRoot(),
    // NgxSpinnerModule,
    AuthModule,
    AppRoutingModule,
    SystemModule,
    NgbModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    AuthGuard,
    { provide: LOCALE_ID, useValue: 'ru' },
    { provide: 'pathStr', useValue: '' },
    { provide: 'fileStr', useValue: '' },
    { provide: 'parentStr', useValue: '' },
    {
      provide: MatPaginatorIntl,
      useClass: forwardRef(() => MatPaginatorIntlRus)
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
