import { Component, OnInit } from '@angular/core';
import { Airuser } from 'src/app/shared/models/airuser.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { GlobalsettingsService } from '../../shared/services/globalsettings.service';
import { AccessstoreService } from '../../shared/services/accessstore.service';
import { AiruserService } from '../../shared/services/airuser.service';
import { PositionService } from '../../shared/services/position.service';
import { SubdivisionService } from '../../shared/services/subdivision.service';
import { AccessrightService } from '../../shared/services/accessright.service';
import { OrganisationstoreService } from '../../shared/services/organisationstore.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';
import { Subdivision } from 'src/app/shared/models/subdivision.model';
import { Position } from 'src/app/shared/models/position.model';

@Component({
  selector: 'app-accessset',
  templateUrl: './accessset.component.html',
  styleUrls: ['./accessset.component.scss']
})
export class AccesssetComponent implements OnInit {
  airusers: Airuser[];
  airusersView: Airuser[];
  userCtrl: FormControl;
  filteredUsers: Observable<Airuser[]>;

  positions: Position[];
  subdivisions: Subdivision[];
  subdivisionsView: Subdivision[];

  selectedId: number;
  // selected: Airuser;
  selectedSubdivId: number;
  selectedUserAccess = [];

  getTt = false;
  editTt = false;
  getTtDisabled = true;
  editTtDisabled = true;

  getHr = false;
  editHr = false;
  editDoc = false;
  getPi = false;
  editPi = false;
  getCertificate = false;
  editCertificate = false;

  getHrDisabled = true;
  editHrDisabled = true;
  editDocDisabled = true;
  getPiDisabled = true;
  editPiDisabled = true;
  editCertificateDisabled = true;
  getCertificateDisabled = true;

  _getHrDisabled = false;
  _editHrDisabled = false;
  _editDocDisabled = false;
  _getPiDisabled = false;
  _editPiDisabled = false;
  _editCertificateDisabled = false;
  _getCertificateDisabled = false;

  ttChanges = false;
  hrChanges = false;
  docChanges = false;
  certChanges = false;
  piChanges = false;

  accessTimetableId: number;
  accessAiruserId: number;
  accessPositionId: number;
  accessAiruserphoneId: number;
  accessEducationId: number;
  accessPreviousworkId: number;
  accessFamilymemberworkId: number;
  accessAiruseremailId: number;
  accessSubdivisionId: number;
  accessAiruserfileId: number;
  accessAirusercertificateId: number;
  accessAirusercertificatefileId: number;
  accessAirdocumentId: number;
  accessAcquaintanceId: number;
  accessActivityId: number;
  accessAiruseractivityId: number;
  accessFlightpointchainId: number;

  constructor(
    public authService: AuthService,
    private globalsettingsService: GlobalsettingsService,
    private accessstoreService: AccessstoreService,
    private airuserService: AiruserService,
    private positionService: PositionService,
    private subdivisionService: SubdivisionService,
    private accessrightService: AccessrightService,
    private organisationstoreService: OrganisationstoreService,
    private spinnerService: NgxSpinnerService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  async ngOnInit() {
    this.userCtrl = new FormControl();
    await this.getData();
    this.filteredUsers = this.userCtrl.valueChanges.pipe(
      startWith(''),
      map(user => {
        return user ? this.filterUsers(user) : this.airusersView;
      })
    );
  }

  async getData() {
    this.spinnerService.show();
    try {
      let airusers = this.airuserService.getAll();
      let positions = this.positionService.getAll();
      let subdivisions = this.subdivisionService.getAll();
      this.airusers = isNullOrUndefined(await airusers) ? [] : await airusers;
      this.positions = isNullOrUndefined(await positions)
        ? []
        : await positions;
      this.subdivisions = isNullOrUndefined(await subdivisions)
        ? []
        : await subdivisions;
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }

    this.airusersView = [];
    this.subdivisionsView = [];
    try {
      /**Выбираем пользователей из своего и дочерних подразделений */
      if (this.authService.loggedInUserRole === 1) {
        if (
          isNullOrUndefined(
            this.positions.find(
              position =>
                position.id === this.authService.loggedInUser.position_id
            )
          )
        ) {
          this.globalsettingsService.openSnackBar(`Нет доступа.`, 'Ok');
          this.spinnerService.hide();
          this.router.navigate(['/sys/organisation/subdivisions']);
        }
        this.airusers.map(airuser => {
          if (
            airuser.isadmin === false &&
            airuser.id !== this.authService.loggedInUser.id &&
            this.organisationstoreService.isAiruserInSubdivision(
              airuser,
              [this.authService.loggedInUserSubdivision],
              this.positions,
              this.subdivisions
            )
          ) {
            this.airusersView.push(airuser);
          }
        });
        /**Устанавливаем подразделения, которыми можно управлять */
        this.subdivisionsView.push(
          this.subdivisions.find(
            subdiv => subdiv.id === this.authService.loggedInUserSubdivision.id
          )
        );
        this.subdivisions.map(subdv => {
          if (
            this.accessstoreService.isSubdivisionInSubdivisions(
              subdv,
              [this.authService.loggedInUserSubdivision],
              this.subdivisions
            )
          ) {
            this.subdivisionsView.push(subdv);
          }
        });
      } else {
        this.airusersView = this.airusers.filter(
          airuser =>
            airuser.isadmin === false &&
            airuser.id !== this.authService.loggedInUser.id
        );
        this.subdivisionsView = this.subdivisions;
      }
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  async selectAiruser(id) {
    this.selectedId = id;
    this.getTtDisabled = true;
    this.editTtDisabled = true;
    this.ttChanges = false;
    if (isNullOrUndefined(this.selectedId)) {
      this.selectedSubdivId = null;
      this.userCtrl.reset();
      this.selectSubdiv(this.selectedSubdivId);
      this.selectedUserAccess = [];
      this.accessTimetableId = null;
      this.getTt = false;
      this.editTt = false;
    } else {
      try {
        let selectedUserAccess = this.accessrightService.getByAiruser(
          this.selectedId
        );
        this.selectedUserAccess = isNullOrUndefined(await selectedUserAccess)
          ? []
          : await selectedUserAccess;

        /**Устанавливаем возможность для текущего пользователя устанавливать права */
        if (this.authService.loggedInUser.isadmin) {
          this.getTtDisabled = true;
          this.editTtDisabled = false;
        } else {
          if (!isNullOrUndefined(this.authService.loggedInUserAccesses)) {
            this.authService.loggedInUserAccesses.map(item => {
              if (item.resourcename === 'timetable') {
                if (item.post) {
                  this.editTtDisabled = false;
                }
              }
            });
          }
        }
        /**Читаем текущие права на службы у пользователя */
        this.accessTimetableId = null;
        this.getTt = true;
        this.editTt = false;
        if (!isNullOrUndefined(this.selectedUserAccess)) {
          this.selectedUserAccess.map(item => {
            if (item.resourcename === 'timetable') {
              this.accessTimetableId = item.id;
              if (item.post) {
                this.editTt = true;
              }
            }
          });
        }
      } catch (err) {
        this.globalsettingsService.exceptionHandling(err);
      }
    }
  }

  async setDefaultAccess() {
    if (
      !isNullOrUndefined(this.selectedUserAccess) &&
      this.selectedUserAccess.length > 0
    ) {
      try {
        this.spinnerService.show();
        await Promise.all(
          this.selectedUserAccess.map(access => {
            return this.accessrightService.deleteOneById(access.id);
          })
        );
      } catch (err) {
        this.globalsettingsService.exceptionHandling(err);
      } finally {
        await this.selectAiruser(this.selectedId);
        this.spinnerService.hide();
      }
    }
  }

  async selectSubdiv(id) {
    /**Читаем текущие права на подразделения и модули */
    this.selectedSubdivId = id;

    this.getHr = false;
    this.editHr = false;
    this.editDoc = false;
    this.getPi = false;
    this.editPi = false;
    this.getCertificate = false;
    this.editCertificate = false;

    this.getHrDisabled = true;
    this.editHrDisabled = true;
    this.editDocDisabled = true;
    this.getPiDisabled = true;
    this.editPiDisabled = true;
    this.editCertificateDisabled = true;
    this.getCertificateDisabled = true;

    this.accessAiruserId = null;
    this.accessPositionId = null;
    this.accessAiruserphoneId = null;
    this.accessEducationId = null;
    this.accessPreviousworkId = null;
    this.accessFamilymemberworkId = null;
    this.accessAiruseremailId = null;
    this.accessSubdivisionId = null;
    this.accessAiruserfileId = null;
    this.accessAirusercertificateId = null;
    this.accessAirusercertificatefileId = null;
    this.accessAirdocumentId = null;
    this.accessAcquaintanceId = null;
    this.accessActivityId = null;
    this.accessAiruseractivityId = null;
    this.accessFlightpointchainId = null;
    this.hrChanges = false;

    if (isNullOrUndefined(this.selectedSubdivId)) {
      return;
    } else {
      this.selectedSubdivId = +this.selectedSubdivId;
    }

    /**Получаем подразделение выбранного пользователя */
    let airuser: Airuser;
    let position: Position;
    let subdivision: Subdivision;
    try {
      this.spinnerService.show();
      airuser = await this.airuserService.getOneById(this.selectedId);
      position = isNullOrUndefined(airuser.position_id)
        ? null
        : await this.positionService.getOneById(airuser.position_id);
      subdivision =
        isNullOrUndefined(position) ||
        isNullOrUndefined(position.subdivision_id)
          ? null
          : await this.subdivisionService.getOneById(position.subdivision_id);
    } catch (err) {
    } finally {
      this.spinnerService.hide();
    }

    /**Устанавливаем возможность для текущего пользователя устанавливать права */
    // if (this.authService.loggedInUser.isadmin) {
    this.getHrDisabled = false;
    this.editHrDisabled = false;
    this.editDocDisabled = false;
    this.getPiDisabled = false;
    this.editPiDisabled = false;
    this.editCertificateDisabled = false;
    this.getCertificateDisabled = false;
    /**Проверим права по умолчанию для пользователя и выставим, запретив изменение */
    /**если сотрудник в выбранном или в родительском выбранному подразделении */
    if (
      subdivision.id === this.selectedSubdivId ||
      this.accessstoreService.isSubdivisionInSubdivisions(
        this.subdivisions.find(item => item.id === this.selectedSubdivId),
        [this.subdivisions.find(item => item.id === subdivision.id)],
        this.subdivisions
      )
    ) {
      this.getHr = true;
      this.getHrDisabled = true;
      if (isNullOrUndefined(position.parent_id)) {
        this.editHr = true;
        this.editHrDisabled = true;
        this.getPi = true;
        this.getPiDisabled = true;
      }
    }
    /**Устанавливаем переключатели в зависимости от текущих прав выбранного
     * пользователя, проверяем есть ли уже записи в таблице, если есть ставим id и флаги*/
    if (!isNullOrUndefined(this.selectedUserAccess)) {
      for (let i = 0; i < this.selectedUserAccess.length; i++) {
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airuser'
        ) {
          this.editHr = this.selectedUserAccess[i].post;
          this.accessAiruserId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airdocument'
        ) {
          if (this.selectedUserAccess[i].post) {
            this.editDocDisabled = false;
          }
          if (this.selectedUserAccess[i].get) {
            this.editDocDisabled = false;
          }
          this.editDoc = this.selectedUserAccess[i].post;
          this.accessAirdocumentId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airusercertificate'
        ) {
          if (this.selectedUserAccess[i].post) {
            this.editCertificateDisabled = false;
          }
          if (this.selectedUserAccess[i].get) {
            this.getCertificateDisabled = false;
          }
          this.getCertificate = this.selectedUserAccess[i].get;
          this.editCertificate = this.selectedUserAccess[i].post;
          this.accessAirusercertificateId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'acquaintance'
        ) {
          this.accessAcquaintanceId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'position'
        ) {
          this.accessPositionId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airuserphone'
        ) {
          this.accessAiruserphoneId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'education'
        ) {
          this.accessEducationId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'previouswork'
        ) {
          this.accessPreviousworkId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'familymember'
        ) {
          this.accessFamilymemberworkId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airuseremail'
        ) {
          this.accessAiruseremailId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'subdivision'
        ) {
          this.accessSubdivisionId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airuserfile'
        ) {
          this.accessAiruserfileId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airusercertificatefile'
        ) {
          this.accessAirusercertificatefileId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'activity'
        ) {
          if (this.selectedUserAccess[i].post) {
            this.editPiDisabled = false;
          }
          if (this.selectedUserAccess[i].get) {
            this.getPiDisabled = false;
          }
          this.getPi = this.selectedUserAccess[i].get;
          this.editPi = this.selectedUserAccess[i].post;
          this.accessActivityId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airuseractivity'
        ) {
          this.accessAiruseractivityId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'flightpointchain'
        ) {
          this.accessFlightpointchainId = this.selectedUserAccess[i].id;
        }
      }
    }
    this.checkAccessToAiruser();
  }

  filterUsers(str: string) {
    return this.airusersView.filter(
      state =>
        state.name.toLowerCase().indexOf(str.toLowerCase()) === 0 ||
        state.surname.toLowerCase().indexOf(str.toLowerCase()) === 0
    );
  }

  writePositionNames() {
    if (!isNullOrUndefined(this.positions)) {
      for (let i = 0; i < this.airusersView.length; i++) {
        this.airusersView[i].position_name = this.getPositionNameById(
          this.airusersView[i].position_id
        );
        if (isNullOrUndefined(this.airusersView[i].position_name)) {
          this.airusersView[i].position_name = '';
        }
      }
    }
  }

  writeSubdivisionNames() {
    if (!isNullOrUndefined(this.subdivisionsView)) {
      for (let i = 0; i < this.airusersView.length; i++) {
        let sbdv = this.accessstoreService.getSubdivisionByPositionId(
          this.airusersView[i].position_id,
          this.positions,
          this.subdivisions
        );
        this.airusersView[i].subdivision_name = isNullOrUndefined(sbdv)
          ? ''
          : sbdv.name;
        if (this.airusersView[i].subdivision_name == null) {
          this.airusersView[i].subdivision_name = '';
        }
      }
    }
  }

  getPositionNameById(id) {
    if (isNullOrUndefined(this.positions)) {
      return;
    }
    return isNullOrUndefined(
      this.positions.find(position => position.id === id)
    )
      ? ''
      : this.positions.find(position => position.id === id).name;
  }

  async onSaveTt() {
    this.spinnerService.show();
    let dataTt = [
      {
        airuser_id: this.selectedId,
        subdivision_id: this.subdivisions.find(item =>
          isNullOrUndefined(item.parent_id)
        ).id,
        resourcename: 'timetable',
        get: this.editTt ? true : this.getTt,
        post: this.editTt,
        delete: this.editTt,
        put: this.editTt,
        sysId: this.accessTimetableId
      }
    ];
    try {
      await Promise.all(
        dataTt.map(async item => {
          if (!isNullOrUndefined(item.sysId)) {
            await this.accessrightService.putOneById(item.sysId, item);
          } else {
            await this.accessrightService.postOne(item);
          }
        })
      );
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.selectAiruser(this.selectedId);
      this.spinnerService.hide();
    }
  }

  checkAccessToAiruser() {
    if (
      this.getCertificate ||
      this.editCertificate ||
      this.editHr ||
      this.editDoc
    ) {
      this.getHr = true;
      this._getHrDisabled = true;
    } else {
      this._getHrDisabled = false;
    }
    if (this.editCertificate) {
      this.getCertificate = true;
      this.getCertificateDisabled = true;
    } else {
      this._getCertificateDisabled = false;
    }
    if (this.editPi) {
      this.getPi = true;
      this._getPiDisabled = true;
    } else {
      this._getPiDisabled = false;
    }
    if (this.editTt) {
      this.getTt = true;
      this.getTtDisabled = true;
    } else {
      this.getTtDisabled = false;
    }
  }

  async onSaveSbdv() {
    this.spinnerService.show();
    let dataHr = [
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airuser',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessAiruserId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'position',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessPositionId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airuserphone',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessAiruserphoneId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'education',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessEducationId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'previouswork',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessPreviousworkId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'familymember',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessFamilymemberworkId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airuseremail',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessAiruseremailId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'subdivision',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessSubdivisionId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airuserfile',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessAiruserfileId
      }
    ];
    let dataDocs = [
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airdocument',
        get: this.editDoc,
        post: this.editDoc,
        delete: this.editDoc,
        put: this.editDoc,
        sysId: this.accessAirdocumentId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'acquaintance',
        get: this.editDoc,
        post: this.editDoc,
        delete: this.editDoc,
        put: this.editDoc,
        sysId: this.accessAcquaintanceId
      }
    ];
    let dataCertificate = [
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airusercertificate',
        get: this.editCertificate ? true : this.getCertificate,
        post: this.editCertificate,
        delete: this.editCertificate,
        put: this.editCertificate,
        sysId: this.accessAirusercertificateId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airusercertificatefile',
        get: this.editCertificate ? true : this.getCertificate,
        post: this.editCertificate,
        delete: this.editCertificate,
        put: this.editCertificate,
        sysId: this.accessAirusercertificatefileId
      }
    ];
    let dataPi = [
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airuseractivity',
        get: this.editPi ? true : this.getPi,
        post: this.editPi,
        delete: this.editPi,
        put: this.editPi,
        sysId: this.accessAiruseractivityId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'activity',
        get: this.editPi ? true : this.getPi,
        post: this.editPi,
        delete: this.editPi,
        put: this.editPi,
        sysId: this.accessActivityId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'flightpointchain',
        get: this.editPi ? true : this.getPi,
        post: this.editPi,
        delete: this.editPi,
        put: this.editPi,
        sysId: this.accessFlightpointchainId
      }
    ];
    try {
      if (this.hrChanges === true) {
        await Promise.all(
          dataHr.map(async item => {
            if (!isNullOrUndefined(item.sysId)) {
              let res = await this.accessrightService.putOneById(
                item.sysId,
                item
              );
            } else {
              let res = await this.accessrightService.postOne(item);
            }
          })
        );
      }

      if (this.docChanges === true) {
        await Promise.all(
          dataDocs.map(async item => {
            if (!isNullOrUndefined(item.sysId)) {
              let res = await this.accessrightService.putOneById(
                item.sysId,
                item
              );
            } else {
              let res = await this.accessrightService.postOne(item);
            }
          })
        );
      }

      if (this.certChanges === true) {
        await Promise.all(
          dataCertificate.map(async item => {
            if (!isNullOrUndefined(item.sysId)) {
              let res = await this.accessrightService.putOneById(
                item.sysId,
                item
              );
            } else {
              let res = await this.accessrightService.postOne(item);
            }
          })
        );
      }
      if (this.piChanges === true) {
        await Promise.all(
          dataPi.map(async item => {
            if (!isNullOrUndefined(item.sysId)) {
              let res = await this.accessrightService.putOneById(
                item.sysId,
                item
              );
            } else {
              let res = await this.accessrightService.postOne(item);
            }
          })
        );
      }
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.selectAiruser(this.selectedId);
      this.selectSubdiv(this.selectedSubdivId);
      this.spinnerService.hide();
    }
  }
}
