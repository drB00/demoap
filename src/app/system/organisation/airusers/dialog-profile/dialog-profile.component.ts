import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';
import { Airport } from 'src/app/shared/models/airport.model';
import { FormControl } from '@angular/forms';
import { AirportService } from 'src/app/system/shared/services/airport.service';
import { isNullOrUndefined } from 'util';
import { startWith, map } from 'rxjs/operators';

@Component({
  selector: 'app-dialog-profile',
  templateUrl: './dialog-profile.component.html',
  styleUrls: ['./dialog-profile.component.scss']
})
export class DialogProfileComponent implements OnInit {
  filtered_departure_airport: Observable<Airport[]>;
  airport_control = new FormControl({
    value: null,
    disabled: this.data.myProfile
  });
  timerId;
  airports: Airport[];

  constructor(
    private airportService: AirportService,
    public dialogRef: MatDialogRef<DialogProfileComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  async ngOnInit() {
    try {
      let airports = this.airportService.getWithCodefilterLimit(100, '');
      let airport;
      if (!isNullOrUndefined(this.data.home_airport_id)) {
        airport = this.airportService.getOneById(this.data.home_airport_id);
      }
      if (!isNullOrUndefined(this.data.home_airport_id)) {
        this.airport_control.patchValue(await airport);
      }
      this.airports = isNullOrUndefined(await airports) ? [] : await airports;
    } catch (err) {
      console.error(err);
    }
  }

  onSave() {
    let obj = { home_airport_id: null, pilot_type: this.data.pilot_type };
    if (!isNullOrUndefined(this.airport_control.value)) {
      obj.home_airport_id = this.airport_control.value.id;
    }
    this.dialogRef.close(obj);
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  inputLink_airport() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.airport_control.value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_departure_airport = this.airport_control.valueChanges.pipe(
            startWith<string | Airport>(''),
            map(value => (typeof value === 'string' ? value : value.code)),
            map(code => (code ? this._filter(code) : this.airports.slice()))
          );
        } catch {}
      }
    }, 250);
  }

  private _filter(code: string): any[] {
    const filterValue = code.toLowerCase();
    return isNullOrUndefined(this.airports)
      ? null
      : this.airports.filter(
          airport => airport.code.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  displayFn(airport?: any): string | undefined {
    return airport ? `${airport.code} ${airport.name_rus}` : undefined;
  }
}
