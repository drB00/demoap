import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-airusers',
  templateUrl: './airusers.component.html',
  styleUrls: ['./airusers.component.scss']
})
export class AirusersComponent implements OnInit {

  constructor(
    private router: Router,
  ) {
    router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (val.url === '/sys/organisation/users') {
          this.router.navigate(['/sys/organisation/users/list']);
        }
      }
    });
  }

  ngOnInit() {
  }

}
