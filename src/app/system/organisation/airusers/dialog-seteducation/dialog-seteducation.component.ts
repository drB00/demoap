import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-dialog-seteducation',
  templateUrl: './dialog-seteducation.component.html',
  styleUrls: ['./dialog-seteducation.component.scss']
})
export class DialogSeteducationComponent implements OnInit {

  editMod = false;

  constructor(
    public dialogRef: MatDialogRef<DialogSeteducationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.editMod = this.isValid();
  }

  isValid(): boolean {
    let res = true;
    for (let prop in this.data) {
      if (this.data.hasOwnProperty(prop)) {
        if (isNullOrUndefined(this.data[prop]) || this.data[prop].length === 0) {
          res = false;
        }
      }
    }
    return res;
  }

}
