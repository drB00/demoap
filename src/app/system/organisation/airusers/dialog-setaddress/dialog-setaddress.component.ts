import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-setaddress',
  templateUrl: './dialog-setaddress.component.html',
  styleUrls: ['./dialog-setaddress.component.scss']
})
export class DialogSetaddressComponent implements OnInit {

  numberMask: any[] = [/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];

  constructor(
    public dialogRef: MatDialogRef<DialogSetaddressComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

}
