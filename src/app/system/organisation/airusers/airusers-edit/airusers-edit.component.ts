import { Component, OnInit, ViewChild } from '@angular/core';
import { saveAs as importedSaveAs } from 'file-saver';
import { Airuser } from 'src/app/shared/models/airuser.model';
import { Dictionary } from 'src/app/shared/models/dict.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { AiruserService } from 'src/app/system/shared/services/airuser.service';
import { AirusermailService } from 'src/app/system/shared/services/airuseremail.service';
import { AiruserphoneService } from 'src/app/system/shared/services/airuserphone.service';
import { PositionService } from 'src/app/system/shared/services/position.service';
import { SubdivisionService } from 'src/app/system/shared/services/subdivision.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { WebsocketService } from 'src/app/websocket';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DialogYesnoComponent } from 'src/app/system/shared/components/dialog-yesno/dialog-yesno.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DictionaryService } from 'src/app/system/shared/services/dictionary.service';
import { isNullOrUndefined } from 'util';
import { EmailService } from 'src/app/shared/services/email.service';
import { FamilymemberService } from 'src/app/system/shared/services/familymember.service';
import { EducationService } from 'src/app/system/shared/services/education.service';
import { PreviousworkService } from 'src/app/system/shared/services/previouswork.service';
import { AiruserfileService } from 'src/app/system/shared/services/airuserfile.service';
import { EducationfileService } from 'src/app/system/shared/services/educationfile.service';
import { Subdivision } from 'src/app/shared/models/subdivision.model';
import { Position } from 'src/app/shared/models/position.model';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { DialogSetaddressComponent } from '../dialog-setaddress/dialog-setaddress.component';
import { DialogSeteducationComponent } from '../dialog-seteducation/dialog-seteducation.component';
import { DialogSetpreviousworkComponent } from '../dialog-setpreviouswork/dialog-setpreviouswork.component';
import { DialogSetfamilymemberComponent } from '../dialog-setfamilymember/dialog-setfamilymember.component';
import {
  NgxGalleryOptions,
  NgxGalleryImage,
  NgxGalleryComponent
} from 'ngx-gallery';
import { OrganisationstoreService } from 'src/app/system/shared/services/organisationstore.service';
import { Observable } from 'rxjs';
import { Airport } from 'src/app/shared/models/airport.model';
import { startWith, map } from 'rxjs/operators';
import { AirportService } from 'src/app/system/shared/services/airport.service';
import { GlobalsettingsService } from 'src/app/system/shared/services/globalsettings.service';

@Component({
  selector: 'app-airusers-edit',
  templateUrl: './airusers-edit.component.html',
  styleUrls: ['./airusers-edit.component.scss']
})
export class AirusersEditComponent implements OnInit {
  phoneMask: any[] = [
    '+',
    /[1-9]/,
    ' ',
    '(',
    /[1-9]/,
    /\d/,
    /\d/,
    ')',
    ' ',
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];
  seriaMask: any[] = [/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];
  numberMask: any[] = [/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/];
  anyNumberMask: any[] = [/[0-9]/];

  maritalstatuses: Dictionary[];
  militarydutyes: Dictionary[];
  relationdegrees: Dictionary[];
  pilotTypes: Dictionary[];

  delUsers: Airuser[] = [];
  airusers: Airuser[] = [];
  subdivisions: Subdivision[];
  subdivisionsView: Subdivision[];
  positions: Position[];
  airuserphones: any = [];

  airuserForm: FormGroup;
  airuserFormEmails: FormGroup[] = [];
  airuserFormPhones: FormGroup[] = [];
  airuserFormEducation: FormGroup[] = [];
  airuserFormFamily: FormGroup[] = [];
  airuserFormPreviouswork: FormGroup[] = [];
  airuserFormEmailsTodel: number[] = [];
  airuserFormPhonesTodel: number[] = [];
  airuserFormEducationTodel: number[] = [];
  airuserFormFamilyTodel: number[] = [];
  airuserFormPreviousworkTodel: number[] = [];

  newEducationFile: FormGroup[] = [];
  newUserFile: FormGroup[] = [];
  newEducationFileTodel: number[] = [];
  newUserFileTodel: number[] = [];

  id: number;
  intType: string;

  checkLogin = -1; // для проверки доступности логина

  inProgress = false;
  inProgressVal = 0;

  selectedUser: Airuser;
  selectedSubdivision: Subdivision;
  selectedSubdivisionAirusers: Airuser[];
  selectedEducationFiles = [];
  selectedUserFiles = [];
  selectedUserPhones = [];
  selectedUserEmails = [];
  selectedUserEducations = [];
  selectedUserFamilymembers = [];
  selectedUserPreviousworks = [];

  /**Галерея */
  galleryOptions: NgxGalleryOptions[] = [];
  galleryImages: NgxGalleryImage[] = [];
  @ViewChild('gallary', { static: false }) gallary: NgxGalleryComponent;
  showGallary = true;

  selectedFlightsubdivision = false;

  filtered_departure_airport: Observable<Airport[]>;
  airport_control = new FormControl();
  timerId;
  airports: Airport[];

  constructor(
    public authService: AuthService,
    private globalsettingsService: GlobalsettingsService,
    private organisationstoreService: OrganisationstoreService,
    private airuserService: AiruserService,
    private airportService: AirportService,
    private airuserphoneService: AiruserphoneService,
    private airusermailService: AirusermailService,
    private familymemberService: FamilymemberService,
    private educationService: EducationService,
    private previousworkService: PreviousworkService,
    private positionService: PositionService,
    private subdivisionService: SubdivisionService,
    private dictionaryService: DictionaryService,
    private airuserfileService: AiruserfileService,
    private educationfileService: EducationfileService,
    private emailService: EmailService,
    private spinnerService: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private wsService: WebsocketService
  ) {
    this.activatedRoute.params.subscribe(param => {
      this.id = param.id;
      if (param.id === 'new') {
        this.intType = 'new';
      } else {
        this.id = +param.id;
        this.intType = 'edit';
      }
    });
  }

  async ngOnInit() {
    this.initForms();
    this.spinnerService.show();
    try {
      let airports = this.airportService.getWithCodefilterLimit(100, '');
      let delUsers = this.airuserService.getWithDeleted();
      let airusers = this.airuserService.getAll();
      let maritalstatuses = this.dictionaryService.getMaritalstatuses();
      let militarydutyes = this.dictionaryService.getMilitarydutyes();
      let relationdegrees = this.dictionaryService.getRelationdegrees();
      let subdivisions = this.subdivisionService.getAll();
      let positions = this.positionService.getAll();
      let airuserphones = this.airuserphoneService.getAll();
      this.subdivisions = isNullOrUndefined(await subdivisions)
        ? []
        : await subdivisions;
      this.positions = isNullOrUndefined(await positions)
        ? []
        : await positions;
      this.maritalstatuses = isNullOrUndefined(await maritalstatuses)
        ? []
        : await maritalstatuses;
      this.militarydutyes = isNullOrUndefined(await militarydutyes)
        ? []
        : await militarydutyes;
      this.relationdegrees = isNullOrUndefined(await relationdegrees)
        ? []
        : await relationdegrees;
      this.delUsers = isNullOrUndefined(await delUsers) ? [] : await delUsers;
      this.airusers = isNullOrUndefined(await airusers) ? [] : await airusers;
      this.airuserphones = isNullOrUndefined(await airuserphones)
        ? []
        : await airuserphones;
      let tree = this.organisationstoreService.getSubdivisionTree(
        this.subdivisions
      );
      this.subdivisions = this.organisationstoreService.getFoldersTree([tree]);
      this.airports = isNullOrUndefined(await airports) ? [] : await airports;
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
    this.pilotTypes = this.dictionaryService.pilotTypes;
    this.subdivisionsView = this.organisationstoreService.getWithAccess(
      this.authService.loggedInUser.isadmin,
      this.authService.loggedInUserRole,
      this.authService.loggedInUserSubdivision,
      this.subdivisions,
      this.authService.loggedInUserAccesses
    );
    if (this.intType === 'edit') {
      this.checkLogin = 1;
      await this.getUsersData();
      this.onSelectAiruser(this.selectedUser);
    }
    this.galleryOptions = [
      { previewZoom: true, previewRotate: true },
      { thumbnails: false },
      { image: false, height: '1px' }
    ];
  }

  async getUsersData() {
    this.airusers = this.airuserService.setPositionnames(
      this.airusers,
      this.positions
    );
    this.selectedUser = this.delUsers.find(item => item.id === this.id);
    let selectedPosition = this.positions.find(
      item => item.id === this.selectedUser.position_id
    );
    if (isNullOrUndefined(selectedPosition)) {
      return;
    }
    this.selectedSubdivision = this.subdivisions.find(
      item => item.id === selectedPosition.subdivision_id
    );
    this.selectedSubdivisionAirusers = [];
    for (let i = 0; i < this.airusers.length; i++) {
      if (this.airusers[i].subdivision_id === this.selectedSubdivision.id) {
        this.selectedSubdivisionAirusers.push(this.airusers[i]);
      }
    }
    this.selectedSubdivisionAirusers = this.airuserService.setPhones(
      this.selectedSubdivisionAirusers,
      this.airuserphones
    );
    this.selectedSubdivisionAirusers.sort((a, b) => {
      return a.id > b.id ? 1 : -1;
    });
  }

  async onSelectAiruser(airuser) {
    this.galleryImages = [];
    this.spinnerService.show();
    try {
      let selectedUser = this.airuserService.getOneById(airuser.id);
      let selectedUserPhones = this.airuserphoneService.getByAiruser(
        airuser.id
      );
      let selectedUserEmails = this.airusermailService.getByAiruser(airuser.id);
      let selectedUserEducations = this.educationService.getByAiruser(
        airuser.id
      );
      let selectedUserFamilymembers = this.familymemberService.getByAiruser(
        airuser.id
      );
      let selectedUserPreviousworks = this.previousworkService.getByAiruser(
        airuser.id
      );
      let selectedEducationFiles = this.educationfileService.getAll();
      let selectedUserFiles = this.airuserfileService.getByAiruser(airuser.id);
      this.selectedUser = isNullOrUndefined(await selectedUser)
        ? null
        : await selectedUser;
      let airport;
      if (!isNullOrUndefined(this.selectedUser.home_airport_id)) {
        airport = this.airportService.getOneById(
          this.selectedUser.home_airport_id
        );
      }
      this.selectedUserPhones = isNullOrUndefined(await selectedUserPhones)
        ? []
        : await selectedUserPhones;
      this.selectedUserEmails = isNullOrUndefined(await selectedUserEmails)
        ? []
        : await selectedUserEmails;
      this.selectedUserEducations = isNullOrUndefined(
        await selectedUserEducations
      )
        ? []
        : await selectedUserEducations;
      this.selectedUserFamilymembers = isNullOrUndefined(
        await selectedUserFamilymembers
      )
        ? []
        : await selectedUserFamilymembers;
      this.selectedUserPreviousworks = isNullOrUndefined(
        await selectedUserPreviousworks
      )
        ? []
        : await selectedUserPreviousworks;
      this.selectedUserFiles = isNullOrUndefined(await selectedUserFiles)
        ? []
        : await selectedUserFiles;
      this.selectedEducationFiles = isNullOrUndefined(
        await selectedEducationFiles
      )
        ? []
        : await selectedEducationFiles;
      let sbdv = this.subdivisionsView.find(
        item => item.id === this.selectedUser.subdivision_id
      );
      let control = this.airuserForm.get('pilot_type');
      if (!isNullOrUndefined(this.selectedUser.home_airport_id)) {
        this.airport_control.patchValue(await airport);
      }
      // (!isNullOrUndefined(sbdv) && sbdv.type > 0) ? control.enable() : control.disable();
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
    this.airuserForm.patchValue({
      isadmin: this.selectedUser.isadmin,
      login: this.selectedUser.login,
      name: this.selectedUser.name,
      surname: this.selectedUser.surname,
      patronymic: this.selectedUser.patronymic,
      salary: this.selectedUser.salary,
      order_number: this.selectedUser.order_number,
      subdivision_id: this.selectedSubdivision.id,
      position_id: this.selectedUser.position_id,
      dt_of_birth: this.selectedUser.dt_of_birth,
      phone: isNullOrUndefined(this.selectedUserPhones[0])
        ? null
        : this.selectedUserPhones[0].phone,
      email: isNullOrUndefined(this.selectedUserEmails[0])
        ? null
        : this.selectedUserEmails[0].email,
      sex: this.selectedUser.sex,
      pass_seria: this.selectedUser.pass_seria,
      pass_number: this.selectedUser.pass_number,
      pass_date: this.selectedUser.pass_date,
      pass_code_dep: this.selectedUser.pass_code_dep,
      pass_kem_vydan: this.selectedUser.pass_kem_vydan,
      pass_birth_place: this.selectedUser.pass_birth_place,
      location_index: this.selectedUser.location_index,
      location_city: this.selectedUser.location_city,
      location_street: this.selectedUser.location_street,
      location_house: this.selectedUser.location_house,
      location_flat: this.selectedUser.location_flat,
      registration_index: this.selectedUser.registration_index,
      registration_city: this.selectedUser.registration_city,
      registration_street: this.selectedUser.registration_street,
      registration_house: this.selectedUser.registration_house,
      registration_flat: this.selectedUser.registration_flat,
      militaryduty_id: this.selectedUser.militaryduty_id,
      maritalstatus_id: this.selectedUser.maritalstatus_id,
      pilot_type: this.selectedUser.pilot_type,
      home_airport_id: this.selectedUser.home_airport_id
    });
    this.onSelectsubdivision(this.selectedSubdivision.id);
    this.airuserFormFamily.length = 0;
    for (let i = 0; i < this.selectedUserFamilymembers.length; i++) {
      this.airuserFormFamily.push(
        new FormGroup({
          id: new FormControl(this.selectedUserFamilymembers[i].id),
          name: new FormControl(this.selectedUserFamilymembers[i].name, [
            Validators.required
          ]),
          surname: new FormControl(this.selectedUserFamilymembers[i].surname, [
            Validators.required
          ]),
          work_place: new FormControl(
            this.selectedUserFamilymembers[i].work_place
          ),
          work_position: new FormControl(
            this.selectedUserFamilymembers[i].work_position
          ),
          dt_of_birth: new FormControl(
            this.selectedUserFamilymembers[i].dt_of_birth
          ),
          relationdegree_id: new FormControl(
            this.selectedUserFamilymembers[i].relationdegree_id,
            [Validators.required]
          ),
          airuser_id: new FormControl(this.selectedUser.id)
        })
      );
    }
    if (this.airuserFormFamily.length === 0) {
      this.onAddFamily();
    }
    this.airuserFormEducation.length = 0;
    for (let i = 0; i < this.selectedUserEducations.length; i++) {
      this.airuserFormEducation.push(
        new FormGroup({
          id: new FormControl(this.selectedUserEducations[i].id),
          airuser_id: new FormControl(this.selectedUser.id),
          institution_place: new FormControl(
            this.selectedUserEducations[i].institution_place,
            [Validators.required]
          ),
          specialty: new FormControl(this.selectedUserEducations[i].specialty, [
            Validators.required
          ]),
          diploma_number: new FormControl(
            this.selectedUserEducations[i].diploma_number,
            [Validators.required]
          ),
          finish_year: new FormControl(
            this.selectedUserEducations[i].finish_year,
            [Validators.required]
          ),
          file: new FormControl(
            this.selectedEducationFiles
              .reverse()
              .find(
                item => item.education_id === this.selectedUserEducations[i].id
              )
          )
        })
      );
    }
    if (this.airuserFormEducation.length === 0) {
      this.onAddEducation();
    }
    this.airuserFormPreviouswork.length = 0;
    for (let i = 0; i < this.selectedUserPreviousworks.length; i++) {
      this.airuserFormPreviouswork.push(
        new FormGroup({
          id: new FormControl(this.selectedUserPreviousworks[i].id),
          airuser_id: new FormControl(this.selectedUser.id),
          work_place: new FormControl(
            this.selectedUserPreviousworks[i].work_place,
            [Validators.required]
          ),
          work_position: new FormControl(
            this.selectedUserPreviousworks[i].work_position,
            [Validators.required]
          ),
          dt_start: new FormControl(this.selectedUserPreviousworks[i].dt_start),
          dt_end: new FormControl(this.selectedUserPreviousworks[i].dt_end)
        })
      );
    }
    if (this.airuserFormPreviouswork.length === 0) {
      this.onAddPreviouswork();
    }
    this.airuserFormPhones.length = 0;
    for (let i = 1; i < this.selectedUserPhones.length; i++) {
      this.airuserFormPhones.push(
        new FormGroup({
          id: new FormControl(this.selectedUserPhones[i].id),
          phone: new FormControl(this.selectedUserPhones[i].phone),
          airuser_id: new FormControl(this.selectedUser.id)
        })
      );
    }
    this.airuserFormEmails.length = 0;
    for (let i = 1; i < this.selectedUserEmails.length; i++) {
      this.airuserFormEmails.push(
        new FormGroup({
          id: new FormControl(this.selectedUserEmails[i].id),
          email: new FormControl(this.selectedUserEmails[i].email),
          airuser_id: new FormControl(this.selectedUser.id)
        })
      );
    }
  }

  initForms() {
    this.airuserFormPhones.length = 0;
    this.airuserFormEmails.length = 0;
    this.newEducationFile.length = 0;
    this.newEducationFile.push(
      new FormGroup({
        education_id: new FormControl(null),
        edctnfile: new FormControl(null, [Validators.required])
      })
    );
    this.newUserFile.length = 0;
    this.newUserFile.push(
      new FormGroup({
        airuser_id: new FormControl(null),
        ausrfile: new FormControl(null, [Validators.required])
      })
    );
    this.airuserFormEducation.length = 0;
    this.airuserFormEducation.push(
      new FormGroup({
        institution_place: new FormControl(null, [Validators.required]),
        specialty: new FormControl(null, [Validators.required]),
        diploma_number: new FormControl(null, [Validators.required]),
        finish_year: new FormControl(null, [Validators.required]),
        airuser_id: new FormControl(null)
      })
    );
    this.airuserFormFamily.length = 0;
    this.airuserFormFamily.push(
      new FormGroup({
        name: new FormControl(null, [Validators.required]),
        surname: new FormControl(null, [Validators.required]),
        work_place: new FormControl(null),
        work_position: new FormControl(null),
        dt_of_birth: new FormControl(null),
        relationdegree_id: new FormControl(null, [Validators.required]),
        airuser_id: new FormControl(null)
      })
    );
    this.airuserFormPreviouswork.length = 0;
    this.airuserFormPreviouswork.push(
      new FormGroup({
        work_place: new FormControl(null, [Validators.required]),
        work_position: new FormControl(null, [Validators.required]),
        dt_start: new FormControl(null),
        dt_end: new FormControl(null),
        airuser_id: new FormControl(null)
      })
    );
    let pass = Math.random()
      .toString(36)
      .substr(3, 8);
    this.airuserForm = new FormGroup({
      isadmin: new FormControl(false),
      login: new FormControl(null, [Validators.required]),
      password: new FormControl(pass, [Validators.required]),
      name: new FormControl(null, [Validators.required]),
      surname: new FormControl(null, [Validators.required]),
      patronymic: new FormControl(null),
      salary: new FormControl(null),
      order_number: new FormControl(null),
      subdivision_id: new FormControl(0),
      position_id: new FormControl(null, [Validators.required]),
      dt_of_birth: new FormControl(null),
      phone: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      sex: new FormControl(null),
      pass_seria: new FormControl(null),
      pass_number: new FormControl(null),
      pass_date: new FormControl(null),
      pass_code_dep: new FormControl(null),
      pass_kem_vydan: new FormControl(null),
      pass_birth_place: new FormControl(null),
      location_index: new FormControl(null),
      location_city: new FormControl(null),
      location_street: new FormControl(null),
      location_house: new FormControl(null),
      location_flat: new FormControl(null),
      registration_index: new FormControl(null),
      registration_city: new FormControl(null),
      registration_street: new FormControl(null),
      registration_house: new FormControl(null),
      registration_flat: new FormControl(null),
      militaryduty_id: new FormControl(null),
      maritalstatus_id: new FormControl(null),
      pilot_type: new FormControl({ value: null, disabled: false }),
      home_airport_id: new FormControl({ value: null, disabled: false })
    });
  }

  onSelectsubdivision(id) {
    let sbdv = this.subdivisionsView.find(item => item.id === +id);
    let control = this.airuserForm.get('pilot_type');
    // (!isNullOrUndefined(sbdv) && sbdv.type > 0) ? control.enable() : control.disable();
  }

  async onSave() {
    if (!/\b\S+@\S+\.\S+\b/g.test(this.airuserForm.value['email'])) {
      this.globalsettingsService.openSnackBar(
        `Адрес электронной почты указаны неверно`,
        'Ok'
      );
      return;
    }
    if (
      !(
        new Date(this.airuserForm.value.dt_of_birth) instanceof Date &&
        !isNaN(new Date(this.airuserForm.value.dt_of_birth).valueOf())
      )
    ) {
      this.globalsettingsService.openSnackBar(
        `Введена не существующая дата рождения`,
        'Ok'
      );
      return;
    }
    if (
      !(
        new Date(this.airuserForm.value.pass_date) instanceof Date &&
        !isNaN(new Date(this.airuserForm.value.pass_date).valueOf())
      )
    ) {
      this.globalsettingsService.openSnackBar(
        `Введена не существующая дата выдачи паспорта`,
        'Ok'
      );
      return;
    }
    for (let i = 0; i < this.airuserFormEducation.length; i++) {
      if (this.airuserFormEducation[i].value['institution_place'] === '') {
        this.globalsettingsService.openSnackBar(
          `Не указано наименование учебного заведения`,
          'Ok'
        );
        return;
      }
      if (this.airuserFormEducation[i].value['specialty'] === '') {
        this.globalsettingsService.openSnackBar(
          `Не указана специальность`,
          'Ok'
        );
        return;
      }
      if (this.airuserFormEducation[i].value['diploma_number'] === '') {
        this.globalsettingsService.openSnackBar(
          `Не указан номер диплома`,
          'Ok'
        );
        return;
      }
      if (this.airuserFormEducation[i].value['finish_year'] === '') {
        this.globalsettingsService.openSnackBar(
          `Не указан год окончания учебного заведения`,
          'Ok'
        );
        return;
      }
    }
    for (let i = 0; i < this.airuserFormPreviouswork.length; i++) {
      if (
        !(
          new Date(this.airuserFormPreviouswork[i].value.dt_start) instanceof
            Date &&
          !isNaN(
            new Date(this.airuserFormPreviouswork[i].value.dt_start).valueOf()
          )
        )
      ) {
        this.globalsettingsService.openSnackBar(
          `Введена не существующая дата начала работы`,
          'Ok'
        );
        return;
      }
      if (
        !(
          new Date(this.airuserFormPreviouswork[i].value.dt_end) instanceof
            Date &&
          !isNaN(
            new Date(this.airuserFormPreviouswork[i].value.dt_end).valueOf()
          )
        )
      ) {
        this.globalsettingsService.openSnackBar(
          `Введена не существующая дата окончания работы`,
          'Ok'
        );
        return;
      }
    }
    for (let i = 0; i < this.airuserFormFamily.length; i++) {
      if (
        !(
          new Date(this.airuserFormFamily[i].value.dt_of_birth) instanceof
            Date &&
          !isNaN(
            new Date(this.airuserFormFamily[i].value.dt_of_birth).valueOf()
          )
        )
      ) {
        this.globalsettingsService.openSnackBar(
          `Введена не существующая дата рождения члена семьи`,
          'Ok'
        );
        return;
      }
    }

    if (!isNullOrUndefined(this.airport_control.value)) {
      this.airuserForm.patchValue({
        home_airport_id: this.airport_control.value.id
      });
    } else {
      this.airuserForm.patchValue({
        home_airport_id: null
      });
    }

    if (this.intType === 'edit') {
      this.spinnerService.show();
      this.inProgress = true;
      this.inProgressVal +=
        100 / (this.newEducationFile.length + this.newUserFile.length);
      try {
        if (this.airuserForm.getRawValue().login === this.selectedUser.login) {
          delete this.airuserForm.value.login;
        }
        let resPutAiruser = this.airuserService.putOneById(
          this.id,
          this.airuserForm.value
        );
        let resPutAiruserphone;
        if (!isNullOrUndefined(this.selectedUserPhones[0])) {
          resPutAiruserphone = this.airuserphoneService.putOneById(
            this.selectedUserPhones[0].id,
            { airuser_id: this.id, phone: this.airuserForm.value.phone }
          );
        } else {
          resPutAiruserphone = this.airuserphoneService.postOne({
            airuser_id: this.id,
            phone: this.airuserForm.value.phone
          });
        }
        let resPutAiruseremail;
        if (!isNullOrUndefined(this.selectedUserEmails[0])) {
          resPutAiruseremail = this.airusermailService.putOneById(
            this.selectedUserEmails[0].id,
            { airuser_id: this.id, email: this.airuserForm.value.email }
          );
        } else {
          resPutAiruseremail = this.airusermailService.postOne({
            airuser_id: this.id,
            email: this.airuserForm.value.email
          });
        }
        /**Удаляем телефоны с пометкой на удаление */
        let deleteAiruserphone = Promise.all(
          this.airuserFormPhonesTodel.map(async todel => {
            let phoneTodel = this.selectedUserPhones.find(
              item => item.id === todel
            );
            if (!isNullOrUndefined(phoneTodel)) {
              await this.airuserphoneService.deleteOneById(phoneTodel.id);
            }
          })
        );
        /**Добавляем новые/апдейтим существующие */
        let updateAiruserphone = Promise.all(
          this.airuserFormPhones.map(async phone => {
            if (isNullOrUndefined(phone.value.id)) {
              phone.patchValue({
                airuser_id: this.id
              });
              await this.airuserphoneService.postOne(phone.value);
            } else {
              let id = phone.value.id;
              await this.airuserphoneService.putOneById(id, phone.value);
            }
          })
        );
        /**Удаляем почты с пометкой на удаление */
        let deleteAiruseremail = Promise.all(
          this.airuserFormEmailsTodel.map(async todel => {
            let emailTodel = this.selectedUserEmails.find(
              item => item.id === todel
            );
            if (!isNullOrUndefined(emailTodel)) {
              await this.airusermailService.deleteOneById(emailTodel.id);
            }
          })
        );
        /**Добавляем новые/апдейтим существующие */
        let updateAiruseremail = Promise.all(
          this.airuserFormEmails.map(async email => {
            if (isNullOrUndefined(email.value.id)) {
              email.patchValue({
                airuser_id: this.id
              });
              await this.airuserphoneService.postOne(email.value);
            } else {
              let id = email.value.id;
              await this.airuserphoneService.putOneById(id, email.value);
            }
          })
        );
        /**Удаляем чс с пометкой на удаление */
        let deleteAiruserfamilemembers = Promise.all(
          this.airuserFormFamilyTodel.map(async todel => {
            let familememberTodel = this.selectedUserFamilymembers.find(
              item => item.id === todel
            );
            if (!isNullOrUndefined(familememberTodel)) {
              await this.familymemberService.deleteOneById(
                familememberTodel.id
              );
            }
          })
        );
        let updateAiruserfamilemember = Promise.all(
          this.airuserFormFamily.map(async familemember => {
            if (familemember.valid) {
              if (isNullOrUndefined(familemember.value.id)) {
                familemember.patchValue({
                  airuser_id: this.id
                });
                await this.familymemberService.postOne(familemember.value);
              } else {
                let id = familemember.value.id;
                await this.familymemberService.putOneById(
                  id,
                  familemember.value
                );
              }
            }
          })
        );
        /**Удаляем образование с пометкой на удаление */
        let deleteAirusereducations = Promise.all(
          this.airuserFormEducationTodel.map(async todel => {
            let educationTodel = this.selectedUserEducations.find(
              item => item.id === todel
            );
            if (!isNullOrUndefined(educationTodel)) {
              await this.educationService.deleteOneById(educationTodel.id);
            }
          })
        );
        let updateAirusereducations = Promise.all(
          this.airuserFormEducation.map(async edication => {
            if (edication.valid) {
              if (isNullOrUndefined(edication.value.id)) {
                edication.patchValue({
                  airuser_id: this.id
                });
                await this.educationService.postOne(edication.value);
              } else {
                let id = edication.value.id;
                await this.educationService.putOneById(id, edication.value);
              }
            }
          })
        );
        /**Удаляем места работы с пометкой на удаление */
        let deletePreviousworks = Promise.all(
          this.airuserFormPreviousworkTodel.map(async todel => {
            let previousworkTodel = this.selectedUserPreviousworks.find(
              item => item.id === todel
            );
            if (!isNullOrUndefined(previousworkTodel)) {
              await this.previousworkService.deleteOneById(
                previousworkTodel.id
              );
            }
          })
        );
        let updatePreviousworks = Promise.all(
          this.airuserFormPreviouswork.map(async previouswork => {
            if (previouswork.valid) {
              if (isNullOrUndefined(previouswork.value.id)) {
                previouswork.patchValue({
                  airuser_id: this.id
                });
                await this.previousworkService.postOne(previouswork.value);
              } else {
                let id = previouswork.value.id;
                await this.previousworkService.putOneById(
                  id,
                  previouswork.value
                );
              }
            }
          })
        );
        /**Удаляем файлы с пометкой на удаление */
        let deleteAiruserfilesById = Promise.all(
          this.selectedUserFiles.map(async curUserFile => {
            if (curUserFile.checkToDel) {
              await this.airuserfileService.deleteOneById(curUserFile.id);
            }
          })
        );
        let addAiruserfilesById = Promise.all(
          this.newUserFile.map(async newUserFile => {
            if (!isNullOrUndefined(newUserFile.value['ausrfile'])) {
              newUserFile.patchValue({
                airuser_id: this.selectedUser.id
              });
              let formFile: FormData = new FormData();
              formFile.append('airuser_id', newUserFile.value['airuser_id']);
              formFile.append('ausrfile', newUserFile.value['ausrfile']);
              await this.airuserfileService.postOne(formFile);
              this.inProgressVal +=
                100 / (this.newEducationFile.length + this.newUserFile.length);
            }
          })
        );
        await resPutAiruser;
        await addAiruserfilesById;
        await deleteAiruserfilesById;
        await updatePreviousworks;
        await deletePreviousworks;
        await updateAirusereducations;
        await deleteAirusereducations;
        await updateAiruserfamilemember;
        await deleteAiruserfamilemembers;
        await updateAiruseremail;
        await deleteAiruseremail;
        await updateAiruserphone;
        await deleteAiruserphone;
        await resPutAiruseremail;
        await resPutAiruserphone;
      } catch (err) {
        this.globalsettingsService.exceptionHandling(err);
      } finally {
        this.inProgress = false;
        await this.ngOnInit();
        this.spinnerService.hide();
        this.onCancel();
      }
    } else {
      this.spinnerService.show();
      this.inProgress = true;
      this.inProgressVal +=
        100 / (this.newEducationFile.length + this.newUserFile.length);
      try {
        let resPostAiruser = await this.airuserService.postOne(
          this.airuserForm.value
        );
        resPostAiruser.phones = [];
        resPostAiruser.emails = [];
        let resMainPostAiuserPhone = await this.airuserphoneService.postOne({
          airuser_id: resPostAiruser.id,
          phone: this.airuserForm.value['phone']
        });
        let resMainPostAiuserEmail = await this.airusermailService.postOne({
          airuser_id: resPostAiruser.id,
          email: this.airuserForm.value['email']
        });
        let dataMainPostAiuserEmail = {
          email: this.airuserForm.value['email'],
          subject: 'Вы зарегистрированы в системе АэроПрофи',
          textbody: `<p>Добро пожаловать в систему АэроПрофи!</p>
					<p>Зайти в Ваш личный кабинет Вы сможете по ссылке <a href="${window.location.origin}">${window.location.origin}</a></p>
					<br><p>Ваш логин: ${this.airuserForm.value.login}, <br> Пароль: ${this.airuserForm.value.password}</p>`
        };
        let resdataMainPostAiuserEmail = this.emailService.sendemail(
          dataMainPostAiuserEmail
        );
        let postAiruserephone = Promise.all(
          this.airuserFormPhones.map(async airuserFormPhone => {
            if (airuserFormPhone.value['phone'] !== '') {
              let resPostAiruserephone = await this.airuserphoneService.postOne(
                {
                  airuser_id: resPostAiruser.id,
                  phone: airuserFormPhone.value['phone']
                }
              );
              resPostAiruser.phones.push(resPostAiruserephone.phone);
            }
          })
        );
        let postAiruseremail = Promise.all(
          this.airuserFormEmails.map(async airuserFormEmail => {
            if (airuserFormEmail.value['email'] !== '') {
              let resPostAiruseremail = await this.airusermailService.postOne({
                airuser_id: resPostAiruser.id,
                email: airuserFormEmail.value['email']
              });
              let extraData = {
                email: airuserFormEmail.value['email'],
                subject: 'Вы зарегистрированы в системе АэроПрофи',
                textbody: `<p>Добро пожаловать в систему АэроПрофи!</p>
							<p>Зайти в Ваш личный кабинет Вы сможете по ссылке <a href="${window.location.origin}">${window.location.origin}</a></p>
							<br><p>Ваш логин: ${this.airuserForm.value.login}, <br> Пароль: ${this.airuserForm.value.password}</p>`
              };
              let resdataPostAiuserEmail = await this.emailService.sendemail(
                extraData
              );
              resPostAiruser.emails.push(resPostAiruseremail.email);
            }
          })
        );
        let postAiruserfile = Promise.all(
          this.newUserFile.map(async newUserFile => {
            if (!isNullOrUndefined(newUserFile.value['ausrfile'])) {
              let formFile: FormData = new FormData();
              formFile.append('airuser_id', resPostAiruser.id);
              formFile.append('ausrfile', newUserFile.value['ausrfile']);
              let res = await this.airuserfileService.postOne(formFile);
              this.inProgressVal +=
                100 / (this.newEducationFile.length + this.newUserFile.length);
            }
          })
        );
        let postFamilymember = Promise.all(
          this.airuserFormFamily.map(async airuserFormFamily => {
            if (airuserFormFamily.valid) {
              airuserFormFamily.patchValue({
                airuser_id: resPostAiruser.id
              });
              let res = this.familymemberService.postOne(
                airuserFormFamily.value
              );
            }
          })
        );
        let postPreviouswork = Promise.all(
          this.airuserFormPreviouswork.map(async airuserFormPreviouswork => {
            if (airuserFormPreviouswork.valid) {
              airuserFormPreviouswork.patchValue({
                airuser_id: resPostAiruser.id
              });
              let res = this.previousworkService.postOne(
                airuserFormPreviouswork.value
              );
            }
          })
        );
        let postEducation = Promise.all(
          this.airuserFormEducation.map(async (airuserFormEducation, ind) => {
            if (airuserFormEducation.valid) {
              airuserFormEducation.patchValue({
                airuser_id: resPostAiruser.id
              });
              let resPostEducation = await this.educationService.postOne(
                airuserFormEducation.value
              );
              if (this.newEducationFile[ind].valid) {
                this.newEducationFile[ind].patchValue({
                  education_id: resPostEducation.id
                });
                let formFile: FormData = new FormData();
                formFile.append(
                  'education_id',
                  this.newEducationFile[ind].value['education_id']
                );
                formFile.append(
                  'edctnfile',
                  this.newEducationFile[ind].value['edctnfile']
                );
                let res = await this.educationfileService.postOne(formFile);
                this.inProgressVal +=
                  100 /
                  (this.newEducationFile.length + this.newUserFile.length);
              }
            }
          })
        );

        resPostAiruser.phones.push(await resMainPostAiuserPhone.phone);
        resPostAiruser.emails.push(await resMainPostAiuserEmail.email);
        await postAiruserephone;
        this.airuserFormPhones.length = 0;
        await postAiruseremail;
        this.airuserFormEmails.length = 0;
        await postAiruserfile;
        await postFamilymember;
        this.airuserFormFamily.length = 0;
        this.airuserFormFamily.push(
          new FormGroup({
            name: new FormControl(null, [Validators.required]),
            surname: new FormControl(null, [Validators.required]),
            work_place: new FormControl(null),
            work_position: new FormControl(null),
            dt_of_birth: new FormControl(null),
            relationdegree_id: new FormControl(null, [Validators.required]),
            airuser_id: new FormControl(null)
          })
        );
        await postPreviouswork;
        this.airuserFormPreviouswork.length = 0;
        this.airuserFormPreviouswork.push(
          new FormGroup({
            work_place: new FormControl(null, [Validators.required]),
            work_position: new FormControl(null, [Validators.required]),
            dt_start: new FormControl(null),
            dt_end: new FormControl(null),
            airuser_id: new FormControl(null)
          })
        );
        await postEducation;
        this.airuserFormEducation.length = 0;
        this.airuserFormEducation.push(
          new FormGroup({
            institution_place: new FormControl(null, [Validators.required]),
            specialty: new FormControl(null, [Validators.required]),
            diploma_number: new FormControl(null, [Validators.required]),
            finish_year: new FormControl(null, [Validators.required]),
            airuser_id: new FormControl(null)
          })
        );
        this.airuserForm.reset();
        this.checkLogin = -1;
        this.globalsettingsService.openSnackBar(
          `Пользователь добавлен, данные для доступа отправлены по почте`,
          'Ok'
        );
      } catch (err) {
        this.globalsettingsService.exceptionHandling(err);
      } finally {
        this.inProgress = false;
        this.spinnerService.hide();
      }
    }
  }

  onEducationFileChange(event, index) {
    this.newEducationFile[index].patchValue({
      edctnfile: event.srcElement.files[0]
    });
  }

  onUserFileChange(event) {
    this.newUserFile.length = 0;
    for (let i = 0; i < event.srcElement.files.length; i++) {
      this.newUserFile.push(
        new FormGroup({
          airuser_id: new FormControl(null),
          ausrfile: new FormControl(event.srcElement.files[i])
        })
      );
    }
  }

  onSameAddr() {
    this.airuserForm.patchValue({
      location_index: this.airuserForm.value.registration_index,
      location_city: this.airuserForm.value.registration_city,
      location_street: this.airuserForm.value.registration_street,
      location_house: this.airuserForm.value.registration_house,
      location_flat: this.airuserForm.value.registration_flat
    });
  }

  onAddEducation() {
    this.airuserFormEducation.push(
      new FormGroup({
        institution_place: new FormControl(null, [Validators.required]),
        specialty: new FormControl(null, [Validators.required]),
        diploma_number: new FormControl(null, [Validators.required]),
        finish_year: new FormControl(null, [Validators.required]),
        airuser_id: new FormControl(null)
      })
    );
    this.newEducationFile.push(
      new FormGroup({
        education_id: new FormControl(null),
        edctnfile: new FormControl(null, [Validators.required])
      })
    );
  }

  onDelEducation(idx) {
    if (!isNullOrUndefined(this.airuserFormEducation[idx].value.id)) {
      this.airuserFormEducationTodel.push(
        this.airuserFormEducation[idx].value.id
      );
    }
    this.airuserFormEducation.splice(idx, 1);
    this.newEducationFile.splice(idx, 1);
  }

  onAddFamily() {
    this.airuserFormFamily.push(
      new FormGroup({
        name: new FormControl(null, [Validators.required]),
        surname: new FormControl(null, [Validators.required]),
        work_place: new FormControl(null),
        work_position: new FormControl(null),
        dt_of_birth: new FormControl(null),
        relationdegree_id: new FormControl(0, [Validators.required]),
        airuser_id: new FormControl(null)
      })
    );
  }

  onDelFamily(idx) {
    if (!isNullOrUndefined(this.airuserFormFamily[idx].value.id)) {
      this.airuserFormFamilyTodel.push(this.airuserFormFamily[idx].value.id);
    }
    this.airuserFormFamily.splice(idx, 1);
  }

  onAddPreviouswork() {
    this.airuserFormPreviouswork.push(
      new FormGroup({
        work_place: new FormControl(null, [Validators.required]),
        work_position: new FormControl(null, [Validators.required]),
        dt_start: new FormControl(null),
        dt_end: new FormControl(null),
        airuser_id: new FormControl(null)
      })
    );
  }

  onDelPreviouswork(idx) {
    if (!isNullOrUndefined(this.airuserFormPreviouswork[idx].value.id)) {
      this.airuserFormPreviousworkTodel.push(
        this.airuserFormPreviouswork[idx].value.id
      );
    }
    this.airuserFormPreviouswork.splice(idx, 1);
  }

  onCheckLogin() {
    for (let i = 0; i < this.delUsers.length; i++) {
      if (
        this.delUsers[i].login === this.airuserForm.value['login'] &&
        this.airuserForm.value['login'] !== this.selectedUser.login
      ) {
        this.checkLogin = 0;
        return;
      }
    }
    this.checkLogin = 1;
  }

  onAddUserPhone() {
    this.airuserFormPhones.push(
      new FormGroup({
        phone: new FormControl(null),
        airuser_id: new FormControl(null)
      })
    );
  }

  onDelUserPhone(idx) {
    if (!isNullOrUndefined(this.airuserFormPhones[idx].value.id)) {
      this.airuserFormPhonesTodel.push(this.airuserFormPhones[idx].value.id);
    }
    this.airuserFormPhones.splice(idx, 1);
  }

  onAddUserMail() {
    this.airuserFormEmails.push(
      new FormGroup({
        email: new FormControl(null),
        airuser_id: new FormControl(null)
      })
    );
  }

  onDelUserMail(idx) {
    if (!isNullOrUndefined(this.airuserFormEmails[idx].value.id)) {
      this.airuserFormEmailsTodel.push(this.airuserFormEmails[idx].value.id);
    }
    this.airuserFormEmails.splice(idx, 1);
  }

  toggleCheckToDel(idx) {
    this.selectedUserFiles[idx].checkToDel = !this.selectedUserFiles[idx]
      .checkToDel;
  }

  hasChanges(): boolean {
    let res = false;
    if (this.intType === 'edit') {
      // if (this.airuserForm.touched && this.airuserForm.dirty) {
      //   res = true;
      // }
    } else {
      for (let prop in this.airuserForm.value) {
        if (this.airuserForm.value.hasOwnProperty(prop)) {
          if (prop === 'password') {
            continue;
          }
          if (
            !isNullOrUndefined(this.airuserForm.value[prop]) &&
            this.airuserForm.value[prop].length > 0
          ) {
            res = true;
          }
        }
      }
    }
    return res;
  }

  /**Функция срабатывает по гварду canDeactivate если было изменение форм. Предложение сохранить. */
  async canDeactivate() {
    if (this.hasChanges() === true) {
      let dialogRef = this.dialog.open(DialogYesnoComponent, {
        data: {
          title:
            'Вы уверены, что хотите покинуть страницу без сохранения изменений?',
          question: ``
        }
      });
      let res = await new Promise((resolve, reject) => {
        dialogRef.afterClosed().subscribe(async result => {
          if (result === true) {
            resolve(true);
          } else if (result === false) {
            resolve(false);
          } else {
            resolve(false);
          }
        });
      });
      return res;
    } else {
      return true;
    }
  }

  getPositionsBySubdivisionId(subdivision_id) {
    if (this.positions && subdivision_id > 0) {
      return this.positions.filter(
        item => item.subdivision_id === +subdivision_id
      );
    } else {
      return [];
    }
  }

  onCancel() {
    this.initForms();
    if (this.intType === 'edit') {
      this.onSelectAiruser(this.selectedUser);
    }
  }

  onDelAiruser() {
    let user = this.airusers.find(item => item.id === this.selectedUser.id);
    let dialogRef = this.dialog.open(DialogYesnoComponent, {
      data: {
        title: `Вы действительно хотите уволить сотрудника ${user.name} ${user.surname}?`
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === true) {
        let res = await this.airuserService.deleteOneById(this.selectedUser.id);
        this.router.navigate([`/sys/organisation/users/list`]);
      }
    });
  }

  async onDownloadEducationFile(file) {
    // this.spinnerService.show();
    // let fileName = '';
    // for (let i = 0; i < this.selectedEducationFiles.length; i++) {
    //   if (this.selectedUserFiles[i] !== undefined) {
    //     if (id === this.selectedUserFiles[i].id) {
    //       fileName = this.selectedUserFiles[i].ausrfile.filename;
    //     }
    //   }
    // }
  }

  async onDownloadUserFile(id) {
    this.spinnerService.show();
    let fileName = '';
    for (let i = 0; i < this.selectedUserFiles.length; i++) {
      if (this.selectedUserFiles[i] !== undefined) {
        if (id === this.selectedUserFiles[i].id) {
          fileName = this.selectedUserFiles[i].ausrfile.filename;
        }
      }
    }
    try {
      let blob = this.airuserfileService.downloadOneById(id);
      this.inProgress = true;
      this.inProgressVal = 0;
      blob.subscribe(event => {
        if (event.type === HttpEventType.DownloadProgress) {
          const percentDone = Math.round((100 * event.loaded) / event.total);
          this.inProgressVal = percentDone;
        } else if (event instanceof HttpResponse) {
          this.inProgress = false;
          this.inProgressVal = 0;
          this.spinnerService.hide();
          importedSaveAs(event.body, fileName);
        }
      });
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
      this.spinnerService.hide();
    }
  }

  onEmailLogPass() {
    let pass = Math.random()
      .toString(36)
      .substr(2, 7);
    this.airuserForm.patchValue({
      password: pass
    });
    for (let i = 0; i < this.selectedUserEmails.length; i++) {
      let data = {
        email: this.selectedUserEmails[i].email,
        subject: 'aero profi',
        textbody: `<h1> Данные для входа в систему aero profi </h1>
				<br> <p>Login: ${this.airuserForm.value.login}, password: ${this.airuserForm.value.password} </p>
				<p> Для входи в систему перейдите по <a href = "${window.location.origin}"> ссылке </a> (${window.location.origin})</p> `
      };
      this.emailService.sendEmail(data).subscribe(res => {
        this.onSave();
      });
    }
  }

  onSetAddress(type: string) {
    let dialogRef = this.dialog.open(DialogSetaddressComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title:
          type === 'registration' ? `Адрес регистрации` : 'Фактический адрес',
        index: this.airuserForm.get(`${type}_index`).value,
        city: this.airuserForm.get(`${type}_city`).value,
        street: this.airuserForm.get(`${type}_street`).value,
        house: this.airuserForm.get(`${type}_house`).value,
        flat: this.airuserForm.get(`${type}_flat`).value
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result && result !== false) {
        this.airuserForm.patchValue({
          [`${type}_index`]: result.index,
          [`${type}_city`]: result.city,
          [`${type}_street`]: result.street,
          [`${type}_house`]: result.house,
          [`${type}_flat`]: result.flat
        });
      }
    });
  }

  onSetEducation(idx) {
    let dialogRef = this.dialog.open(DialogSeteducationComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Образование`,
        institution_place: this.airuserFormEducation[idx].get(
          `institution_place`
        ).value,
        specialty: this.airuserFormEducation[idx].get(`specialty`).value,
        diploma_number: this.airuserFormEducation[idx].get(`diploma_number`)
          .value,
        finish_year: this.airuserFormEducation[idx].get(`finish_year`).value
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === 'delete') {
        if (this.airuserFormEducation.length > 1) {
          this.onDelEducation(idx);
        } else {
          this.onDelEducation(idx);
          this.onAddEducation();
          this.newEducationFile[idx].patchValue({
            edctnfile: null
          });
        }
      } else if (!result || result === false) {
      } else {
        this.airuserFormEducation[idx].patchValue({
          [`institution_place`]: result.institution_place,
          [`specialty`]: result.specialty,
          [`diploma_number`]: result.diploma_number,
          [`finish_year`]: result.finish_year
        });
        if (!isNullOrUndefined(result.event)) {
          this.onEducationFileChange(result.event, idx);
        }
      }
    });
  }

  onSetPreviouswork(idx) {
    let dialogRef = this.dialog.open(DialogSetpreviousworkComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Опыт работы`,
        work_place: this.airuserFormPreviouswork[idx].get(`work_place`).value,
        work_position: this.airuserFormPreviouswork[idx].get(`work_position`)
          .value,
        dt_start: this.airuserFormPreviouswork[idx].get(`dt_start`).value,
        dt_end: this.airuserFormPreviouswork[idx].get(`dt_end`).value
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === 'delete') {
        if (this.airuserFormPreviouswork.length > 1) {
          this.onDelPreviouswork(idx);
        } else {
          this.onDelPreviouswork(idx);
          this.onAddPreviouswork();
          // this.airuserFormPreviouswork[idx].patchValue({
          //   [`work_place`]: null,
          //   [`work_position`]: null,
          //   [`dt_start`]: null,
          //   [`dt_end`]: null
          // });
        }
      } else if (!result || result === false) {
      } else {
        this.airuserFormPreviouswork[idx].patchValue({
          [`work_place`]: result.work_place,
          [`work_position`]: result.work_position,
          [`dt_start`]: result.dt_start,
          [`dt_end`]: result.dt_end
        });
      }
    });
  }

  onSetFamily(idx) {
    let dialogRef = this.dialog.open(DialogSetfamilymemberComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Члены семьи`,
        surname: this.airuserFormFamily[idx].get(`surname`).value,
        name: this.airuserFormFamily[idx].get(`name`).value,
        dt_of_birth: this.airuserFormFamily[idx].get(`dt_of_birth`).value,
        relationdegree_id: this.airuserFormFamily[idx].get(`relationdegree_id`)
          .value,
        work_place: this.airuserFormFamily[idx].get(`work_place`).value,
        work_position: this.airuserFormFamily[idx].get(`work_position`).value,
        relationdegrees: this.relationdegrees
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === 'delete') {
        if (this.airuserFormFamily.length > 1) {
          this.onDelFamily(idx);
        } else {
          this.onDelFamily(idx);
          this.onAddFamily();
          // this.airuserFormFamily[idx].patchValue({
          //   [`id`]: null,
          //   [`surname`]: null,
          //   [`name`]: null,
          //   [`dt_of_birth`]: null,
          //   [`relationdegree_id`]: null,
          //   [`work_place`]: null,
          //   [`work_position`]: null
          // });
        }
      } else if (!result || result === false) {
      } else {
        this.airuserFormFamily[idx].patchValue({
          [`surname`]: result.surname,
          [`name`]: result.name,
          [`dt_of_birth`]: result.dt_of_birth,
          [`relationdegree_id`]: result.relationdegree_id,
          [`work_place`]: result.work_place,
          [`work_position`]: result.work_position
        });
        if (!isNullOrUndefined(result.event)) {
          this.onEducationFileChange(result.event, idx);
        }
      }
    });
  }

  isImgtype(filename: string): boolean {
    let res = false;
    switch (
      filename
        .toLowerCase()
        .split('.')
        .reverse()[0]
    ) {
      case 'png':
      case 'jpg':
      case 'jpeg':
      case 'bmp':
        res = true;
        break;
      default:
        res = false;
        break;
    }
    return res;
  }

  async onShowImg(id) {
    let fname: string;
    this.selectedUserFiles.map(item => {
      if (item.id === id) {
        fname = item.ausrfile.filename;
      }
    });
    if (this.galleryImages.length === 0) {
      this.spinnerService.show();
      this.inProgress = true;
      this.inProgressVal += 100 / this.selectedUserFiles.length;
      await Promise.all(
        this.selectedUserFiles.map(item => {
          return new Promise(async resolve => {
            let type = item.ausrfile.filename.split('.')[1];
            let resfile;
            switch (type.toLowerCase()) {
              case 'png':
              case 'jpg':
              case 'jpeg':
              case 'bmp':
                let file = await this.airuserfileService.getFileById(item.id);
                this.inProgressVal += 100 / this.selectedUserFiles.length;
                resfile = new Blob([file], { type: 'image/jpg' });
                let reader = new FileReader();
                reader.readAsDataURL(resfile);
                reader.onloadend = () => {
                  let base64data = String(reader.result);
                  this.galleryImages.push({
                    small: base64data,
                    medium: base64data,
                    big: base64data,
                    description: item.ausrfile.filename
                  });
                  this.galleryImages.sort((a, b) =>
                    a.description > b.description ? 1 : -1
                  );
                  resolve();
                };
                break;
              default:
                resolve();
                break;
            }
          });
        })
      );
      this.spinnerService.hide();
      this.inProgress = false;
    }
    setTimeout(() => {
      this.showGallary = true;
      let idx = this.galleryImages.findIndex(
        item => item.description === fname
      );
      this.gallary.openPreview(idx);
    }, 300);
  }

  onCloseGallary() {
    this.showGallary = false;
    this.galleryImages = [];
  }

  getIndent(level: number): string {
    let str = '';
    for (let index = 1; index < level; index++) {
      str += '&nbsp;&nbsp;&nbsp;';
    }
    return str;
  }

  inputLink_airport() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.airport_control.value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_departure_airport = this.airport_control.valueChanges.pipe(
            startWith<string | Airport>(''),
            map(value => (typeof value === 'string' ? value : value.code)),
            map(code => (code ? this._filter(code) : this.airports.slice()))
          );
        } catch {}
      }
    }, 250);
  }

  private _filter(code: string): any[] {
    const filterValue = code.toLowerCase();
    return isNullOrUndefined(this.airports)
      ? null
      : this.airports.filter(
          airport => airport.code.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  displayFn(airport?: any): string | undefined {
    return airport ? `${airport.code} ${airport.name_rus}` : undefined;
  }
}
