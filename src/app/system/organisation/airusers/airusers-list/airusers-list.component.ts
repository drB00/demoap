import { Component, OnInit, ViewChild } from '@angular/core';
import { WebsocketService, WS_API } from 'src/app/websocket';
import { AiruserService } from 'src/app/system/shared/services/airuser.service';
import { PositionService } from 'src/app/system/shared/services/position.service';
import { SubdivisionService } from 'src/app/system/shared/services/subdivision.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Airuser } from 'src/app/shared/models/airuser.model';
import { Subdivision } from 'src/app/shared/models/subdivision.model';
import { Position } from 'src/app/shared/models/position.model';
import { isNullOrUndefined } from 'util';
import { Router } from '@angular/router';
import { PageEvent, MatPaginator } from '@angular/material';
import { AiruserphoneService } from 'src/app/system/shared/services/airuserphone.service';
import { Observable } from 'rxjs';
import { GlobalsettingsService } from 'src/app/system/shared/services/globalsettings.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { OrganisationstoreService } from 'src/app/system/shared/services/organisationstore.service';

@Component({
  selector: 'app-airusers-list',
  templateUrl: './airusers-list.component.html',
  styleUrls: ['./airusers-list.component.scss']
})
export class AirusersListComponent implements OnInit {
  airusers: Airuser[];
  airuserphones: any[];
  airusersView: Airuser[];
  subdivisions: Subdivision[];
  positions: Position[];

  searchStr = '';
  length = 100;
  pageSize = 25;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  pageEvent: PageEvent;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  messages$: Observable<string>;

  constructor(
    private airuserService: AiruserService,
    private authService: AuthService,
    private organisationstoreService: OrganisationstoreService,
    private globalsettingsService: GlobalsettingsService,
    private airuserphoneService: AiruserphoneService,
    private positionService: PositionService,
    private subdivisionService: SubdivisionService,
    private spinnerService: NgxSpinnerService,
    private router: Router,
    private wsService: WebsocketService
  ) {}

  async ngOnInit() {
    this.spinnerService.show();
    await this.getData();

    /**Подписка на сокет и сущность airuser*/
    this.messages$ = this.wsService.addEventListener(WS_API.EVENTS.MESSAGES);
    this.messages$.subscribe(async data => {
      data = data.indexOf('action') !== -1 ? JSON.parse(data) : data;
      if (data['resourse'] === 'airuser') {
        this.updateFromSocket(data['resourse'], data['pk'], data['action']);
      }
    });
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add airuser');
  }

  async getData() {
    try {
      let airusers = this.airuserService.getAll();
      let airuserphones = this.airuserphoneService.getAll();
      let subdivisions = this.subdivisionService.getAll();
      let positions = this.positionService.getAll();
      this.airusers = isNullOrUndefined(await airusers) ? [] : await airusers;
      this.airuserphones = isNullOrUndefined(await airuserphones)
        ? []
        : await airuserphones;
      this.subdivisions = isNullOrUndefined(await subdivisions)
        ? []
        : await subdivisions;
      this.positions = isNullOrUndefined(await positions)
        ? []
        : await positions;
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
    this.airusers = this.airusers.slice(2, this.airusers.length);
    this.airusers = this.airuserService.setPositionnames(
      this.airusers,
      this.positions
    );
    this.airusers = this.airuserService.setSubdivisionnames(
      this.airusers,
      this.subdivisions
    );
    this.airusers = this.airuserService.setPhones(
      this.airusers,
      this.airuserphones
    );
    this.length = this.airusers.length;
    if (this.authService.loggedInUser.isadmin === true) {
      this.airusersView = [...this.airusers];
    } else {
      let subdivisionsView = this.organisationstoreService.getWithAccess(
        this.authService.loggedInUser.isadmin,
        this.authService.loggedInUserRole,
        this.authService.loggedInUserSubdivision,
        this.subdivisions,
        this.authService.loggedInUserAccesses
      );
      this.airusersView = this.airusers.filter(airuser => {
        let sbdv = subdivisionsView.find(
          item => item.id === airuser.subdivision_id
        );
        return !isNullOrUndefined(sbdv);
      });
    }
  }

  /**Update по событию сокета */
  async updateFromSocket(resourse: string, pk: number, action: string) {
    if (action === 'UPDATED' || action === 'INSERTED') {
      let airuser: Airuser;
      let airuserphones: any[];
      let position: Position;
      let subdivision: Subdivision;
      try {
        let _airuser = this.airuserService.getOneById(pk);
        let _airuserphones = this.airuserphoneService.getByAiruser(pk);
        airuser = await _airuser;
        let _position = this.positionService.getOneById(airuser.position_id);
        position = await _position;
        let _subdivision = this.subdivisionService.getOneById(
          position.subdivision_id
        );
        subdivision = await _subdivision;
        airuserphones = isNullOrUndefined(await _airuserphones)
          ? []
          : await _airuserphones;
      } catch (err) {
        this.globalsettingsService.exceptionHandling(err);
      }
      airuser.position_name = position.name;
      airuser.subdivision_name = subdivision.name;
      airuser.phones = airuserphones.map(item => {
        return item.phone;
      });
      if (action === 'UPDATED') {
        let idxView = this.airusersView.findIndex(item => item.id === pk);
        if (idxView !== -1) {
          this.airusersView[idxView] = airuser;
        }
        let idx = this.airusers.findIndex(item => item.id === pk);
        if (idx !== -1) {
          this.airusers[idx] = airuser;
        }
      } else if (action === 'INSERTED') {
        this.airusers.push(airuser);
        if (this.pageSize > this.airusersView.length) {
          this.airusersView.push(airuser);
        }
      }
    } else if (action === 'DELETED') {
      let idxView = this.airusersView.findIndex(item => item.id === pk);
      if (idxView !== -1) {
        this.airusersView.splice(idxView, 1);
      }
      let idx = this.airusers.findIndex(item => item.id === pk);
      if (idx !== -1) {
        this.airusers.splice(idx, 1);
      }
    }
  }

  gotoEdit(id) {
    this.router.navigate([`/sys/organisation/users/edit/${id}`]);
  }

  onFilter() {
    this.airusersView = this.airusers.filter(item => {
      return item.surname.indexOf(this.searchStr) !== -1;
    });
    // this.paginator.firstPage();
  }

  setPageSizeOptions(event: PageEvent) {
    if (!isNullOrUndefined(event)) {
      let start = event.pageSize * event.pageIndex;
      let end = start + event.pageSize;
      this.airusersView = this.airusers.slice(start, end);
    }
    return event;
  }
}
