import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-template-subdivision',
  templateUrl: './template-subdivision.component.html',
  styleUrls: ['./template-subdivision.component.scss']
})
export class TemplateSubdivisionComponent implements OnInit {
  @Input() upBtn: string; // arr of objects
  @Input() downBtn: string; // arr of objects
  @Input() list: any[]; // arr of objects
  @Input() propName: string; // object prop with content
  @Input() children = 'children'; // object prop with children
  @Input() propId = 'id'; // object prop with id
  @Input() selectedId = -1; // object prop with id
  @Input() postfix1: string;
  @Input() postfix2: string;
  @Output() listChanged = new EventEmitter<any>();
  @Output() selectedItemId = new EventEmitter<number>();

  firstId = -1;
  secondId = -1;

  constructor() {}

  ngOnInit() {}

  sendList(event) {
    if (!isNullOrUndefined(event.from) && !isNullOrUndefined(event.to)) {
      this.firstId = event.from;
      this.secondId = event.to;
    }
    this.listChanged.emit({ from: this.firstId, to: this.secondId });
  }

  onUp(index: number) {
    if (index === 0) {
      return;
    }
    [this.list[index - 1], this.list[index]] = [
      this.list[index],
      this.list[index - 1]
    ];
    this.firstId = this.list[index - 1].id;
    this.secondId = this.list[index].id;
    this.sendList({});
  }

  onDown(index: number) {
    if (index === this.list.length - 1) {
      return;
    }
    [this.list[index + 1], this.list[index]] = [
      this.list[index],
      this.list[index + 1]
    ];
    this.firstId = this.list[index + 1].id;
    this.secondId = this.list[index].id;
    this.sendList({});
  }

  onSelect(id) {
    this.selectedId = id;
    this.selectedItemId.emit(id);
  }
}
