import { Component, OnInit } from '@angular/core';
import { Airuser } from 'src/app/shared/models/airuser.model';
import { Subdivision } from 'src/app/shared/models/subdivision.model';
import { Position } from 'src/app/shared/models/position.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { AiruserService } from '../../shared/services/airuser.service';
import { SubdivisionService } from '../../shared/services/subdivision.service';
import { PositionService } from '../../shared/services/position.service';
import { isNullOrUndefined } from 'util';
import { MatDialog, MatSnackBar } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SubdivisionfileService } from '../../shared/services/subdivisionfile.service';
import { PositionfileService } from '../../shared/services/positionfile.service';
import { saveAs as importedSaveAs } from 'file-saver';
import { Router } from '@angular/router';
import { DialogYesnoComponent } from '../../shared/components/dialog-yesno/dialog-yesno.component';
import { DictionaryService } from '../../shared/services/dictionary.service';
import { AircraftmodelgroupService } from '../../shared/services/aircraftmodelgroup.service';
import { DialogSubdivisionComponent } from '../dialogs/dialog-subdivision/dialog-subdivision.component';
import { DialogPositionComponent } from '../dialogs/dialog-position/dialog-position.component';
import { GlobalsettingsService } from '../../shared/services/globalsettings.service';

@Component({
  selector: 'app-subdivisions',
  templateUrl: './subdivisions.component.html',
  styleUrls: ['./subdivisions.component.scss']
})
export class SubdivisionsComponent implements OnInit {
  airusers: Airuser[];
  subdivisions: Subdivision[];
  positions: Position[];

  subdivision: Subdivision; // Объект со всей иерархией подразделений
  accessLevel = 0; // 0 - none, 1 - get, 2 - put //, 3 - редактирвоание подразделения
  showNewPositionFileEdit = false;

  upBtnStr = '<i class="fa fa-chevron-up" aria-hidden="true"></i>';
  downBtnStr = '<i class="fa fa-chevron-down" aria-hidden="true"></i>';

  selectedSubdivisionId = -1;
  selectedSubdivision: Subdivision;
  selectedSubdivisionfiles = [];
  selectedPosition: Position;
  selectedPositionId = -1;
  selectedPositionFiles = [];

  subdivisionfiles: any[];
  positionfiles: any[];
  position: Position; // Объект со всей иерархией должностей
  aircraftmodelgroups: any[];

  subdivisionForm: FormGroup;
  positionForm: FormGroup;
  subdivisionFormEdit: FormGroup;
  positionFormEdit: FormGroup;
  newSubdivisionfile: FormGroup[] = []; // форма для добавления новго файла к подразделению
  subdivisionfileEdit: FormGroup[] = []; // форма для добавления файла к уже существующему подразделению
  newPositionfile: FormGroup;
  positionfileEdit: FormGroup;

  name = 'test';

  pilotTypes = [];

  constructor(
    public authService: AuthService,
    private globalsettingsService: GlobalsettingsService,
    private dictionaryService: DictionaryService,
    private airuserService: AiruserService,
    private subdivisionService: SubdivisionService,
    private aircraftmodelgroupService: AircraftmodelgroupService,
    private subdivisionfileService: SubdivisionfileService,
    private positionfileService: PositionfileService,
    private positionService: PositionService,
    private dialog: MatDialog,
    private spinnerService: NgxSpinnerService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  async ngOnInit() {
    // let resPostSubdivision = await this.subdivisionService.postOne({
    //   name: 'Альбатрос'
    // });
    this.setForms();
    this.getData();
  }

  async getData() {
    this.spinnerService.show();
    try {
      this.pilotTypes = this.dictionaryService.pilotTypes;
      let airusers = this.airuserService.getAll();
      let subdivisions = this.subdivisionService.getAll();
      let positions = this.positionService.getAll();
      let subdivisionfiles = this.subdivisionfileService.getAll();
      let positionfiles = this.positionfileService.getAll();
      let aircraftmodelgroups = this.aircraftmodelgroupService.getAll();
      this.subdivisionfiles = isNullOrUndefined(await subdivisionfiles)
        ? []
        : await subdivisionfiles;
      this.positionfiles = isNullOrUndefined(await positionfiles)
        ? []
        : await positionfiles;
      this.aircraftmodelgroups = isNullOrUndefined(await aircraftmodelgroups)
        ? []
        : await aircraftmodelgroups;
      this.airusers = isNullOrUndefined(await airusers) ? [] : await airusers;
      this.subdivisions = isNullOrUndefined(await subdivisions)
        ? []
        : await subdivisions;
      this.positions = isNullOrUndefined(await positions)
        ? []
        : await positions;
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      if (this.subdivisions.length > 0) {
        this.setSubdivisionTree(); // Получаем иерархию подразделений
        this.selectedSubdivisionId = this.subdivision.id;
        this.onSelectSubdivision(this.selectedSubdivisionId);
        this.countAirusers();
        this.setPositionTree();
      }
      this.spinnerService.hide();
    }
  }

  setForms() {
    this.positionForm = new FormGroup({
      subdivision_id: new FormControl(0),
      name: new FormControl(null, [Validators.required]),
      salary: new FormControl(null),
      pilot_type: new FormControl(null),
      parent_id: new FormControl(null)
    });
    this.newSubdivisionfile.push(
      new FormGroup({
        subdivision_id: new FormControl(null, [Validators.required]),
        revnumber: new FormControl(null, [Validators.required]),
        revdate: new FormControl(null, [Validators.required]),
        sbdivfile: new FormControl(null)
      })
    );
    this.subdivisionForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      parent_id: new FormControl(null),
      code: new FormControl(null),
      type: new FormControl(0),
      aircraftmodelgroup_id: new FormControl(null)
    });
    this.subdivisionFormEdit = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      code: new FormControl(null),
      type: new FormControl(0),
      aircraftmodelgroup_id: new FormControl(null)
    });
    this.newPositionfile = new FormGroup({
      position_id: new FormControl(null, [Validators.required]),
      revnumber: new FormControl(null, [Validators.required]),
      revdate: new FormControl(null, [Validators.required]),
      positionfile: new FormControl(null)
    });
    this.positionFormEdit = new FormGroup({
      position_id: new FormControl(null),
      parent_id: new FormControl(null),
      salary: new FormControl(null),
      pilot_type: new FormControl(null),
      name: new FormControl(null, [Validators.required]),
      subdivision_id: new FormControl(null, [Validators.required])
    });
  }

  getCountUsers(subdivisions: Subdivision[]) {
    let sum = 0;
    if (isNullOrUndefined(subdivisions) || !subdivisions.length) {
      return sum;
    }
    for (let i = 0; i < subdivisions.length; i++) {
      sum += subdivisions[i].countOwnUsers;
      if (subdivisions[i].children.length !== 0) {
        sum += this.getCountUsers(subdivisions[i].children);
      }
    }
    return sum;
  }

  setCountUsers(subdivisions: Subdivision[]) {
    if (isNullOrUndefined(subdivisions[0]) || !subdivisions.length) {
      return;
    }
    for (let i = 0; i < subdivisions.length; i++) {
      subdivisions[i].countUsers = this.getCountUsers(subdivisions[i].children);
      this.setCountUsers(subdivisions[i].children);
    }
  }

  countAirusers() {
    // расчет количества пользователей в подразделениях
    for (let k = 0; k < this.subdivisions.length; k++) {
      this.subdivisions[k].countUsers = 0;
      this.subdivisions[k].countOwnUsers = 0;
      for (let i = 0; i < this.positions.length; i++) {
        this.positions[i].countUsers = 0;
        for (let j = 0; j < this.airusers.length; j++) {
          if (this.positions[i].id === this.airusers[j].position_id) {
            this.positions[i].countUsers++;
          }
        }
        if (this.subdivisions[k].id === this.positions[i].subdivision_id) {
          this.subdivisions[k].countOwnUsers += this.positions[i].countUsers;
        }
      }
    }
    this.setCountUsers([this.subdivision]);
  }

  setSubdivisionTree() {
    // создает иерархическую структуру подразделений, потомки в .children для this.subdivision
    if (isNullOrUndefined(this.subdivisions) || !this.subdivisions.length) {
      return;
    }
    this.subdivisions.sort((a, b) => {
      return a.position > b.position ? 1 : -1;
    });
    for (let i = 0; i < this.subdivisions.length; i++) {
      this.subdivisions[i].children = [];
      for (let j = 0; j < this.subdivisions.length; j++) {
        if (this.subdivisions[i].id === this.subdivisions[j].parent_id) {
          this.subdivisions[i].children.push(this.subdivisions[j]);
        }
      }
    }
    this.subdivision = this.subdivisions[0];
  }

  getSubdivisionById(id, subdivisions: Subdivision[]): Subdivision {
    // по id получаем подразделение из иерархической структуры
    let subd: Subdivision;
    for (let i = 0; i < subdivisions.length; i++) {
      if (id === subdivisions[i].id) {
        subd = subdivisions[i];
        return subd;
      } else {
        subd = this.getSubdivisionById(id, subdivisions[i].children);
        if (!isNullOrUndefined(subd)) {
          return subd;
        }
      }
    }
    return subd;
  }

  /**Является ли должность начальником подразделения */
  isPositionChiefOfSubdivision(pos: Position, subdivision: Subdivision) {
    if (pos.parent_id) {
      return false;
    } else {
      if (isNullOrUndefined(subdivision.parent_id)) {
        return pos.subdivision_id === subdivision.id;
      } else {
        return pos.subdivision_id === subdivision.id
          ? true
          : this.isPositionChiefOfSubdivision(
              pos,
              this.getSubdivisionById(subdivision.parent_id, [this.subdivision])
            );
      }
    }
  }

  isPositionInParentSubdivision(pos: Position, subdivision: Subdivision) {
    if (isNullOrUndefined(subdivision.parent_id)) {
      return pos.subdivision_id === subdivision.id;
    } else {
      return pos.subdivision_id === subdivision.id
        ? true
        : this.isPositionInParentSubdivision(
            pos,
            this.getSubdivisionById(subdivision.parent_id, [this.subdivision])
          );
    }
  }

  isAiruserInSubdivision(airuser: Airuser, subdivision: Subdivision[]) {
    if (
      isNullOrUndefined(airuser) ||
      isNullOrUndefined(subdivision) ||
      subdivision.length === 0
    ) {
      return false;
    } else {
      for (let i = 0; i < subdivision.length; i++) {
        if (
          this.positions.find(pos => pos.id === airuser.position_id)
            .subdivision_id === subdivision[i].id
        ) {
          return true;
        } else if (
          this.isAiruserInSubdivision(
            airuser,
            this.subdivisions.filter(
              subdv => subdv.parent_id === subdivision[i].id
            )
          )
        ) {
          return true;
        }
      }
      return false;
    }
  }

  getSubdivisionByPositionId(id) {
    if (
      isNullOrUndefined(this.positions) ||
      isNullOrUndefined(this.subdivisions)
    ) {
      return;
    }
    for (let i = 0; i < this.positions.length; i++) {
      if (this.positions[i].id === id) {
        for (let j = 0; j < this.subdivisions.length; j++) {
          if (this.subdivisions[j].id === this.positions[i].subdivision_id) {
            return this.subdivisions[j];
          }
        }
      }
    }
  }

  /**Проверка является ли subdivision Дочерним к subdivisions */
  isSubdivisionInSubdivisions(
    subdivision: Subdivision,
    subdivisions: Subdivision[]
  ) {
    if (
      isNullOrUndefined(subdivision) ||
      isNullOrUndefined(subdivisions) ||
      subdivisions.length === 0
    ) {
      return false;
    } else {
      for (let i = 0; i < subdivisions.length; i++) {
        if (subdivision.parent_id === subdivisions[i].id) {
          return true;
        } else if (
          this.isSubdivisionInSubdivisions(
            subdivision,
            this.subdivisions.filter(
              subdv => subdv.parent_id === subdivisions[i].id
            )
          )
        ) {
          return true;
        }
      }
    }
    return false;
  }

  getPositionNameById(id) {
    if (isNullOrUndefined(this.positions)) {
      return;
    }
    return isNullOrUndefined(
      this.positions.find(position => position.id === id)
    )
      ? ''
      : this.positions.find(position => position.id === id).name;
  }

  // getSubdivisionNameByPositionId(id) {
  //   if (
  //     isNullOrUndefined(this.positions) ||
  //     isNullOrUndefined(this.subdivisions)
  //   ) {
  //     return;
  //   }
  //   for (let i = 0; i < this.positions.length; i++) {
  //     if (this.positions[i].id === id) {
  //       for (let j = 0; j < this.subdivisions.length; j++) {
  //         if (this.subdivisions[j].id === this.positions[i].subdivision_id) {
  //           return this.subdivisions[j].name;
  //         }
  //       }
  //     }
  //   }
  // }

  // getSubdivisionIdByPositionId(id) {
  //   if (
  //     isNullOrUndefined(this.positions) ||
  //     isNullOrUndefined(this.subdivisions)
  //   ) {
  //     return;
  //   }
  //   for (let i = 0; i < this.positions.length; i++) {
  //     if (this.positions[i].id === id) {
  //       for (let j = 0; j < this.subdivisions.length; j++) {
  //         if (this.subdivisions[j].id === this.positions[i].subdivision_id) {
  //           return this.subdivisions[j].id;
  //         }
  //       }
  //     }
  //   }
  // }

  // /**Является ли childid дочерним к parentid */
  // isParentOfSubdivisionById(parentId: number, childId: number) {
  //   if (parentId === childId) {
  //     return true;
  //   }
  //   if (isNullOrUndefined(childId)) {
  //     return false;
  //   }
  //   if (
  //     isNullOrUndefined(this.subdivisions.find(item => item.id === childId))
  //   ) {
  //     return false;
  //   }
  //   return this.isParentOfSubdivisionById(
  //     parentId,
  //     this.subdivisions.find(item => item.id === childId).parent_id
  //   )
  //     ? true
  //     : false;
  // }

  getPositionsBySubdivisionId(subdivisionId) {
    // получение списка должностей в подразделении
    if (isNullOrUndefined(this.positions)) {
      return [];
    }
    if (subdivisionId === 0) {
      return [];
    }
    let positions: Position[] = [];
    for (let pos of this.positions) {
      if (pos.subdivision_id === subdivisionId) {
        positions.push(pos);
      }
    }
    return positions;
  }

  setPositionTree() {
    // создает иерархическую структуру должностей, потомки в .children this.position
    let pos = this.getPositionsBySubdivisionId(this.selectedSubdivisionId);
    if (isNullOrUndefined(pos)) {
      return;
    }
    pos.sort((a, b) => {
      if (isNullOrUndefined(a.parent_id)) {
        return -1;
      } else if (isNullOrUndefined(b.parent_id)) {
        return 1;
      } else {
        return a.id > b.id ? 1 : -1;
      }
    });
    if (pos.length) {
      for (let i = 0; i < pos.length; i++) {
        pos[i].children = [];
        for (let j = 0; j < pos.length; j++) {
          if (pos[i].id === pos[j].parent_id) {
            pos[i].children.push(pos[j]);
          }
        }
      }
      this.position = pos[0];
    } else {
      this.position = null;
    }
  }

  getPositionById(id, positions: Position[]): Position {
    // по id получаем подразделение из иерархической структуры
    let pos: Position;
    for (let i = 0; i < positions.length; i++) {
      if (isNullOrUndefined(positions[i])) {
        continue;
      }
      if (id === positions[i].id) {
        pos = positions[i];
        return pos;
      } else {
        pos = this.getPositionById(id, positions[i].children);
        if (!isNullOrUndefined(pos)) {
          return pos;
        }
      }
    }
    return pos;
  }

  onSelectSubdivision(id) {
    this.positionFormEdit.patchValue({
      position_id: null,
      parent_id: null,
      pilot_type: null,
      name: null,
      salary: null,
      subdivision_id: null
    });
    this.selectedPosition = null;
    this.selectedPositionId = 0;
    this.selectedSubdivisionId = id;
    this.selectedSubdivision = this.getSubdivisionById(id, [this.subdivision]);

    if (this.authService.loggedInUser.isadmin) {
      this.accessLevel = 3;
    } else {
      let pos: Position;
      for (let i = 0; i < this.positions.length; i++) {
        if (
          this.positions[i].id === this.authService.loggedInUser.position_id
        ) {
          pos = this.positions[i];
        }
      }
      if (isNullOrUndefined(pos)) {
        // проверяем права в подразделении по умолчанию
        this.accessLevel = 0;
      } else if (
        this.isPositionInParentSubdivision(pos, this.selectedSubdivision) &&
        pos.parent_id
      ) {
        this.accessLevel = 1;
      } else if (
        this.isPositionChiefOfSubdivision(pos, this.selectedSubdivision)
      ) {
        this.accessLevel = 1;
      } else {
        this.accessLevel = 0;
      }
      if (!isNullOrUndefined(this.authService.loggedInUserAccesses)) {
        // проверяем на дополнительные права в подразделении
        for (let i = 0; i < this.authService.loggedInUserAccesses.length; i++) {
          if (!isNullOrUndefined(this.authService.loggedInUserAccesses[i])) {
            if (
              this.authService.loggedInUserAccesses[i].subdivision_id === id &&
              this.authService.loggedInUserAccesses[i].get
            ) {
              this.accessLevel = 1;
            }
            if (
              this.authService.loggedInUserAccesses[i].subdivision_id === id &&
              this.authService.loggedInUserAccesses[i].post
            ) {
              this.accessLevel = 2;
            }
          }
        }
      }
    }
    this.subdivisionFormEdit.patchValue({
      name: this.selectedSubdivision.name,
      code: this.selectedSubdivision.code,
      type: this.selectedSubdivision.type,
      aircraftmodelgroup_id: this.selectedSubdivision.aircraftmodelgroup_id
    });
    this.selectedSubdivisionfiles.length = 0;
    if (this.subdivisionfiles !== null) {
      for (let i = 0; i < this.subdivisionfiles.length; i++) {
        if (this.subdivisionfiles[i].subdivision_id === id) {
          this.selectedSubdivisionfiles.push(this.subdivisionfiles[i]);
        }
      }
      this.selectedSubdivisionfiles.sort((a, b) => {
        return a.loaddate > b.loaddate ? 1 : -1;
      });
    }
    this.setPositionTree();
    if (
      !isNullOrUndefined(
        this.getPositionsBySubdivisionId(this.selectedSubdivisionId)[0]
      )
    ) {
      this.positionForm.patchValue({
        parent_id: this.getPositionsBySubdivisionId(
          this.selectedSubdivisionId
        )[0].id
      });
    } else {
      this.positionForm.patchValue({
        parent_id: null
      });
    }
    if (!isNullOrUndefined(this.position)) {
      this.onSelectPosition(this.position.id);
    }
  }

  onSelectPosition(id) {
    this.positionForm.reset();
    this.selectedPositionId = id;
    this.selectedPosition = this.getPositionById(id, [this.position]);
    this.positionFormEdit.patchValue({
      position_id: this.selectedPosition.id,
      parent_id: this.selectedPosition.parent_id,
      pilot_type: isNullOrUndefined(this.selectedPosition.pilot_type)
        ? null
        : this.selectedPosition.pilot_type,
      name: this.selectedPosition.name,
      salary: this.selectedPosition.salary,
      subdivision_id: this.selectedPosition.subdivision_id
    });
    this.positionfileEdit = new FormGroup({
      position_id: new FormControl(null, [Validators.required]),
      revnumber: new FormControl(null, [Validators.required]),
      revdate: new FormControl(null, [Validators.required]),
      positionfile: new FormControl(null)
    });
    this.selectedPositionFiles.length = 0;
    if (this.positionfiles !== null) {
      for (let i = 0; i < this.positionfiles.length; i++) {
        if (this.positionfiles[i].position_id === id) {
          this.selectedPositionFiles.push(this.positionfiles[i]);
        }
      }
      this.selectedPositionFiles.sort((a, b) => {
        return a.loaddate > b.loaddate ? 1 : -1;
      });
    }
  }

  async onAddSubdivision() {
    // добавляем новое подразделение (divisionForm), newDivisionFile - массив форм добавления положений о подразделении
    let dialogRef = this.dialog.open(DialogSubdivisionComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Добавление подразделения в ${this.selectedSubdivision.name}`,
        aircraftmodelgroups: this.aircraftmodelgroups,
        parent_id: this.selectedSubdivision.id
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        this.spinnerService.show();
        try {
          let resPostSubdivision = await this.subdivisionService.postOne(
            result.subdivision
          );
          await this.subdivisionService.putOneById(resPostSubdivision.id, {
            position: resPostSubdivision.id
          });
          if (
            !isNullOrUndefined(result.file['sbdivfile']) &&
            !isNullOrUndefined(result.file['revnumber'])
          ) {
            let file: FormData = new FormData();
            // let date = new Date(result.file['revdate']).toUTCString()
            file.append('subdivision_id', resPostSubdivision.id);
            file.append('sbdivfile', result.file['sbdivfile']);
            file.append('revnumber', result.file['revnumber']);
            file.append(
              'revdate',
              new Date(result.file['revdate']).toDateString()
            );
            let res = await this.subdivisionfileService.postOne(file);
          }
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.getData();
          this.spinnerService.hide();
        }
      }
    });
  }

  async onEditSubdivision() {
    // редактируем существующее подразделение subdivisionFormEdit - фома подразделения, subdivisionfileEdit - форма с новым положение
    let dialogRef = this.dialog.open(DialogSubdivisionComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Редактирование подразделения в ${this.selectedSubdivision.name}`,
        edit: true,
        aircraftmodelgroups: this.aircraftmodelgroups,
        name: this.selectedSubdivision.name,
        parent_id: this.selectedSubdivision.parent_id,
        code: this.selectedSubdivision.code,
        type: this.selectedSubdivision.type,
        aircraftmodelgroup_id: this.selectedSubdivision.aircraftmodelgroup_id
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        this.spinnerService.show();
        try {
          let resPostSubdivision = await this.subdivisionService.putOneById(
            this.selectedSubdivision.id,
            result.subdivision
          );
          await this.subdivisionService.putOneById(resPostSubdivision.id, {
            position: resPostSubdivision.id
          });
          if (
            !isNullOrUndefined(result.file['sbdivfile']) &&
            !isNullOrUndefined(result.file['revnumber'])
          ) {
            let file: FormData = new FormData();
            // let date = new Date(result.file['revdate']).toUTCString()
            file.append('subdivision_id', resPostSubdivision.id);
            file.append('sbdivfile', result.file['sbdivfile']);
            file.append('revnumber', result.file['revnumber']);
            file.append(
              'revdate',
              new Date(result.file['revdate']).toDateString()
            );
            let res = await this.subdivisionfileService.postOne(file);
          }
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.getData();
          this.spinnerService.hide();
        }
      }
    });
    // this.spinnerService.show();
    // try {
    //   let resPutSubdivisionById = await this.subdivisionService.putOneById(
    //     this.selectedSubdivisionId,
    //     this.subdivisionFormEdit.value
    //   );
    //   this.subdivisions.splice(
    //     this.subdivisions.findIndex(i => i.id === resPutSubdivisionById.id),
    //     1
    //   );
    //   this.subdivisions.push(resPutSubdivisionById);
    //   let formFiles: FormData[] = [];
    //   for (let i = 0; i < this.subdivisionfileEdit.length; i++) {
    //     this.subdivisionfileEdit[i].patchValue({
    //       subdivision_id: this.selectedSubdivisionId
    //     });
    //     if (this.subdivisionfileEdit[i].valid) {
    //       let file: FormData = new FormData();
    //       file.append(
    //         'subdivision_id',
    //         this.subdivisionfileEdit[i].value['subdivision_id']
    //       );
    //       file.append(
    //         'sbdivfile',
    //         this.subdivisionfileEdit[i].value['sbdivfile']
    //       );
    //       file.append(
    //         'revnumber',
    //         this.subdivisionfileEdit[i].value['revnumber']
    //       );
    //       file.append('revdate', this.subdivisionfileEdit[i].value['revdate']);
    //       formFiles.push(file);
    //     }
    //   }
    //   await Promise.all(
    //     formFiles.map(async formFile => {
    //       let res = await this.subdivisionfileService.postOne(formFile);
    //       this.subdivisionfiles.push(res);
    //     })
    //   );
    //   this.subdivisionfileEdit.length = 0;
    //   for (let k = 0; k < this.subdivisions.length; k++) {
    //     this.subdivisions[k].countUsers = 0;
    //     this.subdivisions[k].countOwnUsers = 0;
    //     for (let i = 0; i < this.positions.length; i++) {
    //       this.positions[i].countUsers = 0;
    //       for (let j = 0; j < this.airusers.length; j++) {
    //         if (this.positions[i].id === this.airusers[j].position_id) {
    //           this.positions[i].countUsers++;
    //         }
    //       }
    //       if (this.subdivisions[k].id === this.positions[i].subdivision_id) {
    //         this.subdivisions[k].countOwnUsers += this.positions[i].countUsers;
    //       }
    //     }
    //   }
    // } catch (err) {
    //   console.error(
    //     `Ошибка при редактировании подразделения ${JSON.parse(err)}`
    //   );
    //   this.openSnackBar(
    //     `Ошибка при редактировании подразделения. Код ${err.status}.`,
    //     'Ok'
    //   );
    // } finally {
    //   this.setCountUsers([this.subdivision]);
    //   this.setSubdivisionTree(); // Получаем иерархию подразделений
    //   this.selectedSubdivisionId = this.subdivision.id;
    //   this.onSelectSubdivision(this.selectedSubdivisionId);
    //   this.spinnerService.hide();
    // }
  }

  async onDeleteSubdivision(id) {
    let dialogRef = this.dialog.open(DialogYesnoComponent, {
      panelClass: 'app-dialog-base',
      data: {
        title: `Вы действительно хотите удалить подразделение ${
          this.subdivisions.find(item => item.id === id).name
        }?`
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let files = this.subdivisionfiles.filter(
            item => item.subdivision_id === id
          );
          if (!isNullOrUndefined(files)) {
            await Promise.all(
              files.map(item => {
                return this.subdivisionfileService.deleteOneById(item.id);
              })
            );
          }
          await this.subdivisionService.deleteOneById(id);
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.getData();
          this.spinnerService.hide();
        }
      }
    });
  }

  async onAddPosition() {
    let dialogRef = this.dialog.open(DialogPositionComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Добавление должности в ${this.selectedSubdivision.name}`,
        subdivision_id: this.selectedSubdivision.id,
        positions: this.getPositionsBySubdivisionId(this.selectedSubdivision.id)
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        this.spinnerService.show();
        try {
          let resPostPosition = await this.positionService.postOne(
            result.position
          );
          if (
            !isNullOrUndefined(result.file['positionfile']) &&
            !isNullOrUndefined(result.file['revnumber'])
          ) {
            let file: FormData = new FormData();
            file.append('position_id', resPostPosition.id);
            file.append('positionfile', result.file['positionfile']);
            file.append('revnumber', result.file['revnumber']);
            file.append(
              'revdate',
              new Date(result.file['revdate']).toDateString()
            );
            let res = await this.positionfileService.postOne(file);
          }
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.getData();
          this.spinnerService.hide();
        }
      }
    });
  }

  async onEditPosition() {
    let dialogRef = this.dialog.open(DialogPositionComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Редактирование должности в ${this.selectedSubdivision.name}`,
        edit: true,
        positions: this.getPositionsBySubdivisionId(
          this.selectedSubdivision.id
        ).filter(item => item.id !== this.selectedPosition.id),
        position_id: this.selectedPosition.id,
        parent_id: this.selectedPosition.parent_id,
        salary: this.selectedPosition.salary,
        pilot_type: this.selectedPosition.pilot_type,
        name: this.selectedPosition.name,
        subdivision_id: this.selectedPosition.subdivision_id
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        this.spinnerService.show();
        try {
          let resPostPosition = await this.positionService.putOneById(
            this.selectedPosition.id,
            result.position
          );
          /**Для смены начальника */
          if (
            isNullOrUndefined(result.position.parent_id) &&
            !isNullOrUndefined(this.selectedPosition.parent_id)
          ) {
            let _positions = this.getPositionsBySubdivisionId(
              this.selectedPosition.subdivision_id
            );
            for (let i = 0; i < _positions.length; i++) {
              if (isNullOrUndefined(_positions[i].parent_id)) {
                let r = await this.positionService.putOneById(
                  _positions[i].id,
                  {
                    parent_id: this.selectedPositionId
                  }
                );
                this.positions.splice(
                  this.positions.findIndex(i => i.id === r.id),
                  1
                );
                this.positions.push(r);
                break;
              }
            }
          }
          if (
            !isNullOrUndefined(result.file['positionfile']) &&
            !isNullOrUndefined(result.file['revnumber'])
          ) {
            let file: FormData = new FormData();
            file.append('position_id', resPostPosition.id);
            file.append('positionfile', result.file['positionfile']);
            file.append('revnumber', result.file['revnumber']);
            file.append(
              'revdate',
              new Date(result.file['revdate']).toDateString()
            );
            let res = await this.positionfileService.postOne(file);
          }
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.getData();
          this.spinnerService.hide();
        }
      }
    });
    // this.spinnerService.show();
    // try {
    //   let resPutPositionById = await this.positionService.putOneById(
    //     this.selectedPositionId,
    //     this.positionFormEdit.value
    //   );
    //   this.positions.splice(
    //     this.positions.findIndex(i => i.id === resPutPositionById.id),
    //     1
    //   );
    //   this.positions.push(resPutPositionById);
    //   if (this.showNewPositionFileEdit) {
    //     this.positionfileEdit.patchValue({
    //       position_id: this.selectedPositionId
    //     });
    //   }
    //   /**Для смены начальника */
    //   if (
    //     isNullOrUndefined(this.positionFormEdit.value.parent_id) &&
    //     !isNullOrUndefined(this.selectedPosition.parent_id)
    //   ) {
    //     let _positions = this.getPositionsBySubdivisionId(
    //       this.selectedPosition.subdivision_id
    //     );
    //     for (let i = 0; i < _positions.length; i++) {
    //       if (isNullOrUndefined(_positions[i].parent_id)) {
    //         let r = await this.positionService.putOneById(_positions[i].id, {
    //           parent_id: this.selectedPositionId
    //         });
    //         this.positions.splice(
    //           this.positions.findIndex(i => i.id === r.id),
    //           1
    //         );
    //         this.positions.push(r);
    //         break;
    //       }
    //     }
    //   }
    //   if (this.positionfileEdit.valid) {
    //     let formFile: FormData = new FormData();
    //     formFile.append(
    //       'position_id',
    //       this.positionfileEdit.value['position_id']
    //     );
    //     formFile.append(
    //       'positionfile',
    //       this.positionfileEdit.value['positionfile']
    //     );
    //     formFile.append('revnumber', this.positionfileEdit.value['revnumber']);
    //     formFile.append('revdate', this.positionfileEdit.value['revdate']);
    //     let resPostPositionfile = await this.positionfileService.postOne(
    //       formFile
    //     );
    //     this.subdivisionfiles.push(resPostPositionfile);
    //   }
    // } catch (err) {
    //   console.error(`Ошибка при редактировании должности ${JSON.parse(err)}`);
    //   this.openSnackBar(
    //     `Ошибка при редактировании должности. Код ${err.status}.`,
    //     'Ok'
    //   );
    // } finally {
    //   this.newPositionfile.reset();
    //   this.showNewPositionFileEdit = false;
    //   this.setPositionTree();
    //   this.onSelectPosition(this.selectedPositionId);
    //   this.spinnerService.hide();
    // }
  }

  async onDeletePosition(id) {
    if (id === -1) {
      this.globalsettingsService.openSnackBar(
        `Перед удалением должности руководителя - удалите должности подчиненных`,
        'Ok'
      );
      return;
    }
    let dialogRef = this.dialog.open(DialogYesnoComponent, {
      panelClass: 'app-dialog-overview-yes-no-component',
      data: {
        title: `Вы действительно хотите удалить должность ${
          this.positions.find(item => item.id === id).name
        }?`
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === true) {
        this.spinnerService.show();
        try {
          let files = this.positionfiles.filter(
            item => item.position_id === id
          );
          if (!isNullOrUndefined(files)) {
            await Promise.all(
              files.map(item => {
                return this.positionfileService.deleteOneById(item.id);
              })
            );
          }
          await this.positionService.deleteOneById(id);
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.getData();
          this.spinnerService.hide();
        }
      }
    });
  }

  async onDownloadSubdivisionfile(id) {
    let fileName = '';
    for (let i = 0; i < this.selectedSubdivisionfiles.length; i++) {
      if (id === this.selectedSubdivisionfiles[i].id) {
        fileName = this.selectedSubdivisionfiles[i].sbdivfile.filename;
      }
    }
    let file = await this.subdivisionfileService.getFileById(id);
    importedSaveAs(file, fileName);
  }

  async onDownloadPositionfile(id) {
    let fileName = '';
    for (let i = 0; i < this.selectedPositionFiles.length; i++) {
      if (id === this.selectedPositionFiles[i].id) {
        fileName = this.selectedPositionFiles[i].positionfile.filename;
      }
    }
    let file = await this.positionfileService.getFileById(id);
    importedSaveAs(file, fileName);
  }

  onFilesChange(event, index) {
    this.newSubdivisionfile[index].patchValue({
      sbdivfile: event.srcElement.files[0]
    });
  }

  onFilesChangeEdit(event, index) {
    this.subdivisionfileEdit[index].patchValue({
      sbdivfile: event.srcElement.files[0]
    });
  }

  onFilesPositionChange(event) {
    this.newPositionfile.patchValue({
      positionfile: event.srcElement.files[0]
    });
  }

  onFilesPositionChangeEdit(event) {
    this.positionfileEdit.patchValue({
      positionfile: event.srcElement.files[0]
    });
  }

  onAddNewDivisionFile() {
    this.newSubdivisionfile.push(
      new FormGroup({
        subdivision_id: new FormControl(null),
        revnumber: new FormControl(null, [Validators.required]),
        revdate: new FormControl(null, [Validators.required]),
        sbdivfile: new FormControl(null)
      })
    );
  }

  onDelNewDivisionFile() {
    this.newSubdivisionfile.pop();
  }

  onAddNewDivisionFileEdit() {
    this.subdivisionfileEdit.push(
      new FormGroup({
        subdivision_id: new FormControl(null),
        revnumber: new FormControl(null, [Validators.required]),
        revdate: new FormControl(null, [Validators.required]),
        sbdivfile: new FormControl(null)
      })
    );
  }

  onDelNewDivisionFileEdit() {
    this.subdivisionfileEdit.pop();
  }

  onCancel() {
    this.subdivisionForm.reset();
    this.newSubdivisionfile.length = 0;
    this.newSubdivisionfile.push(
      new FormGroup({
        subdivision_id: new FormControl(null),
        revnumber: new FormControl(null, [Validators.required]),
        revdate: new FormControl(null, [Validators.required]),
        sbdivfile: new FormControl(null)
      })
    );
  }

  onCancelPosition() {
    this.positionForm.reset();
  }

  gotoLog() {
    this.router.navigate([`/sys/organization/logs`], {
      queryParams: {
        type: 'subdiv',
        id: this.selectedSubdivisionId
      }
    });
  }

  async toggleList(event) {
    try {
      let resfrom = this.subdivisionService.putOneById(event.from, {
        position: event.to
      });
      let resto = this.subdivisionService.putOneById(event.to, {
        position: event.from
      });
      await resfrom;
      await resto;
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
    }
  }
}
