import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-certificates',
  templateUrl: './dialog-certificates.component.html',
  styleUrls: ['./dialog-certificates.component.scss']
})
export class DialogCertificatesComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DialogCertificatesComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {}

  onClose(airuser_id, certificatetype_id) {
    this.dialogRef.close({
      airuser_id,
      certificatetype_id
    });
  }
}
