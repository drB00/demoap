import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-position',
  templateUrl: './dialog-position.component.html',
  styleUrls: ['./dialog-position.component.scss']
})
export class DialogPositionComponent implements OnInit {
  positionForm: FormGroup;
  positionfileForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogPositionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.positionForm = new FormGroup({
      position_id: new FormControl(null),
      parent_id: new FormControl(null),
      salary: new FormControl(null),
      pilot_type: new FormControl(null),
      name: new FormControl(null, [Validators.required]),
      subdivision_id: new FormControl(this.data.subdivision_id, [
        Validators.required
      ])
    });
    this.positionfileForm = new FormGroup({
      position_id: new FormControl(null, [Validators.required]),
      revnumber: new FormControl(null, [Validators.required]),
      revdate: new FormControl(null, [Validators.required]),
      positionfile: new FormControl(null)
    });
    if ((this.data.edit = true)) {
      this.positionForm.patchValue({
        position_id: this.data.position_id,
        parent_id: this.data.parent_id,
        salary: this.data.salary,
        pilot_type: this.data.pilot_type,
        name: this.data.name,
        subdivision_id: this.data.subdivision_id
      });
    }
  }

  onFilesChange(event) {
    this.positionfileForm.patchValue({
      positionfile: event.srcElement.files[0]
    });
  }

  onSave() {
    this.dialogRef.close({
      position: this.positionForm.value,
      file: this.positionfileForm.value
    });
  }
}
