import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-subdivision',
  templateUrl: './dialog-subdivision.component.html',
  styleUrls: ['./dialog-subdivision.component.scss']
})
export class DialogSubdivisionComponent implements OnInit {
  subdivisionForm: FormGroup;
  subdivisionfileForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogSubdivisionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.subdivisionForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      parent_id: new FormControl(this.data.parent_id),
      code: new FormControl(null),
      type: new FormControl(0),
      aircraftmodelgroup_id: new FormControl(null)
    });
    this.subdivisionfileForm = new FormGroup({
      subdivision_id: new FormControl(null),
      revnumber: new FormControl(null, [Validators.required]),
      revdate: new FormControl(null, [Validators.required]),
      sbdivfile: new FormControl(null)
    });
    if ((this.data.edit = true)) {
      this.subdivisionForm.patchValue({
        name: this.data.name,
        parent_id: this.data.parent_id,
        code: this.data.code,
        type: this.data.type,
        aircraftmodelgroup_id: this.data.aircraftmodelgroup_id
      });
    }
  }

  onFilesChange(event) {
    this.subdivisionfileForm.patchValue({
      sbdivfile: event.srcElement.files[0]
    });
  }

  onSave() {
    this.dialogRef.close({
      subdivision: this.subdivisionForm.value,
      file: this.subdivisionfileForm.value
    });
  }
}
