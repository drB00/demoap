import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-dialog-dictionary',
  templateUrl: './dialog-dictionary.component.html',
  styleUrls: ['./dialog-dictionary.component.scss']
})
export class DialogDictionaryComponent implements OnInit {
  dictForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogDictionaryComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.dictForm = new FormGroup({
      name: new FormControl(null)
    });
    if ((this.data.edit === true)) {
      this.dictForm.patchValue({
        name: this.data.name
      });
    }
  }

  onSave() {
    this.dialogRef.close(this.dictForm.value);
  }
}
