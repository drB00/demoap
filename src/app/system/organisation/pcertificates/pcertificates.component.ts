import { Component, OnInit } from '@angular/core';
import { Airusercertificate } from 'src/app/shared/models/airusercertificate.model';
import { AircraftmodelgroupService } from '../../shared/services/aircraftmodelgroup.service';
import { AirusercertificatefileService } from '../../shared/services/airusercertificatefile.service';
import { AirusercertificateService } from '../../shared/services/airusercertificate.service';
import { CertificatetypeService } from '../../shared/services/certificatetype.service';
import { isNullOrUndefined } from 'util';
import { OrganisationstoreService } from '../../shared/services/organisationstore.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Subdivision } from 'src/app/shared/models/subdivision.model';
import { Position } from 'src/app/shared/models/position.model';
import { Airuser } from 'src/app/shared/models/airuser.model';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar, MatDialog } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';
import { PositionService } from '../../shared/services/position.service';
import { SubdivisionService } from '../../shared/services/subdivision.service';
import { startWith, map } from 'rxjs/operators';
import { AiruserService } from '../../shared/services/airuser.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { DialogCertificatesComponent } from '../dialogs/dialog-certificates/dialog-certificates.component';

@Component({
  selector: 'app-pcertificates',
  templateUrl: './pcertificates.component.html',
  styleUrls: ['./pcertificates.component.scss']
})
export class PcertificatesComponent implements OnInit {
  certificatetypes = [];
  airusercertificatefiles = [];
  airusercertificates: Airusercertificate[] = [];
  aircraftmodelgroups = [];

  problemPanelIsOpen = true;
  accessToPost: boolean;

  userCtrl: FormControl;
  filteredUsers: Observable<any[]>;

  selectedIndex = 0; // индекс вкладки для выбора
  viewId = 1;

  subdivisions: Subdivision[] = [];
  subdivisionsToPost: Subdivision[] = [];
  positions: Position[] = [];
  airusers: Airuser[] = [];
  airusersOfSubdivision: Airuser[] = [];
  _certificatetypes = [];
  allAirusercertificates = [];
  allAirusercertificatesProblems = [];
  airusersWithAirusercertificatesProblems = [];
  airusercertificatesByAitcraftmodel = []; // выборка сертификатов по модели самолета
  airusercertificatesForTab = [];

  certificateForm: FormGroup;
  certificateFiles: FormGroup[] = [];

  selectedAiruser: Airuser;
  selectedAircraftmodelgroup;
  selectedCertificatetypeName = 'ID-карта';
  selectedCertificatetypeId = 1;
  selectAircraftmodelgroupId;

  selectedId: number | string;

  constructor(
    private authService: AuthService,
    private airuserService: AiruserService,
    private aircraftmodelgroupService: AircraftmodelgroupService,
    private airusercertificatefileService: AirusercertificatefileService,
    private airusercertificateService: AirusercertificateService,
    private certificatetypeService: CertificatetypeService,
    private organisationstoreService: OrganisationstoreService,
    private positionService: PositionService,
    private subdivisionService: SubdivisionService,
    private route: ActivatedRoute,
    private spinnerService: NgxSpinnerService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.selectedId = this.route.snapshot.params.id;
  }

  async ngOnInit() {
    this.spinnerService.show();
    this.userCtrl = new FormControl();
    try {
      let aircraftmodelgroups = this.aircraftmodelgroupService.getAll();
      let airusercertificates = this.airusercertificateService.getAll();
      let certificatetypes = await this.certificatetypeService.getAll();
      this.aircraftmodelgroups = isNullOrUndefined(await aircraftmodelgroups)
        ? []
        : await aircraftmodelgroups;
      this.airusercertificates = isNullOrUndefined(await airusercertificates)
        ? []
        : await airusercertificates;
      this.certificatetypes = isNullOrUndefined(await certificatetypes)
        ? []
        : await certificatetypes;
      this.airusercertificates.sort((a, b) => {
        return a.id < b.id ? 1 : -1;
      });
    } catch (err) {
      console.error(`Ошибка ${err}`);
    }
    await this.getData();
  }

  async getData() {
    this.spinnerService.show();
    try {
      let airusercertificatefiles = this.airusercertificatefileService.getAll();
      let positions = this.positionService.getAll();
      let allAirusercertificates = this.airusercertificateService.getAll();
      let airusers = this.airuserService.getAll();
      let subdivisions = await this.subdivisionService.getAll();
      this.airusercertificatefiles = isNullOrUndefined(
        await airusercertificatefiles
      )
        ? []
        : await airusercertificatefiles;
      this.positions = isNullOrUndefined(await positions)
        ? []
        : await positions;

      this._certificatetypes.length = 0;
      if (!isNullOrUndefined(this.certificatetypes)) {
        this.certificatetypes.map(item => {
          if (item.type === this.viewId && item.active === true) {
            // сертификаты пилота или БП
            this._certificatetypes.push(item);
          }
        });
      }
      this.selectedCertificatetypeName = this._certificatetypes[0].name;
      this.subdivisions.length = 0;
      /**Список подразделений на которые есть права */
      this.subdivisionsToPost = [];
      let accesses = this.authService.loggedInUserAccesses.filter(
        loggedInUserAccesse =>
          loggedInUserAccesse.resourcename === 'airusercertificate'
      );
      if (!isNullOrUndefined(subdivisions)) {
        subdivisions.map(item => {
          if (item.type === this.viewId) {
            this.subdivisions.push(item);
            if (this.authService.loggedInUser.isadmin) {
              this.subdivisionsToPost.push(item);
            } else {
              accesses.map(accesse => {
                if (accesse.subdivision_id === item.id && accesse.post) {
                  this.subdivisionsToPost.push(item);
                }
              });
            }
          }
        });
      }
      this.subdivisions.sort((a, b) => {
        return a.id > b.id ? 1 : -1;
      });
      /**Выбираем сотрудников, которые относятся к подразделениям */
      this.airusers.length = 0;
      (await airusers).map(airuser => {
        if (this.isAiruserInSubdivisions(airuser)) {
          let pos = this.positions.find(
            position => position.id === airuser.position_id
          );
          if (!isNullOrUndefined(pos)) {
            airuser.position = pos.name;
          }
          if (
            this.positions.find(position => position.id === airuser.position_id)
              .pilot_type !== 0
          ) {
            this.airusers.push(airuser);
          }
        }
      });
      this.allAirusercertificates = isNullOrUndefined(
        await allAirusercertificates
      )
        ? []
        : await allAirusercertificates;
    } catch (err) {
      console.error(`Ошибка ${err}`);
    } finally {
      this.spinnerService.hide();
    }
    this.allAirusercertificatesProblems.length = 0;
    this.airusersWithAirusercertificatesProblems.length = 0;
    this.airusersOfSubdivision = this.airusers.slice();
    /**Заполнение списка проблемных сертификатов */
    let airusersWithAirusercertificatesProblems = [];
    this.airusers.map(airuser => {
      let certs = this.allAirusercertificates.filter(
        item => item.airuser_id === airuser.id
      );
      certs.sort((a, b) => {
        return a.id < b.id ? 1 : -1;
      });
      let _used = {};
      let _certs = certs.filter(obj => {
        return obj.certificatetype_id in _used
          ? 0
          : (_used[obj.certificatetype_id] = 1);
      });
      for (let i = 0; i < this._certificatetypes.length; i++) {
        let res = _certs.find(
          item => item.certificatetype_id === this._certificatetypes[i].id
        );
        if (!isNullOrUndefined(res)) {
          if (
            this.organisationstoreService.hasAirusercertificateProbles(
              res,
              this.certificatetypes,
              1
            )
          ) {
            this.allAirusercertificatesProblems.push(res);
            airusersWithAirusercertificatesProblems.push(airuser);
          }
        } else {
          let emptyRes = {
            airuser_id: airuser.id,
            certificatetype_id: this._certificatetypes[i].id
          };
          this.allAirusercertificatesProblems.push(emptyRes);
          airusersWithAirusercertificatesProblems.push(airuser);
        }
      }
    });
    let used = {};
    this.airusersWithAirusercertificatesProblems = airusersWithAirusercertificatesProblems.filter(
      obj => {
        return obj.id in used ? 0 : (used[obj.id] = 1);
      }
    );
    for (
      let i = 0;
      i < this.airusersWithAirusercertificatesProblems.length;
      i++
    ) {
      this.airusersWithAirusercertificatesProblems[i].problemCertificates = [];
      this.airusersWithAirusercertificatesProblems[
        i
      ].problemCertificates = this.allAirusercertificatesProblems.filter(
        item =>
          item.airuser_id === this.airusersWithAirusercertificatesProblems[i].id
      );
      this.airusersWithAirusercertificatesProblems[i].problemCertificates.map(
        item => {
          item['certificatetype'] = this.getCertificatetypeById(
            item.certificatetype_id
          ).name;
          item['airuserName'] = this.getAiruserNameById(item.airuser_id);
          item['status'] = this.isAirusercertificateActive(item);
        }
      );
      this.airusersWithAirusercertificatesProblems[i].problemCertificates.sort(
        (a, b) => {
          return a.certificatetype_id > b.certificatetype_id ? 1 : -1;
        }
      );
      this.airusersWithAirusercertificatesProblems.sort(
        (a, b) => a.position_id - b.position_id
      );
    }
    this.filteredUsers = this.userCtrl.valueChanges.pipe(
      startWith(''),
      map(user => {
        return user ? this.filterUsers(user) : this.airusersOfSubdivision;
      })
    );
    if (this.selectedId !== 'all') {
      let id = +this.selectedId;
      let user = this.airusersOfSubdivision.find(item => item.id === id);
      this.userCtrl.setValue(`${user.name} ${user.surname}`);
      this.selectAiruser(id);
    }
  }

  async selectSubdivision(id) {
    this.userCtrl = new FormControl();
    delete this.selectedAiruser;
    this.airusersOfSubdivision.length = 0;
    await this.airusers.map(item => {
      let sbdv = this.getSubdivisionByPositionId(item.position_id);
      if (!isNullOrUndefined(sbdv)) {
        if (sbdv.id === +id) {
          this.airusersOfSubdivision.push(item);
        }
      }
    });
    /**автокомплит */
    this.filteredUsers = this.userCtrl.valueChanges.pipe(
      startWith(''),
      map(user => {
        return user ? this.filterUsers(user) : this.airusersOfSubdivision;
      })
    );
  }

  async selectAiruser(id) {
    if (id === -1) {
      this.selectedAiruser = null;
      this.accessToPost = false;
      this.airusercertificates.length = 0;
      return;
    }
    this.spinnerService.show();
    try {
      this.selectedAiruser = this.airusersOfSubdivision.find(
        item => item.id === id
      );
      /**Наличие прав на редактирование */
      let sbdv = this.subdivisionsToPost.find(
        item =>
          item.id ===
          this.getSubdivisionByPositionId(this.selectedAiruser.position_id).id
      );

      if (isNullOrUndefined(sbdv)) {
        this.accessToPost = false;
      } else {
        this.accessToPost = true;
      }
      if (
        !isNullOrUndefined(
          this.getSubdivisionByPositionId(this.selectedAiruser.position_id)
            .aircraftmodelgroup_id
        )
      ) {
        this.selectAircraftmodelgroup(
          this.getSubdivisionByPositionId(this.selectedAiruser.position_id)
            .aircraftmodelgroup_id
        );
      }
      this.airusercertificates.length = 0;
      this.selectedCertificatetypeId = this.getCertificatetypeIdByName(
        this.selectedCertificatetypeName
      );
      this.prepareTabContent();
      let airusercertificates = await this.airusercertificateService.getByAiruser(
        this.selectedAiruser.id
      );
      this.airusercertificates = isNullOrUndefined(airusercertificates)
        ? []
        : airusercertificates;
      this.airusercertificates.sort((a, b) => {
        return a.id < b.id ? 1 : -1;
      });
      if (!isNullOrUndefined(this.selectedAircraftmodelgroup)) {
        this.airusercertificatesByAitcraftmodel = this.airusercertificates.filter(
          item =>
            item.aircraftmodelgroup_id === this.selectedAircraftmodelgroup.id
        );
      }
    } catch (err) {
      console.error(`Ошибка ${err}`);
    } finally {
      this.spinnerService.hide();
    }
  }

  selectAircraftmodelgroup(id) {
    this.selectAircraftmodelgroupId = +id;
    this.selectedAircraftmodelgroup = this.aircraftmodelgroups.find(
      item => item.id === +id
    );
    if (isNullOrUndefined(this.selectedAircraftmodelgroup)) {
      return;
    }
    this.airusercertificatesByAitcraftmodel = this.airusercertificates.filter(
      item => item.aircraftmodelgroup_id === this.selectedAircraftmodelgroup.id
    );
  }

  getCertificatetypeIdByName(name) {
    return this.certificatetypes.find(item => {
      return item.name === name && item.type === this.viewId ? true : false;
    }).id;
  }

  isAiruserInSubdivisions(user: Airuser) {
    let res = false;
    this.subdivisions.map(subdivision => {
      if (
        !isNullOrUndefined(this.getSubdivisionByPositionId(user.position_id)) &&
        this.getSubdivisionByPositionId(user.position_id).id === subdivision.id
      ) {
        res = true;
      }
    });
    return res;
  }

  getSubdivisionByPositionId(id) {
    if (
      isNullOrUndefined(this.positions) ||
      isNullOrUndefined(this.subdivisions)
    ) {
      return;
    }
    for (let i = 0; i < this.positions.length; i++) {
      if (this.positions[i].id === id) {
        for (let j = 0; j < this.subdivisions.length; j++) {
          if (this.subdivisions[j].id === this.positions[i].subdivision_id) {
            return this.subdivisions[j];
          }
        }
      }
    }
  }

  prepareTabContent() {
    let certificatetypeId = this.getCertificatetypeIdByName(
      this.selectedCertificatetypeName
    );
    for (let i = 0; i < this.airusercertificates.length; i++) {
      if (
        this.airusercertificates[i].certificatetype_id === certificatetypeId
      ) {
        this.airusercertificatesForTab.push(this.airusercertificates[i]);
      }
    }
    delete this.certificateForm;
    switch (this.selectedCertificatetypeName) {
      case 'ID-карта':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text1: new FormControl(null, [Validators.required]), // номер сертификата, место проверки
          _file: new FormControl(null)
        });
        break;
      case 'АСП-вода':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text2: new FormControl(null, [Validators.required]), // место проверки
          _file: new FormControl(null),
          months: new FormControl(24)
        });
        break;
      case 'RNAV':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text1: new FormControl(null, [Validators.required]), // номер сертификата, место проверки
          text2: new FormControl(null, [Validators.required]), // место проверки
          _file: new FormControl(null)
        });
        break;
      case 'Белорусская валидация':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text1: new FormControl(null), // номер сертификата, место проверки
          text2: new FormControl(null, [Validators.required]), // место проверки
          _file: new FormControl(null)
        });
        break;
      case 'Свидетельство пилота':
      case 'Допуск к международным полетам':
      case 'Свидетельство бортпроводника':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          datebegin: new FormControl(null, [Validators.required]),
          text1: new FormControl(null, [Validators.required]) // номер сертификата
        });
        break;
      case 'Заграничный паспорт':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text1: new FormControl(null, [Validators.required]), // номер сертификата
          text2: new FormControl(null, [Validators.required]), // организация, которая выдала
          _file: new FormControl(null)
        });
        break;
      case 'Переподготовка на тип ВС':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          aircraftmodelgroup_id: new FormControl(
            isNullOrUndefined(this.selectedAircraftmodelgroup)
              ? null
              : this.selectedAircraftmodelgroup.id,
            [Validators.required]
          ),
          datebegin: new FormControl(null, [Validators.required]),
          text1: new FormControl(null, [Validators.required]), // номер сертификата
          text2: new FormControl(null, [Validators.required]), // организация, которая выдала
          _file: new FormControl(null)
        });
        break;
      case 'КПК по типу':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          aircraftmodelgroup_id: new FormControl(
            isNullOrUndefined(this.selectedAircraftmodelgroup)
              ? null
              : this.selectedAircraftmodelgroup.id,
            [Validators.required]
          ),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text1: new FormControl(null, [Validators.required]), // номер сертификата
          text2: new FormControl(null, [Validators.required]), // организация, которая выдала
          _file: new FormControl(null),
          months: new FormControl(12)
        });
        break;
      case 'Справка ВЛЭК':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text1: new FormControl(null, [Validators.required]), // номер сертификата
          text2: new FormControl(null, [Validators.required]), // организация, которая выдала
          date1: new FormControl(null, [Validators.required]),
          _file: new FormControl(null),
          months: new FormControl(12)
        });
        break;
      case 'Опасные грузы':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text1: new FormControl(null, [Validators.required]), // номер сертификата
          text2: new FormControl(null, [Validators.required]), // организация, которая выдала
          _file: new FormControl(null),
          months: new FormControl(24)
        });
        break;
      case 'CRM':
      case 'Авиационная безопасность':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text1: new FormControl(null, [Validators.required]), // номер сертификата
          text2: new FormControl(null, [Validators.required]), // организация, которая выдала
          _file: new FormControl(null),
          months: new FormControl(36)
        });
        break;
      case 'RVSM':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          aircraftmodelgroup_id: new FormControl(
            isNullOrUndefined(this.selectedAircraftmodelgroup)
              ? null
              : this.selectedAircraftmodelgroup.id,
            [Validators.required]
          ),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null),
          text1: new FormControl(null, [Validators.required]), // номер сертификата
          text2: new FormControl(null, [Validators.required]), // организация, которая выдала
          _file: new FormControl(null),
          months: new FormControl(24)
        });
        break;
      case 'Уровень владения английским языком':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null),
          text1: new FormControl(null, [Validators.required]), // номер сертификата
          text2: new FormControl(null, [Validators.required]), // организация, которая выдала
          number1: new FormControl(4, [Validators.required]), // значение
          _file: new FormControl(null)
        });
        break;
      case 'Тренажер':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          aircraftmodelgroup_id: new FormControl(
            isNullOrUndefined(this.selectedAircraftmodelgroup)
              ? null
              : this.selectedAircraftmodelgroup.id,
            [Validators.required]
          ),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text2: new FormControl(null, [Validators.required]), // организация, которая выдала
          text3: new FormControl(null, [Validators.required]), // ФИО проверяющего
          _file: new FormControl(null),
          months: new FormControl(8)
        });
        break;
      case 'Проверка техники пилотирования':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          aircraftmodelgroup_id: new FormControl(
            isNullOrUndefined(this.selectedAircraftmodelgroup)
              ? null
              : this.selectedAircraftmodelgroup.id,
            [Validators.required]
          ),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text3: new FormControl(null, [Validators.required]), // ФИО проверяющего
          _file: new FormControl(null),
          months: new FormControl(6)
        });
        break;
      case 'NAT-HLA':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          aircraftmodelgroup_id: new FormControl(
            isNullOrUndefined(this.selectedAircraftmodelgroup)
              ? null
              : this.selectedAircraftmodelgroup.id,
            [Validators.required]
          ),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text3: new FormControl(null, [Validators.required]), // ФИО проверяющего
          _file: new FormControl(null),
          months: new FormControl(12)
        });
        break;
      case 'АСП-суша':
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          aircraftmodelgroup_id: new FormControl(
            isNullOrUndefined(this.selectedAircraftmodelgroup)
              ? null
              : this.selectedAircraftmodelgroup.id,
            [Validators.required]
          ),
          datebegin: new FormControl(null, [Validators.required]),
          dateend: new FormControl(null, [Validators.required]),
          text1: new FormControl(null, [Validators.required]), // номер сертификата
          text2: new FormControl(null, [Validators.required]), // организация, которая выдала
          _file: new FormControl(null),
          months: new FormControl(12)
        });
        break;
      default:
        this.certificateForm = new FormGroup({
          certificatetype_id: new FormControl(certificatetypeId, [
            Validators.required
          ]),
          airuser_id: new FormControl(this.selectedAiruser.id, [
            Validators.required
          ]),
          aircraftmodelgroup_id: new FormControl(null),
          datebegin: new FormControl(null),
          dateend: new FormControl(null),
          text1: new FormControl(null),
          text2: new FormControl(null),
          text3: new FormControl(null),
          number1: new FormControl(null),
          number2: new FormControl(null),
          _file: new FormControl(null)
        });
        break;
    }
  }

  setDateendEng() {
    if (+this.certificateForm.value['number1'] === 6) {
      this.certificateForm.patchValue({
        dateend: null
      });
    } else if (+this.certificateForm.value['number1'] === 5) {
      let dateend = new Date(this.certificateForm.value['datebegin']);
      dateend.setMonth(dateend.getMonth() + 5 * 12);
      this.certificateForm.patchValue({
        dateend: dateend
      });
    } else if (+this.certificateForm.value['number1'] === 4) {
      let dateend = new Date(this.certificateForm.value['datebegin']);
      dateend.setMonth(dateend.getMonth() + 3 * 12);
      this.certificateForm.patchValue({
        dateend: dateend
      });
    }
  }

  setDateend(months) {
    let dateend = new Date(this.certificateForm.value['datebegin']);
    dateend.setMonth(dateend.getMonth() + months);
    this.certificateForm.patchValue({
      dateend: dateend
    });
  }

  getCertificatetypeById(id) {
    return this._certificatetypes.find(item => item.id === id);
  }

  getAiruserNameById(id) {
    let res = this.airusers.find(airuser => airuser.id === id);
    return isNullOrUndefined(res) ? {} : `${res.surname} ${res.name}`;
  }

  isAirusercertificateActive(airusercertificate: Airusercertificate) {
    let today = new Date();
    if (isNullOrUndefined(airusercertificate.id)) {
      return -1;
    }
    let active = -1;
    switch (
      this.certificatetypes.find(
        item => item.id === airusercertificate.certificatetype_id
      ).name
    ) {
      /**Бессрочные сертификаты */
      case 'Свидетельство пилота':
      case 'Свидетельство бортпроводника':
      case 'Допуск к международным полетам':
      case 'Переподготовка на тип ВС':
      case 'RVSM':
      case 'RNAV':
        active = 1;
        break;
      /**Особые правила */
      case 'Уровень владения английским языком':
        if (isNullOrUndefined(airusercertificate.dateend)) {
          active = 1;
        } else {
          if (
            today.getTime() < new Date(airusercertificate.dateend).getTime()
          ) {
            active = 1;
            if (this.hasAirusercertificateProbles(airusercertificate, 1)) {
              active = 0;
            }
          }
        }
        break;
      /**Сертификаты со сроком действия */
      default:
        if (isNullOrUndefined(airusercertificate.date1)) {
          if (
            today.getTime() < new Date(airusercertificate.dateend).getTime()
          ) {
            active = 1;
            if (this.hasAirusercertificateProbles(airusercertificate, 1)) {
              active = 0;
            }
          }
        } else {
          if (today.getTime() < new Date(airusercertificate.date1).getTime()) {
            active = 1;
            if (this.hasAirusercertificateProbles(airusercertificate, 1)) {
              active = 0;
            }
          }
        }
        break;
    }
    return active;
  }

  hasAirusercertificateProbles(
    airusercertificate: Airusercertificate,
    months = 1
  ) {
    // предпреждение об истечении срока
    let today = new Date();
    today.setMonth(today.getMonth() + months);
    today.setDate(1);
    today.setMonth(today.getMonth() + months);
    let problems = false;
    switch (
      this.certificatetypes.find(
        item => item.id === airusercertificate.certificatetype_id
      ).name
    ) {
      /**Бессрочные сертификаты */
      case 'Свидетельство пилота':
      case 'Свидетельство бортпроводника':
      case 'Допуск к международным полетам':
      case 'Переподготовка на тип ВС':
      case 'RVSM':
      case 'RNAV':
        problems = false;
        break;
      /**Особые правила */
      case 'Уровень владения английским языком':
        if (isNullOrUndefined(airusercertificate.dateend)) {
          problems = false;
        } else {
          if (
            today.getTime() > new Date(airusercertificate.dateend).getTime()
          ) {
            problems = true;
          }
        }
        break;
      /**Сертификаты со сроком действия */
      default:
        if (isNullOrUndefined(airusercertificate.date1)) {
          if (
            today.getTime() > new Date(airusercertificate.dateend).getTime()
          ) {
            problems = true;
          }
        } else {
          if (today.getTime() > new Date(airusercertificate.date1).getTime()) {
            problems = true;
          }
        }
        break;
    }
    return problems;
  }

  filterUsers(str: string) {
    return this.airusers.filter(
      state =>
        state.name.toLowerCase().indexOf(str.toLowerCase()) === 0 ||
        state.surname.toLowerCase().indexOf(str.toLowerCase()) === 0
    );
  }

  setCertificate(problemCertificates) {
    let dialogRef = this.dialog.open(DialogCertificatesComponent, {
      panelClass: 'w-100',
      data: {
        problemCertificates
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
      }
    });
  }
}
