import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AiruserService } from '../../shared/services/airuser.service';
import { PositionService } from '../../shared/services/position.service';
import { SubdivisionService } from '../../shared/services/subdivision.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Airuser } from 'src/app/shared/models/airuser.model';
import { Subdivision } from 'src/app/shared/models/subdivision.model';
import { Position } from 'src/app/shared/models/position.model';
import { OrganisationstoreService } from '../../shared/services/organisationstore.service';
import { isNullOrUndefined } from 'util';
import { AuthService } from 'src/app/shared/services/auth.service';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { AccessrightService } from '../../shared/services/accessright.service';
import { startWith, map } from 'rxjs/operators';
import { GlobalsettingsService } from '../../shared/services/globalsettings.service';
import { AccessstoreService } from '../../shared/services/accessstore.service';

@Component({
  selector: 'app-access',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.scss']
})
export class AccessComponent implements OnInit {
  airusers: Airuser[];
  airusersView: Airuser[];
  positions: Position[];
  subdivisions: Subdivision[];
  subdivisionsView: Subdivision[];

  subdivision: Subdivision; // Объект со всей иерархией подразделений

  selectedId = -1;
  selectedSubdivId = -1;
  selectedUserAccess = [];

  /**Записи на get и edit(post, put, delete) */
  getHr = false;
  editHr = false;
  editDoc = false;
  getTt = true;
  editTt = false;
  getPi = false;
  editPi = false;

  /**Активность контролов в зависимости от прав текущего пользователя */
  editDocDisabled = true;
  editHrDisabled = true;
  getTtDisabled = true;
  editTtDisabled = true;
  getPiDisabled = true;
  editPiDisabled = true;
  getHrDisabled = true;
  getCertificate = false;
  editCertificate = false;
  editCertificateDisabled = true;
  getCertificateDisabled = true;

  /**Флаг доступа выбранного пользователя к модулю */
  hasAccessToHr = false;
  hasAccessToCertificates = false;
  hasAccessToDoc = false;
  hasAccessToTt = false;
  hasAccessToPi = false;

  /**Id записи доступа к таблице выбранного пользователя */
  accessAiruserId = 0;
  accessPositionId = 0;
  accessAiruserphoneId = 0;
  accessEducationId = 0;
  accessPreviousworkId = 0;
  accessFamilymemberworkId = 0;
  accessAiruseremailId = 0;
  accessSubdivisionId = 0;
  accessAiruserfileId = 0;
  accessAirusercertificateId = 0;
  accessAirusercertificatefileId = 0;
  accessAirdocumentId = 0;
  accessAcquaintanceId = 0;
  accessActivityId = 0;
  accessAiruseractivityId = 0;
  accessFlightpointchainId = 0;
  accessTimetableId = 0;

  userCtrl: FormControl;
  filteredUsers: Observable<any[]>;

  constructor(
    public authService: AuthService,
    private globalsettingsService: GlobalsettingsService,
    private accessstoreService: AccessstoreService,
    private airuserService: AiruserService,
    private positionService: PositionService,
    private subdivisionService: SubdivisionService,
    private accessrightService: AccessrightService,
    private organisationstoreService: OrganisationstoreService,
    private spinnerService: NgxSpinnerService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  async ngOnInit() {
    this.userCtrl = new FormControl();
    await this.getData();
    this.filteredUsers = this.userCtrl.valueChanges.pipe(
      startWith(''),
      map(user => {
        return user ? this.filterUsers(user) : this.airusersView;
      })
    );
    // let selectedUserAccess = await this.accessrightService.getByAiruser(31);
    // selectedUserAccess.map(async item => {
    //   await this.accessrightService.deleteOneById(item.id);
    // });
  }

  filterUsers(str: string) {
    return this.airusersView.filter(
      state =>
        state.name.toLowerCase().indexOf(str.toLowerCase()) === 0 ||
        state.surname.toLowerCase().indexOf(str.toLowerCase()) === 0
    );
  }

  async onSaveModule() {
    this.spinnerService.show();
    let dataTt = [
      {
        airuser_id: this.selectedId,
        subdivision_id: this.subdivisions.find(item =>
          isNullOrUndefined(item.parent_id)
        ).id,
        resourcename: 'timetable',
        get: this.editTt ? true : this.getTt,
        post: this.editTt,
        delete: this.editTt,
        put: this.editTt,
        sysId: this.accessTimetableId
      }
    ];
    try {
      await Promise.all(
        dataTt.map(async item => {
          if (item.sysId !== 0) {
            let res = await this.accessrightService.putOneById(
              item.sysId,
              item
            );
          } else {
            let res = await this.accessrightService.postOne(item);
          }
        })
      );
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.selectAiruser(this.selectedId);
      this.spinnerService.hide();
    }
  }

  async onSaveSubdivision() {
    this.spinnerService.show();
    let dataHr = [
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airuser',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessAiruserId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'position',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessPositionId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airuserphone',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessAiruserphoneId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'education',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessEducationId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'previouswork',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessPreviousworkId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'familymember',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessFamilymemberworkId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airuseremail',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessAiruseremailId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'subdivision',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessSubdivisionId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airuserfile',
        get: this.editHr ? true : this.getHr,
        post: this.editHr,
        delete: this.editHr,
        put: this.editHr,
        sysId: this.accessAiruserfileId
      }
    ];
    let dataDocs = [
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airdocument',
        get: this.editDoc,
        post: this.editDoc,
        delete: this.editDoc,
        put: this.editDoc,
        sysId: this.accessAirdocumentId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'acquaintance',
        get: this.editDoc,
        post: this.editDoc,
        delete: this.editDoc,
        put: this.editDoc,
        sysId: this.accessAcquaintanceId
      }
    ];
    let dataCertificate = [
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airusercertificate',
        get: this.editCertificate ? true : this.getCertificate,
        post: this.editCertificate,
        delete: this.editCertificate,
        put: this.editCertificate,
        sysId: this.accessAirusercertificateId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airusercertificatefile',
        get: this.editCertificate ? true : this.getCertificate,
        post: this.editCertificate,
        delete: this.editCertificate,
        put: this.editCertificate,
        sysId: this.accessAirusercertificatefileId
      }
    ];
    let dataPi = [
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'airuseractivity',
        get: this.editPi ? true : this.getPi,
        post: this.editPi,
        delete: this.editPi,
        put: this.editPi,
        sysId: this.accessAiruseractivityId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'activity',
        get: this.editPi ? true : this.getPi,
        post: this.editPi,
        delete: this.editPi,
        put: this.editPi,
        sysId: this.accessActivityId
      },
      {
        airuser_id: this.selectedId,
        subdivision_id: this.selectedSubdivId,
        resourcename: 'flightpointchain',
        get: this.editPi ? true : this.getPi,
        post: this.editPi,
        delete: this.editPi,
        put: this.editPi,
        sysId: this.accessFlightpointchainId
      }
    ];
    try {
      await Promise.all(
        dataHr.map(async item => {
          if (item.sysId !== 0) {
            let res = await this.accessrightService.putOneById(
              item.sysId,
              item
            );
          } else {
            let res = await this.accessrightService.postOne(item);
          }
        })
      );

      await Promise.all(
        dataDocs.map(async item => {
          if (item.sysId !== 0) {
            let res = await this.accessrightService.putOneById(
              item.sysId,
              item
            );
          } else {
            let res = await this.accessrightService.postOne(item);
          }
        })
      );

      await Promise.all(
        dataCertificate.map(async item => {
          if (item.sysId !== 0) {
            let res = await this.accessrightService.putOneById(
              item.sysId,
              item
            );
          } else {
            let res = await this.accessrightService.postOne(item);
          }
        })
      );
      await Promise.all(
        dataPi.map(async item => {
          if (item.sysId !== 0) {
            let res = await this.accessrightService.putOneById(
              item.sysId,
              item
            );
          } else {
            let res = await this.accessrightService.postOne(item);
          }
        })
      );
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.selectAiruser(this.selectedId);
      this.spinnerService.hide();
    }
  }

  async selectAiruser(id) {
    this.selectedId = +id;
    if (this.selectedId === -1) {
      this.selectedSubdivId = null;
      this.userCtrl.reset();
    }
    try {
      let selectedUserAccess = this.accessrightService.getByAiruser(
        this.selectedId
      );
      this.selectedUserAccess = await selectedUserAccess;
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.selectSubdiv(this.selectedSubdivId);
    }
  }

  selectSubdiv(id) {
    /**Читаем текущие права на подразделения и модули */
    this.selectedSubdivId = +id;

    this.editTt = false; // расписание
    this.editPi = false; // планирование
    this.editTtDisabled = true;
    this.getPiDisabled = true;
    this.editPiDisabled = true;

    this.getHr = false;
    this.editHr = false;
    this.editHrDisabled = true;
    this.getHrDisabled = true;
    this.getCertificate = false;
    this.editCertificate = false;
    this.editCertificateDisabled = true;
    this.getCertificateDisabled = true;
    this.editDocDisabled = true;
    this.editDoc = false;
    if (this.selectedSubdivId === -1) {
      return;
    }
    /**Устанавливаем возможность для текущего пользователя устанавливать права */
    if (this.authService.loggedInUser.isadmin) {
      this.getHrDisabled = false;
      this.editHrDisabled = false;
      this.editDocDisabled = false;
      this.getCertificateDisabled = false;
      this.editCertificateDisabled = false;
      this.getTtDisabled = false;
      this.editTtDisabled = false;
      this.getPiDisabled = false;
      this.editPiDisabled = false;
    } else {
      if (!isNullOrUndefined(this.authService.loggedInUserAccesses)) {
        this.authService.loggedInUserAccesses.map(item => {
          if (item.subdivision_id === +id) {
            if (item.resourcename === 'airuser') {
              if (item.post) {
                this.editHrDisabled = false;
              }
              if (item.get) {
                this.getHrDisabled = false;
              }
            }
            if (item.resourcename === 'airdocument') {
              if (item.post) {
                this.editDocDisabled = false;
              }
            }
            if (item.resourcename === 'airusercertificate') {
              if (item.post) {
                this.editCertificateDisabled = false;
              }
              if (item.get) {
                this.getCertificateDisabled = false;
              }
            }
            if (item.resourcename === 'activity') {
              if (item.post) {
                this.editPiDisabled = false;
              }
              if (item.get) {
                this.getPiDisabled = false;
              }
            }
          }
          if (item.resourcename === 'timetable') {
            if (item.post) {
              this.editTtDisabled = false;
            }
            if (item.get) {
              this.getTtDisabled = false;
            }
          }
        });
      }
    }
    /**Устанавливаем переключатели в зависимости от текущих прав выбранного
     * пользователя, проверяем есть ли уже записи в таблице, если есть ставим id и флаги*/
    if (!isNullOrUndefined(this.selectedUserAccess)) {
      for (let i = 0; i < this.selectedUserAccess.length; i++) {
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airuser'
        ) {
          if (this.selectedUserAccess[i].post) {
            this.editHrDisabled = false;
          }
          if (this.selectedUserAccess[i].get) {
            this.getHrDisabled = false;
          }
          this.getHr = this.selectedUserAccess[i].get;
          this.editHr = this.selectedUserAccess[i].post;
          this.hasAccessToHr = true;
          this.accessAiruserId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airdocument'
        ) {
          if (this.selectedUserAccess[i].post) {
            this.editDocDisabled = false;
          }
          if (this.selectedUserAccess[i].get) {
            this.editDocDisabled = false;
          }
          this.editDoc = this.selectedUserAccess[i].post;
          this.hasAccessToDoc = true;
          this.accessAirdocumentId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airusercertificate'
        ) {
          if (this.selectedUserAccess[i].post) {
            this.editCertificateDisabled = false;
          }
          if (this.selectedUserAccess[i].get) {
            this.getCertificateDisabled = false;
          }
          this.getCertificate = this.selectedUserAccess[i].get;
          this.editCertificate = this.selectedUserAccess[i].post;
          this.hasAccessToCertificates = true;
          this.accessAirusercertificateId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'acquaintance'
        ) {
          this.accessAcquaintanceId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'position'
        ) {
          this.accessPositionId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airuserphone'
        ) {
          this.accessAiruserphoneId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'education'
        ) {
          this.accessEducationId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'previouswork'
        ) {
          this.accessPreviousworkId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'familymember'
        ) {
          this.accessFamilymemberworkId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airuseremail'
        ) {
          this.accessAiruseremailId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'subdivision'
        ) {
          this.accessSubdivisionId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airuserfile'
        ) {
          this.accessAiruserfileId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airusercertificatefile'
        ) {
          this.accessAirusercertificatefileId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'activity'
        ) {
          if (this.selectedUserAccess[i].post) {
            this.editPiDisabled = false;
          }
          if (this.selectedUserAccess[i].get) {
            this.getPiDisabled = false;
          }
          this.getPi = this.selectedUserAccess[i].get;
          this.editPi = this.selectedUserAccess[i].post;
          this.hasAccessToPi = true;
          this.accessActivityId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'airuseractivity'
        ) {
          this.accessAiruseractivityId = this.selectedUserAccess[i].id;
        }
        if (
          this.selectedUserAccess[i].subdivision_id === +id &&
          this.selectedUserAccess[i].resourcename === 'flightpointchain'
        ) {
          this.accessFlightpointchainId = this.selectedUserAccess[i].id;
        }
        if (this.selectedUserAccess[i].resourcename === 'timetable') {
          if (this.selectedUserAccess[i].post) {
            this.editTtDisabled = false;
          }
          if (this.selectedUserAccess[i].get) {
            this.getTtDisabled = false;
          }
          // this.getTt = this.selectedUserAccess[i].get;
          this.editTt = this.selectedUserAccess[i].post;
          this.hasAccessToTt = true;
          this.accessTimetableId = this.selectedUserAccess[i].id;
        }
      }
    }
    this.checkAccessToAiruser();
  }

  checkAccessToAiruser() {
    if (
      this.getCertificate ||
      this.editCertificate ||
      this.editHr ||
      this.editDoc
    ) {
      this.getHr = true;
      this.getHrDisabled = true;
    } else {
      this.getHrDisabled = false;
    }
    if (this.editCertificate) {
      this.getCertificate = true;
      this.getCertificateDisabled = true;
    } else {
      this.getCertificateDisabled = false;
    }
    if (this.editPi) {
      this.getPi = true;
      this.getPiDisabled = true;
    } else {
      this.getPiDisabled = false;
    }
    if (this.editTt) {
      this.getTt = true;
      this.getTtDisabled = true;
    } else {
      this.getTtDisabled = false;
    }
  }

  async getData() {
    this.spinnerService.show();
    try {
      let airusers = this.airuserService.getAll();
      let positions = this.positionService.getAll();
      let subdivisions = this.subdivisionService.getAll();
      this.airusers = isNullOrUndefined(await airusers) ? [] : await airusers;
      this.positions = isNullOrUndefined(await positions)
        ? []
        : await positions;
      this.subdivisions = isNullOrUndefined(await subdivisions)
        ? []
        : await subdivisions;
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }

    this.airusersView = [];
    this.subdivisionsView = [];
    try {
      /**Выбираем пользователей из своего и дочерних подразделений */
      if (this.authService.loggedInUserRole === 1) {
        if (
          isNullOrUndefined(
            this.positions.find(
              position =>
                position.id === this.authService.loggedInUser.position_id
            )
          )
        ) {
          this.globalsettingsService.openSnackBar(`Нет доступа.`, 'Ok');
          this.spinnerService.hide();
          this.router.navigate(['/sys/organisation/subdivisions']);
        }
        this.airusers.map(airuser => {
          if (
            airuser.id !== this.authService.loggedInUser.id &&
            this.organisationstoreService.isAiruserInSubdivision(
              airuser,
              [
                // this.getSubdivisionByPositionId(
                //   this.authService.loggedInUser.position_id
                // )
                this.authService.loggedInUserSubdivision
              ],
              this.positions,
              this.subdivisions
            )
          ) {
            this.airusersView.push(airuser);
          }
        });

        this.subdivisionsView.push(
          this.subdivisions.find(
            subdiv => subdiv.id === this.authService.loggedInUserSubdivision.id
          )
        );
        this.subdivisions.map(subdv => {
          if (
            this.isSubdivisionInSubdivisions(subdv, [
              this.authService.loggedInUserSubdivision
            ])
          ) {
            this.subdivisionsView.push(subdv);
          }
        });
      } else {
        this.airusersView = [...this.airusers];
        this.airusersView.splice(
          this.airusersView.findIndex(
            i => i.id === this.authService.loggedInUser.id
          ),
          1
        );
        this.subdivisionsView = this.subdivisions;
      }
      this.writePositionNames();
      this.writeSubdivisionNames();
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  // getSubdivisionByPositionId(id) {
  //   if (
  //     isNullOrUndefined(this.positions) ||
  //     isNullOrUndefined(this.subdivisions)
  //   ) {
  //     return;
  //   }
  //   for (let i = 0; i < this.positions.length; i++) {
  //     if (this.positions[i].id === id) {
  //       for (let j = 0; j < this.subdivisions.length; j++) {
  //         if (this.subdivisions[j].id === this.positions[i].subdivision_id) {
  //           return this.subdivisions[j];
  //         }
  //       }
  //     }
  //   }
  // }

  /**Проверка является ли subdivision Дочерним к subdivisions */
  isSubdivisionInSubdivisions(
    subdivision: Subdivision,
    subdivisions: Subdivision[]
  ) {
    if (
      isNullOrUndefined(subdivision) ||
      isNullOrUndefined(subdivisions) ||
      subdivisions.length === 0
    ) {
      return false;
    } else {
      for (let i = 0; i < subdivisions.length; i++) {
        if (subdivision.parent_id === subdivisions[i].id) {
          return true;
        } else if (
          this.isSubdivisionInSubdivisions(
            subdivision,
            this.subdivisions.filter(
              subdv => subdv.parent_id === subdivisions[i].id
            )
          )
        ) {
          return true;
        }
      }
    }
    return false;
  }

  getPositionNameById(id) {
    if (isNullOrUndefined(this.positions)) {
      return;
    }
    return isNullOrUndefined(
      this.positions.find(position => position.id === id)
    )
      ? ''
      : this.positions.find(position => position.id === id).name;
  }

  writePositionNames() {
    if (!isNullOrUndefined(this.positions)) {
      for (let i = 0; i < this.airusersView.length; i++) {
        this.airusersView[i].position_name = this.getPositionNameById(
          this.airusersView[i].position_id
        );
        if (isNullOrUndefined(this.airusersView[i].position_name)) {
          this.airusersView[i].position_name = '';
        }
      }
    }
  }

  writeSubdivisionNames() {
    if (!isNullOrUndefined(this.subdivisionsView)) {
      for (let i = 0; i < this.airusersView.length; i++) {
        let sbdv = this.accessstoreService.getSubdivisionByPositionId(
          this.airusersView[i].position_id,
          this.positions,
          this.subdivisions
        );
        this.airusersView[i].subdivision_name = isNullOrUndefined(sbdv)
          ? ''
          : sbdv.name;
        if (this.airusersView[i].subdivision_name == null) {
          this.airusersView[i].subdivision_name = '';
        }
      }
    }
  }

  // getSubdivisionNameByPositionId(id) {
  //   if (
  //     isNullOrUndefined(this.positions) ||
  //     isNullOrUndefined(this.subdivisions)
  //   ) {
  //     return;
  //   }
  //   for (let i = 0; i < this.positions.length; i++) {
  //     if (this.positions[i].id === id) {
  //       for (let j = 0; j < this.subdivisions.length; j++) {
  //         if (this.subdivisions[j].id === this.positions[i].subdivision_id) {
  //           return this.subdivisions[j].name;
  //         }
  //       }
  //     }
  //   }
  // }
}
