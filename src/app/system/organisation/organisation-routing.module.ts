import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganisationComponent } from './organisation.component';
import { AirusersComponent } from './airusers/airusers.component';
import { AirusersListComponent } from './airusers/airusers-list/airusers-list.component';
import { AirusersEditComponent } from './airusers/airusers-edit/airusers-edit.component';
import { SubdivisionsComponent } from './subdivisions/subdivisions.component';
import { CanDeactivateGuard } from 'src/app/shared/services/can-deactivate.guard';
import { AccessComponent } from './access/access.component';
import { OrgSettingsComponent } from './org-settings/org-settings.component';
import { AccesssetComponent } from './accessset/accessset.component';
import { CertSettingsComponent } from './cert-settings/cert-settings.component';
import { PcertificatesComponent } from './pcertificates/pcertificates.component';

const routes: Routes = [
  {
    path: '',
    component: OrganisationComponent,
    data: { breadcrumb: '' },
    children: [
      {
        path: 'subdivisions',
        data: { breadcrumb: 'Подразделения' },
        component: SubdivisionsComponent
      },
      {
        path: 'access',
        data: { breadcrumb: 'Права доступа' },
        component: AccesssetComponent
      },
      {
        path: 'access2',
        data: { breadcrumb: 'Права доступа' },
        component: AccessComponent
      },
      {
        path: 'users',
        data: { breadcrumb: '' },
        component: AirusersComponent,
        children: [
          {
            path: 'list',
            data: { breadcrumb: 'Сотрудники' },
            component: AirusersListComponent
          },
          {
            path: 'edit/:id',
            data: { breadcrumb: 'Профиль' },
            canDeactivate: [CanDeactivateGuard],
            component: AirusersEditComponent
          }
        ]
      },
      {
        path: 'settings',
        data: { breadcrumb: 'Словари' },
        component: OrgSettingsComponent
      },
      {
        path: 'certsettings',
        data: { breadcrumb: 'Типы сертификатов' },
        component: CertSettingsComponent
      },
      {
        path: 'pilotcert/:id',
        data: { breadcrumb: 'Сертификаты' },
        component: PcertificatesComponent
      }
      // {
      //   path: 'editusers/:id',
      //   data: { breadcrumb: 'Профиль' },
      //   component: ProfileComponent
      // },
      // {
      //   path: 'access',
      //   data: { breadcrumb: 'Права' },
      //   component: AccessComponent
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganisationRoutingModule {}
