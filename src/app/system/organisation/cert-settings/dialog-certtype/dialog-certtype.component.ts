import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-dialog-certtype',
  templateUrl: './dialog-certtype.component.html',
  styleUrls: ['./dialog-certtype.component.scss']
})
export class DialogCerttypeComponent implements OnInit {
  buttonStr = 'Добавить сертификат';
  certificateForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogCerttypeComponent>,
    public authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.certificateForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      type: new FormControl(+this.data.type),
      active: new FormControl(true, [Validators.required])
    });
    if (this.data.edit === true) {
      this.certificateForm.patchValue({
        name: this.data.certificate.name,
        type: this.data.certificate.type,
        active: this.data.certificate.active
      });
      this.buttonStr = 'Сохранить';
    }
  }

  onDel() {
    this.dialogRef.close(false);
  }

  onSave() {
    this.dialogRef.close(this.certificateForm.value);
  }
}
