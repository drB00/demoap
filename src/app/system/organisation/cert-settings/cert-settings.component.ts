import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatDialog } from '@angular/material';
import { CertificatetypeService } from '../../shared/services/certificatetype.service';
import { GlobalsettingsService } from '../../shared/services/globalsettings.service';
import { isNullOrUndefined } from 'util';
import { DialogCerttypeComponent } from './dialog-certtype/dialog-certtype.component';

@Component({
  selector: 'app-cert-settings',
  templateUrl: './cert-settings.component.html',
  styleUrls: ['./cert-settings.component.scss']
})
export class CertSettingsComponent implements OnInit {
  certificatetypesPilot = [];
  certificatetypesPilotSearch = '';
  certificatetypesSteward = [];
  certificatetypesStewardSearch = '';

  constructor(
    private certificatetypeService: CertificatetypeService,
    private globalsettingsService: GlobalsettingsService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  async ngOnInit() {
    await this.getData();
  }

  async getData() {
    this.certificatetypesPilot.length = 0;
    this.certificatetypesSteward.length = 0;
    try {
      let certificatetypes = await this.certificatetypeService.getAll();
      console.log(certificatetypes);
      if (!isNullOrUndefined(certificatetypes)) {
        certificatetypes.map(item => {
          if (item.type === 1) {
            this.certificatetypesPilot.push(item);
          } else if (item.type === 2) {
            this.certificatetypesSteward.push(item);
          }
        });
      }
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    }
  }

  onAddCertificatetype(type) {
    let dialogRef = this.dialog.open(DialogCerttypeComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: 'Добавление типа сертификата',
        type: +type,
        edit: false
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        try {
          await this.certificatetypeService.postOne(result);
        } catch (err) {
          console.error(`Ошибка ${err}`);
        } finally {
          await this.getData();
        }
      }
    });
  }

  onEditCertificatetype(certificate) {
    if (certificate.id <= 27) {
      return;
    }
    let dialogRef = this.dialog.open(DialogCerttypeComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: 'Добавление типа сертификата',
        type: 1,
        edit: true,
        certificate
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result) && result !== false) {
        try {
          await this.certificatetypeService.putOneById(certificate.id, result);
        } catch (err) {
          console.error(`Ошибка ${err}`);
        } finally {
          await this.getData();
        }
      } else if (result === false) {
        try {
          await this.certificatetypeService.deleteOneById(certificate.id);
        } catch (err) {
          console.error(`Ошибка ${err}`);
        } finally {
          await this.getData();
        }
      }
    });
  }
}
