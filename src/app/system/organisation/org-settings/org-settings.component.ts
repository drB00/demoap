import { Component, OnInit } from '@angular/core';
import { Dictionary } from 'src/app/shared/models/dict.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { MatSnackBar, MatDialog } from '@angular/material';
import { DictionaryService } from '../../shared/services/dictionary.service';
import { GlobalsettingsService } from '../../shared/services/globalsettings.service';
import { isNullOrUndefined } from 'util';
import { DialogDictionaryComponent } from '../dialogs/dialog-dictionary/dialog-dictionary.component';

@Component({
  selector: 'app-org-settings',
  templateUrl: './org-settings.component.html',
  styleUrls: ['./org-settings.component.scss']
})
export class OrgSettingsComponent implements OnInit {
  maritalstatuses: Dictionary[];
  militarydutyes: Dictionary[];
  relationdegrees: Dictionary[];

  searchMaritalstatusStr = '';
  searchMilitarydutyeStr = '';
  searchRelationdegreStr = '';

  constructor(
    private spinnerService: NgxSpinnerService,
    private globalsettingsService: GlobalsettingsService,
    private dictionaryService: DictionaryService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {}

  async ngOnInit() {
    try {
      let maritalstatuses = this.dictionaryService.getMaritalstatuses();
      let militarydutyes = this.dictionaryService.getMilitarydutyes();
      let relationdegrees = this.dictionaryService.getRelationdegrees();
      this.maritalstatuses = isNullOrUndefined(await maritalstatuses)
        ? []
        : await maritalstatuses;
      this.militarydutyes = isNullOrUndefined(await militarydutyes)
        ? []
        : await militarydutyes;
      this.relationdegrees = isNullOrUndefined(await relationdegrees)
        ? []
        : await relationdegrees;
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  onAddMaritalstatus() {
    let dialogRef = this.dialog.open(DialogDictionaryComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        edit: false
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        this.spinnerService.show();
        try {
          await this.dictionaryService.postMilitaryduty(result);
          let res = this.dictionaryService.getMilitarydutyes();
          this.maritalstatuses = isNullOrUndefined(await res) ? [] : await res;
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }

  onAddMilitaryduty() {
    let dialogRef = this.dialog.open(DialogDictionaryComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        edit: false
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        this.spinnerService.show();
        try {
          await this.dictionaryService.postMaritalstatus(result);
          let res = this.dictionaryService.getMaritalstatuses();
          this.militarydutyes = isNullOrUndefined(await res) ? [] : await res;
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }
  onAddRelationdegre() {
    let dialogRef = this.dialog.open(DialogDictionaryComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        edit: false
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        this.spinnerService.show();
        try {
          await this.dictionaryService.postRelationdegree(result);
          let res = this.dictionaryService.getRelationdegrees();
          this.relationdegrees = isNullOrUndefined(await res) ? [] : await res;
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }
  onEditMaritalstatus(id: number, name: string) {
    let dialogRef = this.dialog.open(DialogDictionaryComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        edit: true,
        name
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        this.spinnerService.show();
        try {
          if (result === false) {
            await this.dictionaryService.deleteMaritalstatusById(id);
          } else {
            await this.dictionaryService.putMaritalstatus(id, result);
          }
          let res = this.dictionaryService.getMaritalstatuses();
          this.maritalstatuses = isNullOrUndefined(await res) ? [] : await res;
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }
  onEditMilitaryduty(id: number, name: string) {
    let dialogRef = this.dialog.open(DialogDictionaryComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        edit: true,
        name
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        this.spinnerService.show();
        try {
          if (result === false) {
            await this.dictionaryService.deleteMilitarydutyById(id);
          } else {
            await this.dictionaryService.putMilitaryduty(id, result);
          }
          let res = this.dictionaryService.getMilitarydutyes();
          this.militarydutyes = isNullOrUndefined(await res) ? [] : await res;
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }
  onEditRelationdegre(id: number, name: string) {
    let dialogRef = this.dialog.open(DialogDictionaryComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        edit: true,
        name
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        this.spinnerService.show();
        try {
          if (result === false) {
            await this.dictionaryService.deleteRelationdegreeById(id);
          } else {
            await this.dictionaryService.putRelationdegree(id, result);
          }
          let res = this.dictionaryService.getRelationdegrees();
          this.relationdegrees = isNullOrUndefined(await res) ? [] : await res;
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.spinnerService.hide();
        }
      }
    });
  }
}
