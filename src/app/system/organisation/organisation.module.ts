import { NgModule } from '@angular/core';

import { OrganisationRoutingModule } from './organisation-routing.module';
import { OrganisationComponent } from './organisation.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { AirusersComponent } from './airusers/airusers.component';
import { AirusersListComponent } from './airusers/airusers-list/airusers-list.component';
import { AirusersEditComponent } from './airusers/airusers-edit/airusers-edit.component';
import { SubdivisionsComponent } from './subdivisions/subdivisions.component';
import { UserFilterPipe } from '../shared/pipes/user-filter.pipe';
import { AccessComponent } from './access/access.component';
import { TemplateSubdivisionComponent } from './templates/template-subdivision/template-subdivision.component';
import { SanitizeHtmlPipe } from '../shared/pipes/sanitizehtml.pipe';
import { OrgSettingsComponent } from './org-settings/org-settings.component';
import { DictFilterPipe } from '../shared/pipes/dict-filter.pipe';
import { AccesssetComponent } from './accessset/accessset.component';
import { CertSettingsComponent } from './cert-settings/cert-settings.component';
import { DialogCerttypeComponent } from './cert-settings/dialog-certtype/dialog-certtype.component';
import { PcertificatesComponent } from './pcertificates/pcertificates.component';
import { DialogCertificatesComponent } from './dialogs/dialog-certificates/dialog-certificates.component';
import { DialogDictionaryComponent } from './dialogs/dialog-dictionary/dialog-dictionary.component';

@NgModule({
  declarations: [
    OrganisationComponent,
    AirusersComponent,
    AirusersListComponent,
    AirusersEditComponent,
    SubdivisionsComponent,
    UserFilterPipe,
    SanitizeHtmlPipe,
    AccessComponent,
    TemplateSubdivisionComponent,
    OrgSettingsComponent,
    DictFilterPipe,
    AccesssetComponent,
    CertSettingsComponent,
    PcertificatesComponent
  ],
  imports: [SharedModule, OrganisationRoutingModule]
})
export class OrganisationModule {}
