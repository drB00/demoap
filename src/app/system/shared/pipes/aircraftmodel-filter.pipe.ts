import { Pipe, PipeTransform } from '@angular/core';
import { Aircraftmodel } from 'src/app/shared/models/aircraft.model';

@Pipe({
  name: 'aircraftmodelFilter'
})
export class AircraftmodelFilterPipe implements PipeTransform {

  transform(aircraftmodels: Aircraftmodel[], str: string): any {
    if (str === '' || aircraftmodels.length === 0) {
      return aircraftmodels;
    }
    return aircraftmodels.filter((aircraftmodel) => {
      return aircraftmodel.name.toLowerCase().indexOf(str.toLowerCase()) !== -1;
    });
  }

}
