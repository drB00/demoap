import { Pipe, PipeTransform } from '@angular/core';
import { Dictionary } from 'src/app/shared/models/dict.model';

@Pipe({
  name: 'dictFilter'
})
export class DictFilterPipe implements PipeTransform {
  transform(dict: Dictionary[], searchStr?: any): any {
    if (searchStr === '' || dict.length === 0) {
      return dict;
    }
    return dict.filter(item => {
      return item.name.toLowerCase().indexOf(searchStr.toLowerCase()) !== -1;
    });
  }
}
