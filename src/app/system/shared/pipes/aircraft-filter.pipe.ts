import { Pipe, PipeTransform } from '@angular/core';
import { Aircraft } from 'src/app/shared/models/aircraft.model';

@Pipe({
  name: 'aircraftFilter'
})
export class AircraftFilterPipe implements PipeTransform {

  transform(aircrafts: Aircraft[], str: string): any {
    if (str === '' || aircrafts.length === 0) {
      return aircrafts;
    }
    return aircrafts.filter((aircraft) => {
      return aircraft.code.toLowerCase().indexOf(str.toLowerCase()) !== -1;
    });
  }

}
