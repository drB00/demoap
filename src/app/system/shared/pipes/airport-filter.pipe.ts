import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'airportFilter'
})
export class AirportFilterPipe implements PipeTransform {

  transform(airports: any[], str: string): any {
    if (str === '' || airports.length === 0) {
      return airports;
    }
    return airports.filter((airport) => {
      return airport.code.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
        airport.name_rus.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
        airport.city_rus.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
        airport.country_rus.toLowerCase().indexOf(str.toLowerCase()) !== -1;
    });
  }

}
