import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'certificatetypeFilter'
})
export class CertificatetypeFilterPipe implements PipeTransform {
  transform(certificatetypes: any[], str: string): any {
    if (str === '' || certificatetypes.length === 0) {
      return certificatetypes;
    }
    return certificatetypes.filter((certificatetype) => {
      if (str === 'Пилот') {
        return (
          // certificatetype.name === 'ID-карта'
          // || certificatetype.name === 'АСП-вода'
          // || certificatetype.name === 'Свидетельство пилота'
          // || certificatetype.name === 'Допуск к международным полетам'
          // || certificatetype.name === 'Справка ВЛЭК'
          // || certificatetype.name === 'Заграничный паспорт'
          // || certificatetype.name === 'CRM'
          // || certificatetype.name === 'Опасные грузы'
          // || certificatetype.name === 'Авиационная безопасность'
          // // || certificatetype.name === 'RVSM'
          // || certificatetype.name === 'Уровень владения английским языком'
          certificatetype.name === 'Тренажер'
          || certificatetype.name === 'Проверка техники пилотирования'
          || certificatetype.name === 'NAT-HLA'
          || certificatetype.name === 'RVSM'
          || certificatetype.name === 'АСП-суша'
          || certificatetype.name === 'Переподготовка на тип ВС'
          || certificatetype.name === 'КПК по типу'
        ) ? false : true;
      } else if (str === 'Бортпроводник') {
        return (
          certificatetype.name === 'ID-карта'
          || certificatetype.name === 'АСП-вода'
          || certificatetype.name === 'Свидетельство бортпроводника'
          || certificatetype.name === 'Справка ВЛЭК'
          || certificatetype.name === 'Заграничный паспорт'
          || certificatetype.name === 'Проверка'
          || certificatetype.name === 'Опасные грузы'
          || certificatetype.name === 'Авиационная безопасность'
        ) ? true : false;
      } else if (str === 'БортпроводникAircraft') {
        return (
          certificatetype.name === 'АСП-суша'
        ) ? true : false;
      } else if (str === 'ПилотAircraft') {
        return (
          certificatetype.name === 'Тренажер'
          || certificatetype.name === 'Проверка техники пилотирования'
          || certificatetype.name === 'NAT-HLA'
          || certificatetype.name === 'RVSM'
          || certificatetype.name === 'АСП-суша'
          || certificatetype.name === 'Переподготовка на тип ВС'
          || certificatetype.name === 'КПК по типу'
        ) ? true : false;
      }
    });
  }

}
