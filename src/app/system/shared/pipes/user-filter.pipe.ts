import { Pipe, PipeTransform } from '@angular/core';
import { Airuser } from '../../../shared/models/airuser.model';


@Pipe({
  name: 'userFilter'
})
export class UserFilterPipe implements PipeTransform {
  transform(persons: Airuser[], str: string): any {
    if (str === '' || persons.length === 0) {
      return persons;
    }
    return persons.filter((person) => {
      if (person.phones && person.phones[0]) {
        return person.name.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
          person.surname.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
          person.subdivision_name.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
          person.position_name.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
          person.phones[0].toLowerCase().indexOf(str.toLowerCase()) !== -1;
      } else {
        return person.name.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
          person.surname.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
          person.subdivision_name.toLowerCase().indexOf(str.toLowerCase()) !== -1 ||
          person.position_name.toLowerCase().indexOf(str.toLowerCase()) !== -1;
      }
    });
  }

}
