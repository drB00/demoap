import { Pipe, PipeTransform } from '@angular/core';
import { Aircraftmodelgroup } from 'src/app/shared/models/aircraft.model';

@Pipe({
  name: 'aircraftmodelgroupFilter'
})
export class AircraftmodelgroupFilterPipe implements PipeTransform {

  transform(aircraftmodelgroups: Aircraftmodelgroup[], str: string): any {
    if (str === '' || aircraftmodelgroups.length === 0) {
      return aircraftmodelgroups;
    }
    return aircraftmodelgroups.filter((aircraftmodelgroup) => {
      return aircraftmodelgroup.name.toLowerCase().indexOf(str.toLowerCase()) !== -1;
    });
  }

}
