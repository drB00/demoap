import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { distinctUntilChanged, filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent implements OnInit {

  pageName = 'Подразделения';

  constructor(
    private activatedRoute: ActivatedRoute,
    public router: Router,
    public translate: TranslateService
  ) {
    translate.setDefaultLang('ru');
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.pageName = this.getPagename(this.activatedRoute.root);
      }
    });
  }

  getPagename(route: ActivatedRoute) {
    if (route.firstChild && route.firstChild.routeConfig.data) {
      return this.getPagename(route.firstChild);
    } else {
      return route.routeConfig.data['breadcrumb'];
    }
  }

}
