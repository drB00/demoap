import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-dialog-yesno',
  templateUrl: './dialog-yesno.component.html',
  styleUrls: ['./dialog-yesno.component.scss']
})
export class DialogYesnoComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogYesnoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

}
