import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../shared/services/auth.service';
import { Airuser } from '../../../../shared/models/airuser.model';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material';
import { DialogProfileComponent } from 'src/app/system/organisation/airusers/dialog-profile/dialog-profile.component';
import { DictionaryService } from '../../services/dictionary.service';
import { Dictionary } from 'src/app/shared/models/dict.model';
import { Subdivision } from 'src/app/shared/models/subdivision.model';
import { Position } from 'src/app/shared/models/position.model';
import { AiruserService } from '../../services/airuser.service';
import { SubdivisionService } from '../../services/subdivision.service';
import { PositionService } from '../../services/position.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  loggedInUser: Airuser;
  pilotTypes: Dictionary[];

  constructor(
    private authService: AuthService,
    private airuserService: AiruserService,
    // private subdivisionService: SubdivisionService,
    // private positionService: PositionService,
    private dictionaryService: DictionaryService,
    public dialog: MatDialog,
    private translate: TranslateService
  ) {
    this.translate.setDefaultLang('ru');
  }

  async ngOnInit() {
    this.loggedInUser = this.authService.loggedInUser;
    this.pilotTypes = this.dictionaryService.pilotTypes;
  }

  logout() {
    this.authService.logout();
  }

  async gotoProfile() {
    let dialogRef = this.dialog.open(DialogProfileComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: 'Профиль',
        myProfile: true,
        surname: this.authService.loggedInUser.surname,
        name: this.authService.loggedInUser.name,
        home_airport_id: this.authService.loggedInUser.home_airport_id,
        pilot_type: this.authService.loggedInUser.pilot_type,
        patronymic: this.authService.loggedInUser.patronymic,
        subdivision: !isNullOrUndefined(
          this.authService.loggedInUserSubdivision
        )
          ? this.authService.loggedInUserSubdivision.name
          : '',
        position: !isNullOrUndefined(this.authService.loggedInUserPosition)
          ? this.authService.loggedInUserPosition.name
          : '',
        pilotTypes: this.pilotTypes
      }
    });
    dialogRef.afterClosed().subscribe(async result => {});
    // let dialogRef = this.dialog.open(DialogProfileComponent, {
    //   panelClass: 'app-dialog-extend',
    //   data: {
    //     title: 'Профиль',
    //     myProfile: true,
    //     surname: this.loggedInUser.surname,
    //     name: this.loggedInUser.name,
    //     phone: this.loggedInUser.name,
    //     email: this.loggedInUser.name,
    //     pilot_type: this.loggedInUser.pilot_type,
    //     patronymic: this.loggedInUser.patronymic,
    //     subdivision: this.authService.loggedInUserSubdivision.name,
    //     position: this.authService.loggedInUserPosition.name,
    //     pilotTypes: this.pilotTypes
    //   }
    // });
  }
}
