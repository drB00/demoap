import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogSetstringComponent } from './dialog-setstring.component';

describe('DialogSetstringComponent', () => {
  let component: DialogSetstringComponent;
  let fixture: ComponentFixture<DialogSetstringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogSetstringComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogSetstringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
