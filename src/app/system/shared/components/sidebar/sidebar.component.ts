import { Component, OnInit } from '@angular/core';
import { Link } from '../../../../shared/models/link.model';
import { AuthService } from '../../../../shared/services/auth.service';
import { Router } from '@angular/router';
import { isNullOrUndefined } from 'util';

import { GlobalsettingsService } from '../../services/globalsettings.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  mainMenuHrsys: Link[] = [];
  mainMenuTimetablesys: Link[] = [];
  bdMenuTimetablesys: Link[] = [];
  mainMenuPilotsys: Link[] = [];
  mainMenuDocsys: Link[] = [];

  acquaintanceLoggedInUserCount = 0;

  constructor(
    private authService: AuthService,
    private globalsettingsService: GlobalsettingsService,
    public router: Router
  ) {}

  async ngOnInit() {
    try {
      this.globalsettingsService.countAirdocumentsToAcquaintance.subscribe(
        value => (this.acquaintanceLoggedInUserCount = value)
      );
      if (this.authService.loggedInUserRole === 0) {
        // полный интерфейс редактирования
        this.mainMenuHrsys = [
          {
            modulLink: '/sys/organisation',
            link: 'subdivisions',
            text: 'Подразделения',
            icon: 'fa-sitemap'
          },
          {
            modulLink: '/sys/organisation',
            link: 'users',
            text: 'Сотрудники',
            icon: 'fa-address-card'
          },
          {
            modulLink: '/sys/organisation',
            link: 'access',
            text: 'Права доступа',
            icon: 'fa-universal-access'
          },
          // { modulLink: '/sys/organisation', link: 'access2', text: 'Права доступа', icon: 'fa-universal-access' },
          {
            modulLink: '/sys/organisation',
            link: 'settings',
            text: 'Словари',
            icon: 'fa-list-ol'
          }
        ];
        this.mainMenuTimetablesys = [
          {
            modulLink: '/sys/timetable/fpgraphic',
            link: '0',
            text: 'Расписание',
            icon: 'fa-calendar'
          }
        ];
        this.bdMenuTimetablesys = [
          {
            modulLink: '/sys/timetable',
            link: 'aircrafts',
            text: 'Самолеты',
            icon: 'fa-plane'
          },
          {
            modulLink: '/sys/timetable',
            link: 'airports',
            text: 'Аэропорты',
            icon: 'fa-fort-awesome'
          }
        ];
        this.mainMenuPilotsys = [
          {
            modulLink: '/sys/timetable',
            link: 'ppgraphic',
            params: [],
            text: 'Планирование',
            icon: 'fa-male'
          },
          {
            modulLink: '/sys/organisation',
            link: 'pilotcert',
            params: ['all'],
            text: 'Сертификаты',
            icon: 'fa-certificate'
          },
          {
            modulLink: '/sys/organisation',
            link: 'certsettings',
            params: [],
            text: 'Настройки',
            icon: 'fa-list-ol'
          }
        ];
        this.mainMenuDocsys = [
          {
            modulLink: '/sys/docs',
            link: 'downloadacq',
            text: 'Документы',
            icon: 'fa-file-text-o'
          },
          {
            modulLink: '/sys/docs',
            link: 'downloaddocs',
            text: 'Библиотека',
            icon: 'fa-book'
          }
        ];
      } else if (this.authService.loggedInUserRole === 1) {
        // руководитель
        this.mainMenuHrsys = [
          {
            modulLink: '/sys/organisation',
            link: 'subdivisions',
            text: 'Подразделения',
            icon: 'fa-sitemap'
          },
          {
            modulLink: '/sys/organisation',
            link: 'users',
            text: 'Сотрудники',
            icon: 'fa-address-card'
          },
          {
            modulLink: '/sys/organisation',
            link: 'access',
            text: 'Права доступа',
            icon: 'fa-universal-access'
          }
        ];
        this.mainMenuTimetablesys = [
          {
            modulLink: '/sys/timetable/fpgraphic',
            link: '0',
            text: 'Расписание',
            icon: 'fa-calendar'
          }
        ];
        this.bdMenuTimetablesys = [];
        this.mainMenuPilotsys = [];
        if (this.authService.loggedInUserSubdivision.type > 0) {
          this.mainMenuPilotsys.push({
            modulLink: '/sys/timetable',
            link: 'ppgraphic',
            params: [],
            text: 'Планирование',
            icon: 'fa-male'
          });
          this.mainMenuPilotsys.push({
            modulLink: '/sys/certification',
            link: 'certificates',
            params: [this.authService.loggedInUserSubdivision.type],
            text: 'Сертификация',
            icon: 'fa-certificate'
          });
        }
        if (
          !isNullOrUndefined(this.authService.loggedInUserPosition.pilot_type)
        ) {
          this.mainMenuPilotsys.push({
            modulLink: '/sys/pilot',
            link: 'activity',
            params: [],
            text: 'План работ',
            icon: 'fa-male'
          });
          this.mainMenuPilotsys.push({
            modulLink: '/sys/certification',
            link: 'mycertificates',
            params: [],
            text: 'Мои сертификаты',
            icon: 'fa-certificate'
          });
        }
        this.mainMenuDocsys = [
          {
            modulLink: '/sys/docs',
            link: 'downloadacq',
            text: 'Документы',
            icon: 'fa-file-text-o'
          },
          {
            modulLink: '/sys/docs',
            link: 'downloaddocs',
            text: 'Библиотека',
            icon: 'fa-book'
          }
        ];
      } else {
        // просто сотрудник,
        this.mainMenuHrsys = [];
        this.mainMenuTimetablesys = [
          {
            modulLink: '/sys/timetable/fpgraphic',
            link: '0',
            text: 'Расписание',
            icon: 'fa-calendar'
          }
        ];

        this.bdMenuTimetablesys = [];
        this.mainMenuPilotsys = [];

        if (!isNullOrUndefined(this.authService.loggedInUserSubdivision.type)) {
          if (
            !isNullOrUndefined(this.authService.loggedInUser.pilot_type) &&
            this.authService.loggedInUser.pilot_type > 0
          ) {
            this.mainMenuPilotsys.push({
              modulLink: '/sys/timetable',
              link: 'ppmyplan',
              params: [],
              text: 'Мой план',
              icon: 'fa-male'
            });
          }
          // если пилот или стюард
          this.mainMenuPilotsys.push({
            modulLink: '/sys/certification',
            link: 'mycertificates',
            params: [],
            text: 'Мои сертификаты',
            icon: 'fa-certificate'
          });
        }
        if (
          this.authService.loggedInUserAccesses.filter(item =>
            item.resourcename === 'airusercertificate' ? item.get : false
          ).length > 0
        ) {
          this.mainMenuPilotsys.push({
            modulLink: '/sys/certification',
            link: 'certificates',
            params: [1],
            text: 'Сертификаты',
            icon: 'fa-certificate'
          });
        }
        if (
          this.authService.loggedInUserAccesses.filter(item =>
            item.resourcename === 'activity' ? item.get : false
          ).length > 0
        ) {
          this.mainMenuPilotsys.push({
            modulLink: '/sys/timetable',
            link: 'ppgraphic',
            params: [],
            text: 'Планирование',
            icon: 'fa-male'
          });
        }
        if (
          this.authService.loggedInUserAccesses.filter(item =>
            item.resourcename === 'timetable' ? item.get : false
          ).length > 0
        ) {
        }
        this.mainMenuDocsys = [
          {
            modulLink: '/sys/docs',
            link: 'downloadacq',
            text: 'Документы',
            icon: 'fa-file-text-o'
          },
          {
            modulLink: '/sys/docs',
            link: 'downloaddocs',
            text: 'Библиотека',
            icon: 'fa-book'
          }
        ];
      }
    } catch (err) {
      console.error(err);
    }
  }
}
