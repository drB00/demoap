import { NgModule } from '@angular/core';
import { SystemRoutingModule } from './system-routing.module';
import { SystemComponent } from './system.component';
import { SharedModule } from '../shared/shared.module';
import { HeaderComponent } from './shared/components/header/header.component';
import { LogoComponent } from './shared/components/logo/logo.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { environment } from 'src/environments/environment';
import { WebsocketModule } from '../websocket';
import { DialogSetstringComponent } from './shared/components/dialog-setstring/dialog-setstring.component';
import { DialogYesnoComponent } from './shared/components/dialog-yesno/dialog-yesno.component';
import { DialogProfileComponent } from './organisation/airusers/dialog-profile/dialog-profile.component';
import { DialogSetaddressComponent } from './organisation/airusers/dialog-setaddress/dialog-setaddress.component';
import { DialogSeteducationComponent } from './organisation/airusers/dialog-seteducation/dialog-seteducation.component';
import { AirportFilterPipe } from './shared/pipes/airport-filter.pipe';
import { CertificatetypeFilterPipe } from './shared/pipes/certificatetype-filter.pipe';
import { DialogSetpreviousworkComponent } from './organisation/airusers/dialog-setpreviouswork/dialog-setpreviouswork.component';
import { DialogSetfamilymemberComponent } from './organisation/airusers/dialog-setfamilymember/dialog-setfamilymember.component';
import { DialogAirportComponent } from './timetable/airports/dialog-airport/dialog-airport.component';
import { DialogAircraftComponent } from './timetable/aircrafts/dialog-aircraft/dialog-aircraft.component';
import { DialogAircraftmodelComponent } from './timetable/aircrafts/dialog-aircraftmodel/dialog-aircraftmodel.component';
import { DialogAircraftmodelgroupComponent } from './timetable/aircrafts/dialog-aircraftmodelgroup/dialog-aircraftmodelgroup.component';
import { DialogFlightComponent } from './timetable/flight-plan/dialog-flight/dialog-flight.component';
import { DialogScenarioComponent } from './timetable/flight-plan/dialog-scenario/dialog-scenario.component';
import { TooltipFlightComponent } from './timetable/tooltip/tooltip-flight.component';
import { DialogActivityComponent } from './timetable/pilot-plan/dialog-activity/dialog-activity.component';
import { DialogEditactivityComponent } from './timetable/pilot-plan/dialog-editactivity/dialog-editactivity.component';
import { DialogSubdivisionComponent } from './organisation/dialogs/dialog-subdivision/dialog-subdivision.component';
import { DialogPositionComponent } from './organisation/dialogs/dialog-position/dialog-position.component';
import { TooltipActivityComponent } from './timetable/tooltip/tooltip-activity.component';
import { DialogCerttypeComponent } from './organisation/cert-settings/dialog-certtype/dialog-certtype.component';
import { DialogFlightregularComponent } from './timetable/flight-plan/dialog-flightregular/dialog-flightregular.component';
import { DialogPeriodflightComponent } from './timetable/flight-plan/dialog-periodflight/dialog-periodflight.component';
import { DialogCertificatesComponent } from './organisation/dialogs/dialog-certificates/dialog-certificates.component';
import { DialogDictionaryComponent } from './organisation/dialogs/dialog-dictionary/dialog-dictionary.component';

@NgModule({
  imports: [
    SystemRoutingModule,
    SharedModule,
    WebsocketModule.config({
      url: environment.ws
    })
  ],
  declarations: [
    SystemComponent,
    HeaderComponent,
    LogoComponent,
    SidebarComponent,
    DialogYesnoComponent,
    DialogSetstringComponent,
    DialogProfileComponent,
    DialogSetaddressComponent,
    DialogSeteducationComponent,
    DialogSetpreviousworkComponent,
    DialogSetfamilymemberComponent,
    DialogAirportComponent,
    DialogCerttypeComponent,
    DialogAircraftComponent,
    DialogAircraftmodelComponent,
    DialogAircraftmodelgroupComponent,
    AirportFilterPipe,
    CertificatetypeFilterPipe,
    DialogFlightComponent,
    DialogScenarioComponent,
    TooltipFlightComponent,
    TooltipActivityComponent,
    DialogActivityComponent,
    DialogEditactivityComponent,
    DialogSubdivisionComponent,
    DialogPositionComponent,
    DialogFlightregularComponent,
    DialogPeriodflightComponent,
    DialogCertificatesComponent,
    DialogDictionaryComponent
  ],
  entryComponents: [
    DialogYesnoComponent,
    DialogSetstringComponent,
    DialogProfileComponent,
    DialogSetaddressComponent,
    DialogSeteducationComponent,
    DialogSetpreviousworkComponent,
    DialogSetfamilymemberComponent,
    DialogAirportComponent,
    DialogCerttypeComponent,
    DialogAircraftComponent,
    DialogAircraftmodelComponent,
    DialogAircraftmodelgroupComponent,
    DialogFlightComponent,
    DialogScenarioComponent,
    TooltipFlightComponent,
    TooltipActivityComponent,
    DialogActivityComponent,
    DialogEditactivityComponent,
    DialogSubdivisionComponent,
    DialogPositionComponent,
    DialogFlightregularComponent,
    DialogPeriodflightComponent,
    DialogCertificatesComponent,
    DialogDictionaryComponent
  ]
})
export class SystemModule {}
