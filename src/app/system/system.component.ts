import { Component, OnInit } from '@angular/core';
import { WebsocketService, IMessage, WS_API } from '../websocket';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-system',
  templateUrl: './system.component.html',
  styleUrls: ['./system.component.scss']
})
export class SystemComponent implements OnInit {
  messages$: Observable<string>;
  init = true;

  constructor(
    private spinnerService: NgxSpinnerService,
    private wsService: WebsocketService
  ) {}

  ngOnInit() {
    /**Соединение с сокетом */
    this.spinnerService.show();
    this.messages$ = this.wsService.addEventListener(WS_API.EVENTS.MESSAGES);
    this.messages$.subscribe(data => {
      if (data.indexOf('Successfully') !== -1 && this.init === true) {
        this.init = false;
        this.spinnerService.hide();
      }
    });
  }
}
