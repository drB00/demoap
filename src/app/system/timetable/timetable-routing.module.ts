import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimetableComponent } from './timetable.component';
import { AircraftsComponent } from './aircrafts/aircrafts.component';
import { AirportsComponent } from './airports/airports.component';
import { FpGraphicviewComponent } from './flight-plan/fp-graphicview/fp-graphicview.component';
import { PpGraphicviewComponent } from './pilot-plan/pp-graphicview/pp-graphicview.component';
import { PpMyplanComponent } from './pilot-plan/pp-myplan/pp-myplan.component';

// const routes: Routes = [
//   {
//     path: '', component: TimetableComponent, children: [
//       { path: 'airplanes', component: AircraftsComponent },
//       { path: 'settings', component: SettingsComponent },
//       { path: 'list/:id', component: ListviewComponent },
//       { path: 'graphic/:id', component: GraphicviewComponent },
//     ]
//   }
// ];

const routes: Routes = [
  {
    path: '',
    component: TimetableComponent,
    data: { breadcrumb: '' },
    children: [
      {
        path: 'aircrafts',
        data: { breadcrumb: 'Самолеты' },
        component: AircraftsComponent
      },
      {
        path: 'airports',
        data: { breadcrumb: 'Аэропорты' },
        component: AirportsComponent
      },
      {
        path: 'fpgraphic/:id',
        data: { breadcrumb: 'Расписание' },
        component: FpGraphicviewComponent
      },
      {
        path: 'ppgraphic',
        data: { breadcrumb: 'Планирование' },
        component: PpGraphicviewComponent
      },
      {
        path: 'ppmyplan',
        data: { breadcrumb: 'Мой план' },
        component: PpMyplanComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimetableRoutingModule {}
