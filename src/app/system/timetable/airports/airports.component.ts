import { Component, OnInit } from '@angular/core';
import { PageEvent, MatDialog, MatSnackBar } from '@angular/material';
import { Airport } from 'src/app/shared/models/airport.model';
import { AirportService } from '../../shared/services/airport.service';
import { isNullOrUndefined } from 'util';
import { DialogAirportComponent } from './dialog-airport/dialog-airport.component';

@Component({
  selector: 'app-airports',
  templateUrl: './airports.component.html',
  styleUrls: ['./airports.component.scss']
})
export class AirportsComponent implements OnInit {
  airports: Airport[];

  searchStr = '';
  length = 100;
  pageSize = 10;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  pageEvent: PageEvent;

  constructor(
    public airportService: AirportService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar
  ) {}

  async ngOnInit() {
    await this.getData();
    try {
      this.length = (await this.airportService.getAll_count()).rowcount;
    } catch (err) {
      console.error(`Ошибка ${err}`);
    }

    // let all = await this.airportService.getAll();
    // let str = '';
    // all.map(item => {
    //   delete item.id;
    //   str += `await this.airportService.postOne(${JSON.stringify(item)});
    //   `;
    // });
    // console.log(str);
    
  }

  async getData() {
    if (!isNullOrUndefined(this.pageEvent)) {
      this.pageEvent.pageIndex = 0;
    }
    try {
      let airports;
      let length;
      if (this.searchStr !== '') {
        length = this.airportService.getWithFilter_count(this.searchStr);
        airports = this.airportService.getWithFilterLimitOffset(
          0,
          this.pageSize,
          this.searchStr
        );
      } else {
        length = this.airportService.getAll_count();
        airports = this.airportService.getWithOffsetLimit(0, this.pageSize);
      }
      this.length = (await length).rowcount;
      this.airports = isNullOrUndefined(await airports) ? [] : await airports;
    } catch (err) {
      console.error(`Ошибка ${err}`);
    }
  }

  async onCreateAirport() {
    let dialogRef = this.dialog.open(DialogAirportComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: 'Добавление аэропорта',
        edit: false
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        try {
          await this.airportService.postOne(result);
          this.getData();
        } catch (err) {
          console.error(`Ошибка ${err}`);
        }
      }
    });
  }

  async onEditAirport(id) {
    let dialogRef = this.dialog.open(DialogAirportComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: 'Редактирование аэропорта',
        edit: true,
        airport: this.airports.find(item => item.id === id)
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        if (result === false) {
          try {
            await this.airportService.deleteOneById(id);
            let airports = this.airportService.getAll();
            this.airports = isNullOrUndefined(await airports)
              ? []
              : await airports;
          } catch (err) {
            console.error(`Ошибка ${err}`);
          }
        } else {
          try {
            await this.airportService.putOneById(id, result);
            this.getData();
            this.openSnackBar(`Сохранено.`, 'Ok');
          } catch (err) {
            console.error(`Ошибка ${err}`);
          }
        }
      }
    });
  }

  async setPageSizeOptions(event: PageEvent) {
    let start = event.pageSize * event.pageIndex;
    try {
      let airports;
      if (this.searchStr !== '') {
        airports = this.airportService.getWithFilterLimitOffset(
          start,
          event.pageSize,
          this.searchStr
        );
      } else {
        airports = this.airportService.getWithOffsetLimit(
          start,
          event.pageSize
        );
      }
      this.airports = isNullOrUndefined(await airports) ? [] : await airports;
    } catch (err) {
      console.error(`Ошибка ${err}`);
    }
    this.pageSize = event.pageSize;
    this.pageEvent = event;
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }
}
