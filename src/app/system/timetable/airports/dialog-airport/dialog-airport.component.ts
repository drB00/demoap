import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-dialog-airport',
  templateUrl: './dialog-airport.component.html',
  styleUrls: ['./dialog-airport.component.scss']
})
export class DialogAirportComponent implements OnInit {
  buttonStr = 'Добавить аэропорт';
  airportForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogAirportComponent>,
    public authService: AuthService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.airportForm = new FormGroup({
      code: new FormControl(null, [Validators.required]),
      icao_code: new FormControl(null),
      name_rus: new FormControl(null, [Validators.required]),
      name_eng: new FormControl(null),
      city_rus: new FormControl(null, [Validators.required]),
      city_eng: new FormControl(null),
      country_rus: new FormControl(null, [Validators.required]),
      country_eng: new FormControl(null),
      summer_timeshift: new FormControl(null, [Validators.required]),
      winter_timeshift: new FormControl(null, [Validators.required]),
      city_id: new FormControl(null, [Validators.required])
    });
    if (this.data.edit === true) {
      this.airportForm.patchValue({
        code: this.data.airport.code,
        icao_code: this.data.airport.icao_code,
        name_rus: this.data.airport.name_rus,
        name_eng: this.data.airport.name_eng,
        city_rus: this.data.airport.city_rus,
        city_eng: this.data.airport.city_eng,
        country_rus: this.data.airport.country_rus,
        country_eng: this.data.airport.country_eng,
        summer_timeshift: this.data.airport.summer_timeshift,
        winter_timeshift: this.data.airport.winter_timeshift,
        city_id: this.data.airport.city_id
      });
      this.buttonStr = 'Сохранить';
    }
  }

  onClose(): void {
    this.dialogRef.close();
  }

  onDel() {
    this.dialogRef.close(false);
  }

  onSave() {
    this.dialogRef.close(this.airportForm.value);
  }
}
