import { Component, OnInit } from '@angular/core';
import { Aircraft, Aircraftmodel, Aircraftmodelgroup } from 'src/app/shared/models/aircraft.model';
import { AircraftService } from '../../shared/services/aircraft.service';
import { AircraftmodelgroupService } from '../../shared/services/aircraftmodelgroup.service';
import { AircraftmodelService } from '../../shared/services/aircraftmodel.service';
import { MatSnackBar, MatDialog } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'util';
import { DialogAircraftComponent } from './dialog-aircraft/dialog-aircraft.component';
import { DialogAircraftmodelComponent } from './dialog-aircraftmodel/dialog-aircraftmodel.component';
import { DialogAircraftmodelgroupComponent } from './dialog-aircraftmodelgroup/dialog-aircraftmodelgroup.component';

@Component({
  selector: 'app-aircrafts',
  templateUrl: './aircrafts.component.html',
  styleUrls: ['./aircrafts.component.scss']
})
export class AircraftsComponent implements OnInit {

  searchAircraftStr = '';
  searchAircraftmodelStr = '';
  searchAircraftmodelgroupStr = '';

  aircrafts: Aircraft[];
  aircraftmodels: Aircraftmodel[];
  aircraftmodelgroups: Aircraftmodelgroup[];

  constructor(
    public aircraftService: AircraftService,
    public aircraftmodelService: AircraftmodelService,
    public aircraftmodelgroupService: AircraftmodelgroupService,
    public dialog: MatDialog,
    private spinnerService: NgxSpinnerService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.getData();
  }

  async getData() {
    this.spinnerService.show();
    try {
      let aircrafts = this.aircraftService.getAll();
      let aircraftmodels = this.aircraftmodelService.getAll();
      let aircraftmodelgroups = this.aircraftmodelgroupService.getAll();
      this.aircrafts = (isNullOrUndefined(await aircrafts)) ? [] : await aircrafts;
      this.aircraftmodels = (isNullOrUndefined(await aircraftmodels)) ? [] : await aircraftmodels;
      this.aircraftmodelgroups = (isNullOrUndefined(await aircraftmodelgroups)) ? [] : await aircraftmodelgroups;
    } catch (err) {
      console.error(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  getAircraftmodelByid(aircraftmodel_id) {
    if (isNullOrUndefined(this.aircraftmodels)) {
      return {};
    }
    let res = this.aircraftmodels.find(item => item.id === aircraftmodel_id);
    return (isNullOrUndefined(res)) ? {} : res;
  }
  getAircraftmodelgroupByid(aircraftmodelgroup_id) {
    if (isNullOrUndefined(this.aircraftmodelgroups)) {
      return {};
    }
    let res = this.aircraftmodelgroups.find(item => item.id === aircraftmodelgroup_id);
    return (isNullOrUndefined(res)) ? {} : res;
  }

  onCreateAircraft() {
    let dialogRef = this.dialog.open(DialogAircraftComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Самолет`,
        code: '',
        aircraftmodel_id: null,
        aircraftmodels: this.aircraftmodels
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        try {
          await this.aircraftService.postOne(result);
        } catch (err) {
          console.error(err);
        } finally {
          this.getData();
        }
      }
    });
  }
  onEditAircraft(id) {
    let aircraft: Aircraft = this.aircrafts.find(item => item.id === id);
    let dialogRef = this.dialog.open(DialogAircraftComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Самолет`,
        editMode: true,
        code: aircraft.code,
        aircraftmodel_id: aircraft.aircraftmodel_id,
        aircraftmodels: this.aircraftmodels
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result) && result !== 'delete') {
        try {
          await this.aircraftService.putOneById(id, result);
        } catch (err) {
          console.error(err);
        } finally {
          this.getData();
        }
      } else if (result === 'delete') {
        try {
          await this.aircraftService.deleteOneById(id);
        } catch (err) {
          console.error(err);
        } finally {
          this.getData();
        }
      }
    });
  }
  onCreateAircraftmodel() {
    let dialogRef = this.dialog.open(DialogAircraftmodelComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Модель`,
        name: '',
        aircraftmodelgroup_id: null,
        aircraftmodelgroups: this.aircraftmodelgroups
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        try {
          await this.aircraftmodelService.postOne(result);
        } catch (err) {
          console.error(err);
        } finally {
          this.getData();
        }
      }
    });
  }
  onEditAircraftmodel(id) {
    let aircraftmodel: Aircraftmodel = this.aircraftmodels.find(item => item.id === id);
    let dialogRef = this.dialog.open(DialogAircraftmodelComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Модель`,
        editMode: true,
        name: aircraftmodel.name,
        aircraftmodelgroup_id: aircraftmodel.aircraftmodelgroup_id,
        aircraftmodelgroups: this.aircraftmodelgroups
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result) && result !== 'delete') {
        try {
          await this.aircraftmodelService.putOneById(id, result);
        } catch (err) {
          console.error(err);
        } finally {
          this.getData();
        }
      } else if (result === 'delete') {
        try {
          await this.aircraftmodelService.deleteOneById(id);
        } catch (err) {
          console.error(err);
        } finally {
          this.getData();
        }
      }
    });
  }
  onCreateAircraftmodelgroup() {
    let dialogRef = this.dialog.open(DialogAircraftmodelgroupComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Модель`,
        name: ''
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result)) {
        try {
          await this.aircraftmodelgroupService.postOne(result);
        } catch (err) {
          console.error(err);
        } finally {
          this.getData();
        }
      }
    });
  }
  onEditAircraftmodelgroup(id) {
    let aircraftmodelgroup: Aircraftmodelgroup = this.aircraftmodelgroups.find(item => item.id === id);
    let dialogRef = this.dialog.open(DialogAircraftmodelgroupComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: `Группа`,
        editMode: true,
        name: aircraftmodelgroup.name
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result) && result !== 'delete') {
        try {
          await this.aircraftmodelgroupService.putOneById(id, result);
        } catch (err) {
          console.error(err);
        } finally {
          this.getData();
        }
      } else if (result === 'delete') {
        try {
          await this.aircraftmodelgroupService.deleteOneById(id);
        } catch (err) {
          console.error(err);
        } finally {
          this.getData();
        }
      }
    });
  }

}
