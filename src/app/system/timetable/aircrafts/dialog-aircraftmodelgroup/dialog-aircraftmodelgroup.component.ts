import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-dialog-aircraftmodelgroup',
  templateUrl: './dialog-aircraftmodelgroup.component.html',
  styleUrls: ['./dialog-aircraftmodelgroup.component.scss']
})
export class DialogAircraftmodelgroupComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogAircraftmodelgroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  isValid(): boolean {
    let res = true;
    for (let prop in this.data) {
      if (this.data.hasOwnProperty(prop)) {
        if (isNullOrUndefined(this.data[prop]) || this.data[prop].length === 0) {
          res = false;
        }
      }
    }
    return res;
  }

}
