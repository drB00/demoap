import {
  Directive,
  HostListener,
  ElementRef,
  Input,
  ComponentFactoryResolver,
  ViewContainerRef,
  Inject,
  OnDestroy
} from '@angular/core';
import { isNullOrUndefined } from 'util';

import { TooltipActivityComponent } from './tooltip-activity.component';
import { DOCUMENT } from '@angular/common';

@Directive({
  selector: '[appTooltipActivity]'
})
export class TooltipActivityDirective implements OnDestroy {
  @Input() public data: any[];
  @Input() public airports: any[];
  private isClear = true;
  mouseOverElement: Element;
  contentCmpRef;

  constructor(
    private _componentFactoryResolver: ComponentFactoryResolver,
    private _viewContainerRef: ViewContainerRef,
    private _ef: ElementRef,
    @Inject(DOCUMENT) private _document: any
  ) {}

  ngOnDestroy(): void {
    if (this.contentCmpRef) {
      this.contentCmpRef.destroy();
      this.isClear = true;
    }
  }
  @HostListener('mouseover', ['$event']) onMouseHover(event: MouseEvent) {
    if (!this.isClear) {
      return;
    }
    this.isClear = false;
    this.mouseOverElement = document.elementFromPoint(
      event.clientX,
      event.clientY
    );
    let elmFlight = this.findFlight(this.mouseOverElement.classList);
    if (!elmFlight) {
      do {
        this.mouseOverElement = this.mouseOverElement.parentElement;
        elmFlight = this.findFlight(this.mouseOverElement.classList);
      } while (!elmFlight);
    }
    this.buildTooltip(this.mouseOverElement);
  }
  @HostListener('mouseleave') hideTooltip() {
    if (this.contentCmpRef) {
      this.contentCmpRef.destroy();
      this.isClear = true;
    }
  }

  get container(): HTMLElement {
    return this._document.querySelector('.ng-tool-tip-content') as HTMLElement;
  }

  private buildTooltip(element: Element) {
    // передаем mouseOverElement чтобы расчитать положение тултипа
    let componentFactory: any;
    componentFactory = this._componentFactoryResolver.resolveComponentFactory(
      TooltipActivityComponent
    );
    this.contentCmpRef = this._viewContainerRef.createComponent(
      componentFactory
    );

    this._document
      .querySelector('.field')
      .appendChild(this.contentCmpRef.location.nativeElement);
    this.contentCmpRef.instance.scroll =
      document.documentElement.scrollTop || document.body.scrollTop;
    this.contentCmpRef.instance.options = element.getClientRects();
    this.contentCmpRef.instance.data = this.data;
    this.contentCmpRef.instance.airports = this.airports;
    this.contentCmpRef.instance.container = this._document
      .querySelector('.field')
      .getClientRects();
    this.isClear = false;
  }

  private findFlight(classList: DOMTokenList): string {
    if (isNullOrUndefined(classList)) {
      return;
    }
    let elm;
    if (classList.length > 0) {
      let index = classList.contains('tooltipP');
      if (index) {
        elm = true;
      }
    }
    return elm;
  }
}
