import { Component, ElementRef } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Component({
  templateUrl: 'tooltip-flight.component.html',
  styleUrls: ['tooltip-flight.component.scss']
})
export class TooltipFlightComponent {
  public data: any[];
  public airports: any[];

  public _options;
  public _container;
  public _scroll;
  set options(op) {
    if (op) {
      this._options = op;
    }
  }
  get options() {
    return this._options;
  }
  set container(container) {
    if (container) {
      this._container = container;
    }
  }
  get container() {
    return this._container;
  }
  set scroll(scroll) {
    if (!isNullOrUndefined(scroll)) {
      this._scroll = scroll;
    }
  }
  get scroll() {
    return this._scroll;
  }

  constructor(private elRef: ElementRef) {}

  getRev(): boolean {
    if (this._container && this._options) {
      return this._container[0].right > this._options[0].left + 340;
    }
  }

  getLeft(): number {
    if (this._container && this._options) {
      if (this._container[0].right > this._options[0].left + 340) {
        return this._options[0].left - 10 > this._container[0].left
          ? this._options[0].left - 10 - this._container[0].left
          : 30;
      } else {
        return this._options[0].left - 660;
      }
    }
  }

  getAirportStringById(id: number): string {
    let airport = this.airports.find(item => item.id === id);
    return `${airport.name_rus}(${airport.code})`;
  }

  dateFromIsoString(str: string | Date) {
    // 2018-09-14T23:00:00
    if (typeof str === 'string') {
      let _arr = str.split('.')[0].split('T');
      let arr = _arr[0].split('-').concat(_arr[1].split(':')); // yyyy mm dd hh mm ss
      return new Date(+arr[0], +arr[1] - 1, +arr[2], +arr[3], +arr[4]);
    } else {
      return new Date(str);
    }
  }

  getArrivalDelay() {
    let delay =
      this.data[1].arrival_datetime !== this.data[1].calc_arrival_datatime;
    let time =
      +this.dateFromIsoString(this.data[1].calc_arrival_datatime) -
      +this.dateFromIsoString(this.data[1].arrival_datetime);
    let timestr = this.fromMsecToObj(Math.abs(time)).strHM;
    return { timestr, delay, time };
  }

  getDepartureDelay() {
    let delay =
      this.data[0].departure_datetime !== this.data[0].calc_departure_datatime;
    let time =
      +this.dateFromIsoString(this.data[0].calc_departure_datatime) -
      +this.dateFromIsoString(this.data[0].departure_datetime);
    let timestr = this.fromMsecToObj(Math.abs(time)).strHM;
    return { timestr, delay, time };
  }

  fromMsecToObj(msec: number) {
    let sec = msec / 1000;
    let min = sec / 60;
    let hour = min / 60;
    let str = '';
    let strHM = '';
    if (Math.floor(hour / 24) > 0) {
      str += Math.floor(hour / 24);
      str += ':';
    }
    if (Math.floor(hour % 24) > 0) {
      str +=
        Math.floor(hour % 24) < 10
          ? '0' + Math.floor(hour % 24)
          : Math.floor(hour % 24);
      str += ':';
    }
    strHM += Math.floor(hour) < 10 ? '0' + Math.floor(hour) : Math.floor(hour);
    strHM += 'ч ';
    str +=
      Math.floor(min % 60) < 10
        ? '0' + Math.floor(min % 60)
        : Math.floor(min % 60);
    strHM +=
      Math.floor(min % 60) < 10
        ? '0' + Math.floor(min % 60)
        : Math.floor(min % 60);
    strHM += 'м';
    return { val: msec, str: str, strHM: strHM };
  }
}
