import { NgModule } from '@angular/core';

import { TimetableRoutingModule } from './timetable-routing.module';
import { TimetableComponent } from './timetable.component';
import { AirportsComponent } from './airports/airports.component';
import { AircraftsComponent } from './aircrafts/aircrafts.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FpGraphicviewComponent } from './flight-plan/fp-graphicview/fp-graphicview.component';
import { PpGraphicviewComponent } from './pilot-plan/pp-graphicview/pp-graphicview.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { PpMyplanComponent } from './pilot-plan/pp-myplan/pp-myplan.component';

@NgModule({
  declarations: [
    TimetableComponent,
    AirportsComponent,
    AircraftsComponent,
    FpGraphicviewComponent,
    PpGraphicviewComponent,
    PpMyplanComponent
  ],
  imports: [SharedModule, TimetableRoutingModule, ScrollingModule]
})
export class TimetableModule {}
