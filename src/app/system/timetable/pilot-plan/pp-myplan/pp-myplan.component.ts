import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  NgZone
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {
  Activity,
  Airuseractivity
} from 'src/app/shared/models/activity.model';
import { isNullOrUndefined } from 'util';
import { Specificflight } from 'src/app/shared/models/specificflight.model';
import { Flight } from 'src/app/shared/models/flight.model';
import { Flighttemplate } from 'src/app/shared/models/flighttemplate.model';
import { Flightpoint } from 'src/app/shared/models/flightpoint.model';
import { Airuser } from 'src/app/shared/models/airuser.model';
import { Scenario } from 'src/app/shared/models/scenario.model';
import { AircraftService } from 'src/app/system/shared/services/aircraft.service';
import { AircraftmodelService } from 'src/app/system/shared/services/aircraftmodel.service';
import { AircraftmodelgroupService } from 'src/app/system/shared/services/aircraftmodelgroup.service';
import { TimetablestoreService } from 'src/app/system/shared/services/timetablestore.service';
import { FlightService } from 'src/app/system/shared/services/flight.service';
import { SpecificflightService } from 'src/app/system/shared/services/specificflight.service';
import { FlighttemplateService } from 'src/app/system/shared/services/flighttemplate.service';
import { FlightpointService } from 'src/app/system/shared/services/flightpoint.service';
import { FlightpointchainService } from 'src/app/system/shared/services/flightpointchain.service';
import { ActivityService } from 'src/app/system/shared/services/activity.service';
import { AirportService } from 'src/app/system/shared/services/airport.service';
import { ScenarioService } from 'src/app/system/shared/services/scenario.service';
import { Subdivision } from 'src/app/shared/models/subdivision.model';
import { Position } from 'src/app/shared/models/position.model';
import { Observable } from 'rxjs';
import { MatMenuTrigger, MatDialog, MatSnackBar } from '@angular/material';
import { Flightpointchain } from 'src/app/shared/models/flightpointchain.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { AccessstoreService } from 'src/app/system/shared/services/accessstore.service';
import { DictionaryService } from 'src/app/system/shared/services/dictionary.service';
import { AiruserService } from 'src/app/system/shared/services/airuser.service';
import { PositionService } from 'src/app/system/shared/services/position.service';
import { SubdivisionService } from 'src/app/system/shared/services/subdivision.service';
import { GlobalsettingsService } from 'src/app/system/shared/services/globalsettings.service';
import { WebsocketService, WS_API } from 'src/app/websocket';
import { NgxSpinnerService } from 'ngx-spinner';
import { CertificatetypeService } from 'src/app/system/shared/services/certificatetype.service';

@Component({
  selector: 'app-pp-myplan',
  templateUrl: './pp-myplan.component.html',
  styleUrls: ['./pp-myplan.component.scss']
})
export class PpMyplanComponent implements OnInit {
  filterForm: FormGroup;
  from: Date;
  to: Date;

  aircraftmodelgroups = [];
  aircraftmodels = [];
  aircrafts = [];
  airports = [];

  flights: Flight[] = [];
  flighttempltates: Flighttemplate[] = [];
  specificflights: Specificflight[] = [];
  flightpoints: Flightpoint[] = [];
  flightpointsInchains: Flightpoint[];
  activitys: Activity[];

  activitynames: any[];

  airuseractivitys: Airuseractivity[];

  pilots: Airuser[] = [];
  pilot: Airuser;
  pilotTypes = [];

  pilotsView: Airuser[];
  subdivisions: Subdivision[];
  positions: Position[];
  subdivisionsView: Subdivision[];

  scenarioes: Scenario[];

  selectedIndex = 0; // индекс вкладки для выбора

  scales = [10, 5, 2];
  scale = 0;

  selectedSpecificflight: Specificflight; // для контекстного меню

  messages$: Observable<string>;
  timeout;

  constructor(
    public authService: AuthService,
    public accessstoreService: AccessstoreService,
    private aircraftService: AircraftService,
    private aircraftmodelService: AircraftmodelService,
    private aircraftmodelgroupService: AircraftmodelgroupService,
    private timetablestoreService: TimetablestoreService,
    private flightService: FlightService,
    private specificflightService: SpecificflightService,
    private flighttemplateService: FlighttemplateService,
    private flightpointService: FlightpointService,
    private flightpointchainService: FlightpointchainService,
    private activityService: ActivityService,
    private airportService: AirportService,
    private scenarioService: ScenarioService,
    private dictionaryService: DictionaryService,
    private airuserService: AiruserService,
    private positionService: PositionService,
    private subdivisionService: SubdivisionService,
    private globalsettingsService: GlobalsettingsService,
    private wsService: WebsocketService,
    private dialog: MatDialog,
    private spinnerService: NgxSpinnerService,
    private zone: NgZone,
    private snackBar: MatSnackBar,
    public certificatetypeService: CertificatetypeService
  ) {
    this.spinnerService.show();
  }

  async ngOnInit() {
    this.filterForm = new FormGroup({
      from: new FormControl(null, [Validators.required]),
      to: new FormControl(null, [Validators.required])
    });
    let today = new Date();
    today.setDate(today.getDate() - 3);
    let tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 30);
    this.from = today;
    this.to = tomorrow;
    this.filterForm.patchValue({
      from: today,
      to: tomorrow
    });
    await this.getData();
    this.subdivisionsView = this.subdivisions.map(subdivision => {
      let res = false;
      if (subdivision.type === 1) {
        this.pilots.map(pilot => {
          if (
            this.positions.find(position => position.id === pilot.position_id)
              .subdivision_id === subdivision.id
          ) {
            res = true;
          }
        });
      }
      subdivision['disable'] = !res;
      return subdivision;
    });
    /**Подписка на сокет*/
    this.messages$ = this.wsService.addEventListener(WS_API.EVENTS.MESSAGES);
    this.messages$.subscribe(async data => {
      data = data.indexOf('action') !== -1 ? JSON.parse(data) : data;
      switch (data['resourse']) {
        case 'flightpoint':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights();
          }, 500);
          break;
        case 'specificflight':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights();
          }, 500);
          break;
        case 'flighttemplate':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights();
          }, 500);
          break;
        case 'flight':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights();
          }, 500);
          break;
        case 'activity':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights();
            this.getPilots();
          }, 500);
          break;
      }
    });
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add flight');
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add flighttemplate');
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add specificflight');
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add flightpoint');
    this.wsService.sendMessage(
      WS_API.COMMANDS.SEND_TEXT,
      'Add Airuseractivity'
    );
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add Activity');
    await this.getFlights();
    await this.getPilots();
  }

  async getData() {
    this.pilotTypes = this.dictionaryService.pilotTypes;
    try {
      let aircraftmodelgroups = this.aircraftmodelgroupService.getAll();
      let aircraftmodels = this.aircraftmodelService.getAll();
      let aircrafts = this.aircraftService.getAll();
      let airports = this.airportService.getAll();
      let scenarioes = this.scenarioService.getAll();
      let activitynames = this.dictionaryService.getActivitynames();
      let _positions = this.positionService.getAll();
      let _subdivisions = this.subdivisionService.getAll();
      let _airusers = this.airuserService.getAll();
      let airusers = isNullOrUndefined(await _airusers) ? [] : await _airusers;
      this.positions = isNullOrUndefined(await _positions)
        ? []
        : await _positions;
      this.subdivisions = isNullOrUndefined(await _subdivisions)
        ? []
        : await _subdivisions;
      this.activitynames = isNullOrUndefined(await activitynames)
        ? []
        : await activitynames;
      airusers.map(airuser => {
        let position = this.positions.find(
          item => item.id === airuser.position_id
        );
        if (isNullOrUndefined(position)) {
          return false;
        }
        let subdivision = this.subdivisions.find(
          item => item.id === position.subdivision_id
        );
        if (isNullOrUndefined(subdivision)) {
          return false;
        }
        if (!isNullOrUndefined(airuser.pilot_type) && airuser.pilot_type > 0) {
          let pilotType = this.pilotTypes.find(
            item => item.id === airuser.pilot_type
          );
          if (!isNullOrUndefined(pilotType)) {
            airuser['pilotTypestr'] = pilotType.name;
            airuser[
              'airuseraccesstype'
            ] = this.accessstoreService.accessTypeForAiruser(
              airuser,
              this.positions,
              this.subdivisions,
              'airuser'
            );
            this.pilots.push(airuser);
          }
        }
      });
      // let res = await this.scenarioService.putOneById(1, { status: 1 })
      this.aircraftmodelgroups = isNullOrUndefined(await aircraftmodelgroups)
        ? []
        : await aircraftmodelgroups;
      this.aircraftmodels = isNullOrUndefined(await aircraftmodels)
        ? []
        : await aircraftmodels;
      this.aircrafts = isNullOrUndefined(await aircrafts)
        ? []
        : await aircrafts;
      this.airports = isNullOrUndefined(await airports) ? [] : await airports;
      this.scenarioes = isNullOrUndefined(await scenarioes)
        ? []
        : await scenarioes;
      this.scenarioes = this.scenarioes.filter(
        item =>
          item.status === 1 ||
          item.airuser_id === this.authService.loggedInUser.id
      );
    } catch (err) {
      console.error(`Ошибка ${err}`);
      this.openSnackBar(`${err.error}.`, 'Ok');
    }
  }

  async getFlights() {
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.from) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.from).valueOf())
      )
    ) {
      return;
    }
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.to) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.to).valueOf())
      )
    ) {
      return;
    }
    this.flights.length = 0;
    this.flighttempltates.length = 0;
    this.specificflights.length = 0;
    this.flightpoints.length = 0;
    try {
      let __from = new Date(
        this.from.getFullYear(),
        this.from.getMonth(),
        this.from.getDate() - 1,
        0,
        0,
        0
      );
      let __to = new Date(
        this.to.getFullYear(),
        this.to.getMonth(),
        this.to.getDate() + 1,
        0,
        0,
        0
      );
      /**Получение одиночных рейсов и активностей в выбранном диапазоне */
      let _specificflights = this.specificflightService.getByDatesScenario(
        `${__from.getFullYear()}-${__from.getMonth() + 1}-${__from.getDate()}`,
        `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
        this.scenarioes.find(item => item.status === 1).id
      );
      let _activitys = this.activityService.getByDatesScenario(
        `${__from.getFullYear()}-${__from.getMonth() + 1}-${__from.getDate()}`,
        `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
        this.scenarioes.find(item => item.status === 1).id
      );
      let specificflights: Specificflight[] = isNullOrUndefined(
        await _specificflights
      )
        ? []
        : await _specificflights;
      /**Если выбран временный сенарий, то получаем активности и из него */
      // let activitystmp: Activity[] = [];
      let activitys: Activity[] = isNullOrUndefined(await _activitys)
        ? []
        : await _activitys;
      activitys = activitys.map(item => {
        item.status = 1;
        return item;
      });
      let _activitystmp = this.activityService.getByDatesScenario(
        `${__from.getFullYear()}-${__from.getMonth() + 1}-${__from.getDate()}`,
        `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
        this.scenarioes.find(
          item => item.airuser_id === this.authService.loggedInUser.id
        ).id
      );

      /**Получение связанных с ними данных по массивам id */
      let specificflights_id = specificflights.map(item => item.id);
      let flights_id: number[] = specificflights.map(item => item.flight_id);
      let flighttemplates_id: number[] = specificflights.map(
        item => item.flighttemplate_id
      );
      let used = {};
      flights_id = flights_id.filter(obj => {
        return obj in used ? 0 : (used[obj] = 1);
      });
      used = {};
      flighttemplates_id = flighttemplates_id.filter(obj => {
        return obj in used ? 0 : (used[obj] = 1);
      });
      if (isNullOrUndefined(activitys[0])) {
        activitys = [];
      }
      let _flights = this.flightService.getByIds(flights_id);
      let _flighttemplates = this.flighttemplateService.getByIds(
        flighttemplates_id
      );
      let _flightpoints = this.flightpointService.getBySpecificflights(
        specificflights_id
      );
      let flights: Flight[] = isNullOrUndefined(await _flights)
        ? []
        : await _flights;
      let flighttemplates: Flighttemplate[] = isNullOrUndefined(
        await _flighttemplates
      )
        ? []
        : await _flighttemplates;
      let flightpoints: Flightpoint[] = isNullOrUndefined(await _flightpoints)
        ? []
        : await _flightpoints;
      let res = await this.timetablestoreService.getAircrafts(
        this.aircrafts,
        flights,
        flighttemplates,
        specificflights,
        flightpoints,
        activitys,
        // airuseractivitys,
        this.pilots,
        this.from,
        this.to,
        this.scales[this.scale]
      );
      this.aircrafts = res.aircrafts;
      this.aircrafts.map(item => {
        let idx = [];
        for (let i = 0; i < item.specificflights.length; i++) {
          if (
            isNullOrUndefined(item.specificflights[i].flightpoints) ||
            item.specificflights[i].flightpoints.length === 0 ||
            +this.dateFromIsoString(
              item.specificflights[i].calc_arrival_datatime
            ) < +this.from
          ) {
            idx.push(i);
          }
        }
        idx.sort((a, b) => b - a);
        for (let i = 0; i < idx.length; i++) {
          item.specificflights.splice(idx[i], 1);
        }
      });
      this.flights = res.flights;
      this.flighttempltates = res.flighttemplates;
      this.specificflights = res.specificflights;
      this.flightpoints = res.flightpoints;
      let flightpointsInchains = this.flightpointService.getByFlightpointchains(
        res.flightpointchainIds
      );
      this.flightpointsInchains = isNullOrUndefined(await flightpointsInchains)
        ? []
        : await flightpointsInchains;
    } catch (err) {
      console.error(err);
    }
  }

  async getPilots() {
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.from) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.from).valueOf())
      )
    ) {
      return;
    }
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.to) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.to).valueOf())
      )
    ) {
      return;
    }
    try {
      let __from = new Date(
        this.from.getFullYear(),
        this.from.getMonth(),
        this.from.getDate() - 1,
        0,
        0,
        0
      );
      let __to = new Date(
        this.to.getFullYear(),
        this.to.getMonth(),
        this.to.getDate() + 1,
        0,
        0,
        0
      );
      // console.log(this.to);
      // console.log(__to);
      /**Получение активностей в выбранном диапазоне */
      let _activitys = this.activityService.getByDatesScenario(
        `${__from.getFullYear()}-${__from.getMonth() + 1}-${__from.getDate()}`,
        `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
        this.scenarioes.find(item => item.status === 1).id
      );
      /**Если выбран временный сенарий, то получаем активности и из него */
      let activitys: Activity[] = isNullOrUndefined(await _activitys)
        ? []
        : await _activitys;
      activitys = activitys.map(item => {
        item.status = 1;
        return item;
      });
      if (this.selectedIndex === 1) {
        let _activitystmp = this.activityService.getByDatesScenario(
          `${__from.getFullYear()}-${__from.getMonth() +
            1}-${__from.getDate()}`,
          `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
          this.scenarioes.find(
            item => item.airuser_id === this.authService.loggedInUser.id
          ).id,
          [this.scenarioes.find(item => item.status === 1).id]
        );
      }
      /**Получение связанных с ними данных по массивам id */
      if (isNullOrUndefined(activitys[0])) {
        activitys = [];
      }

      let res = await this.timetablestoreService.getPilots(
        this.pilots,
        activitys,
        // airuseractivitys,
        this.flightpoints,
        this.positions,
        this.subdivisions,
        this.authService.loggedInUser,
        this.authService.loggedInUserAccesses,
        this.from,
        this.to,
        this.scales[this.scale]
      );
      this.pilots = res.pilots;
      this.activitys = res.activitys;
      this.activitys.map(item => {
        item.airport = this.getAirportByid(item.airport_id);
        item.end_airport = this.getAirportByid(item.end_airport_id);
      });
      /**добавляем в активности с цепочками лэги */
      this.pilots.map(pilot => {
        if (!isNullOrUndefined(pilot.home_airport_id)) {
          pilot.home_airport = this.getAirportByid(pilot.home_airport_id);
        }
        pilot.activitys.map((activity, indexact) => {
          if (
            !isNullOrUndefined(activity.flightpointchain_id) &&
            activity.flightpointchain_id > 0
          ) {
            activity['flightpoints'] = this.getFlightpointsByFlightpointchain(
              activity.flightpointchain_id
            );
          }
        });
      });
      this.pilot = this.pilots.find(
        pilot => pilot.id === this.authService.loggedInUser.id
      );
      this.pilot.activitys.sort((a, b) => {
        return this.dateFromIsoString(a.begin_datetime) >
          this.dateFromIsoString(b.begin_datetime)
          ? 1
          : -1;
      });
    } catch (err) {
      console.error(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  getAirportByid(id) {
    let airport = this.airports.find(airport => airport.id === id);
    return isNullOrUndefined(airport) ? {} : airport;
  }

  getFlightpointsByFlightpointchain(flightpointchain_id) {
    let flightpoints = this.flightpoints.filter(
      fp => fp.flightpointchain_id === flightpointchain_id
    );
    return isNullOrUndefined(flightpoints) ? [] : flightpoints;
  }

  async updateData(type: boolean = true, spinner = false) {
    if (type === true) {
      if (
        isNullOrUndefined(this.filterForm.value.from) ||
        !(
          new Date(this.filterForm.value.from) instanceof Date &&
          !isNaN(new Date(this.filterForm.value.from).valueOf())
        )
      ) {
        return;
      }
      if (
        isNullOrUndefined(this.filterForm.value.from) ||
        !(
          new Date(this.filterForm.value.to) instanceof Date &&
          !isNaN(new Date(this.filterForm.value.to).valueOf())
        )
      ) {
        return;
      }
      let _from = new Date(this.filterForm.value.from);
      let from = new Date(
        _from.getFullYear(),
        _from.getMonth(),
        _from.getDate(),
        0,
        0,
        0
      );
      let _to = new Date(this.filterForm.value.to);
      let to = new Date(
        _to.getFullYear(),
        _to.getMonth(),
        _to.getDate(),
        0,
        0,
        0
      );

      this.from = new Date(
        _from.getFullYear(),
        _from.getMonth(),
        _from.getDate(),
        0,
        0,
        0
      );
      this.to = new Date(
        _to.getFullYear(),
        _to.getMonth(),
        _to.getDate(),
        0,
        0,
        0
      );

      // if (+this.to === +to && +this.from === +from) {
      //   return;
      // }
    }
    if (spinner === true) {
      this.spinnerService.show();
    }
    await this.getFlights();
    await this.getPilots();
    this.spinnerService.hide();
  }

  dateFromIsoString(str: string | Date) {
    // 2018-09-14T23:00:00
    if (typeof str === 'string') {
      let _arr = str.split('.')[0].split('T');
      let arr = _arr[0].split('-').concat(_arr[1].split(':')); // yyyy mm dd hh mm ss
      return new Date(+arr[0], +arr[1] - 1, +arr[2], +arr[3], +arr[4]);
    } else {
      return new Date(str);
    }
  }

  getActivitynameByid(id) {
    return this.activitynames.find(activityname => activityname.id === id);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }
}
