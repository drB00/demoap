import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Airport } from 'src/app/shared/models/airport.model';
import { Observable } from 'rxjs';
import { Airuser } from 'src/app/shared/models/airuser.model';
import { Subdivision } from 'src/app/shared/models/subdivision.model';
import { Position } from 'src/app/shared/models/position.model';
import { AuthService } from 'src/app/shared/services/auth.service';
import { AiruserService } from 'src/app/system/shared/services/airuser.service';
import { PositionService } from 'src/app/system/shared/services/position.service';
import { SubdivisionService } from 'src/app/system/shared/services/subdivision.service';
import { AccessrightService } from 'src/app/system/shared/services/accessright.service';
import { AirportService } from 'src/app/system/shared/services/airport.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { isNull, isNullOrUndefined } from 'util';
import { startWith, map } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivityService } from 'src/app/system/shared/services/activity.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { saveAs as importedSaveAs } from 'file-saver';
import { TravelapiService } from 'src/app/system/shared/services/travelapi.service';

@Component({
  selector: 'app-dialog-editactivity',
  templateUrl: './dialog-editactivity.component.html',
  styleUrls: ['./dialog-editactivity.component.scss']
})
export class DialogEditactivityComponent implements OnInit {
  loggedInUser: Airuser;
  loggedInUserPosition: Position;
  loggedInUserSubdivision: Subdivision;
  loggedInUserAccesses;
  loggedInUserRole;

  timeMaskInput: any[] = [/[0-2]/, /\d/, ':', /[0-6]/, /\d/];

  selectedIndex: number;

  activityForm: FormGroup;

  disabled: boolean;

  flightpoint1_id = null;
  flightpoint2_id = null;

  airports: Airport[];

  departure_airport_control: FormControl;
  arrival_airport_control: FormControl;
  filtered_arrival_airport: Observable<any[]>;
  filtered_departure_airport: Observable<any[]>;
  timerId;

  activeFile;
  deleteFile = false;

  price = '';

  constructor(
    public authService: AuthService,
    public airuserService: AiruserService,
    public positionService: PositionService,
    public subdivisionService: SubdivisionService,
    public accessrightService: AccessrightService,
    public airportService: AirportService,
    private spinnerService: NgxSpinnerService,
    private activityService: ActivityService,
    private travelapiService: TravelapiService,
    public dialogRef: MatDialogRef<DialogEditactivityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  async ngOnInit() {
    this.loggedInUserAccesses = this.authService.loggedInUserAccesses;
    this.loggedInUser = this.authService.loggedInUser;
    this.loggedInUserPosition = this.authService.loggedInUserPosition;
    this.loggedInUserSubdivision = this.authService.loggedInUserSubdivision;
    if (this.loggedInUser.isadmin === true) {
      this.loggedInUserRole = 0;
    } else if (isNull(this.loggedInUserPosition.parent_id)) {
      this.loggedInUserRole = 1;
    } else {
      this.loggedInUserRole = 2;
    }
    this.disabled = !isNullOrUndefined(this.data.activity.flightpoint1_id);
    this.departure_airport_control = new FormControl({
      value: null,
      disabled: this.disabled
    });
    this.arrival_airport_control = new FormControl({
      value: null,
      disabled: this.disabled
    });
    this.activityForm = new FormGroup({
      activityname_id: new FormControl(
        { value: this.disabled ? 6 : null, disabled: this.disabled },
        [Validators.required]
      ),
      datebegin: new FormControl({ value: null, disabled: this.disabled }, [
        Validators.required
      ]),
      timebegin: new FormControl({ value: null, disabled: this.disabled }, [
        Validators.required
      ]),
      dateend: new FormControl({ value: null, disabled: this.disabled }, [
        Validators.required
      ]),
      timeend: new FormControl({ value: null, disabled: this.disabled }, [
        Validators.required
      ]),
      address: new FormControl(null),
      information: new FormControl(null)
    });
    // this.departure_airport_control.patchValue({ value: null, disabled: this.disabled });
    // this.arrival_airport_control.patchValue({ value: null, disabled: this.disabled });
    let datebegin_ = this.data.activity.str_begin_datetime.split('T');
    let datebegin = datebegin_[0].split('-').concat(datebegin_[1].split(':'));
    let dateend_ = this.data.activity.str_end_datetime.split('T');
    let dateend = dateend_[0].split('-').concat(dateend_[1].split(':')); // yyyy mm dd hh mm ss
    this.activityForm.patchValue({
      datebegin: this.dateFromIsoString(this.data.activity.begin_datetime),
      timebegin: `${datebegin[3]}:${datebegin[4]}`,
      dateend: this.dateFromIsoString(this.data.activity.end_datetime),
      timeend: `${dateend[3]}:${dateend[4]}`,
      airport_id: this.data.activity.airport_id,
      end_airport_id: this.data.activity.end_airport_id,
      activityname_id: this.data.activity.activityname_id,
      information: this.data.activity.information
    });
    let airport_id;
    let end_airport_id;
    try {
      if (!isNullOrUndefined(this.data.activity.airport_id)) {
        airport_id = await this.airportService.getOneById(
          this.data.activity.airport_id
        );
        this.departure_airport_control.patchValue(airport_id);
      } else {
        airport_id = null;
      }
      if (!isNullOrUndefined(this.data.activity.end_airport_id)) {
        end_airport_id = await this.airportService.getOneById(
          this.data.activity.end_airport_id
        );
        this.arrival_airport_control.patchValue(end_airport_id);
      } else {
        end_airport_id = null;
      }
    } catch (err) {
      console.error(err);
    }
    // console.log(await airport_id);
    let airports;
    try {
      airports = this.airportService.getWithCodefilterLimit(100, '');
    } catch (err) {
      console.error(err);
    }
    this.airports = isNullOrUndefined(await airports) ? [] : await airports;
    this.filtered_departure_airport = this.departure_airport_control.valueChanges.pipe(
      startWith<string | Airport | null>(''),
      map(value =>
        typeof value === 'string'
          ? value
          : isNullOrUndefined(value)
          ? ''
          : value.code
      ),
      map(code =>
        code
          ? this._filter(code)
          : isNullOrUndefined(this.airports)
          ? []
          : this.airports.slice()
      )
    );
    this.filtered_arrival_airport = this.arrival_airport_control.valueChanges.pipe(
      startWith<string | any>(''),
      map(value =>
        typeof value === 'string'
          ? value
          : isNullOrUndefined(value)
          ? ''
          : value.code
      ),
      map(code =>
        code
          ? this._filter(code)
          : isNullOrUndefined(this.airports)
          ? []
          : this.airports.slice()
      )
    );
  }

  async onDownloadUserFile(name) {
    this.spinnerService.show();
    try {
      let blob = this.activityService.downloadOneById(this.data.activity.id);
      blob.subscribe(event => {
        if (event.type === HttpEventType.DownloadProgress) {
        } else if (event instanceof HttpResponse) {
          this.spinnerService.hide();
          importedSaveAs(event.body, name);
        }
      });
    } catch (err) {
      this.spinnerService.hide();
    }
  }

  onBuy() {
    // console.log(this.data.activity)
    let str = 'http://hydra.aviasales.ru/?marker=199129';
    str += `&origin_iata=${this.data.activity.airport.code}`;
    str += `&destination_iata=${this.data.activity.end_airport.code}`;
    let depart_date = this.dateFromIsoString(this.data.activity.begin_datetime);
    str += `&depart_date=${depart_date.getFullYear()}-${depart_date.getMonth() +
      1}-${depart_date.getDate()}`;
    str += `&with_request=true&adults=1&children=0&infants=0&trip_class=0&locale=ru&one_way=true&currency=rub`;
    window.open(str, '_blank');
  }

  onFileChange(event) {
    this.activeFile = event.srcElement.files[0];
  }

  onSave() {
    let obj = {};
    obj['flightpoint1_id'] = isNullOrUndefined(this.flightpoint1_id)
      ? null
      : this.flightpoint1_id;
    obj['flightpoint2_id'] = isNullOrUndefined(this.flightpoint2_id)
      ? null
      : this.flightpoint2_id;
    obj['scenario_id'] = this.data.scenario_id;
    obj['activityname_id'] = this.activityForm.getRawValue().activityname_id;
    obj['address'] = this.activityForm.getRawValue().address;
    obj['information'] = this.activityForm.getRawValue().information;
    obj['airport_id'] = isNullOrUndefined(this.departure_airport_control.value)
      ? null
      : this.departure_airport_control.value.id;
    obj['end_airport_id'] = isNullOrUndefined(
      this.arrival_airport_control.value
    )
      ? null
      : this.arrival_airport_control.value.id;
    obj['information'] = this.activityForm.getRawValue().information;
    let begin_datetime = new Date(this.activityForm.getRawValue().datebegin);
    begin_datetime.setHours(
      this.activityForm.getRawValue().timebegin.split(':')[0]
    );
    begin_datetime.setMinutes(
      this.activityForm.getRawValue().timebegin.split(':')[1] -
        begin_datetime.getTimezoneOffset()
    );
    obj['begin_datetime'] = begin_datetime.toUTCString();
    let end_datetime = new Date(this.activityForm.getRawValue().dateend);
    end_datetime.setHours(
      this.activityForm.getRawValue().timeend.split(':')[0]
    );
    end_datetime.setMinutes(
      this.activityForm.getRawValue().timeend.split(':')[1] -
        end_datetime.getTimezoneOffset()
    );
    obj['end_datetime'] = end_datetime.toUTCString();
    // this.dialogRef.close(obj);
    this.dialogRef.close({
      form: obj,
      file: this.activeFile,
      deleteFile: this.deleteFile
    });
  }

  onDel() {
    this.dialogRef.close('delete');
  }

  onClose(): void {
    this.dialogRef.close();
  }

  private _filter(code: string): any[] {
    const filterValue = code.toLowerCase();
    return isNullOrUndefined(this.airports)
      ? null
      : this.airports.filter(
          airport => airport.code.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  inputLink_departure() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.departure_airport_control.value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_departure_airport = this.departure_airport_control.valueChanges.pipe(
            startWith<string | Airport | null>(''),
            map(value =>
              typeof value === 'string'
                ? value
                : isNullOrUndefined(value)
                ? ''
                : value.code
            ),
            map(code =>
              code
                ? this._filter(code)
                : isNullOrUndefined(this.airports)
                ? []
                : this.airports.slice()
            )
          );
        } catch {}
      }
    }, 250);
  }

  inputLink_arrival() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.arrival_airport_control.value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_arrival_airport = this.arrival_airport_control.valueChanges.pipe(
            startWith<string | any>(''),
            map(value =>
              typeof value === 'string'
                ? value
                : isNullOrUndefined(value)
                ? ''
                : value.code
            ),
            map(code =>
              code
                ? this._filter(code)
                : isNullOrUndefined(this.airports)
                ? []
                : this.airports.slice()
            )
          );
        } catch {}
      }
    }, 250);
  }

  dateFromIsoString(str: string | Date) {
    // 2018-09-14T23:00:00
    if (typeof str === 'string') {
      let _arr = str.split('.')[0].split('T');
      let arr = _arr[0].split('-').concat(_arr[1].split(':')); // yyyy mm dd hh mm ss
      return new Date(+arr[0], +arr[1] - 1, +arr[2], +arr[3], +arr[4]);
    } else {
      return new Date(str);
    }
  }

  displayFn(airport?: any): string | undefined {
    return airport ? airport.code : undefined;
  }

  isValid(): boolean {
    let res = true;
    if (!this.activityForm.valid) {
      res = false;
    }
    if (
      this.activityForm.getRawValue().activityname_id === 2 &&
      (isNullOrUndefined(this.arrival_airport_control.value) ||
        isNullOrUndefined(this.departure_airport_control.value) ||
        isNullOrUndefined(this.arrival_airport_control.value.id) ||
        isNullOrUndefined(this.departure_airport_control.value.id))
    ) {
      res = false;
    }
    return res;
  }

  async getPrice() {
    let str = '';
    str += `?sorting=price&currency=rub&limit=1`;
    str += `&origin=${this.departure_airport_control.value.code}`;
    str += `&destination=${this.arrival_airport_control.value.code}`;
    let depart_date = this.dateFromIsoString(
      new Date(this.activityForm.getRawValue().datebegin)
    );
    str += `&beginning_of_period=${depart_date.getFullYear()}-${depart_date.getMonth() +
      1}-${depart_date.getDate()}`;
    str += `&with_request=true&adults=1&children=0&infants=0&trip_class=0&locale=ru&one_way=true&currency=rub`;
    // this.spinnerService.show();
    try {
      let res = await this.travelapiService.get(str);
      if (
        !isNullOrUndefined(res) &&
        !isNullOrUndefined(res['data']) &&
        !isNullOrUndefined(res['data'][0])
      ) {
        this.price = `${res['data'][0]['value']} руб.`;
      } else {
        this.price = '';
      }
    } catch (err) {
      this.price = '';
    } finally {
      // this.spinnerService.hide();
    }
  }
}
