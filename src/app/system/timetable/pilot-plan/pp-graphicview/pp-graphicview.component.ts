import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  NgZone,
  HostListener,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import { Flight } from 'src/app/shared/models/flight.model';
import { Flighttemplate } from 'src/app/shared/models/flighttemplate.model';
import { Specificflight } from 'src/app/shared/models/specificflight.model';
import { Flightpoint } from 'src/app/shared/models/flightpoint.model';
import {
  Activity,
  Airuseractivity
} from 'src/app/shared/models/activity.model';
import { Airuser } from 'src/app/shared/models/airuser.model';
import { Scenario } from 'src/app/shared/models/scenario.model';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatMenuTrigger, MatDialog, MatSnackBar } from '@angular/material';
import { AuthService } from 'src/app/shared/services/auth.service';
import { AircraftService } from 'src/app/system/shared/services/aircraft.service';
import { AircraftmodelService } from 'src/app/system/shared/services/aircraftmodel.service';
import { AircraftmodelgroupService } from 'src/app/system/shared/services/aircraftmodelgroup.service';
import { TimetablestoreService } from 'src/app/system/shared/services/timetablestore.service';
import { FlightService } from 'src/app/system/shared/services/flight.service';
import { SpecificflightService } from 'src/app/system/shared/services/specificflight.service';
import { FlighttemplateService } from 'src/app/system/shared/services/flighttemplate.service';
import { FlightpointService } from 'src/app/system/shared/services/flightpoint.service';
import { ActivityService } from 'src/app/system/shared/services/activity.service';
import { AirportService } from 'src/app/system/shared/services/airport.service';
import { ScenarioService } from 'src/app/system/shared/services/scenario.service';
import { DictionaryService } from 'src/app/system/shared/services/dictionary.service';
import { AiruserService } from 'src/app/system/shared/services/airuser.service';
import { PositionService } from 'src/app/system/shared/services/position.service';
import { SubdivisionService } from 'src/app/system/shared/services/subdivision.service';
import { GlobalsettingsService } from 'src/app/system/shared/services/globalsettings.service';
import { WebsocketService, WS_API } from 'src/app/websocket';
import { NgxSpinnerService } from 'ngx-spinner';
import { isNullOrUndefined } from 'util';
import { Subdivision } from 'src/app/shared/models/subdivision.model';
import { Position } from 'src/app/shared/models/position.model';
import { Flightpointchain } from 'src/app/shared/models/flightpointchain.model';
import { FlightpointchainService } from 'src/app/system/shared/services/flightpointchain.service';
import { DialogActivityComponent } from '../dialog-activity/dialog-activity.component';
import { DialogProfileComponent } from 'src/app/system/organisation/airusers/dialog-profile/dialog-profile.component';
import { DialogEditactivityComponent } from '../dialog-editactivity/dialog-editactivity.component';
import { Airport } from 'src/app/shared/models/airport.model';
import { DialogYesnoComponent } from 'src/app/system/shared/components/dialog-yesno/dialog-yesno.component';
import { AccessstoreService } from 'src/app/system/shared/services/accessstore.service';
import { CertificatetypeService } from 'src/app/system/shared/services/certificatetype.service';

interface Tempchain {
  flightpoint_id: number;
  aircraft_idx: number;
  specificflight_idx: number;
  flightpoint_idx: number;
}

@Component({
  selector: 'app-pp-graphicview',
  templateUrl: './pp-graphicview.component.html',
  styleUrls: ['./pp-graphicview.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PpGraphicviewComponent implements OnInit, OnDestroy {
  pilotHeight = 0;
  allHeight = 0;
  aircraftmodelgroups = [];
  aircraftmodels = [];
  aircrafts = [];
  airports = [];

  flights: Flight[] = [];
  flighttempltates: Flighttemplate[] = [];
  specificflights: Specificflight[] = [];
  flightpoints: Flightpoint[] = [];
  activitys: Activity[];
  activitystmp: Activity[] = [];
  airuseractivitys: Airuseractivity[];

  pilots: Airuser[] = [];
  pilotTypes = [];

  pilotsView: Airuser[];
  subdivisions: Subdivision[];
  positions: Position[];
  subdivisionsView: Subdivision[];

  scenarioes: Scenario[];

  selectedIndex = 0; // индекс вкладки для выбора

  messages$: Observable<string>;
  timeout;

  filterForm: FormGroup;
  from: Date;
  to: Date;
  now: Date;

  accessLevel = 0; // 0 - none, 1 - view, 2 - edit

  scales = [10, 5, 2];
  scale = 0;
  scroll = 0;

  width; // ширина поля (по датам от и до)
  pastWidth; // ширина поля (по текущей даты)
  dayWidth; // ширина дня
  timeScale = []; // разделители времени
  dateScale = []; // разделители дат
  dayBlocks = []; // блоки дней

  intervalTime;

  outofaircrafts = []; // рейсы, которым не назначены ВС
  dragSpecificflight: Specificflight; // перетаскиваемый рейс

  @ViewChild('timelineArea', { static: false }) timelineArea: ElementRef;
  @ViewChild('timelineAreaPilot', { static: false })
  timelineAreaPilot: ElementRef;
  @ViewChild('areaPilot', { static: false }) areaPilot: ElementRef;

  @ViewChild(MatMenuTrigger, { static: false })
  private menuTrigger: MatMenuTrigger;
  selectedSpecificflight: Specificflight; // для контекстного меню

  contextmenuZone: string; // зона вызова контекстного меню (pilot, timetable)
  chainStarted = false; // переключатель режима создания цепочки контекстным меню
  chainStartedbtn = false; // переключатель режима создания цепочки ctrl
  clickOnflight = false; // признак контекстного меню у сегмента рейса
  clickOnchain = false; // признак контекстного меню у сегмента рейса в цепочке
  clickOntempchain = false; // признак контекстного меню у сегмента рейса в текущей цепочке
  tempFlightpoints: Tempchain[] = []; // массив id flightpoint в текущей цепочке
  /**selected... нужны для связывания методов, вызываемых по контексту и по клику */
  selectedFlightpoint1: Flightpoint;
  selectedFlightpoint2: Flightpoint;
  selectedAircraft_idx = -1;
  selectedSpecificflight_idx = -1;
  selectedFlightpoint_idx = -1;
  selectedAccessPilot = 0;
  chainId_hover = -1; // id цепочки, для подсветки при наведении на сегмент
  accessLevelchain = 0; // доступ к созданию цепочек

  selectedPilotId = -1; // id пилота, у которого вызвано контекстное меню
  mouseEvent: any;
  contextFlightpoints: Flightpoint[]; // перелеты, которые попадают в область вызова контекстного меню
  contextFlightpointchains: Flightpointchain[]; // цепочки, которые попадают в область вызова контекстного меню
  flightpointchains: Flightpointchain[]; // цепочки, которые попадают в область вызова контекстного меню
  selectedDate: Date;

  flightpointsInchains: Flightpoint[];

  timePrepare; // Мин
  timeRest; // Мин

  scrolling = false;
  timer;

  count = new Date();

  tempScenario: Scenario;

  activitynames: any[];

  certificatetypes: any[];

  timebeforelag = 60;
  timeafterlag = 30;

  detectTimer;

  hasActivitys = false;

  mouse_pilot_id: number;
  mouse_flightpoints_id: number[] = [];

  addMode = false; // Возможность задавать рейс по клику

  timeStamp = 0;

  constructor(
    private cdr: ChangeDetectorRef,
    public authService: AuthService,
    public accessstoreService: AccessstoreService,
    private aircraftService: AircraftService,
    private aircraftmodelService: AircraftmodelService,
    private aircraftmodelgroupService: AircraftmodelgroupService,
    private timetablestoreService: TimetablestoreService,
    private flightService: FlightService,
    private specificflightService: SpecificflightService,
    private flighttemplateService: FlighttemplateService,
    private flightpointService: FlightpointService,
    private flightpointchainService: FlightpointchainService,
    private activityService: ActivityService,
    private airportService: AirportService,
    private scenarioService: ScenarioService,
    private dictionaryService: DictionaryService,
    private airuserService: AiruserService,
    private positionService: PositionService,
    private subdivisionService: SubdivisionService,
    private globalsettingsService: GlobalsettingsService,
    private wsService: WebsocketService,
    private dialog: MatDialog,
    private spinnerService: NgxSpinnerService,
    private zone: NgZone,
    private snackBar: MatSnackBar,
    public certificatetypeService: CertificatetypeService
  ) {
    this.spinnerService.show();
    this.detectTimer = setInterval(() => {
      this.count = new Date();
      this.cdr.detectChanges();
    }, 1000);
  }

  ngOnDestroy(): void {
    clearInterval(this.detectTimer);
  }

  @HostListener('document:keydown', ['$event'])
  public _startChain(event: KeyboardEvent): void {
    if (
      (event.ctrlKey === true || event.metaKey === true) &&
      this.chainStarted === false &&
      event.type === 'keydown'
    ) {
      setTimeout(() => (this.chainStarted = true), 100);
      this.tempFlightpoints.length = 0;
      this.addMode = true;
    }
    event.stopPropagation();
  }
  @HostListener('document:keyup', ['$event'])
  public _stopChain(event: KeyboardEvent): void {
    if (this.chainStarted === true && event.type === 'keyup') {
      this.saveChain();
    }
    event.stopPropagation();
  }

  async ngOnInit() {
    this.spinnerService.show();
    let certificatetypes = this.certificatetypeService.getAll();
    let today = new Date();
    today.setDate(today.getDate() - 20);
    let tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 10); // по умолчанию фильтр на 10 дней с текущей даты
    /** Установка дат от и до из localstorage */
    let from = isNullOrUndefined(this.globalsettingsService.timetableFilter)
      ? null
      : new Date(this.globalsettingsService.timetableFilter.from);
    let to = isNullOrUndefined(this.globalsettingsService.timetableFilter)
      ? null
      : new Date(this.globalsettingsService.timetableFilter.to);
    this.filterForm = new FormGroup({
      sbdv: new FormControl(0),
      role: new FormControl(0),
      from: new FormControl(isNullOrUndefined(from) ? today : from, [
        Validators.required
      ]),
      to: new FormControl(isNullOrUndefined(to) ? tomorrow : to, [
        Validators.required
      ])
    });
    await this.getData();
    this.subdivisionsView = this.subdivisions.map(subdivision => {
      let res = false;
      if (subdivision.type === 1) {
        this.pilots.map(pilot => {
          if (
            this.positions.find(position => position.id === pilot.position_id)
              .subdivision_id === subdivision.id
          ) {
            res = true;
          }
        });
      }
      subdivision['disable'] = !res;
      return subdivision;
    });
    /**Подписка на сокет*/
    this.messages$ = this.wsService.addEventListener(WS_API.EVENTS.MESSAGES);
    this.messages$.subscribe(async data => {
      data = data.indexOf('action') !== -1 ? JSON.parse(data) : data;
      switch (data['resourse']) {
        case 'flightpoint':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights(false);
          }, 500);
          break;
        case 'specificflight':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights(false);
          }, 500);
          break;
        case 'flighttemplate':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights(false);
          }, 500);
          break;
        case 'flight':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights(false);
          }, 500);
          break;
        case 'activity':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights(false);
            this.getPilots(false);
          }, 500);
          break;
      }
    });
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add flight');
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add flighttemplate');
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add specificflight');
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add flightpoint');
    this.wsService.sendMessage(
      WS_API.COMMANDS.SEND_TEXT,
      'Add Airuseractivity'
    );
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add Activity');
    this.onSelectFilter();
    this.zone.runOutsideAngular(() => {
      document.addEventListener('contextmenu', (e: MouseEvent) => {
        e.preventDefault();
      });
    });
    /**Значение для скрола, масштаба таймлайна */
    this.scroll = isNullOrUndefined(this.globalsettingsService.timetableScroll)
      ? 0
      : +this.globalsettingsService.timetableScroll;
    this.scale = isNullOrUndefined(this.globalsettingsService.timetableScale)
      ? 0
      : +this.globalsettingsService.timetableScale;
    this.scenarioes.sort((a, b) => {
      return a.status > b.status ? 1 : -1;
    });
    await this.getFlights();
    await this.getPilots();
    this.certificatetypes = isNullOrUndefined(await certificatetypes)
      ? []
      : await certificatetypes;
    this.count = new Date();
    this.cdr.detectChanges();
    this.zone.runOutsideAngular(() => {
      this.timelineArea.nativeElement.addEventListener(
        'contextmenu',
        (e: MouseEvent) => {
          e.preventDefault();
        }
      );
      this.timelineAreaPilot.nativeElement.addEventListener(
        'contextmenu',
        (e: MouseEvent) => {
          e.preventDefault();
        }
      );
      // this.timelineArea.nativeElement.addEventListener(
      //   'wheel',
      //   (e: MouseEvent) => {
      //     // e.preventDefault();
      //     // e.stopPropagation();
      //   }
      // );
      this.timelineArea.nativeElement.addEventListener(
        'wheel',
        (e: MouseEvent) => {
          // e.preventDefault();
          clearTimeout(this.timer);
          if (!this.scrolling) {
            this.timer = setTimeout(() => {
              this.scrolling = true;
              this.timelineAreaPilot.nativeElement.scrollTo(
                this.timelineArea.nativeElement.scrollLeft,
                0
              );
              this.scrolling = false;
            }, 1);
          }
        }
      );
      this.timelineAreaPilot.nativeElement.addEventListener(
        'wheel',
        (e: MouseEvent) => {
          // e.preventDefault();
          clearTimeout(this.timer);
          if (e.shiftKey !== true) {
          } else if (!this.scrolling) {
            this.timer = setTimeout(() => {
              this.scrolling = true;
              this.timelineArea.nativeElement.scrollTo(
                this.timelineAreaPilot.nativeElement.scrollLeft,
                0
              );
              this.scrolling = false;
            }, 1);
          }
        }
      );
      this.timelineArea.nativeElement.addEventListener(
        'scroll',
        (e: MouseEvent) => {
          // console.log(e.timeStamp);
          // if (e.timeStamp - this.timeStamp < 100) {
          //   // e.preventDefault();
          //   return;
          // }
          // this.timeStamp = e.timeStamp;
          // e.preventDefault();
          // this.timelineAreaPilot.nativeElement.scrollTo(this.timelineArea.nativeElement.scrollLeft, 0);
          if (!this.scrolling) {
            clearTimeout(this.timer);
            this.timer = setTimeout(() => {
              this.scrolling = true;
              this.timelineAreaPilot.nativeElement.scrollTo(
                this.timelineArea.nativeElement.scrollLeft,
                0
              );
              this.scrolling = false;
            }, 1);
          }
          // if (!this.scrolling) {
          //   this.scrolling = true;
          //   this.timelineAreaPilot.nativeElement.scrollTo(this.timelineArea.nativeElement.scrollLeft, 0);
          //   this.scrolling = false;
          // }
        }
      );
      this.timelineAreaPilot.nativeElement.addEventListener(
        'scroll',
        (e: MouseEvent) => {
          // if (e.timeStamp - this.timeStamp < 100) {
          //   // e.preventDefault();
          //   return;
          // }
          // this.timeStamp = e.timeStamp;
          // e.preventDefault();
          // this.timelineArea.nativeElement.scrollTo(this.timelineAreaPilot.nativeElement.scrollLeft, 0);
          if (!this.scrolling) {
            clearTimeout(this.timer);
            this.timer = setTimeout(() => {
              this.scrolling = true;
              this.timelineArea.nativeElement.scrollTo(
                this.timelineAreaPilot.nativeElement.scrollLeft,
                0
              );
              this.scrolling = false;
            }, 1);
          }
          // if (!this.scrolling) {
          //   this.scrolling = true;
          //   this.timelineArea.nativeElement.scrollTo(this.timelineAreaPilot.nativeElement.scrollLeft, 0);
          //   this.scrolling = false;
          // }
        }
      );
    });

    this.timePrepare = 10 * this.scales[this.scale];
    this.timeRest = 60 * this.scales[this.scale];

    if (this.authService.loggedInUser.isadmin === true) {
      this.accessLevel = 2;
      this.accessLevelchain = 1;
    } else {
      let access = this.authService.loggedInUserAccesses.find(
        item => item.resourcename === 'timetable'
      );
      if (!isNullOrUndefined(access)) {
        if (access.get === true) {
          this.accessLevel = 1;
        }
        if (access.post === true) {
          this.accessLevel = 2;
        }
      }
      let chainaccesses = this.authService.loggedInUserAccesses.filter(
        item => item.resourcename === 'flightpointchain'
      );
      let chainaccess = chainaccesses.find(item => item.post === true);
      if (!isNullOrUndefined(chainaccess)) {
        this.accessLevelchain = 1;
      }
    }
    /**Создание (если нет) сценария для временного планирования */
    let tempScenario = this.scenarioService.getByAiruser(
      this.authService.loggedInUser.id
    );
    this.tempScenario = isNullOrUndefined(await tempScenario)
      ? null
      : (await tempScenario)[0];
    if (isNullOrUndefined(this.tempScenario)) {
      this.tempScenario = await this.scenarioService.postOne({
        name: 'Тестовый сценарий',
        status: 2,
        airuser_id: this.authService.loggedInUser.id
      });
    }
    this.pilotHeight = window.innerHeight - 270 - this.aircrafts.length * 61;
    this.allHeight = this.areaPilot.nativeElement.scrollHeight;
    this.spinnerService.hide();
    // console.log(this.pilotsView);
  }

  async getData() {
    this.pilotTypes = this.dictionaryService.pilotTypes;
    this.pilots = [];
    try {
      let aircraftmodelgroups = this.aircraftmodelgroupService.getAll();
      let aircraftmodels = this.aircraftmodelService.getAll();
      let aircrafts = this.aircraftService.getAll();
      let airports = this.airportService.getAll();
      let scenarioes = this.scenarioService.getAll();
      let activitynames = this.dictionaryService.getActivitynames();
      let _positions = this.positionService.getAll();
      let _subdivisions = this.subdivisionService.getAll();
      let _airusers = this.airuserService.getAll();
      let airusers = isNullOrUndefined(await _airusers) ? [] : await _airusers;
      this.positions = isNullOrUndefined(await _positions)
        ? []
        : await _positions;
      this.subdivisions = isNullOrUndefined(await _subdivisions)
        ? []
        : await _subdivisions;
      this.activitynames = isNullOrUndefined(await activitynames)
        ? []
        : await activitynames;
      airusers.map(airuser => {
        let position = this.positions.find(
          item => item.id === airuser.position_id
        );
        if (isNullOrUndefined(position)) {
          return false;
        }
        let subdivision = this.subdivisions.find(
          item => item.id === position.subdivision_id
        );
        if (isNullOrUndefined(subdivision)) {
          return false;
        }
        if (!isNullOrUndefined(airuser.pilot_type) && airuser.pilot_type > 0) {
          let pilotType = this.pilotTypes.find(
            item => item.id === airuser.pilot_type
          );
          if (!isNullOrUndefined(pilotType)) {
            airuser['pilotTypestr'] = pilotType.name;
            airuser[
              'airuseraccesstype'
            ] = this.accessstoreService.accessTypeForAiruser(
              airuser,
              this.positions,
              this.subdivisions,
              'airuser'
            );
            this.pilots.push(airuser);
          }
        }
      });
      // let res = await this.scenarioService.putOneById(1, { status: 1 })
      this.aircraftmodelgroups = isNullOrUndefined(await aircraftmodelgroups)
        ? []
        : await aircraftmodelgroups;
      this.aircraftmodels = isNullOrUndefined(await aircraftmodels)
        ? []
        : await aircraftmodels;
      this.aircrafts = isNullOrUndefined(await aircrafts)
        ? []
        : await aircrafts;
      this.airports = isNullOrUndefined(await airports) ? [] : await airports;
      this.scenarioes = isNullOrUndefined(await scenarioes)
        ? []
        : await scenarioes;
      this.scenarioes = this.scenarioes.filter(
        item =>
          item.status === 1 ||
          item.airuser_id === this.authService.loggedInUser.id
      );
    } catch (err) {
      console.error(`Ошибка ${err}`);
      this.openSnackBar(`${err.error}.`, 'Ok');
    }
  }

  async getFlights(init = true) {
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.from) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.from).valueOf())
      )
    ) {
      return;
    }
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.to) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.to).valueOf())
      )
    ) {
      return;
    }
    this.flights.length = 0;
    this.flighttempltates.length = 0;
    this.specificflights.length = 0;
    this.flightpoints.length = 0;
    if (init === true) {
      await this.getScale();
    }
    try {
      let __from = new Date(
        this.from.getFullYear(),
        this.from.getMonth(),
        this.from.getDate() - 1,
        0,
        0,
        0
      );
      let __to = new Date(
        this.to.getFullYear(),
        this.to.getMonth(),
        this.to.getDate() + 1,
        0,
        0,
        0
      );
      /**Получение одиночных рейсов и активностей в выбранном диапазоне */
      let _specificflights = this.specificflightService.getByDatesScenario(
        `${__from.getFullYear()}-${__from.getMonth() + 1}-${__from.getDate()}`,
        `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
        this.scenarioes.find(item => item.status === 1).id
      );
      let _activitys = this.activityService.getByDatesScenario(
        `${__from.getFullYear()}-${__from.getMonth() + 1}-${__from.getDate()}`,
        `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
        this.scenarioes.find(item => item.status === 1).id
      );
      let specificflights: Specificflight[] = isNullOrUndefined(
        await _specificflights
      )
        ? []
        : await _specificflights;
      /**Если выбран временный сенарий, то получаем активности и из него */
      // let activitystmp: Activity[] = [];
      let activitys: Activity[] = isNullOrUndefined(await _activitys)
        ? []
        : await _activitys;
      activitys = activitys.map(item => {
        item.status = 1;
        return item;
      });
      let _activitystmp = this.activityService.getByDatesScenario(
        `${__from.getFullYear()}-${__from.getMonth() + 1}-${__from.getDate()}`,
        `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
        this.scenarioes.find(
          item => item.airuser_id === this.authService.loggedInUser.id
        ).id
      );
      this.activitystmp = isNullOrUndefined(await _activitystmp)
        ? []
        : await _activitystmp;
      if (this.activitystmp.length > 0) {
        this.hasActivitys = true;
      } else {
        this.hasActivitys = false;
      }
      this.activitystmp = this.activitystmp.map(item => {
        item.status = 2;
        return item;
      });
      if (this.selectedIndex === 1) {
        activitys = [...this.activitystmp];
        // console.log(this.activitys);
        // console.log(this.now);
        // activitys = [...activitys, ...this.activitystmp];
      }
      // let activitys: Airuseractivity[] = (isNullOrUndefined(await _activitys)) ? [] : await _activitys;

      /**Получение связанных с ними данных по массивам id */
      let specificflights_id = specificflights.map(item => item.id);
      let flights_id: number[] = specificflights.map(item => item.flight_id);
      let flighttemplates_id: number[] = specificflights.map(
        item => item.flighttemplate_id
      );
      let used = {};
      flights_id = flights_id.filter(obj => {
        return obj in used ? 0 : (used[obj] = 1);
      });
      used = {};
      flighttemplates_id = flighttemplates_id.filter(obj => {
        return obj in used ? 0 : (used[obj] = 1);
      });
      if (isNullOrUndefined(activitys[0])) {
        activitys = [];
      }
      // let activitys_id: number[] = activitys.map(item => item.id);
      let _flights = this.flightService.getByIds(flights_id);
      let _flighttemplates = this.flighttemplateService.getByIds(
        flighttemplates_id
      );
      // let _flightpoints = this.flightpointService.getBySpecificflights(
      //   specificflights_id
      // );
      // let _airuseractivitys = this.airuseractivityService.getByActivitys(
      //   activitys_id
      // );
      let chunk = 300;
      let arrspecificflights_id = [];
      for (let i = 0, j = specificflights_id.length; i < j; i += chunk) {
        arrspecificflights_id.push(specificflights_id.slice(i, i + chunk));
      }

      let flightpoints = [];
      await Promise.all(
        arrspecificflights_id.map(async ids => {
          let _res = await this.flightpointService.getBySpecificflights(ids);
          let res = isNullOrUndefined(await _res) ? [] : await _res;
          flightpoints = [...flightpoints, ...res];
        })
      );
      let flights: Flight[] = isNullOrUndefined(await _flights)
        ? []
        : await _flights;
      let flighttemplates: Flighttemplate[] = isNullOrUndefined(
        await _flighttemplates
      )
        ? []
        : await _flighttemplates;
      // let flightpoints: Flightpoint[] = isNullOrUndefined(await _flightpoints)
      //   ? []
      //   : await _flightpoints;
      // let airuseractivitys: Airuseractivity[] = isNullOrUndefined(
      //   await _airuseractivitys
      // )
      //   ? []
      //   : await _airuseractivitys;
      let res = await this.timetablestoreService.getAircrafts(
        this.aircrafts,
        flights,
        flighttemplates,
        specificflights,
        flightpoints,
        activitys,
        // airuseractivitys,
        this.pilots,
        this.from,
        this.to,
        this.scales[this.scale]
      );
      this.aircrafts = res.aircrafts;
      this.aircrafts.map(item => {
        let idx = [];
        for (let i = 0; i < item.specificflights.length; i++) {
          if (
            isNullOrUndefined(item.specificflights[i].flightpoints) ||
            item.specificflights[i].flightpoints.length === 0 ||
            +this.dateFromIsoString(
              item.specificflights[i].calc_arrival_datatime
            ) < +this.from
          ) {
            idx.push(i);
          }
        }
        idx.sort((a, b) => b - a);
        for (let i = 0; i < idx.length; i++) {
          item.specificflights.splice(idx[i], 1);
        }
      });
      this.flights = res.flights;
      this.flighttempltates = res.flighttemplates;
      this.specificflights = res.specificflights;
      this.flightpoints = res.flightpoints;
      let flightpointsInchains = this.flightpointService.getByFlightpointchains(
        res.flightpointchainIds
      );
      this.flightpointsInchains = isNullOrUndefined(await flightpointsInchains)
        ? []
        : await flightpointsInchains;
    } catch (err) {
      console.error(err);
    }
  }

  async getPilots(init = true) {
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.from) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.from).valueOf())
      )
    ) {
      return;
    }
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.to) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.to).valueOf())
      )
    ) {
      return;
    }
    try {
      let __from = new Date(
        this.from.getFullYear(),
        this.from.getMonth(),
        this.from.getDate() - 1,
        0,
        0,
        0
      );
      let __to = new Date(
        this.to.getFullYear(),
        this.to.getMonth(),
        this.to.getDate() + 1,
        0,
        0,
        0
      );
      /**Получение активностей в выбранном диапазоне */
      let _activitys = this.activityService.getByDatesScenario(
        `${__from.getFullYear()}-${__from.getMonth() + 1}-${__from.getDate()}`,
        `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
        this.scenarioes.find(item => item.status === 1).id
      );
      /**Если выбран временный сенарий, то получаем активности и из него */
      // let activitystmp: Activity[] = [];
      let activitys: Activity[] = isNullOrUndefined(await _activitys)
        ? []
        : await _activitys;
      activitys = activitys.map(item => {
        item.status = 1;
        return item;
      });
      if (this.selectedIndex === 1) {
        let _activitystmp = this.activityService.getByDatesScenario(
          `${__from.getFullYear()}-${__from.getMonth() +
            1}-${__from.getDate()}`,
          `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
          this.scenarioes.find(
            item => item.airuser_id === this.authService.loggedInUser.id
          ).id,
          [this.scenarioes.find(item => item.status === 1).id]
        );
        this.activitystmp = isNullOrUndefined(await _activitystmp)
          ? []
          : await _activitystmp;
        this.activitystmp = this.activitystmp.map(item => {
          item.status = 2;
          return item;
        });
        activitys = [...this.activitystmp];
        // activitys = [...activitys, ...this.activitystmp];
      }
      /**Получение связанных с ними данных по массивам id */
      if (isNullOrUndefined(activitys[0])) {
        activitys = [];
      }
      // let activitys_ids = activitys.map(activity => activity.id);
      // let _airuseractivitys = this.airuseractivityService.getByActivitys(
      //   activitys_ids
      // );
      // let airuseractivitys: Airuseractivity[] = isNullOrUndefined(
      //   await _airuseractivitys
      // )
      //   ? []
      //   : await _airuseractivitys;
      let res = await this.timetablestoreService.getPilots(
        this.pilots,
        activitys,
        // airuseractivitys,
        this.flightpoints,
        this.positions,
        this.subdivisions,
        this.authService.loggedInUser,
        this.authService.loggedInUserAccesses,
        this.from,
        this.to,
        this.scales[this.scale]
      );
      this.pilots = res.pilots;
      this.activitys = res.activitys;
      this.activitys.map(item => {
        item.airport = this.getAirportByid(item.airport_id);
        item.end_airport = this.getAirportByid(item.end_airport_id);
      });
      /**добавляем в активности с цепочками лэги */
      this.pilots.map(pilot => {
        if (!isNullOrUndefined(pilot.home_airport_id)) {
          pilot.home_airport = this.getAirportByid(pilot.home_airport_id);
        }
        pilot.activitys.map((activity, indexact) => {
          if (
            !isNullOrUndefined(activity.flightpointchain_id) &&
            activity.flightpointchain_id > 0
          ) {
            activity['flightpoints'] = this.getFlightpointsByFlightpointchain(
              activity.flightpointchain_id
            );
          }

          activity['_rest'] = this.getRestTime(activity, pilot);
          if (indexact > 0) {
            let timebtwmin =
              (+this.dateFromIsoString(activity.str_begin_datetime) -
                +this.dateFromIsoString(
                  pilot.activitys[indexact - 1].str_end_datetime
                )) /
              (60 * 1000);
            timebtwmin -= this.timebeforelag + this.timeafterlag;
            // if (activity.activityname_id === 6) {
            if (pilot.activitys[indexact - 1].activityname_id === 6) {
              if (timebtwmin < pilot.activitys[indexact - 1]._rest.restMin) {
                activity['_rest']['danger'] = true;
                activity['_rest']['warning'] = false;
              } else if (
                timebtwmin < pilot.activitys[indexact - 1]._rest.restMax
              ) {
                activity['_rest']['warning'] = true;
                activity['_rest']['danger'] = false;
              } else {
                activity['_rest']['warning'] = false;
                activity['_rest']['danger'] = false;
              }
            } else {
              activity['_rest']['warning'] = false;
              activity['_rest']['danger'] = false;
            }
            // }
          }
        });
      });
    } catch (err) {
      console.error(err);
    } finally {
      // this.spinnerService.hide()
    }
  }

  async setScale(type = true) {
    this.spinnerService.show();
    if (type === true) {
      this.globalsettingsService.timetableScale = ++this.scale;
    } else {
      this.globalsettingsService.timetableScale = --this.scale;
    }
    await this.updateData(true);
  }

  getScale() {
    if (
      !(
        new Date(this.filterForm.value.from) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.from).valueOf())
      )
    ) {
      return;
    }
    if (
      !(
        new Date(this.filterForm.value.to) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.to).valueOf())
      )
    ) {
      return;
    }
    this.timeScale.length = 0;
    this.dateScale.length = 0;
    this.dayBlocks.length = 0;
    this.outofaircrafts.length = 0;
    this.flights.length = 0;
    this.flighttempltates.length = 0;
    this.specificflights.length = 0;
    this.flightpoints.length = 0;
    this.dayWidth = 0;
    this.pastWidth = 0;
    delete this.now;
    let _from = new Date(this.filterForm.value.from);
    this.from = new Date(
      _from.getFullYear(),
      _from.getMonth(),
      _from.getDate(),
      0,
      0,
      0
    );
    let _to = new Date(this.filterForm.value.to);
    this.to = new Date(
      _to.getFullYear(),
      _to.getMonth(),
      _to.getDate(),
      0,
      0,
      0
    );
    let __to = new Date(
      this.to.getFullYear(),
      this.to.getMonth(),
      this.to.getDate() + 1,
      0,
      0,
      0
    );
    let res = +__to - +this.from;
    let _res = res / (1000 * 60 * this.scales[this.scale]);
    if (_res > 1000000) {
      this.openSnackBar(`Выбран слишком большой временной промежуток.`, 'Ok');
      return;
    }
    let s_filterfrom = new Date(this.from);
    s_filterfrom.setMinutes(
      s_filterfrom.getMinutes() - s_filterfrom.getTimezoneOffset()
    );
    let s_filterto = new Date(this.to);
    s_filterto.setMinutes(
      s_filterto.getMinutes() - s_filterto.getTimezoneOffset()
    );
    this.globalsettingsService.timetableFilter = {
      from: s_filterfrom.toISOString(),
      to: s_filterto.toISOString()
    };
    this.width = { str: `${_res}px`, val: _res };
    this.now = new Date();
    this.now.setMinutes(this.now.getMinutes() + this.now.getTimezoneOffset());
    clearInterval(this.intervalTime);
    if (this.now < this.from) {
      this.pastWidth = { str: `0px`, val: 0 };
    } else if (__to < this.now) {
      this.pastWidth = this.width;
    } else {
      this.pastWidth = this.getWidth(this.from, this.now);
      this.intervalTime = setInterval(() => {
        this.now = new Date();
        this.now.setMinutes(
          this.now.getMinutes() + this.now.getTimezoneOffset()
        );
        this.pastWidth = this.getWidth(this.from, this.now);
      }, 10000);
    }
    this.dayWidth = {
      str: `${24 * (60 / this.scales[this.scale]) - 0}px`,
      val: 24 * (60 / this.scales[this.scale]) - 0
    };
    let dev = +__to - +this.from;
    let _resScale = dev / (1000 * 60 * 60 * 24);
    let _width = Math.floor(this.width.val / 95);
    for (let i = 0; i < _width + 1; i++) {
      let _d =
        this.from.getTime() + (i + 1) * this.scales[this.scale] * 60000 * 95;
      this.timeScale.push(new Date(_d));
    }
    for (let i = 0; i < _resScale; i++) {
      this.dateScale.push({
        val: i * 24 * (60 / this.scales[this.scale]),
        _val: i * 24 * (60 / this.scales[this.scale]),
        date: new Date(
          this.from.getFullYear(),
          this.from.getMonth(),
          this.from.getDate() + i
        ),
        day: new Date(
          this.from.getFullYear(),
          this.from.getMonth(),
          this.from.getDate() + i
        ).toLocaleString('ru', { weekday: 'short' })
      });
    }
  }

  getWidth(date1, date2) {
    // ширина по датам
    let from = this.timetablestoreService.dateFromIsoString(date1);
    let to = this.timetablestoreService.dateFromIsoString(date2);
    let res = +to - +from;
    let _res = res / (1000 * 60 * this.scales[this.scale]);
    return { str: `${_res}px`, val: _res };
  }

  async selectScenario(event) {
    if (event.index > 0) {
      this.from = new Date(
        this.now.getFullYear(),
        this.now.getMonth(),
        this.now.getDate(),
        0,
        0,
        0
      );
      if (this.from > this.to) {
        this.to = new Date(
          this.now.getFullYear(),
          this.now.getMonth(),
          this.now.getDate() + 30,
          0,
          0,
          0
        );
      }
      this.filterForm.patchValue({ from: this.from, to: this.to });
    }
    // this.spinnerService.show();
    await this.updateData(false, true);
    // this.spinnerService.hide();
  }

  async updateData(type: boolean = true, spinner = false) {
    if (type === true) {
      if (
        isNullOrUndefined(this.filterForm.value.from) ||
        !(
          new Date(this.filterForm.value.from) instanceof Date &&
          !isNaN(new Date(this.filterForm.value.from).valueOf())
        )
      ) {
        return;
      }
      if (
        isNullOrUndefined(this.filterForm.value.from) ||
        !(
          new Date(this.filterForm.value.to) instanceof Date &&
          !isNaN(new Date(this.filterForm.value.to).valueOf())
        )
      ) {
        return;
      }
      let _from = new Date(this.filterForm.value.from);
      let from = new Date(
        _from.getFullYear(),
        _from.getMonth(),
        _from.getDate(),
        0,
        0,
        0
      );
      let _to = new Date(this.filterForm.value.to);
      let to = new Date(
        _to.getFullYear(),
        _to.getMonth(),
        _to.getDate(),
        0,
        0,
        0
      );

      if (+this.to === +to && +this.from === +from) {
        return;
      }
    }
    if (spinner === true) {
      this.spinnerService.show();
    }
    await this.resetScroll();
    await this.getFlights();
    await this.getPilots();
    this.spinnerService.hide();
  }

  onSelectFilter() {
    if (+this.filterForm.value.sbdv === 0) {
      this.pilotsView = [...this.pilots];
    } else {
      this.pilotsView.length = 0;
      this.pilots.map(pilot => {
        let pos = this.positions.find(
          position => position.id === pilot.position_id
        );
        if (pos.subdivision_id === +this.filterForm.value.sbdv) {
          this.pilotsView.push(pilot);
        }
      });
    }
    if (+this.filterForm.value.role !== 0) {
      this.pilotsView = this.pilotsView.filter(pilot => {
        return pilot.pilot_type === +this.filterForm.value.role;
      });
    }
    this.pilotsView.sort((a, b) => {
      if (a.pilot_type < b.pilot_type) {
        return -1;
      } else if (a.pilot_type > b.pilot_type) {
        return 1;
      } else {
        return a.surname < b.surname ? -1 : 1;
      }
    });
    this.pilotsView.sort((a, b) => (a.surname > b.surname ? 1 : -1));
    this.pilotsView.sort((a, b) => {
      if (
        a.pilot_type === 1 ||
        a.pilot_type === 2 ||
        a.pilot_type === 3 ||
        a.pilot_type === 9
      ) {
        a['basetype'] = 1;
      } else if (
        a.pilot_type === 4 ||
        a.pilot_type === 5 ||
        a.pilot_type === 6
      ) {
        a['basetype'] = 2;
      } else if (a.pilot_type === 7 || a.pilot_type === 8) {
        a['basetype'] = 3;
      }
      if (
        b.pilot_type === 1 ||
        b.pilot_type === 2 ||
        b.pilot_type === 3 ||
        b.pilot_type === 9
      ) {
        b['basetype'] = 1;
      } else if (
        b.pilot_type === 4 ||
        b.pilot_type === 5 ||
        b.pilot_type === 6
      ) {
        b['basetype'] = 2;
      } else if (a.pilot_type === 7 || a.pilot_type === 8) {
        b['basetype'] = 3;
      }
      return b['basetype'] - a['basetype'];
    });
  }

  onContext(
    event: any,
    airuser_id: number,
    date,
    aicraft_idx: number,
    specificflight_idx: number,
    flightpoint_idx: number,
    flightpoint1: Flightpoint,
    flightpoint2: Flightpoint,
    access: number
  ) {
    if (this.contextmenuZone === 'pilot') {
      if (
        access < 1 ||
        (this.selectedIndex === 0 &&
          this.authService.loggedInUser.isadmin === false)
      ) {
        return;
      }
      this.selectedAccessPilot = access;
      this.selectedPilotId = airuser_id;
      this.selectedDate = new Date(
        date.date.getFullYear(),
        date.date.getMonth(),
        date.date.getDate() + 1,
        0,
        this.scales[this.scale] * event.layerX
      );
      this.contextFlightpoints = this.getFlightpointsByevent(event);
      this.contextFlightpointchains = this.getFlightpointchainsByevent(event);
    } else if (this.contextmenuZone === 'timetable') {
      if (this.accessLevelchain < 1) {
        return;
      }
      if (this.clickOnflight === true) {
        if (!isNullOrUndefined(flightpoint2.calc_departure_datatime)) {
          flightpoint2 = flightpoint1;
        }
        this.selectedFlightpoint1 = flightpoint1;
        this.selectedFlightpoint2 = flightpoint2;
        this.selectedAircraft_idx = aicraft_idx;
        this.selectedSpecificflight_idx = specificflight_idx;
        this.selectedFlightpoint_idx = flightpoint_idx;
        if (
          !isNullOrUndefined(flightpoint1) &&
          !isNullOrUndefined(flightpoint1.flightpointchain_id)
        ) {
          this.clickOnchain = true;
        } else {
          this.clickOnchain = false;
        }
        if (
          !isNullOrUndefined(this.selectedFlightpoint1) &&
          this.isFlightpointSelected(this.selectedFlightpoint1.id)
        ) {
          this.clickOntempchain = true;
        } else {
          this.clickOntempchain = false;
        }
      } else {
        this.selectedFlightpoint1 = {};
        this.selectedFlightpoint2 = {};
        this.selectedAircraft_idx = null;
        this.selectedSpecificflight_idx = null;
        this.selectedFlightpoint_idx = null;
      }
    } else if (this.contextmenuZone === 'airuser') {
      this.selectedPilotId = airuser_id;
    }
    let menu = document.getElementById('menuBtn');
    menu.style.display = '';
    menu.style.position = 'absolute';
    menu.style.left = event.pageX + 5 + 'px';
    menu.style.top = event.pageY + -10 + 'px';
    this.menuTrigger.openMenu();
    this.mouseEvent = event;
  }

  onChainhover(id: number) {
    if (!isNullOrUndefined(id)) {
      this.chainId_hover = id;
    }
  }

  onaddFlightpointTochain(
    flightpoint1: Flightpoint,
    flightpoint2: Flightpoint,
    aircraft_idx,
    specificflight_idx,
    flightpoint_idx
  ) {
    // if (!isNullOrUndefined(flightpoint2.calc_departure_datatime)) {
    //   flightpoint2 = flightpoint1;
    // }
    if (
      !isNullOrUndefined(flightpoint1) &&
      !isNullOrUndefined(flightpoint1.flightpointchain_id)
    ) {
      this.clickOnchain = true;
    } else {
      this.clickOnchain = false;
    }
    if (
      !isNullOrUndefined(flightpoint1) &&
      this.isFlightpointSelected(flightpoint1.id) &&
      !isNullOrUndefined(flightpoint2) &&
      this.isFlightpointSelected(flightpoint2.id)
    ) {
      this.clickOntempchain = true;
    } else {
      this.clickOntempchain = false;
    }
    if (
      this.contextmenuZone === 'timetable' &&
      this.chainStarted === true &&
      this.clickOnflight === true &&
      this.clickOnchain === false &&
      this.clickOntempchain === false
    ) {
      this.addFlightpointTochain(
        flightpoint1,
        flightpoint2,
        aircraft_idx,
        specificflight_idx,
        flightpoint_idx
      );
    } else if (
      this.contextmenuZone === 'timetable' &&
      this.chainStarted === true &&
      this.clickOnflight === true &&
      this.clickOnchain === false &&
      this.clickOntempchain === true
    ) {
      this.selectedFlightpoint1 = flightpoint1;
      this.selectedFlightpoint2 = flightpoint2;
      this.delFlightpointFromchain();
    }
  }

  addFlightpointTochain(
    selectedFlightpoint1,
    selectedFlightpoint2,
    aircraft_idx,
    specificflight_idx,
    flightpoint_idx
  ) {
    let tempchain1: Tempchain = {
      flightpoint_id: selectedFlightpoint1.id,
      aircraft_idx,
      specificflight_idx,
      flightpoint_idx
    };
    let tempchain2: Tempchain = {
      flightpoint_id: selectedFlightpoint2.id,
      aircraft_idx,
      specificflight_idx,
      flightpoint_idx
    };
    this.tempFlightpoints.push(tempchain1);
    this.tempFlightpoints.push(tempchain2);
  }

  getFlightpointsByevent(event: any): Flightpoint[] {
    let _x =
      +event.clientX +
      +this.scroll -
      +this.timelineArea.nativeElement.getBoundingClientRect().left;
    let flightpoints: Flightpoint[] = this.flightpoints.filter(_flightpoint => {
      if (+_flightpoint._left < _x && +_flightpoint._right > _x) {
        _flightpoint.flight_number = this.flights.find(
          flight =>
            flight.id ===
            this.specificflights.find(
              specificflight =>
                specificflight.id === _flightpoint.specificflight_id
            ).flight_id
        ).flight_number;
        return [];
      }
    });
    return flightpoints;
  }

  getFlightpointchainsByevent(event: any): Flightpointchain[] {
    let flightpoints = this.getFlightpointsByevent(event);
    let flightpointchains = [];
    for (let i = 0; i < flightpoints.length; i++) {
      if (!isNullOrUndefined(flightpoints[i].flightpointchain_id)) {
        let flightpointchain: Flightpointchain = new Flightpointchain();
        let flightpointsinchain = this.flightpointsInchains.filter(
          item =>
            item.flightpointchain_id === flightpoints[i].flightpointchain_id
        );
        let departure_flightpointsinchain = flightpointsinchain.filter(
          item => !isNullOrUndefined(item.calc_departure_datatime)
        );
        let arrival_flightpointsinchain = flightpointsinchain.filter(
          item => !isNullOrUndefined(item.calc_arrival_datatime)
        );
        departure_flightpointsinchain.sort((a, b) =>
          this.timetablestoreService.dateFromIsoString(
            a.calc_departure_datatime
          ) >
          this.timetablestoreService.dateFromIsoString(
            b.calc_departure_datatime
          )
            ? 1
            : -1
        );
        arrival_flightpointsinchain.sort((a, b) =>
          this.timetablestoreService.dateFromIsoString(
            a.calc_arrival_datatime
          ) >
          this.timetablestoreService.dateFromIsoString(b.calc_arrival_datatime)
            ? 1
            : -1
        );
        flightpointchain.flightpoint_1 = departure_flightpointsinchain[0];
        flightpointchain.flightpoint_2 =
          arrival_flightpointsinchain[arrival_flightpointsinchain.length - 1];
        let str = '';
        for (let j = 0; j < departure_flightpointsinchain.length; j++) {
          str += this.getAirportByid(
            departure_flightpointsinchain[j].airport_id
          ).code;
          str += '-';
        }
        str += this.getAirportByid(
          arrival_flightpointsinchain[arrival_flightpointsinchain.length - 1]
            .airport_id
        ).code;
        flightpointchain.name = str;
        flightpointchains.push(flightpointchain);
      }
    }
    return flightpointchains;
  }

  geyDateByevent(event: any): string {
    let _x =
      +event.clientX +
      +this.scroll -
      +this.timelineArea.nativeElement.getBoundingClientRect().left;

    return 'flightpoints';
  }

  delFlightpointFromchain() {
    this.tempFlightpoints.splice(
      this.tempFlightpoints.findIndex(
        i => i.flightpoint_id === this.selectedFlightpoint1.id
      ),
      1
    );
    this.tempFlightpoints.splice(
      this.tempFlightpoints.findIndex(
        i => i.flightpoint_id === this.selectedFlightpoint2.id
      ),
      1
    );
  }

  getAirportByid(id) {
    let airport = this.airports.find(airport => airport.id === id);
    return isNullOrUndefined(airport) ? {} : airport;
  }

  resetScroll() {
    this.scroll = 0;
    this.globalsettingsService.timetableScroll = this.scroll;
    this.timelineArea.nativeElement.scrollTo(0, 0);
    // this.timelineAreaPilot.nativeElement.scrollTo(0, 0);
  }

  getScrollPos() {
    this.scroll = this.timelineArea.nativeElement.scrollLeft;
    this.globalsettingsService.timetableScroll = this.scroll;
  }

  isFlightpointSelected(id: number): boolean {
    return this.tempFlightpoints.findIndex(i => i.flightpoint_id === id) !== -1;
  }

  getFlightpointWidth(date1, date2) {
    // ширина перелета с учетом from to
    let from = this.timetablestoreService.dateFromIsoString(date1);
    let to = this.timetablestoreService.dateFromIsoString(date2);
    let _to = new Date(
      this.to.getFullYear(),
      this.to.getMonth(),
      this.to.getDate() + 1,
      0,
      0,
      0
    );
    let __from = from > this.from ? from : this.from;
    let __to = to < _to ? to : _to;
    let res = +__to - +__from;
    let _res = res / (1000 * 60 * this.scales[this.scale]);
    return {
      str: `${_res}px`,
      val: _res,
      cutTo: to > _to ? true : false,
      cutFrom: __from > _to || from < this.from ? true : false
    };
  }

  onMenuClosed(): void {
    let menu = document.getElementById('menuBtn');
    if (menu) {
      menu.style.display = 'none';
    }
    this.clickOnchain = false;
    this.clickOnflight = false;
    this.clickOntempchain = false;
  }

  startChain() {
    setTimeout(() => (this.chainStarted = true), 300);
    this.tempFlightpoints.length = 0;
  }

  async saveChain() {
    if (this.tempFlightpoints.length <= 2) {
      this.stopChain();
      return;
    }
    let used = {};
    this.tempFlightpoints = this.tempFlightpoints.filter(obj => {
      return obj.flightpoint_id in used ? 0 : (used[obj.flightpoint_id] = 1);
    });
    try {
      let data = { scenario_id: this.scenarioes[this.selectedIndex].id };
      let respost = await this.flightpointchainService.postOne(data);
      await Promise.all(
        this.tempFlightpoints.map(async flightpoint => {
          await this.flightpointService.putOneById(flightpoint.flightpoint_id, {
            flightpointchain_id: respost.id
          });
          this.aircrafts[flightpoint.aircraft_idx].specificflights[
            flightpoint.specificflight_idx
          ].flightpoints[flightpoint.flightpoint_idx].flightpointchain_id =
            respost.id;
        })
      );
      let flightpointsInchains = this.flightpointService.getByFlightpointchains(
        [respost.id]
      );
      let _flightpointsInchains = isNullOrUndefined(await flightpointsInchains)
        ? []
        : await flightpointsInchains;
      this.flightpointsInchains = this.flightpointsInchains.concat(
        _flightpointsInchains
      );
    } catch (err) {
      console.error(`Ошибка ${err}`);
      this.openSnackBar(`${err.error}.`, 'Ok');
    } finally {
      this.stopChain();
    }
  }

  stopChain() {
    setTimeout(() => (this.chainStarted = false), 300);
    this.selectedAircraft_idx = null;
    this.selectedSpecificflight_idx = null;
    this.selectedFlightpoint_idx = null;
    this.selectedFlightpoint1 = null;
    this.selectedFlightpoint2 = null;
    this.tempFlightpoints.length = 0;
  }

  async delChain() {
    this.spinnerService.show();
    let chainid = this.selectedFlightpoint1.flightpointchain_id;
    try {
      // let _activitys = this.activityService.getByChain(chainid);
      // let activitys = isNullOrUndefined(await _activitys)
      //   ? []
      //   : await _activitys;
      // if (activitys.length > 0) {
      //   this.openSnackBar(`К цепочке привязаны активности.`, 'Ok');
      //   return;
      // }
      // let _flightpoints = this.flightpointService.getByFlightpointchains([
      //   chainid
      // ]);
      // let flightpoints = isNullOrUndefined(await _flightpoints)
      //   ? []
      //   : await _flightpoints;
      // await Promise.all(
      //   flightpoints.map(async flightpoint => {
      //     await this.flightpointService.putOneById(flightpoint.id, {
      //       flightpointchain_id: null
      //     });
      //   })
      // );
      await this.flightpointchainService.deleteOneById(chainid);
      // this.aircrafts.map(aircraft => {
      //   aircraft.specificflights.map(specificflight => {
      //     specificflight.flightpoints.map(flightpoint => {
      //       if (flightpoint.flightpointchain_id === chainid) {
      //         flightpoint.flightpointchain_id = null;
      //       }
      //     });
      //   });
      // });
    } catch (err) {
      console.error(`Ошибка ${err}`);
      this.openSnackBar(`${err.error}.`, 'Ok');
    } finally {
      await this.getFlights(false);
      await this.getPilots(false);
      this.spinnerService.hide();
    }
  }

  onStop(event) {
    event.stopPropagation();
    event.preventDefault();
  }

  /**Проверяем есть ли перелеты в области клика, если есть - вызываем создание активности по перелету */
  onAddActivity(event: any, airuser_id: number, access: number) {
    if (
      this.selectedIndex === 0 &&
      this.authService.loggedInUser.isadmin === false
    ) {
      return;
    }
    if (this.addMode === false) {
      return;
    }
    if (event.ctrlKey === false && event.metaKey === false) {
      return;
    }

    let flightpointsinchain = this.getFlightpointchainsByevent(event);
    if (flightpointsinchain.length > 0) {
      this.addActivityonchain(
        flightpointsinchain[0],
        airuser_id,
        access,
        flightpointsinchain
      );
    } else {
      let flightpoints = this.getFlightpointsByevent(event);
      if (flightpoints.length === 0) {
        return;
      }
      this.addActivity(flightpoints, airuser_id, access);
    }
  }

  async addActivity(
    flightpoints: Flightpoint[],
    airuser_id: number,
    access: number,
    activityname_id = 6
  ) {
    if (access < 1) {
      return;
    }

    if (flightpoints.length === 1) {
      this.spinnerService.show();
      let result = {};
      result['activityname_id'] = activityname_id;
      result['airport_id'] = flightpoints[0].airport_id;
      result['airuser_id'] = airuser_id;
      result['begin_datetime'] = flightpoints[0].calc_departure_datatime;
      result['end_airport_id'] = flightpoints[0].end_airport_id;
      result['end_datetime'] = flightpoints[0].end_datatime;
      result['flightpoint1_id'] = flightpoints[0].id;
      result['flightpoint2_id'] = flightpoints[0].next_id;
      result['scenario_id'] = this.scenarioes[this.selectedIndex].id;
      if (activityname_id === 2) {
        result['information'] = flightpoints[0].flight_number;
      }
      // result['airuser_id'] = airuser_id;
      try {
        let resActivity = await this.activityService.postOne(result);
        // result['activity_id'] = resActivity.id;
        // await this.airuseractivityService.postOne(result);
      } catch (err) {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`${err.error}.`, 'Ok');
      } finally {
        // await this.updateFlight();
        await this.getFlights(false);
        await this.getPilots(false);
        this.spinnerService.hide();
      }
    } else {
      let checkDate = new Date(this.selectedDate);
      checkDate.setDate(checkDate.getDate() - 1);
      let pilot = this.pilots.find(p => p.id === airuser_id);
      let departureAirport: Airport;
      let arrivalAirport: Airport;
      if (!isNullOrUndefined(pilot)) {
        let before = pilot.activitys
          .reverse()
          .find(
            item =>
              +this.timetablestoreService.dateFromIsoString(item.end_datetime) <
              +this.timetablestoreService.dateFromIsoString(checkDate)
          );
        if (!isNullOrUndefined(before)) {
          departureAirport = before.end_airport;
        }
        let after = pilot.activitys.find(
          item =>
            +this.timetablestoreService.dateFromIsoString(item.begin_datetime) >
            +this.timetablestoreService.dateFromIsoString(checkDate)
        );
        if (!isNullOrUndefined(after)) {
          arrivalAirport = after.airport;
        }
      }
      let dialogRef = this.dialog.open(DialogActivityComponent, {
        panelClass: 'app-dialog-base',
        data: {
          certificatetypes: this.certificatetypes,
          type: 'new',
          title: 'Новая активность',
          begindate: this.selectedDate,
          activitynames: this.activitynames,
          flightpoints: flightpoints,
          scenario_id: this.scenarioes[this.selectedIndex].id,
          isadmin: this.authService.loggedInUser.isadmin,
          departureAirport,
          arrivalAirport
        }
      });
      dialogRef.afterClosed().subscribe(async result => {
        if (isNullOrUndefined(result) || !result) {
          return;
        }
        this.spinnerService.show();
        try {
          result.form['airuser_id'] = +airuser_id;
          // result.append('airuser_id', +airuser_id);
          // console.log(result);
          // let formFile: FormData = new FormData();
          // // formFile.append('airuser_id', newUserFile.value['airuser_id']);

          // for (let item in result) {
          //   formFile.append(item, result[item]);
          //   // console.log(item);
          // }
          // console.log(result);
          const formData = new FormData();
          for (const key of Object.keys(result.form)) {
            const value = result.form[key];
            if (
              !isNullOrUndefined(value) &&
              value !== 'null' &&
              value !== 'undefined'
            ) {
              formData.append(key, value);
            }
          }
          formData.append('ticketfile', result.file);
          let resActivity = await this.activityService.postOne(formData);
          // result['activity_id'] = resActivity.id;
          // let resAiruseractivity = await this.airuseractivityService.postOne(
          //   result
          // );
          // this.activitys.push(resActivity);
        } catch (err) {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`${err.error}.`, 'Ok');
        } finally {
          // await this.updateFlight();
          await this.getFlights(false);
          await this.getPilots(false);
          this.spinnerService.hide();
        }
      });
    }
  }

  toFormData<T>(formValue: T) {
    const formData = new FormData();

    for (const key of Object.keys(formValue)) {
      const value = formValue[key];
      if (
        !isNullOrUndefined(value) &&
        value !== 'null' &&
        value !== 'undefined'
      ) {
        formData.append(key, value);
      }
    }

    return formData;
  }

  async updateFlight() {
    let activitys_ids = this.activitys.map(activity => activity.id);
    if (activitys_ids.length > 0) {
      let airuseractivitys = this.activityService.getByIds(activitys_ids);
      this.airuseractivitys = isNullOrUndefined(await airuseractivitys)
        ? []
        : await airuseractivitys;
    } else {
      this.airuseractivitys = [];
    }
    this.aircrafts.map(aircraft => {
      aircraft.specificflights.map(specificflight => {
        for (let i = 0; i < specificflight.flightpoints.length - 1; i++) {
          specificflight.flightpoints[i].activitys = [];
          specificflight.flightpoints[i].activitys = this.activitys.filter(
            activity => {
              return (
                activity.flightpoint1_id ===
                  specificflight.flightpoints[i].id &&
                activity.flightpoint2_id ===
                  specificflight.flightpoints[i + 1].id
              );
            }
          );
          specificflight.flightpoints[i].pilots = [];
          specificflight.flightpoints[i].activitys.map(act => {
            let airuseractivity = this.airuseractivitys.find(
              item => item.activity_id === act.id
            );
            let pilot = this.pilots.find(
              item => item.id === airuseractivity.airuser_id
            );
            specificflight.flightpoints[i].pilots.push(pilot);
          });
        }
      });
    });
  }

  getFlightpointchainsByid(id: number): Flightpointchain[] {
    let flightpoints = this.flightpointsInchains.filter(
      item => item.flightpointchain_id === id
    );
    let flightpointchains = [];
    for (let i = 0; i < flightpoints.length; i++) {
      if (!isNullOrUndefined(flightpoints[i].flightpointchain_id)) {
        let flightpointchain: Flightpointchain = new Flightpointchain();
        let flightpointsinchain = this.flightpointsInchains.filter(
          item =>
            item.flightpointchain_id === flightpoints[i].flightpointchain_id
        );
        let departure_flightpointsinchain = flightpointsinchain.filter(
          item => !isNullOrUndefined(item.calc_departure_datatime)
        );
        let arrival_flightpointsinchain = flightpointsinchain.filter(
          item => !isNullOrUndefined(item.calc_arrival_datatime)
        );
        departure_flightpointsinchain.sort((a, b) =>
          this.timetablestoreService.dateFromIsoString(
            a.calc_departure_datatime
          ) >
          this.timetablestoreService.dateFromIsoString(
            b.calc_departure_datatime
          )
            ? 1
            : -1
        );
        arrival_flightpointsinchain.sort((a, b) =>
          this.timetablestoreService.dateFromIsoString(
            a.calc_arrival_datatime
          ) >
          this.timetablestoreService.dateFromIsoString(b.calc_arrival_datatime)
            ? 1
            : -1
        );
        flightpointchain.flightpoint_1 = departure_flightpointsinchain[0];
        flightpointchain.flightpoint_2 =
          arrival_flightpointsinchain[arrival_flightpointsinchain.length - 1];
        let str = '';
        for (let j = 0; j < departure_flightpointsinchain.length; j++) {
          str += this.getAirportByid(
            departure_flightpointsinchain[j].airport_id
          ).code;
          str += '-';
        }
        str += this.getAirportByid(
          arrival_flightpointsinchain[arrival_flightpointsinchain.length - 1]
            .airport_id
        ).code;
        flightpointchain.name = str;
        flightpointchains.push(flightpointchain);
      }
    }
    return flightpointchains;
  }

  onEditActivity(event, activity, access: number) {
    if (access < 1) {
      return;
    }
    if (
      this.selectedIndex === 0 &&
      this.authService.loggedInUser.isadmin === false
    ) {
      this.selectedIndex = 1;
      return;
    }
    event.stopPropagation();
    let dialogRef = this.dialog.open(DialogEditactivityComponent, {
      panelClass: 'app-dialog-base',
      data: {
        certificatetypes: this.certificatetypes,
        type: 'edit',
        title: 'Активность',
        activity: activity,
        activitynames: this.activitynames,
        flightpointchain: isNullOrUndefined(activity.flightpointchain_id)
          ? null
          : this.getFlightpointchainsByid(activity.flightpointchain_id)[0],
        scenario_id: this.scenarioes[this.selectedIndex].id,
        isadmin: this.authService.loggedInUser.isadmin
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (isNullOrUndefined(result) || !result) {
        return;
      } else if (result === 'delete') {
        this.spinnerService.show();
        try {
          // await this.airuseractivityService.deleteOneById(
          //   activity.airuseractivity_id
          // );
          await this.activityService.deleteOneById(activity.id);
          // this.activitys.splice(
          //   this.activitys.findIndex(i => i.id === activity.id),
          //   1
          // );
        } catch (err) {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`${err.error}.`, 'Ok');
        } finally {
          await this.getPilots(false);
          await this.getFlights(false);
          this.spinnerService.hide();
          return;
        }
      } else {
        this.spinnerService.show();
        try {
          // await this.activityService.putOneById(activity.id, result);
          const formData = new FormData();
          for (const key of Object.keys(result.form)) {
            const value = result.form[key];
            if (
              !isNullOrUndefined(value) &&
              value !== 'null' &&
              value !== 'undefined'
            ) {
              formData.append(key, value);
            }
          }
          formData.append('ticketfile', result.file);
          await this.activityService.putOneById(activity.id, formData);
        } catch (err) {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`${err.error}.`, 'Ok');
        } finally {
          await this.getPilots(false);
          await this.getFlights(false);
          this.spinnerService.hide();
        }
      }
    });
  }

  async onEditprofile(selectedPilotId, selectedAccessPilot) {
    let pilot: Airuser;
    let sbdv: Subdivision;
    let pos: Position;
    try {
      pilot = await this.airuserService.getOneById(selectedPilotId);
      pos = this.positions.find(item => item.id === pilot.position_id);
      sbdv = this.subdivisions.find(item => item.id === pos.subdivision_id);
    } catch (err) {
      console.error(err);
      this.openSnackBar(`${err.error}.`, 'Ok');
    }
    let dialogRef = this.dialog.open(DialogProfileComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        title: 'Профиль',
        myProfile: false,
        surname: pilot.surname,
        name: pilot.name,
        phone: pilot.name,
        email: pilot.name,
        home_airport_id: pilot.home_airport_id,
        pilot_type: pilot.pilot_type,
        patronymic: pilot.patronymic,
        subdivision: !isNullOrUndefined(sbdv) ? sbdv.name : '',
        position: !isNullOrUndefined(pos) ? pos.name : '',
        pilotTypes: this.pilotTypes
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (!isNullOrUndefined(result) && result !== false) {
        try {
          this.spinnerService.show();
          let res = await this.airuserService.putOneById(selectedPilotId, {
            home_airport_id: result.home_airport_id,
            pilot_type: result.pilot_type
          });
        } catch (err) {
          console.error(err);
          this.openSnackBar(`${err.error.error}.`, 'Ok');
        } finally {
          await this.getData();
          await this.getPilots(false);
          await this.getFlights(false);
          this.onSelectFilter();
          this.spinnerService.hide();
        }
      }
    });
  }

  onEditCartificate() {}

  async addActivityonchain(
    flightpointchain: Flightpointchain,
    airuser_id: number,
    access: number,
    flightpointschain?: Flightpointchain[]
  ) {
    if (access < 1) {
      return;
    }
    if (
      !isNullOrUndefined(flightpointschain) &&
      flightpointschain.length === 1
    ) {
      let result = {
        activityname_id: 6,
        airport_id: flightpointchain.flightpoint_1.airport_id,
        end_airport_id: flightpointchain.flightpoint_2.airport_id,
        // 'airports': this.airports,
        scenario_id: this.scenarioes[this.selectedIndex].id,
        flightpointchain_id: flightpointchain.flightpoint_1.flightpointchain_id,
        begin_datetime: flightpointchain.flightpoint_1.calc_departure_datatime,
        end_datetime: flightpointchain.flightpoint_2.calc_arrival_datatime,
        airuser_id: airuser_id
      };
      this.spinnerService.show();
      try {
        let resActivity = await this.activityService.postOne(result);
        // result['activity_id'] = resActivity.id;
        // let resAiruseractivity = await this.airuseractivityService.postOne(
        //   result
        // );
      } catch (err) {
        console.error(`Ошибка ${err}`);
        this.openSnackBar(`${err.error}.`, 'Ok');
      } finally {
        await this.getFlights(false);
        await this.getPilots(false);
        this.spinnerService.hide();
      }
    } else {
      let dialogRef = this.dialog.open(DialogActivityComponent, {
        panelClass: 'app-dialog-base',
        data: {
          certificatetypes: this.certificatetypes,
          title: 'Новая активность',
          begindate: this.selectedDate,
          activitynames: this.activitynames,
          // 'airports': this.airports,
          flightpointchain: flightpointchain,
          flightpointsinchain: flightpointschain,
          scenario_id: this.scenarioes[this.selectedIndex].id,
          isadmin: this.authService.loggedInUser.isadmin
        }
      });
      dialogRef.afterClosed().subscribe(async result => {
        if (isNullOrUndefined(result) || !result) {
          return;
        }
        this.spinnerService.show();
        try {
          result['airuser_id'] = airuser_id;
          result['flightpointchain_id'] =
            flightpointchain.flightpoint_1.flightpointchain_id;
          let resActivity = await this.activityService.postOne(result);
          // result['activity_id'] = resActivity.id;
          // let resAiruseractivity = await this.airuseractivityService.postOne(
          //   result
          // );
          this.activitys.push(resActivity);
        } catch (err) {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`${err.error}.`, 'Ok');
        } finally {
          // await this.updateFlight();
          await this.getFlights(false);
          await this.getPilots(false);
          this.spinnerService.hide();
        }
      });
    }
  }

  async onAccept() {
    try {
      this.spinnerService.show();
      this.activityService.copyToMain(
        this.now.toISOString().split('T')[0],
        this.scenarioes.find(
          item => item.airuser_id === this.authService.loggedInUser.id
        ).id,
        true,
        '2020-12-01'
      );
      // await Promise.all(
      //   this.activitystmp.map(async item => {
      //     let res = await this.activityService.putOneById(item.id, {
      //       scenario_id: this.scenarioes.find(scenario => scenario.status === 1)
      //         .id
      //     });
      //   })
      // );
    } catch (err) {
      console.error(err);
    } finally {
      await this.getPilots(false);
      await this.getFlights(false);
      this.spinnerService.hide();
    }
  }

  isComplete(pilots: Airuser[], fligthpoint: Flightpoint): boolean {
    let res = false;
    let first;
    let second;
    if (isNullOrUndefined(pilots) || pilots.length === 0) {
      return false;
    } else {
      // let rpilots = pilots.slice(1, pilots.length - 1);
      // pilots.map(pilot => {
      let old = 0;
      for (let j = 0; j < pilots.length; j++) {
        if (!isNullOrUndefined(pilots[j].dt_of_birth)) {
          let age =
            new Date().getUTCFullYear() -
            new Date(pilots[j].dt_of_birth).getUTCFullYear();
          if (age > 60) {
            old++;
            if (old > 1) {
              return false;
            }
          }
        }
        if (
          !isNullOrUndefined(pilots[j].activitys) &&
          !isNullOrUndefined(fligthpoint)
        ) {
          for (let i = 0; i < pilots[j].activitys.length; i++) {
            if (
              pilots[j].activitys[i].activityname_id === 6 &&
              (pilots[j].activitys[i].flightpoint1_id === fligthpoint.id ||
                pilots[j].activitys[i].flightpoint2_id === fligthpoint.id)
            ) {
              if (pilots[j].activitys[i]._rest.danger === true) {
                return false;
              }
              if (
                !isNullOrUndefined(pilots[j].activitys[i].last_end_airports) &&
                pilots[j].activitys[i].last_end_airports[0].city_id !==
                  pilots[j].activitys[i].airport.city_id
              ) {
                return false;
              }
              if (
                pilots[j].activitys[i].last_end_airports == null &&
                pilots[j].home_airport.city_id !==
                  pilots[j].activitys[i].airport.city_id
              ) {
                return false;
              }
            }
          }
        }
      }
      let rpilots = [...pilots];
      rpilots.shift();
      switch (pilots[0].pilot_type) {
        case 1: // ВП наблюдатель (ПИ или ПИЭ + >=ВП)
          first = rpilots.find(
            item => item.pilot_type === 7 || item.pilot_type === 8
          );
          if (!isNullOrUndefined(first)) {
            second = rpilots.find(
              item => item.pilot_type >= 3 && item.id !== first.id
            );
          }
          if (isNullOrUndefined(first) || isNullOrUndefined(second)) {
            res = false;
          } else {
            res = true;
          }
          break;
        case 2: // ВП - стажер (ПИЭ + >=ВП)
          first = rpilots.find(item => item.pilot_type === 8);
          if (!isNullOrUndefined(first)) {
            second = rpilots.find(
              item => item.pilot_type >= 3 && item.id !== first.id
            );
          }
          if (isNullOrUndefined(first) || isNullOrUndefined(second)) {
            res = false;
          } else {
            res = true;
          }
          break;
        case 3: // ВП (КВС или КВС с доп справа + ПИ или ПИЭ)
          first = rpilots.find(
            item =>
              item.pilot_type === 5 ||
              item.pilot_type === 6 ||
              item.pilot_type === 7 ||
              item.pilot_type === 8
          );
          // if (!isNullOrUndefined(first)) {
          //   second = rpilots.find(item => ((item.pilot_type === 7 || item.pilot_type === 8) && item.id !== first.id));
          // }
          // if(pilots[0].surname === 'Марьясов') {
          // }
          if (isNullOrUndefined(first)) {
            res = false;
          } else {
            res = true;
          }
          break;
        case 4: // КВС заплечные (ПИ или ПИЭ + ВП)
          first = rpilots.find(
            item => item.pilot_type === 7 || item.pilot_type === 8
          );
          if (!isNullOrUndefined(first)) {
            second = rpilots.find(
              item => item.pilot_type === 3 && item.id !== first.id
            );
          }
          if (isNullOrUndefined(first) || isNullOrUndefined(second)) {
            res = false;
          } else {
            res = true;
          }
          break;
        case 5: // КВС (ВП или ПИЭ или КВС с доп справа)
          first = rpilots.find(
            item =>
              item.pilot_type === 3 ||
              item.pilot_type === 6 ||
              item.pilot_type === 7 ||
              item.pilot_type === 8
          );
          if (isNullOrUndefined(first)) {
            res = false;
          } else {
            res = true;
          }
          break;
        case 6: // КВС с доп справа
        case 7: // ПИ
        case 8: // ПИЭ
          first = rpilots.find(
            item =>
              item.pilot_type === 3 ||
              item.pilot_type === 4 ||
              item.pilot_type === 5 ||
              item.pilot_type === 6 ||
              item.pilot_type === 7 ||
              item.pilot_type === 8
          );
          if (isNullOrUndefined(first)) {
            res = false;
          } else {
            res = true;
          }
          break;
        case 9: // ВП(ввод КВС) (ПИЭ)
          first = rpilots.find(
            item => item.pilot_type === 8 || item.pilot_type === 7
          );
          if (isNullOrUndefined(first)) {
            res = false;
          } else {
            res = true;
          }
          break;
        default:
          res = false;
          break;
      }
      return res;
    }
  }

  setmouse_pilot_id(event, id) {
    this.mouse_pilot_id = id;
    event.stopPropagation();
  }

  setmouse_flightpoints_id(event) {
    let flightpoints = this.getFlightpointsByevent(event);
    this.mouse_flightpoints_id = flightpoints.map(item => item.id);
    event.stopPropagation();
  }

  inmouse_flightpoints_id(id): boolean {
    let res = false;
    if (isNullOrUndefined(this.mouse_flightpoints_id)) {
      return res;
    }
    let fp = this.mouse_flightpoints_id.find(item => item === id);
    if (!isNullOrUndefined(fp)) {
      res = true;
    }
    return res;
  }

  getFlightpointsByFlightpointchain(flightpointchain_id) {
    let flightpoints = this.flightpoints.filter(
      fp => fp.flightpointchain_id === flightpointchain_id
    );
    return isNullOrUndefined(flightpoints) ? [] : flightpoints;
  }

  getRestTime(activity: Activity, pilot: Airuser) {
    // if (activity.activityname_id !== 6) {
    if (false) {
      return null;
    } else {
      let restMin = 0;
      let restMax = 0;
      let airport;
      let pairport;
      let differ = 0;
      let end = activity._left.val + activity._flightWidth.val;
      if (this.width.val - end > 1) {
        if (
          this.getAirportByid(activity.end_airport_id).city_id ===
          this.getAirportByid(pilot.home_airport_id).city_id
        ) {
          airport = this.airports.find(
            airport => airport.id === activity.end_airport_id
          );
          if (!isNullOrUndefined(activity.bases)) {
            differ =
              (+this.dateFromIsoString(activity.str_end_datetime) -
                +this.dateFromIsoString(activity.bases[0].begin_datetime)) /
              (60 * 60 * 1000);
          }
          let work_time =
            Math.abs(
              +this.dateFromIsoString(activity.str_begin_datetime) -
                +this.dateFromIsoString(activity.str_end_datetime)
            ) /
            (60 * 60 * 1000);
          work_time += (this.timebeforelag + this.timeafterlag) / 60;
          if (
            differ > 48 &&
            activity.airport.summer_timeshift -
              this.getAirportByid(pilot.home_airport_id).summer_timeshift >=
              4
          ) {
            restMin = 48 * 60;
          } else {
            if (work_time <= 12) {
              restMin = 12 * 60;
            } else if (work_time > 12 && work_time <= 14) {
              restMin = 14 * 60;
            } else if (work_time > 14) {
              restMin = 18 * 60;
            }
          }
          restMax = work_time * 60 * 2;
        } else {
          pairport = this.airports.find(
            airport => airport.id === pilot.home_airport_id
          );
          let work_time =
            Math.abs(
              +this.dateFromIsoString(activity.str_begin_datetime) -
                +this.dateFromIsoString(activity.str_end_datetime)
            ) /
            (60 * 60 * 1000);
          work_time += (this.timebeforelag + this.timeafterlag) / 60;
          let dif = 0;
          if (!isNullOrUndefined(pairport) && !isNullOrUndefined(airport)) {
            dif =
              Math.abs(airport.summer_timeshift - pairport.summer_timeshift) *
              30;
          }
          if (work_time <= 12) {
            restMin = 10 * 60 + dif;
          } else if (work_time > 12 && work_time <= 14) {
            restMin = 12 * 60 + dif;
          } else if (work_time > 14) {
            restMin = 16 * 60 + dif;
          }
          restMax = work_time * 60 * 2;
        }
      }
      let restMinPx = restMin / this.scales[this.scale];
      let restMaxPx = restMax / this.scales[this.scale];
      return { restMin, restMax, restMinPx, restMaxPx };
    }
  }

  dateFromIsoString(str: string | Date) {
    // 2018-09-14T23:00:00
    if (typeof str === 'string') {
      let _arr = str.split('.')[0].split('T');
      let arr = _arr[0].split('-').concat(_arr[1].split(':')); // yyyy mm dd hh mm ss
      return new Date(+arr[0], +arr[1] - 1, +arr[2], +arr[3], +arr[4]);
    } else {
      return new Date(str);
    }
  }

  onCopyFrom() {
    let dialogRef = this.dialog.open(DialogYesnoComponent, {
      panelClass: 'app-dialog-overview-yes-no-component',
      data: {
        title: `Вы действительно хотите скопировать активности из основного сценария с заменой?`
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === true) {
        try {
          this.spinnerService.show();
          this.activityService.copyFromMain(
            this.now.toISOString().split('T')[0],
            this.scenarioes.find(
              item => item.airuser_id === this.authService.loggedInUser.id
            ).id,
            true,
            '2020-12-01'
          );
        } catch (err) {
          console.error(`Ошибка ${err}`);
          this.openSnackBar(`Ошибка. ${err.error}.`, 'Ok');
        } finally {
          await this.getFlights(false);
          await this.getPilots(false);
          this.spinnerService.hide();
        }
      }
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000
    });
  }
}
