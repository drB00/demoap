import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Airport } from 'src/app/shared/models/airport.model';
import { Observable } from 'rxjs';
import { Airuser } from 'src/app/shared/models/airuser.model';
import { Subdivision } from 'src/app/shared/models/subdivision.model';
import { Position } from 'src/app/shared/models/position.model';
import { AccessrightService } from 'src/app/system/shared/services/accessright.service';
import { AiruserService } from 'src/app/system/shared/services/airuser.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { PositionService } from 'src/app/system/shared/services/position.service';
import { SubdivisionService } from 'src/app/system/shared/services/subdivision.service';
import { isNullOrUndefined, isNull } from 'util';
import { AirportService } from 'src/app/system/shared/services/airport.service';
import { startWith, map } from 'rxjs/operators';
import _ from 'lodash';
import { TravelapiService } from 'src/app/system/shared/services/travelapi.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CertificatetypeService } from 'src/app/system/shared/services/certificatetype.service';

@Component({
  selector: 'app-dialog-activity',
  templateUrl: './dialog-activity.component.html',
  styleUrls: ['./dialog-activity.component.scss']
})
export class DialogActivityComponent implements OnInit {
  loggedInUser: Airuser;
  loggedInUserPosition: Position;
  loggedInUserSubdivision: Subdivision;
  loggedInUserAccesses;
  loggedInUserRole;

  timeMaskInput: any[] = [/[0-2]/, /\d/, ':', /[0-6]/, /\d/];

  selectedIndex = 0;

  activityForm: FormGroup;
  activityFormDate: FormData = new FormData();

  disabled: boolean;

  flightpoint1_id = null;
  flightpoint2_id = null;

  airports: Airport[];

  departure_airport_control: FormControl;
  arrival_airport_control: FormControl;
  filtered_arrival_airport: Observable<any[]>;
  filtered_departure_airport: Observable<any[]>;
  timerId;

  activeFile: File;

  price = '';

  constructor(
    public authService: AuthService,
    public airuserService: AiruserService,
    public positionService: PositionService,
    public subdivisionService: SubdivisionService,
    public accessrightService: AccessrightService,
    public airportService: AirportService,
    private travelapiService: TravelapiService,
    private spinnerService: NgxSpinnerService,
    public dialogRef: MatDialogRef<DialogActivityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  async ngOnInit() {
    this.loggedInUserAccesses = this.authService.loggedInUserAccesses;
    this.loggedInUser = this.authService.loggedInUser;
    this.loggedInUserPosition = this.authService.loggedInUserPosition;
    this.loggedInUserSubdivision = this.authService.loggedInUserSubdivision;
    if (this.loggedInUser.isadmin === true) {
      this.loggedInUserRole = 0;
    } else if (isNull(this.loggedInUserPosition.parent_id)) {
      this.loggedInUserRole = 1;
    } else {
      this.loggedInUserRole = 2;
    }
    this.disabled = true;
    // console.log(this.data.departureAirport);
    // console.log(this.data.arrivalAirport);
    this.departure_airport_control = new FormControl({
      value: isNullOrUndefined(this.data.departureAirport)
        ? null
        : this.data.departureAirport,
      disabled: this.disabled
    });
    this.arrival_airport_control = new FormControl({
      value: isNullOrUndefined(this.data.arrivalAirport)
        ? null
        : this.data.arrivalAirport,
      disabled: this.disabled
    });
    if (isNullOrUndefined(this.data.flightpointchain)) {
      // this.disabled = false;
      this.disabled = this.data.flightpoints.length !== 0;
      this.activityForm = new FormGroup({
        activityname_id: new FormControl(
          { value: this.disabled ? 6 : null, disabled: this.disabled },
          [Validators.required]
        ),
        datebegin: new FormControl({ value: null, disabled: this.disabled }, [
          Validators.required
        ]),
        timebegin: new FormControl({ value: null, disabled: this.disabled }, [
          Validators.required
        ]),
        dateend: new FormControl({ value: null, disabled: this.disabled }, [
          Validators.required
        ]),
        timeend: new FormControl({ value: null, disabled: this.disabled }, [
          Validators.required
        ]),
        address: new FormControl(null),
        information: new FormControl(null),
        ticketfile: new FormControl(null)
        // ticketfile: new FormControl(null)
        // 'airport_id': new FormControl({ value: null, disabled: this.disabled }, [Validators.required]),
        // 'end_airport_id': new FormControl({ value: null, disabled: this.disabled }, [])
      });
      // this.departure_airport_control.patchValue({ value: null, disabled: this.disabled });
      // this.arrival_airport_control.patchValue({ value: null, disabled: this.disabled });
      if (this.disabled) {
        this.onSelectflight(0);
      } else {
        let dateend = new Date(this.data.begindate);
        let dateOffset = 24 * 60 * 60 * 1000 * 1;
        let datebegin = new Date(dateend);
        datebegin.setTime(datebegin.getTime() - dateOffset);
        this.activityForm.patchValue({
          datebegin: datebegin,
          timebegin: '00:00',
          dateend: dateend,
          timeend: '00:00'
        });
      }
    } else {
      this.activityForm = new FormGroup({
        activityname_id: new FormControl({ value: 6, disabled: true }, [
          Validators.required
        ]),
        datebegin: new FormControl({ value: null, disabled: true }, [
          Validators.required
        ]),
        timebegin: new FormControl({ value: null, disabled: true }, [
          Validators.required
        ]),
        dateend: new FormControl({ value: null, disabled: true }, [
          Validators.required
        ]),
        timeend: new FormControl({ value: null, disabled: true }, [
          Validators.required
        ]),
        address: new FormControl(null),
        information: new FormControl(null),
        ticketfile: new FormControl(null)
        // 'airport_id': new FormControl({ value: null, disabled: true }, [Validators.required]),
        // 'end_airport_id': new FormControl({ value: null, disabled: true }, [Validators.required])
      });
      // this.departure_airport_control.patchValue({ value: null, disabled: true });
      // this.arrival_airport_control.patchValue({ value: null, disabled: true });
      this.flightpoint1_id = this.data.flightpointchain.flightpoint_1.id;
      this.flightpoint2_id = this.data.flightpointchain.flightpoint_2.id;
      let datebegin_ = this.data.flightpointchain.flightpoint_1.calc_departure_datatime.split(
        'T'
      );
      let datebegin = datebegin_[0].split('-').concat(datebegin_[1].split(':'));
      let dateend_ = this.data.flightpointchain.flightpoint_2.calc_arrival_datatime.split(
        'T'
      );
      let dateend = dateend_[0].split('-').concat(dateend_[1].split(':')); // yyyy mm dd hh mm ss
      this.activityForm.patchValue({
        datebegin: this.dateFromIsoString(
          this.data.flightpointchain.flightpoint_1.calc_departure_datatime
        ),
        timebegin: `${datebegin[3]}:${datebegin[4]}`,
        dateend: this.dateFromIsoString(
          this.data.flightpointchain.flightpoint_2.calc_arrival_datatime
        ),
        timeend: `${dateend[3]}:${dateend[4]}`,
        airport_id: this.data.flightpointchain.flightpoint_1.airport_id,
        end_airport_id: this.data.flightpointchain.flightpoint_2.airport_id
      });
      let airport_id;
      let end_airport_id;
      try {
        airport_id = this.airportService.getOneById(
          this.data.flightpointchain.flightpoint_1.airport_id
        );
        end_airport_id = this.airportService.getOneById(
          this.data.flightpointchain.flightpoint_2.airport_id
        );
      } catch (err) {
        console.error(err);
      }
      this.departure_airport_control.patchValue(await airport_id);
      this.arrival_airport_control.patchValue(await end_airport_id);
    }
    let airports;
    try {
      airports = this.airportService.getWithCodefilterLimit(100, '');
    } catch (err) {
      console.error(err);
    }
    this.airports = isNullOrUndefined(await airports) ? [] : await airports;
    this.filtered_departure_airport = this.departure_airport_control.valueChanges.pipe(
      startWith<string | Airport | null>(''),
      map(value =>
        typeof value === 'string'
          ? value
          : isNullOrUndefined(value)
          ? ''
          : value.code
      ),
      map(code =>
        code
          ? this._filter(code)
          : isNullOrUndefined(this.airports)
          ? []
          : this.airports.slice()
      )
    );
    this.filtered_arrival_airport = this.arrival_airport_control.valueChanges.pipe(
      startWith<string | any>(''),
      map(value =>
        typeof value === 'string'
          ? value
          : isNullOrUndefined(value)
          ? ''
          : value.code
      ),
      map(code =>
        code
          ? this._filter(code)
          : isNullOrUndefined(this.airports)
          ? []
          : this.airports.slice()
      )
    );
    if (
      !isNullOrUndefined(this.data.flightpoints) &&
      this.data.flightpoints.length > 1
    ) {
      this.onSelectflight(0);
    }
  }

  dateFromIsoString(str: string | Date) {
    // 2018-09-14T23:00:00
    if (typeof str === 'string') {
      let _arr = str.split('.')[0].split('T');
      let arr = _arr[0].split('-').concat(_arr[1].split(':')); // yyyy mm dd hh mm ss
      return new Date(+arr[0], +arr[1] - 1, +arr[2], +arr[3], +arr[4]);
    } else {
      return new Date(str);
    }
  }

  onFileChange(event) {
    this.activeFile = event.srcElement.files[0];
  }

  onSave() {
    if (
      !(
        this.activityForm.getRawValue().activityname_id === 6 ||
        this.activityForm.getRawValue().activityname_id === 2
      )
    ) {
      this.arrival_airport_control.patchValue(null);
    }
    let obj = {};
    obj['flightpoint1_id'] = isNullOrUndefined(this.flightpoint1_id)
      ? null
      : this.flightpoint1_id;
    obj['flightpoint2_id'] = isNullOrUndefined(this.flightpoint2_id)
      ? null
      : this.flightpoint2_id;
    obj['scenario_id'] = this.data.scenario_id;
    obj['activityname_id'] = this.activityForm.getRawValue().activityname_id;
    obj['address'] = this.activityForm.getRawValue().address;
    obj['information'] = this.activityForm.getRawValue().information;
    obj['ticketfile'] = this.activityForm.getRawValue().ticketfile;
    obj['airport_id'] = this.activityForm.getRawValue().airport_id;
    obj['end_airport_id'] = this.activityForm.getRawValue().end_airport_id;
    obj['airport_id'] = isNullOrUndefined(this.departure_airport_control.value)
      ? null
      : this.departure_airport_control.value.id;
    if (
      !isNullOrUndefined(this.arrival_airport_control) &&
      !isNullOrUndefined(this.arrival_airport_control.value)
    ) {
      obj['end_airport_id'] = this.arrival_airport_control.value.id;
    }
    let begin_datetime = new Date(this.activityForm.getRawValue().datebegin);
    begin_datetime.setHours(
      this.activityForm.getRawValue().timebegin.split(':')[0]
    );
    begin_datetime.setMinutes(
      this.activityForm.getRawValue().timebegin.split(':')[1] -
        begin_datetime.getTimezoneOffset()
    );
    obj['begin_datetime'] = begin_datetime.toUTCString();
    let end_datetime = new Date(this.activityForm.getRawValue().dateend);
    end_datetime.setHours(
      this.activityForm.getRawValue().timeend.split(':')[0]
    );
    end_datetime.setMinutes(
      this.activityForm.getRawValue().timeend.split(':')[1] -
        end_datetime.getTimezoneOffset()
    );
    obj['end_datetime'] = end_datetime.toUTCString();
    // obj['airport_id'] = this.activityForm.getRawValue().airport_id;
    // for (let item in obj) {
    //   if (item !== 'flightpoint1_id' && item !== 'flightpoint2_id') {
    //     this.activityFormDate.append(item, obj[item]);
    //   }
    // }
    // console.log(this.activityForm);
    // this.dialogRef.close();
    // console.log(obj);
    this.dialogRef.close({ form: obj, file: this.activeFile });
  }

  onBuy() {
    // console.log(this.data.activity)
    let str = 'http://hydra.aviasales.ru/?marker=199129';
    str += `&origin_iata=${this.departure_airport_control.value.code}`;
    str += `&destination_iata=${this.arrival_airport_control.value.code}`;
    let depart_date = this.dateFromIsoString(
      new Date(this.activityForm.getRawValue().datebegin)
    );
    str += `&depart_date=${depart_date.getFullYear()}-${depart_date.getMonth() +
      1}-${depart_date.getDate()}`;
    str += `&with_request=true&adults=1&children=0&infants=0&trip_class=0&locale=ru&one_way=true&currency=rub`;
    window.open(str, '_blank');
  }

  async getPrice() {
    let str = '';
    str += `?sorting=price&currency=rub&limit=1`;
    str += `&origin=${this.departure_airport_control.value.code}`;
    str += `&destination=${this.arrival_airport_control.value.code}`;
    let depart_date = this.dateFromIsoString(
      new Date(this.activityForm.getRawValue().datebegin)
    );
    str += `&beginning_of_period=${depart_date.getFullYear()}-${depart_date.getMonth() +
      1}-${depart_date.getDate()}`;
    str += `&with_request=true&adults=1&children=0&infants=0&trip_class=0&locale=ru&one_way=true&currency=rub`;
    // this.spinnerService.show();
    try {
      let res = await this.travelapiService.get(str);
      if (
        !isNullOrUndefined(res) &&
        !isNullOrUndefined(res['data']) &&
        !isNullOrUndefined(res['data'][0])
      ) {
        this.price = `${res['data'][0]['value']} руб.`;
      } else {
        this.price = '';
      }
    } catch (err) {
      this.price = '';
    } finally {
      // this.spinnerService.hide();
    }
  }

  async onSelectflight(index) {
    this.selectedIndex = index;
    this.flightpoint1_id = this.data.flightpoints[this.selectedIndex].id;
    this.flightpoint2_id = this.data.flightpoints[this.selectedIndex].next_id;
    let datebegin_ = this.data.flightpoints[
      this.selectedIndex
    ].calc_departure_datatime.split('T');
    let datebegin = datebegin_[0].split('-').concat(datebegin_[1].split(':'));
    let dateend_ = this.data.flightpoints[
      this.selectedIndex
    ].end_datatime.split('T');
    let dateend = dateend_[0].split('-').concat(dateend_[1].split(':')); // yyyy mm dd hh mm ss
    this.activityForm.patchValue({
      datebegin: this.dateFromIsoString(
        this.data.flightpoints[this.selectedIndex].calc_departure_datatime
      ),
      timebegin: `${datebegin[3]}:${datebegin[4]}`,
      dateend: this.dateFromIsoString(
        this.data.flightpoints[this.selectedIndex].end_datatime
      ),
      timeend: `${dateend[3]}:${dateend[4]}`,
      airport_id: this.data.flightpoints[this.selectedIndex].airport_id,
      end_airport_id: this.data.flightpoints[this.selectedIndex].end_airport_id
    });
    let airport_id;
    let end_airport_id;
    try {
      airport_id = this.airportService.getOneById(
        this.data.flightpoints[this.selectedIndex].airport_id
      );
      end_airport_id = this.airportService.getOneById(
        this.data.flightpoints[this.selectedIndex].end_airport_id
      );
    } catch (err) {
      console.error(err);
    }
    this.departure_airport_control.patchValue(await airport_id);
    this.arrival_airport_control.patchValue(await end_airport_id);
  }

  async onSelectflightpointinchain(index) {
    this.selectedIndex = index;
    this.flightpoint1_id = this.data.flightpointsinchain[
      index
    ].flightpoint_1.id;
    this.flightpoint2_id = this.data.flightpointsinchain[
      index
    ].flightpoint_2.id;
    let datebegin_ = this.data.flightpointsinchain[
      index
    ].flightpoint_1.calc_departure_datatime.split('T');
    let datebegin = datebegin_[0].split('-').concat(datebegin_[1].split(':'));
    let dateend_ = this.data.flightpointsinchain[
      index
    ].flightpoint_2.calc_arrival_datatime.split('T');
    let dateend = dateend_[0].split('-').concat(dateend_[1].split(':')); // yyyy mm dd hh mm ss
    this.activityForm.patchValue({
      datebegin: this.dateFromIsoString(
        this.data.flightpointsinchain[index].flightpoint_1
          .calc_departure_datatime
      ),
      timebegin: `${datebegin[3]}:${datebegin[4]}`,
      dateend: this.dateFromIsoString(
        this.data.flightpointsinchain[index].flightpoint_2.calc_arrival_datatime
      ),
      timeend: `${dateend[3]}:${dateend[4]}`,
      airport_id: this.data.flightpointsinchain[index].flightpoint_1.airport_id,
      end_airport_id: this.data.flightpointsinchain[index].flightpoint_2
        .airport_id
    });
    let airport_id;
    let end_airport_id;
    try {
      airport_id = this.airportService.getOneById(
        this.data.flightpointsinchain[index].flightpoint_1.airport_id
      );
      end_airport_id = this.airportService.getOneById(
        this.data.flightpointsinchain[index].flightpoint_2.airport_id
      );
    } catch (err) {
      console.error(err);
    }
    this.departure_airport_control.patchValue(await airport_id);
    this.arrival_airport_control.patchValue(await end_airport_id);
  }

  private _filter(code: string): any[] {
    const filterValue = code.toLowerCase();
    return isNullOrUndefined(this.airports)
      ? null
      : this.airports.filter(
          airport => airport.code.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  inputLink_departure() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.departure_airport_control.value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_departure_airport = this.departure_airport_control.valueChanges.pipe(
            startWith<string | Airport | null>(''),
            map(value =>
              typeof value === 'string'
                ? value
                : isNullOrUndefined(value)
                ? ''
                : value.code
            ),
            map(code =>
              code
                ? this._filter(code)
                : isNullOrUndefined(this.airports)
                ? []
                : this.airports.slice()
            )
          );
        } catch {}
      }
    }, 250);
  }

  inputLink_arrival() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.arrival_airport_control.value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_arrival_airport = this.arrival_airport_control.valueChanges.pipe(
            startWith<string | any>(''),
            map(value =>
              typeof value === 'string'
                ? value
                : isNullOrUndefined(value)
                ? ''
                : value.code
            ),
            map(code =>
              code
                ? this._filter(code)
                : isNullOrUndefined(this.airports)
                ? []
                : this.airports.slice()
            )
          );
        } catch {}
      }
    }, 250);
  }

  displayFn(airport?: any): string | undefined {
    return airport ? airport.code : undefined;
  }

  isValid(): boolean {
    let res = true;
    if (!this.activityForm.valid) {
      res = false;
    }
    if (
      this.activityForm.getRawValue().activityname_id === 2 &&
      (isNullOrUndefined(this.arrival_airport_control.value) ||
        isNullOrUndefined(this.departure_airport_control.value) ||
        isNullOrUndefined(this.arrival_airport_control.value.id) ||
        isNullOrUndefined(this.departure_airport_control.value.id))
    ) {
      res = false;
    }
    return res;
  }
}
