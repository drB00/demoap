import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Airport } from 'src/app/shared/models/airport.model';
import { Flight } from 'src/app/shared/models/flight.model';
import { Flightlayout } from 'src/app/shared/models/layout.model';
import { Observable } from 'rxjs';
import { DictionaryService } from 'src/app/system/shared/services/dictionary.service';
import { AirportService } from 'src/app/system/shared/services/airport.service';
import { AircraftService } from 'src/app/system/shared/services/aircraft.service';
import { AircraftmodelService } from 'src/app/system/shared/services/aircraftmodel.service';
import { FlightService } from 'src/app/system/shared/services/flight.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { startWith, map } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-dialog-periodflight',
  templateUrl: './dialog-periodflight.component.html',
  styleUrls: ['./dialog-periodflight.component.scss']
})
export class DialogPeriodflightComponent implements OnInit {
  selectedDays = [];
  flightlayoutTypes: any[];
  flightTypes: any[];
  weekdays: any[];
  formPeriodFlight: FormGroup;
  formsPeriodDays: FormGroup[] = [];
  formsPeriodDaysDeparture: FormControl[] = [];
  formsPeriodDaysArrival: FormControl[] = [];
  formsPeriodExtraairports = [];
  formsPeriodExtraDaysArrival = [];

  airports: Airport[];
  aircrafts: any[];
  aircraftmodels: any[];

  filtered_departure_airport: Observable<any[]>;
  filtered_arrival_airport: Observable<any[]>;
  filtered_extra_airport: Observable<any[]>;
  timerId;

  flights: Flight[]; // список рейсов
  flightlayouts: Flightlayout[]; // список шаблонов рейсов

  flightCtrl: FormControl;
  flightlayoutCtrl: FormControl;
  filteredFlights: Observable<any[]>;
  filteredFlightlayouts: Observable<any[]>;

  timeMask: any[] = [/\d/, /\d/, ':', /\d/, /\d/];
  timeMaskInput: any[] = [/[0-2]/, /\d/, ':', /[0-6]/, /\d/];

  loadedlayout = false;

  selectedIndex = 0; // селектор типа интерфеса

  today: Date;

  // saveValid = false;
  constructor(
    private dictionaryService: DictionaryService,
    private airportService: AirportService,
    private aircraftService: AircraftService,
    private aircraftmodelService: AircraftmodelService,
    private flightService: FlightService,
    public dialogRef: MatDialogRef<DialogPeriodflightComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  async ngOnInit() {
    this.setForms();
    this.flightlayoutTypes = this.dictionaryService.flightlayoutsTypes;
    this.flightTypes = this.dictionaryService.flightType;
    this.weekdays = this.dictionaryService.weekdays;
    let airports = this.airportService.getWithCodefilterLimit(100, '');

    let aircraftmodels = this.aircraftmodelService.getAll();
    let aircrafts = this.aircraftService.getAll();
    let flights = this.flightService.getByscenario(this.data.scenario_id);
    this.flights = isNullOrUndefined(await flights) ? [] : await flights;
    this.flights = this.flights.filter(item => item.flight_number !== null);
    this.filteredFlights = this.flightCtrl.valueChanges.pipe(
      startWith(''),
      map(flight => {
        return flight ? this.filterFlights(flight) : this.flights;
      })
    );
    this.aircrafts = isNullOrUndefined(await aircrafts) ? [] : await aircrafts;
    this.aircrafts.unshift({ code: '', id: null });
    this.aircraftmodels = isNullOrUndefined(await aircraftmodels)
      ? []
      : await aircraftmodels;
    this.airports = isNullOrUndefined(await airports) ? [] : await airports;
  }

  setForms() {
    this.flightCtrl = new FormControl();
    this.flightlayoutCtrl = new FormControl();
    this.filteredFlights = this.flightCtrl.valueChanges.pipe(
      startWith(''),
      map(flight => {
        return flight ? this.filterFlights(flight) : this.flights;
      })
    );
    this.formPeriodFlight = new FormGroup({
      Flight_flight_number: new FormControl(null),
      aircraft_id: new FormControl(null),
      Flight_flight_id: new FormControl(null),
      Flight_flight_direction: new FormControl(0),
      Flight_flight_type: new FormControl(0, [Validators.required]),
      Flighttemplate_aircraftmodel_id: new FormControl(null, [
        Validators.required
      ]),
      Specificflight_aircraft_id: new FormControl(null, [Validators.required]),
      Flightlayout_type: new FormControl(1, [Validators.required]),
      Flightlayout_name: new FormControl(null),
      Flightlayout_id: new FormControl(null, [Validators.required]),
      Flighttemplate_fitst_day: new FormControl(null, [Validators.required]),
      Flighttemplate_last_day: new FormControl(null, [Validators.required]),
      Flighttemplate_flightlayout_id: new FormControl(null, [
        Validators.required
      ])
    });
    this.selectType();
  }

  onAddPeriodDay() {
    this.formsPeriodDays.push(
      new FormGroup({
        day: new FormControl(null),
        timebegin: new FormControl(
          this.formsPeriodDays[this.formsPeriodDays.length - 1].value.timebegin,
          [Validators.required]
        ),
        timeend: new FormControl(
          this.formsPeriodDays[this.formsPeriodDays.length - 1].value.timeend,
          [Validators.required]
        ),
        aircraft_id: new FormControl(
          this.formsPeriodDays[
            this.formsPeriodDays.length - 1
          ].value.aircraft_id
        ),
        flight_type: new FormControl(
          this.formsPeriodDays[
            this.formsPeriodDays.length - 1
          ].value.flight_type,
          [Validators.required]
        ),
        time: new FormControl(
          this.formsPeriodDays[this.formsPeriodDays.length - 1].value.time
        ),
        departure_airport_id: new FormControl(
          this.formsPeriodDays[
            this.formsPeriodDays.length - 1
          ].value.departure_airport_id,
          []
        ),
        arrival_airport_id: new FormControl(
          this.formsPeriodDays[
            this.formsPeriodDays.length - 1
          ].value.arrival_airport_id,
          []
        )
      })
    );
    this.formsPeriodExtraairports.push([]);
    this.formsPeriodExtraDaysArrival.push([]);
    this.formsPeriodDaysDeparture.push(
      new FormControl(null, [Validators.required])
    );
    this.formsPeriodDaysArrival.push(
      new FormControl(null, [Validators.required])
    );
  }

  onDelPeriodDay() {
    this.formsPeriodDays.pop();
    this.formsPeriodExtraairports.pop();
    this.formsPeriodExtraDaysArrival.pop();
    this.formsPeriodDaysDeparture.pop();
    this.formsPeriodDaysArrival.pop();
  }

  setFlight_number() {
    let flight = this.flights.find(
      item => item.flight_number === this.flightCtrl.value
    );
    if (isNullOrUndefined(flight)) {
      // this.formPeriodFlight.value.Flight_flight_number = this.flightCtrl.value;
      // this.formPeriodFlight.value.Flight_flight_id = null;
      this.formPeriodFlight.patchValue({
        Flight_flight_number: this.flightCtrl.value,
        Flight_flight_id: null
      });
    } else {
      // this.formPeriodFlight.value.Flight_flight_number = '';
      // this.formPeriodFlight.value.Flight_flight_id = flight.id;
      this.formPeriodFlight.patchValue({
        // Flight_flight_number: null,
        Flight_flight_number: this.flightCtrl.value,
        Flight_flight_id: flight.id
      });
    }
    // console.log(this.formPeriodFlight.value);
  }

  // setFlight_numberInput() {
  //   if (this.formPeriodFlight.value.Flight_flight_number !== '') {
  //     this.flightCtrl.setValue('');
  //     this.flightCtrl.disable();
  //     this.formPeriodFlight.value.Flight_flight_id = null;
  //   } else {
  //     this.flightCtrl.enable();
  //   }
  // }

  selectType() {
    this.formsPeriodDays = [];
    this.formsPeriodDaysDeparture = [];
    this.formsPeriodDaysArrival = [];
    this.formsPeriodDaysDeparture.push(
      new FormControl(null, [Validators.required])
    );
    this.formsPeriodDaysArrival.push(
      new FormControl(null, [Validators.required])
    );
    this.formsPeriodDays.push(
      new FormGroup({
        day: new FormControl(null),
        timebegin: new FormControl(null, [Validators.required]),
        timeend: new FormControl(null, [Validators.required]),
        aircraft_id: new FormControl(null),
        flight_type: new FormControl(null),
        time: new FormControl(null),
        departure_airport_id: new FormControl(null),
        arrival_airport_id: new FormControl(null)
      })
    );
    this.formsPeriodExtraairports.push([]);
    this.formsPeriodExtraDaysArrival.push([]);
  }

  selectDays(i) {
    this.formsPeriodDays.length = 0;
    for (let j = 0; j < this.formsPeriodDays.length; j++) {
      this.selectedDays = this.selectedDays.concat(
        this.formsPeriodDays[i].value.day
      );
    }
  }

  isDaySelected(i, k) {
    for (let j = 0; j < this.formsPeriodDays.length; j++) {
      if (j === i) {
        continue;
      } else if (!isNullOrUndefined(this.formsPeriodDays[j].value.day)) {
        for (let _i = 0; _i < this.formsPeriodDays[j].value.day.length; _i++) {
          if (k === this.formsPeriodDays[j].value.day[_i]) {
            return true;
          }
        }
      }
    }
    return false;
  }

  onCalcTime(b) {}

  onAddPeriodExtraairport(i) {
    this.formsPeriodExtraDaysArrival[i].push(
      new FormControl(null, [Validators.required])
    );
    this.formsPeriodExtraairports[i].push(
      new FormGroup({
        airport_id: new FormControl(null),
        timebegin: new FormControl(null, [Validators.required]),
        timeend: new FormControl(null, [Validators.required]),
        time: new FormControl(null)
      })
    );
  }

  onDelPeriodExtraairport(i) {
    this.formsPeriodExtraairports[i].pop();
    this.formsPeriodExtraDaysArrival[i].pop();
  }

  saveValid() {
    let res = true;

    if (
      isNullOrUndefined(this.formPeriodFlight.value.Flighttemplate_fitst_day) ||
      !(
        new Date(
          this.formPeriodFlight.value.Flighttemplate_fitst_day
        ) instanceof Date &&
        !isNaN(
          new Date(
            this.formPeriodFlight.value.Flighttemplate_fitst_day
          ).valueOf()
        )
      )
    ) {
      return false;
    }
    if (
      isNullOrUndefined(this.formPeriodFlight.value.Flighttemplate_last_day) ||
      !(
        new Date(this.formPeriodFlight.value.Flighttemplate_last_day) instanceof
          Date &&
        !isNaN(
          new Date(
            this.formPeriodFlight.value.Flighttemplate_last_day
          ).valueOf()
        )
      )
    ) {
      return false;
    }
    if (
      isNullOrUndefined(
        this.formPeriodFlight.value.Flighttemplate_aircraftmodel_id
      )
    ) {
      return false;
    }
    if (isNullOrUndefined(this.formPeriodFlight.value.Flightlayout_type)) {
      return false;
    }
    if (
      (isNullOrUndefined(this.formPeriodFlight.value.Flight_flight_number) ||
        this.formPeriodFlight.value.Flight_flight_number.trim() === '') &&
      isNullOrUndefined(this.formPeriodFlight.value.Flight_flight_id)
    ) {
      return false;
    }
    for (let j = 0; j < this.formsPeriodDays.length; j++) {
      if (
        isNullOrUndefined(this.formsPeriodDays[j].value.day) ||
        this.formsPeriodDays[j].value.day.length === 0
      ) {
        return false;
      }
      if (this.formsPeriodDays[j].invalid === true) {
        return false;
      }
      if (this.formsPeriodDaysDeparture[j].invalid === true) {
        return false;
      }
      if (this.formsPeriodDaysArrival[j].invalid === true) {
        return false;
      }
      if (
        !isNullOrUndefined(this.formsPeriodExtraairports[j]) &&
        this.formsPeriodExtraairports[j].length > 0
      ) {
        for (let k = 0; k < this.formsPeriodExtraairports[j].length; k++) {
          if (this.formsPeriodExtraDaysArrival[j][k].invalid === true) {
            return false;
          }
          if (this.formsPeriodExtraairports[j][k].invalid === true) {
            return false;
          }
        }
      }
    }

    return res;
  }

  onSave() {
    let obj = {};
    obj['_type'] = 'multi';
    obj['aircraft_id'] = this.formPeriodFlight.value.aircraft_id;
    let first_day = new Date(
      this.formPeriodFlight.value.Flighttemplate_fitst_day
    );
    first_day.setHours(0);
    first_day.setMinutes(0 - first_day.getTimezoneOffset());
    obj['first_day'] = first_day;
    let last_day = new Date(
      this.formPeriodFlight.value.Flighttemplate_last_day
    );
    last_day.setHours(0);
    last_day.setMinutes(0 - last_day.getTimezoneOffset());
    obj['first_day'] = first_day;
    obj['last_day'] = last_day;
    // obj['flight_number'] = this.formPeriodFlight.value.Flight_flight_number;
    // obj['flight_type'] = this.formPeriodFlight.value.Flight_flight_type;
    if (isNullOrUndefined(this.formPeriodFlight.value.Flight_flight_id)) {
      obj['flight_number'] = this.formPeriodFlight.value.Flight_flight_number;
      obj['flight_type'] = this.formPeriodFlight.value.Flight_flight_type;
    } else {
      obj['flight_id'] = this.formPeriodFlight.value.Flight_flight_id;
    }
    if (this.loadedlayout !== true) {
      obj['type'] = +this.formPeriodFlight.value.Flightlayout_type;
      obj['name'] = this.formPeriodFlight.value.Flightlayout_name;
    }
    obj[
      'aircraftmodel_id'
    ] = this.formPeriodFlight.value.Flighttemplate_aircraftmodel_id;
    obj['lsf'] = [];

    /**Массив с днями шаблона */
    let num: number;
    if (obj['type'] === 1) {
      num = 7;
    } else if (obj['type'] === 2) {
      num = 31;
    } else if (obj['type'] === 3) {
      num = 2;
    }
    for (let i = 0; i < num; i++) {
      for (let j = 0; j < this.formsPeriodDays.length; j++) {
        if (
          !isNullOrUndefined(
            this.formsPeriodDays[j].value.day.find(item => item === i)
          )
        ) {
          // let _day = this.periodDays[j].value;
          let _lsf = {};
          // _lsf['aircraft_id'] = this.formsPeriodDays[j].value['aircraft_id'];
          let _timebegin: string; // P2DT1H20M0S
          let _timeend: string;
          _timebegin = `P${i}DT${
            this.formsPeriodDays[j].value['timebegin'].split(':')[0]
          }H${this.formsPeriodDays[j].value['timebegin'].split(':')[1]}M0S`;
          _timeend = `P${
            +this.formsPeriodDays[j].value['timebegin'].split(':')[0] <
            +this.formsPeriodDays[j].value['timeend'].split(':')[0]
              ? i
              : i + 1
          }DT${this.formsPeriodDays[j].value['timeend'].split(':')[0]}H${
            this.formsPeriodDays[j].value['timeend'].split(':')[1]
          }M0S`;
          _lsf['departure_datetime'] = _timebegin;
          _lsf['arrival_datetime'] = _timeend;

          _lsf['departure_airport_id'] = this.formsPeriodDaysDeparture[
            j
          ].value.id;
          // _lsf['departure_airport_id'] = this.formsPeriodDays[j].value[
          //   'departure_airport_id'
          // ];
          _lsf['arrival_airport_id'] = this.formsPeriodDaysArrival[j].value.id;
          // _lsf['arrival_airport_id'] = this.formsPeriodDays[j].value[
          //   'arrival_airport_id'
          // ];
          _lsf['lfp'] = [];
          let _lsp_first = {};

          _lsp_first['airport_id'] = this.formsPeriodDaysDeparture[j].value.id;
          // _lsp_first['airport_id'] = this.formsPeriodDays[j].value[
          //   'departure_airport_id'
          // ];
          let __timebegin_first: string;
          let __timeend_first: string;
          __timebegin_first = `P${i}DT${
            this.formsPeriodDays[j].value['timebegin'].split(':')[0]
          }H${this.formsPeriodDays[j].value['timebegin'].split(':')[1]}M0S`;
          __timeend_first = null;
          _lsp_first['departure_datetime'] = __timebegin_first;
          _lsp_first['arrival_datetime'] = __timeend_first;
          _lsf['lfp'].push(_lsp_first);
          this.formsPeriodExtraairports[j].map((point, index, arr) => {
            let _lsp = {};
            _lsp['airport_id'] = this.formsPeriodExtraDaysArrival[j][
              index
            ].value.id;
            // _lsp['airport_id'] = point.value['airport_id'];
            let __timebegin: string;
            let __timeend: string;
            __timebegin = `P${i}DT${point.value['timebegin'].split(':')[0]}H${
              point.value['timebegin'].split(':')[1]
            }M0S`;
            __timeend = `P${
              +point.value['timebegin'].split(':')[0] >
              +point.value['timeend'].split(':')[0]
                ? i
                : i + 1
            }DT${point.value['timeend'].split(':')[0]}H${
              point.value['timeend'].split(':')[1]
            }M0S`;
            _lsp['departure_datetime'] = __timebegin;
            _lsp['arrival_datetime'] = __timeend;
            _lsf['lfp'].push(_lsp);
          });
          let _lsp_last = {};
          _lsp_last['airport_id'] = this.formsPeriodDaysArrival[j].value.id;
          // _lsp_last['airport_id'] = this.formsPeriodDays[j].value[
          //   'arrival_airport_id'
          // ];
          let __timebegin_last: string;
          let __timeend_last: string;
          __timebegin_last = null;
          __timeend_last = `P${
            +this.formsPeriodDays[j].value['timebegin'].split(':')[0] <
            +this.formsPeriodDays[j].value['timeend'].split(':')[0]
              ? i
              : i + 1
          }DT${this.formsPeriodDays[j].value['timeend'].split(':')[0]}H${
            this.formsPeriodDays[j].value['timeend'].split(':')[1]
          }M0S`;
          _lsp_last['departure_datetime'] = __timebegin_last;
          _lsp_last['arrival_datetime'] = __timeend_last;
          _lsf['lfp'].push(_lsp_last);
          obj['lsf'].push(_lsf);
          break;
        }
      }
    }
    this.dialogRef.close(obj);
  }

  filterFlights(str: any) {
    return this.flights.filter(
      flight =>
        flight.flight_number.toLowerCase().indexOf(str.toLowerCase()) === 0
    );
  }

  displayFn(flight?: any): string | undefined {
    return flight ? `${flight}` : undefined;
  }

  displayAn(airport?: any): string | undefined {
    return airport ? `${airport.code} ${airport.name_rus}` : undefined;
  }

  inputLink_departure(i) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.formsPeriodDaysDeparture[i].value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_departure_airport = this.formsPeriodDaysDeparture[
            i
          ].valueChanges.pipe(
            startWith<any>(''),
            map(value =>
              typeof value === 'string'
                ? value
                : isNullOrUndefined(value)
                ? value
                : value.code
            ),
            map(code => (code ? this._filter(code) : this.airports.slice()))
          );
        } catch {}
      }
    }, 250);
  }
  inputLink_arrival(i) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.formsPeriodDaysArrival[i].value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_arrival_airport = this.formsPeriodDaysArrival[
            i
          ].valueChanges.pipe(
            startWith<any>(''),
            map(value =>
              typeof value === 'string'
                ? value
                : isNullOrUndefined(value)
                ? value
                : value.code
            ),
            map(code => (code ? this._filter(code) : this.airports.slice()))
          );
        } catch {}
      }
    }, 250);
  }
  inputLink_arrival_extra(i, j) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.formsPeriodExtraDaysArrival[i][j].value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_arrival_airport = this.formsPeriodExtraDaysArrival[i][
            j
          ].valueChanges.pipe(
            startWith<Airport | string>(''),
            map(value =>
              typeof value === 'string'
                ? value
                : isNullOrUndefined(value)
                ? value
                : value['code']
            ),
            map(code => (code ? this._filter(code) : this.airports.slice()))
          );
        } catch {}
      }
    }, 250);
  }

  private _filter(code): any[] {
    const filterValue = code.toLowerCase();
    return isNullOrUndefined(this.airports)
      ? null
      : this.airports.filter(
          airport => airport.code.toLowerCase().indexOf(filterValue) !== -1
        );
  }
}
