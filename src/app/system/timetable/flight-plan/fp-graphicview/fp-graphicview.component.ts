import {
  Component,
  OnInit,
  NgZone,
  ElementRef,
  ViewChild,
  ChangeDetectionStrategy,
  OnDestroy,
  ChangeDetectorRef,
  HostListener
} from '@angular/core';
import { Airuser } from 'src/app/shared/models/airuser.model';
import { Scenario } from 'src/app/shared/models/scenario.model';
import { AircraftService } from 'src/app/system/shared/services/aircraft.service';
import { AircraftmodelService } from 'src/app/system/shared/services/aircraftmodel.service';
import { AircraftmodelgroupService } from 'src/app/system/shared/services/aircraftmodelgroup.service';
import { MatDialog, MatSnackBar, MatMenuTrigger } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';
import { TimetablestoreService } from 'src/app/system/shared/services/timetablestore.service';
import { FlightService } from 'src/app/system/shared/services/flight.service';
import { Specificflight } from 'src/app/shared/models/specificflight.model';
import { SpecificflightService } from 'src/app/system/shared/services/specificflight.service';
import { FlighttemplateService } from 'src/app/system/shared/services/flighttemplate.service';
import { DictionaryService } from 'src/app/system/shared/services/dictionary.service';
import { AirportService } from 'src/app/system/shared/services/airport.service';
import { ScenarioService } from 'src/app/system/shared/services/scenario.service';
import { PositionService } from 'src/app/system/shared/services/position.service';
import { SubdivisionService } from 'src/app/system/shared/services/subdivision.service';
import { AiruserService } from 'src/app/system/shared/services/airuser.service';
import { isNullOrUndefined } from 'util';
import { WebsocketService, WS_API } from 'src/app/websocket';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GlobalsettingsService } from 'src/app/system/shared/services/globalsettings.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import {
  Airuseractivity,
  Activity
} from 'src/app/shared/models/activity.model';
import { Flightpoint } from 'src/app/shared/models/flightpoint.model';
import { Flighttemplate } from 'src/app/shared/models/flighttemplate.model';
import { Flight } from 'src/app/shared/models/flight.model';
import { ActivityService } from 'src/app/system/shared/services/activity.service';
import { FlightpointService } from 'src/app/system/shared/services/flightpoint.service';
import { AiruseractivityService } from 'src/app/system/shared/services/airuseractivity.service';
import { DialogScenarioComponent } from '../dialog-scenario/dialog-scenario.component';
import { DialogFlightComponent } from '../dialog-flight/dialog-flight.component';
import { DialogYesnoComponent } from 'src/app/system/shared/components/dialog-yesno/dialog-yesno.component';
import { DialogFlightregularComponent } from '../dialog-flightregular/dialog-flightregular.component';
import { DialogPeriodflightComponent } from '../dialog-periodflight/dialog-periodflight.component';
import _ from 'lodash';

@Component({
  selector: 'app-fp-graphicview',
  templateUrl: './fp-graphicview.component.html',
  styleUrls: ['./fp-graphicview.component.scss'],
  // encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FpGraphicviewComponent implements OnInit, OnDestroy {
  aircraftmodelgroups = [];
  aircraftmodels = [];
  aircrafts = [];
  airports = [];

  flights: Flight[] = [];
  flighttempltates: Flighttemplate[] = [];
  specificflights: Specificflight[] = [];
  flightpoints: Flightpoint[] = [];
  activitys: Activity[];
  airuseractivitys: Airuseractivity[];

  pilots: Airuser[] = [];
  pilotTypes = [];

  scenarioes: Scenario[];

  selectedIndex = 0; // индекс вкладки для выбора

  messages$: Observable<string>;
  timeout;

  filterForm: FormGroup;
  from: Date;
  to: Date;
  now: Date;

  accessLevel = 0; // 0 - none, 1 - view, 2 - edit

  scales = [10, 5, 2];
  scale = 0;
  scroll = 0;

  width; // ширина поля (по датам от и до)
  pastWidth; // ширина поля (по текущей даты)
  dayWidth; // ширина дня
  timeScale = []; // разделители времени
  dateScale = []; // разделители дат
  dayBlocks = []; // блоки дней

  intervalTime;

  outofaircrafts = []; // рейсы, которым не назначены ВС
  dragSpecificflight: Specificflight; // перетаскиваемый рейс

  @ViewChild('timelineArea', { static: false }) timelineArea: ElementRef;

  @ViewChild(MatMenuTrigger, { static: false })
  private menuTrigger: MatMenuTrigger;
  selectedSpecificflight: Specificflight; // для контекстного меню

  detectTimer;
  count = new Date();

  selectedSpecificflights: Specificflight[] = [];

  currentaircraftmodel_id;

  @HostListener('window:keydown', ['$event'])
  onKeyPress(event: KeyboardEvent) {
    if (event.altKey && event.keyCode == 46) {
      this.deleteSelected();
    }
  }

  constructor(
    private cdr: ChangeDetectorRef,
    public authService: AuthService,
    private aircraftService: AircraftService,
    private aircraftmodelService: AircraftmodelService,
    private aircraftmodelgroupService: AircraftmodelgroupService,
    private timetablestoreService: TimetablestoreService,
    private flightService: FlightService,
    private specificflightService: SpecificflightService,
    private flighttemplateService: FlighttemplateService,
    private flightpointService: FlightpointService,
    private activityService: ActivityService,
    // private airuseractivityService: AiruseractivityService,
    private airportService: AirportService,
    private scenarioService: ScenarioService,
    private dictionaryService: DictionaryService,
    private airuserService: AiruserService,
    private positionService: PositionService,
    private subdivisionService: SubdivisionService,
    private globalsettingsService: GlobalsettingsService,
    private wsService: WebsocketService,
    private dialog: MatDialog,
    private spinnerService: NgxSpinnerService,
    private zone: NgZone,
    private snackBar: MatSnackBar
  ) {
    this.detectTimer = setInterval(() => {
      this.count = new Date();
      this.cdr.detectChanges();
    }, 1000);
  }

  async deleteSelected() {
    if (this.selectedSpecificflights.length === 0) {
      return;
    }
    let dialogRef = this.dialog.open(DialogYesnoComponent, {
      panelClass: 'app-dialog-overview-yes-no-component',
      data: {
        title: `Вы действительно хотите удалить выбранные рейсы?`
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (result === true) {
        this.spinnerService.show();
        // for (let i = 0; i < this.selectedSpecificflights.length; i++) {
        //   try {
        //     /**Удаление с layout */
        //     let layout = {};
        //     layout['flight_id'] = this.selectedSpecificflights[i].flight_id;
        //     layout['type'] = 0; // Один день
        //     layout['name'] = null;
        //     layout['first_day'] = this.selectedSpecificflights[
        //       i
        //     ].departure_datetime;
        //     layout['last_day'] = this.selectedSpecificflights[
        //       i
        //     ].arrival_datetime;
        //     layout['aircraftmodel_id'] = this.selectedSpecificflights[
        //       i
        //     ].aircraftmodel_id;
        //     layout['lsf'] = [];
        //     let resGen = await this.flightService.createlayout({
        //       layoutdata: JSON.stringify(layout)
        //     });
        //     let resSet = await this.flightService.setlayout({
        //       aircraft_id: this.selectedSpecificflights[i].aircraft_id,
        //       flight_id: layout['flight_id'],
        //       first_day: layout['first_day'],
        //       last_day: layout['last_day'],
        //       flightlayout_id: resGen.id
        //     });
        //   } catch (err) {
        //     this.globalsettingsService.exceptionHandling(err);
        //   }
        // }
        let resSetArray = [];
        await Promise.all(
          this.selectedSpecificflights.map(async specificflight => {
            try {
              /**Удаление с layout */
              let layout = {};
              layout['flight_id'] = specificflight.flight_id;
              layout['type'] = 0; // Один день
              layout['name'] = null;
              layout['first_day'] = specificflight.departure_datetime;
              layout['last_day'] = specificflight.arrival_datetime;
              layout['aircraftmodel_id'] = specificflight.aircraftmodel_id;
              layout['lsf'] = [];
              let resGen = this.flightService.createlayout({
                layoutdata: JSON.stringify(layout)
              });
              resSetArray.push({
                aircraft_id: specificflight.aircraft_id,
                flight_id: layout['flight_id'],
                first_day: layout['first_day'],
                last_day: layout['last_day'],
                flightlayout_id: (await resGen).id
              });
              return resGen;
            } catch (err) {
              this.globalsettingsService.exceptionHandling(err);
            }
          })
        );
        if (resSetArray.length > 0) {
          try {
            await this.flightService.setlayoutmulti(resSetArray);
            // this.flightService.setlayoutmulti([resSetArray[0]]);
            // this.flightService.setlayoutmulti([resSetArray[1]]);
            // this.flightService.setlayoutmulti([resSetArray[2]]);
            // this.flightService.setlayoutmulti([resSetArray[3]]);
            // this.flightService.setlayoutmulti([resSetArray[4]]);
          } catch (err) {
            this.globalsettingsService.exceptionHandling(err);
          } finally {
            await this.getFlights(false);
            this.spinnerService.hide();
          }
        }
        // await Promise.all(
        //   this.selectedSpecificflights.map(async specificflight => {
        //     try {
        //       /**Удаление с layout */
        //       let layout = {};
        //       layout['flight_id'] = specificflight.flight_id;
        //       layout['type'] = 0; // Один день
        //       layout['name'] = null;
        //       layout['first_day'] = specificflight.departure_datetime;
        //       layout['last_day'] = specificflight.arrival_datetime;
        //       layout['aircraftmodel_id'] = specificflight.aircraftmodel_id;
        //       layout['lsf'] = [];
        //       let resGen = await this.flightService.createlayout({
        //         layoutdata: JSON.stringify(layout)
        //       });
        //       let resSet = this.flightService.setlayout({
        //         aircraft_id: specificflight.aircraft_id,
        //         flight_id: layout['flight_id'],
        //         first_day: layout['first_day'],
        //         last_day: layout['last_day'],
        //         flightlayout_id: resGen.id
        //       });
        //       return resSet;
        //     } catch (err) {
        //       this.globalsettingsService.exceptionHandling(err);
        //     }
        //   })
        // );
      }
    });
  }

  ngOnDestroy(): void {
    clearInterval(this.detectTimer);
  }

  async ngOnInit() {
    let today = new Date();
    today.setDate(today.getDate() - 20);
    let tomorrow = new Date();
    tomorrow.setDate(today.getDate() + 10); // по умолчанию фильтр на 10 дней с текущей даты
    /** Установка дат от и до из localstorage */
    let from = isNullOrUndefined(this.globalsettingsService.timetableFilter)
      ? null
      : new Date(this.globalsettingsService.timetableFilter.from);
    let to = isNullOrUndefined(this.globalsettingsService.timetableFilter)
      ? null
      : new Date(this.globalsettingsService.timetableFilter.to);
    this.filterForm = new FormGroup({
      from: new FormControl(isNullOrUndefined(from) ? today : from, [
        Validators.required
      ]),
      to: new FormControl(isNullOrUndefined(to) ? tomorrow : to, [
        Validators.required
      ])
    });
    await this.getData();
    /**Подписка на сокет*/
    this.messages$ = this.wsService.addEventListener(WS_API.EVENTS.MESSAGES);
    this.messages$.subscribe(async data => {
      data = data.indexOf('action') !== -1 ? JSON.parse(data) : data;
      switch (data['resourse']) {
        case 'flightpoint':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights(false);
          }, 500);
          break;
        case 'specificflight':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights(false);
          }, 500);
          break;
        case 'flighttemplate':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights(false);
          }, 500);
          break;
        case 'flight':
          clearTimeout(this.timeout);
          this.timeout = setTimeout(() => {
            this.getFlights(false);
          }, 500);
          break;
      }
    });
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add flight');
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add flighttemplate');
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add specificflight');
    this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, 'Add flightpoint');
    /**Значение для скрола, масштаба таймлайна */
    this.scroll = isNullOrUndefined(this.globalsettingsService.timetableScroll)
      ? 0
      : +this.globalsettingsService.timetableScroll;
    this.scale = isNullOrUndefined(this.globalsettingsService.timetableScale)
      ? 0
      : +this.globalsettingsService.timetableScale;
    this.scenarioes.sort((a, b) => {
      return a.status > b.status ? 1 : -1;
    });
    this.spinnerService.show();
    await this.getFlights();
    this.spinnerService.hide();

    if (this.authService.loggedInUser.isadmin === true) {
      this.accessLevel = 2;
    } else {
      let access = this.authService.loggedInUserAccesses.find(
        item => item.resourcename === 'timetable'
      );
      if (!isNullOrUndefined(access)) {
        if (access.get === true) {
          this.accessLevel = 1;
        }
        if (access.post === true) {
          this.accessLevel = 2;
        }
      }
    }
    this.count = new Date();
    this.cdr.detectChanges();
    this.zone.runOutsideAngular(() => {
      this.timelineArea.nativeElement.addEventListener(
        'contextmenu',
        (e: MouseEvent) => {
          e.preventDefault();
        }
      );
    });
  }

  async selectScenario(event) {
    // if (event.tab.textLabel === 'Добавить') {
    if (true) {
      let dialogRef = this.dialog.open(DialogScenarioComponent, {
        panelClass: 'app-dialog-overview-set-string-component',
        data: {
          title: 'Сценарий',
          question: 'Введите название нового сценария.'
        }
      });
      dialogRef.afterClosed().subscribe(async result => {
        if (isNullOrUndefined(result)) {
          return;
        }
        try {
          let obj = {};
          obj['status'] = result.status;
          obj['name'] = result.str;
          let scenario = await this.scenarioService.postOne(obj);
          this.scenarioes.push(scenario);
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          this.selectedIndex = event.index - 1;
        }
      });
    } else {
      await this.getFlights(false);
    }
  }

  onContext(event: any, specificflight: Specificflight): void {
    if (this.accessLevel < 2) {
      return;
    }
    let menu = document.getElementById('menuBtn');
    menu.style.display = '';
    menu.style.position = 'absolute';
    menu.style.left = event.pageX + 5 + 'px';
    menu.style.top = event.pageY + 5 + 'px';
    this.menuTrigger.openMenu();
    this.selectedSpecificflight = specificflight;
  }

  async getData() {
    this.pilotTypes = this.dictionaryService.pilotTypes;
    try {
      let aircraftmodelgroups = this.aircraftmodelgroupService.getAll();
      let aircraftmodels = this.aircraftmodelService.getAll();
      let aircrafts = this.aircraftService.getAll();
      let airports = this.airportService.getAll();
      let scenarioes = this.scenarioService.getAll();
      let _positions = this.positionService.getAll();
      let _subdivisions = this.subdivisionService.getAll();
      let _airusers = this.airuserService.getAll();
      let airusers = isNullOrUndefined(await _airusers) ? [] : await _airusers;
      let positions = isNullOrUndefined(await _positions)
        ? []
        : await _positions;
      let subdivisions = isNullOrUndefined(await _subdivisions)
        ? []
        : await _subdivisions;
      airusers.map(airuser => {
        let position = positions.find(item => item.id === airuser.position_id);
        if (isNullOrUndefined(position)) {
          return false;
        }
        let subdivision = subdivisions.find(
          item => item.id === position.subdivision_id
        );
        if (isNullOrUndefined(subdivision)) {
          return false;
        }
        if (!isNullOrUndefined(airuser.pilot_type) && airuser.pilot_type > 0) {
          let pilotType = this.pilotTypes.find(
            item => item.id === airuser.pilot_type
          );
          if (!isNullOrUndefined(pilotType)) {
            airuser['pilotTypestr'] = pilotType.name;
            this.pilots.push(airuser);
          }
        }
      });
      this.aircraftmodelgroups = isNullOrUndefined(await aircraftmodelgroups)
        ? []
        : await aircraftmodelgroups;
      this.aircraftmodels = isNullOrUndefined(await aircraftmodels)
        ? []
        : await aircraftmodels;
      this.aircrafts = isNullOrUndefined(await aircrafts)
        ? []
        : await aircrafts;
      this.airports = isNullOrUndefined(await airports) ? [] : await airports;
      this.scenarioes = isNullOrUndefined(await scenarioes)
        ? []
        : await scenarioes;
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    }
  }

  async getFlights(init = true) {
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.from) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.from).valueOf())
      )
    ) {
      return;
    }
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.to) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.to).valueOf())
      )
    ) {
      return;
    }
    this.flights.length = 0;
    this.flighttempltates.length = 0;
    this.specificflights.length = 0;
    this.flightpoints.length = 0;
    if (init === true) {
      await this.getScale();
    }
    try {
      let __from = new Date(
        this.from.getFullYear(),
        this.from.getMonth(),
        this.from.getDate() - 1,
        0,
        0,
        0
      );
      let __to = new Date(
        this.to.getFullYear(),
        this.to.getMonth(),
        this.to.getDate() + 1,
        0,
        0,
        0
      );
      /**Получение одиночных рейсов и активностей в выбранном диапазоне */
      let _specificflights = this.specificflightService.getByDatesScenario(
        `${__from.getFullYear()}-${__from.getMonth() + 1}-${__from.getDate()}`,
        `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
        this.scenarioes[this.selectedIndex].id
      );
      let _activitys = this.activityService.getByDatesScenario(
        `${__from.getFullYear()}-${__from.getMonth() + 1}-${__from.getDate()}`,
        `${__to.getFullYear()}-${__to.getMonth() + 1}-${__to.getDate()}`,
        this.scenarioes[this.selectedIndex].id
      );
      let specificflights: Specificflight[] = isNullOrUndefined(
        await _specificflights
      )
        ? []
        : await _specificflights;
      let activitys: Activity[] = isNullOrUndefined(await _activitys)
        ? []
        : await _activitys;
      /**Получение связанных с ними данных по массивам id */
      let specificflights_id = specificflights.map(item => item.id);
      let flights_id: number[] = specificflights.map(item => item.flight_id);
      let flighttemplates_id: number[] = specificflights.map(
        item => item.flighttemplate_id
      );
      let used = {};
      flights_id = flights_id.filter(obj => {
        return obj in used ? 0 : (used[obj] = 1);
      });
      used = {};
      flighttemplates_id = flighttemplates_id.filter(obj => {
        return obj in used ? 0 : (used[obj] = 1);
      });
      // let activitys_id: number[] = activitys.map(item => item.id);
      let _flighttemplates = this.flighttemplateService.getByIds(
        flighttemplates_id
      );

      let chunk = 300;
      let arrspecificflights_id = [];
      for (let i = 0, j = specificflights_id.length; i < j; i += chunk) {
        arrspecificflights_id.push(specificflights_id.slice(i, i + chunk));
      }

      let flightpoints = [];
      await Promise.all(
        arrspecificflights_id.map(async ids => {
          let _res = await this.flightpointService.getBySpecificflights(ids);
          let res = isNullOrUndefined(await _res) ? [] : await _res;
          flightpoints = [...flightpoints, ...res];
        })
      );
      // let flightpoints: Flightpoint[] = isNullOrUndefined(await _flightpoints)
      //   ? []
      //   : await _flightpoints;

      // specificflights_id = specificflights_id.slice(0, 300);
      // let _flightpoints = this.flightpointService.getBySpecificflights(
      //   specificflights_id
      // );
      let _flights = this.flightService.getByIds(flights_id);
      // let _airuseractivitys = this.airuseractivityService.getByIds(
      //   activitys_id
      // );
      let flighttemplates: Flighttemplate[] = isNullOrUndefined(
        await _flighttemplates
      )
        ? []
        : await _flighttemplates;
      // let flights_id1: number[] = flighttemplates.map(item => item.flight_id);
      // flights_id = [...flights_id, ...flights_id1];
      // used = {};
      // flights_id = flights_id.filter(obj => {
      //   return obj in used ? 0 : (used[obj] = 1);
      // });
      let flights: Flight[] = isNullOrUndefined(await _flights)
        ? []
        : await _flights;
      // let flightpoints: Flightpoint[] = isNullOrUndefined(await _flightpoints)
      //   ? []
      //   : await _flightpoints;
      // let airuseractivitys: Airuseractivity[] = isNullOrUndefined(
      //   await _airuseractivitys
      // )
      //   ? []
      //   : await _airuseractivitys;
      // console.log(specificflights)

      // console.log(specificflights);
      // console.log(flightpoints);
      // specificflights.map(async item => {
      //   if (item.arrival_airport_id === 7) {
      //     await this.specificflightService.putOneById(item.id, {
      //       arrival_airport_id: 9383
      //     });
      //   }
      //   if (item.departure_airport_id === 7) {
      //     await this.specificflightService.putOneById(item.id, {
      //       departure_airport_id: 9383
      //     });
      //   }
      //   if (item.arrival_airport_id === 9) {
      //     await this.specificflightService.putOneById(item.id, {
      //       arrival_airport_id: 1306
      //     });
      //   }
      //   if (item.departure_airport_id === 9) {
      //     await this.specificflightService.putOneById(item.id, {
      //       departure_airport_id: 1306
      //     });
      //   }
      //   if (item.arrival_airport_id === 10) {
      //     await this.specificflightService.putOneById(item.id, {
      //       arrival_airport_id: 2946
      //     });
      //   }
      //   if (item.departure_airport_id === 10) {
      //     await this.specificflightService.putOneById(item.id, {
      //       departure_airport_id: 2946
      //     });
      //   }
      // });
      // await Promise.all(
      //   flightpoints.map(async item => {
      //     if (item.airport_id === 7) {
      //       await this.flightpointService.putOneById(item.id, {
      //         airport_id: 9383
      //       });
      //     }
      //     if (item.end_airport_id === 7) {
      //       await this.flightpointService.putOneById(item.id, {
      //         end_airport_id: 9383
      //       });
      //     }
      //     if (item.airport_id === 9) {
      //       await this.flightpointService.putOneById(item.id, {
      //         airport_id: 1306
      //       });
      //     }
      //     if (item.end_airport_id === 9) {
      //       await this.flightpointService.putOneById(item.id, {
      //         end_airport_id: 1306
      //       });
      //     }
      //     if (item.airport_id === 10) {
      //       await this.flightpointService.putOneById(item.id, {
      //         airport_id: 2946
      //       });
      //     }
      //     if (item.end_airport_id === 10) {
      //       await this.flightpointService.putOneById(item.id, {
      //         end_airport_id: 2946
      //       });
      //     }
      //   })
      // );
      // console.log(activitys)
      let res = await this.timetablestoreService.getAircrafts(
        this.aircrafts,
        flights,
        flighttemplates,
        specificflights,
        flightpoints,
        activitys,
        // airuseractivitys,
        this.pilots,
        this.from,
        this.to,
        this.scales[this.scale]
      );
      this.outofaircrafts = res.outofaircrafts;
      this.aircrafts = res.aircrafts;
      this.aircrafts.map(item => {
        let idx = [];
        for (let i = 0; i < item.specificflights.length; i++) {
          if (
            isNullOrUndefined(item.specificflights[i].flightpoints) ||
            item.specificflights[i].flightpoints.length === 0 ||
            +this.dateFromIsoString(
              item.specificflights[i].calc_arrival_datatime
            ) < +this.from
          ) {
            idx.push(i);
          }
        }
        idx.sort((a, b) => b - a);
        for (let i = 0; i < idx.length; i++) {
          item.specificflights.splice(idx[i], 1);
        }
      });
      this.flights = res.flights;
      this.flighttempltates = res.flighttemplates;
      this.specificflights = res.specificflights;
      this.flightpoints = res.flightpoints;
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      // this.spinnerService.hide();
    }
  }

  async updateData(spinner = false) {
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.from) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.from).valueOf())
      )
    ) {
      return;
    }
    if (
      isNullOrUndefined(this.filterForm.value.from) ||
      !(
        new Date(this.filterForm.value.to) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.to).valueOf())
      )
    ) {
      return;
    }
    if (spinner === true) {
      this.spinnerService.show();
    }
    let _from = new Date(this.filterForm.value.from);
    let from = new Date(
      _from.getFullYear(),
      _from.getMonth(),
      _from.getDate(),
      0,
      0,
      0
    );
    let _to = new Date(this.filterForm.value.to);
    let to = new Date(
      _to.getFullYear(),
      _to.getMonth(),
      _to.getDate(),
      0,
      0,
      0
    );

    if (+this.to === +to && +this.from === +from) {
      this.spinnerService.hide();
      return;
    }
    await this.resetScroll();
    await this.getFlights();
    this.spinnerService.hide();
  }

  resetScroll() {
    this.scroll = 0;
    this.globalsettingsService.timetableScroll = this.scroll;
    this.timelineArea.nativeElement.scrollTo(0, 0);
  }

  getScrollPos() {
    this.scroll = this.timelineArea.nativeElement.scrollLeft;
    this.globalsettingsService.timetableScroll = this.scroll;
  }

  getScale() {
    if (
      !(
        new Date(this.filterForm.value.from) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.from).valueOf())
      )
    ) {
      return;
    }
    if (
      !(
        new Date(this.filterForm.value.to) instanceof Date &&
        !isNaN(new Date(this.filterForm.value.to).valueOf())
      )
    ) {
      return;
    }
    this.timeScale.length = 0;
    this.dateScale.length = 0;
    this.dayBlocks.length = 0;
    this.outofaircrafts.length = 0;
    this.flights.length = 0;
    this.flighttempltates.length = 0;
    this.specificflights.length = 0;
    this.flightpoints.length = 0;
    this.dayWidth = 0;
    this.pastWidth = 0;
    delete this.now;
    let _from = new Date(this.filterForm.value.from);
    this.from = new Date(
      _from.getFullYear(),
      _from.getMonth(),
      _from.getDate(),
      0,
      0,
      0
    );
    let _to = new Date(this.filterForm.value.to);
    this.to = new Date(
      _to.getFullYear(),
      _to.getMonth(),
      _to.getDate(),
      0,
      0,
      0
    );
    let __to = new Date(
      this.to.getFullYear(),
      this.to.getMonth(),
      this.to.getDate() + 1,
      0,
      0,
      0
    );
    let res = +__to - +this.from;
    let _res = res / (1000 * 60 * this.scales[this.scale]);
    if (_res > 1000000) {
      this.globalsettingsService.openSnackBar(
        `Выбран слишком большой временной промежуток.`,
        'Ok'
      );
      return;
    }
    let s_filterfrom = new Date(this.from);
    s_filterfrom.setMinutes(
      s_filterfrom.getMinutes() - s_filterfrom.getTimezoneOffset()
    );
    let s_filterto = new Date(this.to);
    s_filterto.setMinutes(
      s_filterto.getMinutes() - s_filterto.getTimezoneOffset()
    );
    this.globalsettingsService.timetableFilter = {
      from: s_filterfrom.toISOString(),
      to: s_filterto.toISOString()
    };
    this.width = { str: `${_res}px`, val: _res };
    this.now = new Date();
    this.now.setMinutes(this.now.getMinutes() + this.now.getTimezoneOffset());
    clearInterval(this.intervalTime);
    if (this.now < this.from) {
      this.pastWidth = { str: `0px`, val: 0 };
    } else if (__to < this.now) {
      this.pastWidth = this.width;
    } else {
      this.pastWidth = this.getWidth(this.from, this.now);
      this.intervalTime = setInterval(() => {
        this.now = new Date();
        this.now.setMinutes(
          this.now.getMinutes() + this.now.getTimezoneOffset()
        );
        this.pastWidth = this.getWidth(this.from, this.now);
      }, 10000);
    }
    this.dayWidth = {
      str: `${24 * (60 / this.scales[this.scale]) - 0}px`,
      val: 24 * (60 / this.scales[this.scale]) - 0
    };
    let dev = +__to - +this.from;
    let _resScale = dev / (1000 * 60 * 60 * 24);
    let _width = Math.floor(this.width.val / 95);
    for (let i = 0; i < _width + 1; i++) {
      let _d =
        this.from.getTime() + (i + 1) * this.scales[this.scale] * 60000 * 95;
      this.timeScale.push(new Date(_d));
    }
    for (let i = 0; i < _resScale; i++) {
      this.dateScale.push({
        val: i * 24 * (60 / this.scales[this.scale]),
        _val: i * 24 * (60 / this.scales[this.scale]),
        date: new Date(
          this.from.getFullYear(),
          this.from.getMonth(),
          this.from.getDate() + i
        ),
        day: new Date(
          this.from.getFullYear(),
          this.from.getMonth(),
          this.from.getDate() + i
        ).toLocaleString('ru', { weekday: 'short' })
      });
    }
  }

  /**Перенос рейса drag'n drop */
  onDragStart(specificflight) {
    this.dragSpecificflight = specificflight;
  }
  onDragMove(event: number) {}
  async onDragStop(event, res_specificflight, idx) {
    if (event.enableToDrop === true) {
      try {
        /**Проверка на наличие пересечений с другими рейсами ВС */
        if (
          !this.timetablestoreService.isIntervalValid(
            event,
            res_specificflight.departure_datetime,
            res_specificflight.arrival_datetime
          )
        ) {
          this.globalsettingsService.openSnackBar(
            `Есть пересечения с уже существующими рейсами.`,
            'Ok'
          );
          setTimeout(() => {
            this.outofaircrafts[idx].specificflights.push(
              this.dragSpecificflight
            );
          }, 1);
          this.outofaircrafts[idx].specificflights.splice(
            this.outofaircrafts[idx].specificflights.findIndex(
              item => item.id === res_specificflight.id
            ),
            1
          );
          return;
        }
        let putSpecificflight = await this.specificflightService.putOneById(
          res_specificflight.id,
          { aircraft_id: event.aircraft_id }
        );
        let specificflight = this.outofaircrafts[idx].specificflights.find(
          item => item.id === putSpecificflight.id
        );
        specificflight.aircraft_id = putSpecificflight.aircraft_id;
        this.aircrafts.map(aircraft => {
          if (aircraft.id === putSpecificflight.aircraft_id) {
            aircraft.specificflights.push(specificflight);
            aircraft = this.setOnGroundTime(aircraft);
          }
        });
        this.outofaircrafts[idx].specificflights.splice(
          this.outofaircrafts[idx].specificflights.findIndex(
            item => item.id === putSpecificflight.id
          ),
          1
        );
      } catch (err) {
        this.globalsettingsService.exceptionHandling(err);
      }
    }
  }

  setOnGroundTime(aircraft) {
    let _specificflights = this.specificflights
      .filter(specificflight =>
        isNullOrUndefined(specificflight)
          ? false
          : specificflight.aircraft_id === aircraft.id
      )
      .sort((a, b) =>
        this.timetablestoreService.dateFromIsoString(a.calc_arrival_datatime) >
        this.timetablestoreService.dateFromIsoString(b.calc_arrival_datatime)
          ? 1
          : -1
      );
    let flightTime = 0; // полетное время
    let _to = new Date(
      this.to.getFullYear(),
      this.to.getMonth(),
      this.to.getDate() + 1,
      0,
      0,
      0
    );
    if (!isNullOrUndefined(_specificflights)) {
      for (let i = 0; i < _specificflights.length; i++) {
        for (let j = 1; j < _specificflights[i].flightpoints.length; j++) {
          let __from;
          let __to;
          __from = this.timetablestoreService.dateFromIsoString(
            _specificflights[i].flightpoints[j - 1].calc_departure_datatime
          );
          __to = this.timetablestoreService.dateFromIsoString(
            _specificflights[i].flightpoints[j].calc_arrival_datatime
          );
          if (__to > this.from && __from < _to) {
            let __fromres = __from > this.from ? __from : this.from;
            let __tores = __to < _to ? __to : _to;
            flightTime += +__tores - +__fromres;
          }
        }
        if (i < _specificflights.length - 1) {
          _specificflights[i][
            'onGroundTime'
          ] = this.timetablestoreService.fromMsecToObj(
            +this.timetablestoreService.dateFromIsoString(
              _specificflights[i + 1].calc_departure_datatime
            ) -
              +this.timetablestoreService.dateFromIsoString(
                _specificflights[i].calc_arrival_datatime
              )
          );
        }
      }
    }
    aircraft.specificflights = _specificflights;
    aircraft.flightTime = this.timetablestoreService.fromMsecToObj(+flightTime);
    return aircraft;
  }

  getWidth(date1, date2) {
    // ширина по датам
    let from = this.timetablestoreService.dateFromIsoString(date1);
    let to = this.timetablestoreService.dateFromIsoString(date2);
    let res = +to - +from;
    let _res = res / (1000 * 60 * this.scales[this.scale]);
    return { str: `${_res}px`, val: _res };
  }

  setScale(type = true) {
    if (type === true) {
      this.globalsettingsService.timetableScale = ++this.scale;
    } else {
      this.globalsettingsService.timetableScale = --this.scale;
    }
  }

  onMenuClosed(): void {
    let menu = document.getElementById('menuBtn');
    if (menu) {
      menu.style.display = 'none';
    }
  }

  async onCreateFlight(e) {
    // if (e.shiftKey === true) {
    //   let dialogRef = this.dialog.open(DialogFlightregularComponent, {
    //     panelClass: 'app-dialog-extend',
    //     data: {
    //       scenario_id: this.scenarioes[this.selectedIndex].id,
    //       type: 'create',
    //       title: 'Рейс в периоде',
    //       aircrafts: this.aircrafts,
    //       from: this.from,
    //       to: this.to
    //     }
    //   });
    //   return;
    // }
    if (e === 1) {
      let dialogRef = this.dialog.open(DialogPeriodflightComponent, {
        panelClass: 'app-dialog-extend',
        data: {
          scenario_id: this.scenarioes[this.selectedIndex].id,
          type: 'create',
          title: 'Рейс в периоде',
          aircrafts: this.aircrafts,
          from: this.from,
          to: this.to
        }
      });
      dialogRef.afterClosed().subscribe(async result => {
        if (isNullOrUndefined(result) || result === false) {
          return;
        }
        // let delresult = _.cloneDeep(result);
        // delresult.lsf.map(item => (item.lfp = []));
        // console.log(delresult);
        // return;
        this.spinnerService.show();
        try {
          if (isNullOrUndefined(result.flight_id)) {
            let flight = {};
            flight['scenario_id'] = this.scenarioes[this.selectedIndex].id;
            flight['flight_type'] = result.flight_type;
            flight['flight_number'] = result.flight_number;
            flight['flight_direction'] = 0;
            let res_flight = await this.flightService.postOne(flight);
            result['flight_id'] = res_flight.id;
          }
          // let delresult = _.cloneDeep(result);
          // delresult.lsf.map(item => (item.lfp = []));
          // let _resGen = await this.flightService.createlayout({
          //   layoutdata: JSON.stringify(delresult)
          // });
          // let _resSet = await this.flightService.setlayout({
          //   aircraft_id: result['aircraft_id'],
          //   flight_id: result['flight_id'],
          //   first_day: result['first_day'],
          //   last_day: result['last_day'],
          //   flightlayout_id: _resGen.id
          // });
          // /**Удаление существующих */
          // let layout = {};
          // layout = { ...result };
          // layout['lsf'] = [];
          // let _resGen = await this.flightService.createlayout({
          //   layoutdata: JSON.stringify(layout)
          // });
          // let _resSet = await this.flightService.setlayout({
          //   aircraft_id: result['aircraft_id'],
          //   flight_id: result['flight_id'],
          //   first_day: result['first_day'],
          //   last_day: result['last_day'],
          //   flightlayout_id: _resGen.id
          // });
          // return;
          let resGen = await this.flightService.createlayout({
            layoutdata: JSON.stringify(result)
          });
          let resSet = await this.flightService.setlayout({
            aircraft_id: result['aircraft_id'],
            flight_id: result['flight_id'],
            first_day: result['first_day'],
            last_day: result['last_day'],
            flightlayout_id: resGen.id
          });
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          await this.getFlights(false);
          this.spinnerService.hide();
        }
      });
      return;
    }

    let dialogRef = this.dialog.open(DialogFlightComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        scenario_id: this.scenarioes[this.selectedIndex].id,
        type: 'create',
        title: 'Создание рейса',
        aircrafts: this.aircrafts,
        from: this.from,
        to: this.to
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (isNullOrUndefined(result)) {
        return;
      }
      if (result._type === 'single') {
        this.spinnerService.show();
        try {
          let layout = {};
          let res = await this.flightService.getByFlightnumberScenarioFlighttype(
            result.flight_number,
            this.scenarioes[this.selectedIndex].id,
            result.flight_type
          );
          if (res && res.length > 0) {
            res.sort((a, b) => {
              return a.id > b.id ? 1 : -1;
            });
            layout['flight_id'] = res[0].id;
          } else {
            let flight = {};
            flight['scenario_id'] = this.scenarioes[this.selectedIndex].id;
            flight['flight_type'] = result.flight_type;
            flight['flight_number'] = result.flight_number;
            flight['flight_direction'] = 0;
            let res_flight = await this.flightService.postOne(flight);
            layout['flight_id'] = res_flight.id;
          }
          layout['type'] = 0; // Один день
          layout['name'] = null;
          layout['first_day'] = result.first_day;
          layout['last_day'] = result.last_day;
          layout['aircraftmodel_id'] = result.aircraftmodel_id;
          let lsf = {};
          lsf['aircraft_id'] = result.aircraft_id;
          lsf['departure_datetime'] = result.departure_datetime_l;
          lsf['arrival_datetime'] = result.arrival_datetime_l;
          lsf['departure_airport_id'] = result.departure_airport_id;
          lsf['arrival_airport_id'] = result.arrival_airport_id;
          lsf['lfp'] = [];
          let first_lfp = {};
          first_lfp['arrival_datetime'] = null;
          first_lfp['departure_datetime'] = result.departure_datetime_l;
          first_lfp['airport_id'] = result.departure_airport_id;
          lsf['lfp'].push(first_lfp);

          for (let i = 0; i < result.flightpoints.length; i++) {
            let lfp = {};
            lfp['arrival_datetime'] = result.flightpoints[i].arrival_datetime_l;
            lfp['departure_datetime'] =
              result.flightpoints[i].departure_datetime_l;
            lfp['airport_id'] = result.flightpoints[i].airport_id;
            lsf['lfp'].push(lfp);
          }
          let last_lfp = {};
          last_lfp['arrival_datetime'] = result.arrival_datetime_l;
          last_lfp['departure_datetime'] = null;
          last_lfp['airport_id'] = result.arrival_airport_id;
          lsf['lfp'].push(last_lfp);

          layout['lsf'] = [lsf];
          let resGen = await this.flightService.createlayout({
            layoutdata: JSON.stringify(layout)
          });
          // console.log(resGen);
          let resSet = await this.flightService.setlayout({
            aircraft_id: result.aircraft_id,
            flight_id: layout['flight_id'],
            first_day: layout['first_day'],
            last_day: layout['last_day'],
            flightlayout_id: resGen.id
          });
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          await this.getFlights(false);
          this.spinnerService.hide();
        }
      } else if (result._type === 'multi') {
        this.spinnerService.show();
        try {
          if (isNullOrUndefined(result.flight_id)) {
            let flight = {};
            flight['scenario_id'] = this.scenarioes[this.selectedIndex].id;
            flight['flight_type'] = result.flight_type;
            flight['flight_number'] = result.flight_number;
            flight['flight_direction'] = 0;
            let res_flight = await this.flightService.postOne(flight);
            result['flight_id'] = res_flight.id;
          }
          let resGen = await this.flightService.createlayout({
            layoutdata: JSON.stringify(result)
          });
          let resSet = await this.flightService.setlayout({
            aircraft_id: result['aircraft_id'],
            flight_id: result['flight_id'],
            first_day: result['first_day'],
            last_day: result['last_day'],
            flightlayout_id: resGen.id
          });
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          await this.getFlights(false);
          this.spinnerService.hide();
        }
      }
    });
  }

  isSelected(specifitflight_id) {
    let sf = this.selectedSpecificflights.find(
      item => item.id === specifitflight_id
    );
    return !isNullOrUndefined(sf);
  }

  onEditFlight(specificflight, flighttemplate, flight, type, event) {
    if (this.accessLevel < 2) {
      return;
    }
    if (event.altKey === true) {
      if (this.isSelected(specificflight.id)) {
        this.selectedSpecificflights.splice(
          this.selectedSpecificflights.findIndex(
            item => item.id === specificflight.id
          ),
          1
        );
      } else {
        this.selectedSpecificflights.push(specificflight);
      }
      return;
    }
    if (type === 'single') {
      let dialogRef = this.dialog.open(DialogFlightComponent, {
        panelClass: 'app-dialog-extend',
        data: {
          specificflight,
          flighttemplate,
          flight,
          scenario_id: this.scenarioes[this.selectedIndex].id,
          isadmin: this.authService.loggedInUser.isadmin,
          type: 'edit',
          aircrafts: this.aircrafts,
          from: this.from,
          to: this.to
        }
      });
      dialogRef.afterClosed().subscribe(async result => {
        if (isNullOrUndefined(result) || !result) {
          return;
        } else if (result === 'delete') {
          this.spinnerService.show();
          try {
            /**Удаление с layout */
            let layout = {};
            layout['flight_id'] = flight.id;
            layout['type'] = 0; // Один день
            layout['name'] = null;
            layout['first_day'] = specificflight.departure_datetime;
            layout['last_day'] = specificflight.departure_datetime;
            layout['aircraftmodel_id'] = specificflight.aircraftmodel_id;
            layout['lsf'] = [];
            let resGen = await this.flightService.createlayout({
              layoutdata: JSON.stringify(layout)
            });
            let resSet = await this.flightService.setlayout({
              aircraft_id: specificflight.aircraft_id,
              flight_id: layout['flight_id'],
              first_day: layout['first_day'],
              last_day: layout['last_day'],
              flightlayout_id: resGen.id
            });
          } catch (err) {
            this.globalsettingsService.exceptionHandling(err);
          } finally {
            await this.getFlights(false);
            this.spinnerService.hide();
            return;
          }
        }
        this.spinnerService.show();
        try {
          /**Редактирование с layout */
          let layout = {};
          if (flight.flight_number !== result.flight_number) {
            let res = await this.flightService.getByFlightnumberScenarioFlighttype(
              result.flight_number,
              this.scenarioes[this.selectedIndex].id,
              result.flight_type
            );
            if (res && res.length > 0) {
              res.sort((a, b) => {
                return a.id > b.id ? 1 : -1;
              });
              layout['flight_id'] = res[0].id;
            } else {
              let _flight = {};
              _flight['scenario_id'] = this.scenarioes[this.selectedIndex].id;
              _flight['flight_type'] = result.flight_type;
              _flight['flight_number'] = result.flight_number;
              _flight['flight_direction'] = 0;
              let res_flight = await this.flightService.postOne(_flight);
              layout['flight_id'] = res_flight.id;
            }
            /**Удалить старый рейс */
            let _layout = {};
            _layout['flight_id'] = flight.id;
            _layout['type'] = 0; // Один день
            _layout['name'] = null;
            _layout['first_day'] = specificflight.departure_datetime;
            _layout['last_day'] = specificflight.departure_datetime;
            _layout['aircraftmodel_id'] = specificflight.aircraftmodel_id;
            _layout['lsf'] = [];
            let resGen = await this.flightService.createlayout({
              layoutdata: JSON.stringify(_layout)
            });
            let resSet = await this.flightService.setlayout({
              aircraft_id: specificflight.aircraft_id,
              flight_id: _layout['flight_id'],
              first_day: _layout['first_day'],
              last_day: _layout['last_day'],
              flightlayout_id: resGen.id
            });
          } else {
            layout['flight_id'] = flight.id;
          }
          layout['type'] = 0; // Один день
          layout['name'] = null;
          layout['first_day'] = result.first_day;
          layout['last_day'] = result.first_day;
          layout['aircraftmodel_id'] = result.aircraftmodel_id;
          let lsf = {};
          lsf['aircraft_id'] = result.aircraft_id;
          lsf['departure_datetime'] = result.departure_datetime_l;
          lsf['arrival_datetime'] = result.arrival_datetime_l;
          lsf['departure_airport_id'] = result.departure_airport_id;
          lsf['arrival_airport_id'] = result.arrival_airport_id;
          lsf['lfp'] = [];

          let first_lfp = {};
          first_lfp['arrival_datetime'] = null;
          first_lfp['departure_datetime'] = result.departure_datetime_l;
          first_lfp['airport_id'] = result.departure_airport_id;
          lsf['lfp'].push(first_lfp);

          for (let i = 0; i < result.flightpoints.length; i++) {
            let lfp = {};
            lfp['arrival_datetime'] = result.flightpoints[i].arrival_datetime_l;
            lfp['departure_datetime'] =
              result.flightpoints[i].departure_datetime_l;
            lfp['airport_id'] = result.flightpoints[i].airport_id;
            lsf['lfp'].push(lfp);
          }

          let last_lfp = {};
          last_lfp['arrival_datetime'] = result.arrival_datetime_l;
          last_lfp['departure_datetime'] = null;
          last_lfp['airport_id'] = result.arrival_airport_id;
          lsf['lfp'].push(last_lfp);

          layout['lsf'] = [lsf];

          let _resGen = await this.flightService.createlayout({
            layoutdata: JSON.stringify(layout)
          });
          let _resSet = await this.flightService.setlayout({
            aircraft_id: result.aircraft_id,
            flight_id: layout['flight_id'],
            first_day: layout['first_day'],
            last_day: layout['last_day'],
            flightlayout_id: _resGen.id
          });
        } catch (err) {
          this.globalsettingsService.exceptionHandling(err);
        } finally {
          await this.getFlights(false);
          this.spinnerService.hide();
        }
      });
    } else if (type === 'multi') {
    }
  }

  getFlighttempltatebyId(id) {
    return this.flighttempltates.find(item => item.id === id);
  }

  getFlightbyId(id) {
    return this.flights.find(item => item.id === id);
  }

  onMoveflight(specificflight, flighttemplate, flight) {
    let dialogRef = this.dialog.open(DialogFlightComponent, {
      panelClass: 'app-dialog-extend',
      data: {
        specificflight,
        flighttemplate,
        flight,
        scenario_id: this.scenarioes[this.selectedIndex].id,
        isadmin: this.authService.loggedInUser.isadmin,
        type: 'move',
        aircrafts: this.aircrafts,
        from: this.from,
        to: this.to
      }
    });
    dialogRef.afterClosed().subscribe(async result => {
      if (isNullOrUndefined(result) || !result) {
        return;
      }
      this.spinnerService.show();
      try {
        let _specificflight = {};
        _specificflight['calc_arrival_datatime'] = result.arrival_datetime;
        _specificflight['calc_departure_datatime'] = result.departure_datetime;
        _specificflight['id'] = specificflight.id;
        let _flightpoints = [];
        let firstflightpoint = {};
        firstflightpoint['calc_departure_datatime'] = result.departure_datetime;
        firstflightpoint['id'] = specificflight.flightpoints[0].id;
        _flightpoints.push(firstflightpoint);
        for (let i = 0; i < result.flightpoints.length; i++) {
          let _flightpoint = {};
          _flightpoint['calc_arrival_datatime'] =
            result.flightpoints[i].arrival_datetime;
          _flightpoint['calc_departure_datatime'] =
            result.flightpoints[i].departure_datetime;
          _flightpoint['id'] = specificflight.flightpoints[i + 1].id;
          _flightpoints.push(_flightpoint);
        }
        let lastflightpoint = {};
        lastflightpoint['calc_arrival_datatime'] = result.arrival_datetime;
        lastflightpoint['id'] =
          specificflight.flightpoints[
            specificflight.flightpoints.length - 1
          ].id;
        _flightpoints.push(lastflightpoint);
        this.specificflightService.putOneById(
          _specificflight['id'],
          _specificflight
        );
        _flightpoints.map(_flightpoint => {
          this.flightpointService.putOneById(_flightpoint['id'], _flightpoint);
        });
      } catch (err) {
        this.globalsettingsService.exceptionHandling(err);
      } finally {
        await this.getFlights(false);
        this.spinnerService.hide();
      }
    });
  }

  onPreEditFlight(specificflight, flighttemplate, flight) {
    // если рейс не одиночный предложение редактировать шаблон
  }

  getAirportByid(id) {
    return this.airports.find(airport => airport.id === id);
  }

  getFlightpointWidth(date1, date2) {
    // ширина перелета с учетом from to убрать из шаблона!!!
    let from = this.timetablestoreService.dateFromIsoString(date1);
    let to = this.timetablestoreService.dateFromIsoString(date2);
    let _to = new Date(
      this.to.getFullYear(),
      this.to.getMonth(),
      this.to.getDate() + 1,
      0,
      0,
      0
    );
    let __from = from > this.from ? from : this.from;
    let __to = to < _to ? to : _to;
    let res = +__to - +__from;
    let _res = res / (1000 * 60 * this.scales[this.scale]);
    return {
      str: `${_res}px`,
      val: _res,
      cutTo: to > _to ? true : false,
      cutFrom: __from > _to || from < this.from ? true : false
    };
  }

  dateFromIsoString(str: string | Date) {
    // 2018-09-14T23:00:00
    if (typeof str === 'string') {
      let _arr = str.split('.')[0].split('T');
      let arr = _arr[0].split('-').concat(_arr[1].split(':')); // yyyy mm dd hh mm ss
      return new Date(+arr[0], +arr[1] - 1, +arr[2], +arr[3], +arr[4]);
    } else {
      return new Date(str);
    }
  }

  getLeft2(date1, date2) {
    let from = this.timetablestoreService.dateFromIsoString(date1);
    let to = this.timetablestoreService.dateFromIsoString(date2);
    let res = +to - +from;
    let _res = res / (1000 * 60 * this.scales[this.scale]);
    return { str: `${_res}px`, val: _res };
  }
}
