import { Component, OnInit, Inject, AfterContentChecked } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Airport } from 'src/app/shared/models/airport.model';
import { DictionaryService } from 'src/app/system/shared/services/dictionary.service';
import { isNullOrUndefined } from 'util';
import { TimetablestoreService } from 'src/app/system/shared/services/timetablestore.service';
import { AirportService } from 'src/app/system/shared/services/airport.service';
import { startWith, map } from 'rxjs/operators';
import { AircraftService } from 'src/app/system/shared/services/aircraft.service';
import { AircraftmodelService } from 'src/app/system/shared/services/aircraftmodel.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FlightService } from 'src/app/system/shared/services/flight.service';
import { GlobalsettingsService } from 'src/app/system/shared/services/globalsettings.service';
import { FlighttemplateService } from 'src/app/system/shared/services/flighttemplate.service';
import { SpecificflightService } from 'src/app/system/shared/services/specificflight.service';
import { FlightpointService } from 'src/app/system/shared/services/flightpoint.service';

@Component({
  selector: 'app-dialog-flight',
  templateUrl: './dialog-flight.component.html',
  styleUrls: ['./dialog-flight.component.scss']
})
export class DialogFlightComponent implements OnInit, AfterContentChecked {
  departure_date_str = 'Дата и время вылета';
  arrival_date_str = 'Дата и время прилета';
  intMove = false;

  /**формы для одиночных рейсов */
  formFlight: FormGroup;
  departure_airport_control = new FormControl();
  arrival_airport_control = new FormControl();
  extra_airport_controls: FormControl[] = [];
  filtered_departure_airport: Observable<any[]>;
  filtered_arrival_airport: Observable<any[]>;
  filtered_extra_airport: Observable<any[]>;
  timerId;

  formsExtraairports: FormGroup[] = [];

  today: Date;

  flightTypes: any[];

  saveValid = false;

  airports: Airport[];
  aircrafts: any[];
  aircraftmodels: any[];

  timeMask: any[] = [/\d/, /\d/, ':', /\d/, /\d/];
  timeMaskInput: any[] = [/[0-2]/, /\d/, ':', /[0-6]/, /\d/];

  constructor(
    private dictionaryService: DictionaryService,
    private airportService: AirportService,
    private aircraftService: AircraftService,
    private aircraftmodelService: AircraftmodelService,
    private timetablestoreService: TimetablestoreService,
    public snackBar: MatSnackBar,
    private spinnerService: NgxSpinnerService,
    private flightService: FlightService,
    private specificflightService: SpecificflightService,
    private flightpointService: FlightpointService,
    private globalsettingsService: GlobalsettingsService,
    public dialogRef: MatDialogRef<DialogFlightComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  async ngOnInit() {
    this.intMove = this.data.type === 'move';
    this.setForms();
    if (this.data.type !== 'create') {
      this._setForms();
    }
    this.today = new Date();
    if (this.intMove === true) {
      this.departure_date_str = 'Расчетное время вылета';
      this.arrival_date_str = 'Расчетное время прилета';
    }
    this.flightTypes = this.dictionaryService.flightType;
    let airports = this.airportService.getWithCodefilterLimit(100, '');
    let aircraftmodels = this.aircraftmodelService.getAll();
    let aircrafts = this.aircraftService.getAll();
    this.aircrafts = isNullOrUndefined(await aircrafts) ? [] : await aircrafts;
    this.aircrafts.unshift({ code: '', id: null });
    this.aircraftmodels = isNullOrUndefined(await aircraftmodels)
      ? []
      : await aircraftmodels;
    this.airports = isNullOrUndefined(await airports) ? [] : await airports;
  }

  ngAfterContentChecked(): void {
    this.isSaveValid();
  }

  onAddExtraairport() {
    this.formsExtraairports.push(
      new FormGroup({
        airport_id: new FormControl(null),
        datebegin: new FormControl(null, [Validators.required]),
        timebegin: new FormControl(null, [Validators.required]),
        dateend: new FormControl(null, [Validators.required]),
        timeend: new FormControl(null, [Validators.required]),
        time: new FormControl(null, [Validators.required])
      })
    );
    this.extra_airport_controls.push(new FormControl());
  }

  setForms() {
    this.formFlight = new FormGroup({
      flight_number: new FormControl(null, [Validators.required]),
      datebegin: new FormControl(null, [Validators.required]),
      timebegin: new FormControl(null, [Validators.required]),
      dateend: new FormControl(null, [Validators.required]),
      timeend: new FormControl(null, [Validators.required]),
      aircraftmodel_id: new FormControl(null, [Validators.required]),
      aircraft_id: new FormControl(null),
      flight_type: new FormControl(null, [Validators.required]),
      time: new FormControl(null, [Validators.required]),
      departure_airport_id: new FormControl(null),
      arrival_airport_id: new FormControl(null)
    });
  }

  async _setForms() {
    let datebegin_ =
      this.intMove === true
        ? this.data.specificflight.calc_departure_datatime.split('T')
        : this.data.specificflight.departure_datetime.split('T');
    let datebegin = datebegin_[0].split('-').concat(datebegin_[1].split(':')); // yyyy mm dd hh mm ss
    let dateend_ =
      this.intMove === true
        ? this.data.specificflight.calc_arrival_datatime.split('T')
        : this.data.specificflight.arrival_datetime.split('T');
    let dateend = dateend_[0].split('-').concat(dateend_[1].split(':')); // yyyy mm dd hh mm ss
    this.formFlight.patchValue({
      flight_number: this.data.flight.flight_number,
      datebegin:
        this.intMove === true
          ? this.timetablestoreService.dateFromIsoString(
              this.data.specificflight.calc_departure_datatime
            )
          : this.timetablestoreService.dateFromIsoString(
              this.data.specificflight.departure_datetime
            ),
      timebegin: `${datebegin[3]}:${datebegin[4]}`,
      dateend:
        this.intMove === true
          ? this.timetablestoreService.dateFromIsoString(
              this.data.specificflight.calc_arrival_datatime
            )
          : this.timetablestoreService.dateFromIsoString(
              this.data.specificflight.arrival_datetime
            ),
      timeend: `${dateend[3]}:${dateend[4]}`,
      aircraftmodel_id: this.data.flighttemplate.aircraftmodel_id,
      flight_type: this.data.flight.flight_type,
      aircraft_id: this.data.specificflight.aircraft_id
    });
    let departure_airport_id;
    let arrival_airport_id;
    try {
      departure_airport_id = this.airportService.getOneById(
        this.data.specificflight.departure_airport_id
      );
      arrival_airport_id = this.airportService.getOneById(
        this.data.specificflight.arrival_airport_id
      );
    } catch (err) {
      console.error(err);
    }
    let airport_id = [];
    for (let i = 1; i < this.data.specificflight.flightpoints.length - 1; i++) {
      let _datebegin_ =
        this.intMove === true
          ? this.data.specificflight.flightpoints[
              i
            ].calc_departure_datatime.split('T')
          : this.data.specificflight.flightpoints[i].departure_datetime.split(
              'T'
            );
      let _datebegin = _datebegin_[0]
        .split('-')
        .concat(_datebegin_[1].split(':')); // yyyy mm dd hh mm ss

      let _dateend_ =
        this.intMove === true
          ? this.data.specificflight.flightpoints[
              i
            ].calc_arrival_datatime.split('T')
          : this.data.specificflight.flightpoints[i].arrival_datetime.split(
              'T'
            );
      let _dateend = _dateend_[0].split('-').concat(_dateend_[1].split(':')); // yyyy mm dd hh mm ss
      this.formsExtraairports.push(
        new FormGroup({
          datebegin: new FormControl(
            this.intMove === true
              ? this.timetablestoreService.dateFromIsoString(
                  this.data.specificflight.flightpoints[i]
                    .calc_departure_datatime
                )
              : this.timetablestoreService.dateFromIsoString(
                  this.data.specificflight.flightpoints[i].departure_datetime
                ),
            [Validators.required]
          ),
          timebegin: new FormControl(`${_datebegin[3]}:${_datebegin[4]}`, [
            Validators.required
          ]),
          dateend: new FormControl(
            this.intMove === true
              ? this.timetablestoreService.dateFromIsoString(
                  this.data.specificflight.flightpoints[i].calc_arrival_datatime
                )
              : this.timetablestoreService.dateFromIsoString(
                  this.data.specificflight.flightpoints[i].arrival_datetime
                ),
            [Validators.required]
          ),
          timeend: new FormControl(`${_dateend[3]}:${_dateend[4]}`, [
            Validators.required
          ]),
          time: new FormControl(null)
        })
      );
      this.extra_airport_controls.push(new FormControl());
      try {
        let airport = await this.airportService.getOneById(
          this.data.specificflight.flightpoints[i].airport_id
        );
        airport_id.push(airport);
      } catch (err) {
        console.error(err);
      }
    }
    this.departure_airport_control.patchValue(await departure_airport_id);
    this.arrival_airport_control.patchValue(await arrival_airport_id);
    for (let i = 0; i < airport_id.length; i++) {
      this.extra_airport_controls[i].patchValue(airport_id[i]);
    }
    this.onCalcTime(true);
  }

  onSave(): void {
    if (this.data.type === 'create') {
      let obj = {};
      obj['_type'] = 'single';
      obj['flight_number'] = this.formFlight.value.flight_number;
      obj['aircraft_id'] = this.formFlight.getRawValue().aircraft_id;
      let first_day = new Date(this.formFlight.value.datebegin);
      first_day.setHours(0);
      first_day.setMinutes(0 - first_day.getTimezoneOffset());
      obj['first_day'] = first_day.toUTCString();
      obj['last_day'] = first_day.toUTCString();
      let departure_datetime = new Date(
        this.formFlight.getRawValue().datebegin
      );
      departure_datetime.setHours(
        this.formFlight.value.timebegin.split(':')[0]
      );
      departure_datetime.setMinutes(
        this.formFlight.value.timebegin.split(':')[1] -
          departure_datetime.getTimezoneOffset()
      );
      obj['departure_datetime'] = departure_datetime.toUTCString();
      let arrival_datetime = new Date(this.formFlight.value.dateend);
      arrival_datetime.setHours(this.formFlight.value.timeend.split(':')[0]);
      arrival_datetime.setMinutes(
        this.formFlight.value.timeend.split(':')[1] -
          arrival_datetime.getTimezoneOffset()
      );
      obj['arrival_datetime'] = arrival_datetime.toUTCString();
      if (!this.isIntervalValid(departure_datetime, arrival_datetime)) {
        this.openSnackBar(`Есть пересечения с уже существующими рейсами.`, '');
        return;
      }
      let timebegin: string; // P2DT1H20M0S
      let timeend: string;
      timebegin = `P${0}DT${this.formFlight.value.timebegin.split(':')[0]}H${
        this.formFlight.value.timebegin.split(':')[1]
      }M0S`;
      timeend = `P${
        arrival_datetime.getUTCDate() === departure_datetime.getUTCDate()
          ? 0
          : 1
      }DT${this.formFlight.value.timeend.split(':')[0]}H${
        this.formFlight.value.timeend.split(':')[1]
      }M0S`;
      obj['departure_datetime_l'] = timebegin;
      obj['arrival_datetime_l'] = timeend;
      obj['aircraftmodel_id'] = this.formFlight.value.aircraftmodel_id;
      obj['flight_type'] = this.formFlight.value.flight_type;
      obj['departure_airport_id'] = this.departure_airport_control.value.id;
      obj['arrival_airport_id'] = this.arrival_airport_control.value.id;
      obj['flightpoints'] = [];
      for (let i = 0; i < this.formsExtraairports.length; i++) {
        let _obj = {};
        _obj['airport_id'] = this.extra_airport_controls[i].value.id;
        let _departure_datetime = new Date(
          this.formsExtraairports[i].value.datebegin
        );
        _departure_datetime.setHours(
          this.formsExtraairports[i].value.timebegin.split(':')[0]
        );
        _departure_datetime.setMinutes(
          this.formsExtraairports[i].value.timebegin.split(':')[1] -
            _departure_datetime.getTimezoneOffset()
        );
        _obj['departure_datetime'] = _departure_datetime.toUTCString();
        let _arrival_datetime = new Date(
          this.formsExtraairports[i].value.dateend
        );
        _arrival_datetime.setHours(
          this.formsExtraairports[i].value.timeend.split(':')[0]
        );
        _arrival_datetime.setMinutes(
          this.formsExtraairports[i].value.timeend.split(':')[1] -
            _arrival_datetime.getTimezoneOffset()
        );
        _obj['arrival_datetime'] = _arrival_datetime.toUTCString();
        let _timebegin: string; // P2DT1H20M0S
        let _timeend: string;
        _timebegin = `P${
          _departure_datetime.getUTCDate() === departure_datetime.getUTCDate()
            ? 0
            : 1
        }DT${this.formsExtraairports[i].value.timebegin.split(':')[0]}H${
          this.formsExtraairports[i].value.timebegin.split(':')[1]
        }M0S`;
        _timeend = `P${
          _arrival_datetime.getUTCDate() === departure_datetime.getUTCDate()
            ? 0
            : 1
        }DT${this.formsExtraairports[i].value.timeend.split(':')[0]}H${
          this.formsExtraairports[i].value.timeend.split(':')[1]
        }M0S`;
        _obj['departure_datetime_l'] = _timebegin;
        _obj['arrival_datetime_l'] = _timeend;
        obj['flightpoints'].push(_obj);
      }
      this.dialogRef.close(obj);
    } else {
      this._onSave();
    }
  }

  _onSave(): void {
    let obj = {};
    if (true) {
      obj['_type'] = 'single';
      obj['flight_number'] = this.formFlight.getRawValue().flight_number;
      obj['aircraft_id'] = this.formFlight.getRawValue().aircraft_id;
      let first_day = new Date(this.formFlight.getRawValue().datebegin);
      first_day.setHours(0);
      first_day.setMinutes(0 - first_day.getTimezoneOffset());
      obj['first_day'] = first_day.toUTCString();
      obj['last_day'] = first_day.toUTCString();
      let departure_datetime = new Date(
        this.formFlight.getRawValue().datebegin
      );
      departure_datetime.setHours(
        this.formFlight.getRawValue().timebegin.split(':')[0]
      );
      departure_datetime.setMinutes(
        this.formFlight.getRawValue().timebegin.split(':')[1] -
          departure_datetime.getTimezoneOffset()
      );
      obj['departure_datetime'] = departure_datetime.toUTCString();
      let arrival_datetime = new Date(this.formFlight.getRawValue().dateend);
      arrival_datetime.setHours(
        this.formFlight.getRawValue().timeend.split(':')[0]
      );
      arrival_datetime.setMinutes(
        this.formFlight.getRawValue().timeend.split(':')[1] -
          arrival_datetime.getTimezoneOffset()
      );
      obj['arrival_datetime'] = arrival_datetime.toUTCString();
      if (!this.isIntervalValid(departure_datetime, arrival_datetime)) {
        this.openSnackBar(`Есть пересечения с уже существующими рейсами.`, '');
        // this.dialogRef.close(false);
        return;
      }
      let timebegin: string; // P2DT1H20M0S
      let timeend: string;
      timebegin = `P${0}DT${
        this.formFlight.getRawValue().timebegin.split(':')[0]
      }H${this.formFlight.getRawValue().timebegin.split(':')[1]}M0S`;
      timeend =
        // `P${(+departure_datetime < +arrival_datetime) ? 0 : 1
        `P${
          arrival_datetime.getUTCDate() === departure_datetime.getUTCDate()
            ? 0
            : 1
        }DT${this.formFlight.getRawValue().timeend.split(':')[0]}H${
          this.formFlight.getRawValue().timeend.split(':')[1]
        }M0S`;
      obj['departure_datetime_l'] = timebegin;
      obj['arrival_datetime_l'] = timeend;
      obj['aircraftmodel_id'] = this.formFlight.getRawValue().aircraftmodel_id;
      obj['flight_type'] = this.formFlight.getRawValue().flight_type;
      // obj['departure_airport_id'] = this.formFlight.getRawValue().departure_airport_id;
      // obj['arrival_airport_id'] = this.formFlight.getRawValue().arrival_airport_id;
      obj['departure_airport_id'] = this.departure_airport_control.value.id;
      obj['arrival_airport_id'] = this.arrival_airport_control.value.id;
      obj['flightpoints'] = [];
      for (let i = 0; i < this.formsExtraairports.length; i++) {
        let _obj = {};
        // _obj['airport_id'] = this.formsExtraairports[i].getRawValue().airport_id;
        _obj['airport_id'] = this.extra_airport_controls[i].value.id;
        let _departure_datetime = new Date(
          this.formsExtraairports[i].getRawValue().datebegin
        );
        _departure_datetime.setHours(
          this.formsExtraairports[i].getRawValue().timebegin.split(':')[0]
        );
        _departure_datetime.setMinutes(
          this.formsExtraairports[i].getRawValue().timebegin.split(':')[1] -
            _departure_datetime.getTimezoneOffset()
        );
        _obj['departure_datetime'] = _departure_datetime.toUTCString();
        let _arrival_datetime = new Date(
          this.formsExtraairports[i].getRawValue().dateend
        );
        _arrival_datetime.setHours(
          this.formsExtraairports[i].getRawValue().timeend.split(':')[0]
        );
        _arrival_datetime.setMinutes(
          this.formsExtraairports[i].getRawValue().timeend.split(':')[1] -
            _arrival_datetime.getTimezoneOffset()
        );
        _obj['arrival_datetime'] = _arrival_datetime.toUTCString();
        let _timebegin: string; // P2DT1H20M0S
        let _timeend: string;
        // _timebegin =
        // 	`P${0}DT${this.formsExtraairports[i].getRawValue()
        // .timebegin.split(':')[0]}H${this.formsExtraairports[i].getRawValue().timebegin.split(':')[1]}M0S`;
        _timebegin = `P${
          _departure_datetime.getUTCDate() === departure_datetime.getUTCDate()
            ? 0
            : 1
        }DT${
          this.formsExtraairports[i].getRawValue().timebegin.split(':')[0]
        }H${
          this.formsExtraairports[i].getRawValue().timebegin.split(':')[1]
        }M0S`;
        _timeend = `P${
          _arrival_datetime.getUTCDate() === departure_datetime.getUTCDate()
            ? 0
            : 1
        }DT${this.formsExtraairports[i].getRawValue().timeend.split(':')[0]}H${
          this.formsExtraairports[i].getRawValue().timeend.split(':')[1]
        }M0S`;
        _obj['departure_datetime_l'] = _timebegin;
        _obj['arrival_datetime_l'] = _timeend;
        obj['flightpoints'].push(_obj);
      }
    } else if (false) {
    }
    this.dialogRef.close(obj);
  }

  onDelExtraairport() {
    this.formsExtraairports.pop();
    this.extra_airport_controls.pop();
    this.onCalcTime(true);
  }

  isIntervalValid(departure_datetime, arrival_datetime): boolean {
    if (this.intMove) {
      return true;
    } else {
      let aircraft;
      if (
        !isNullOrUndefined(this.data.specificflight) &&
        !isNullOrUndefined(this.data.specificflight.aircraft_id)
      ) {
        aircraft = this.data.aircrafts.find(
          item => item.id === this.data.specificflight.aircraft_id
        );
      }
      if (
        !isNullOrUndefined(this.data.specificflight) &&
        !isNullOrUndefined(this.data.specificflight.id)
      ) {
        aircraft.specificflights.splice(
          aircraft.specificflights.findIndex(
            i => i.id === this.data.specificflight.id
          ),
          1
        );
      }
      if (!isNullOrUndefined(aircraft)) {
        return this.timetablestoreService.isIntervalValid(
          aircraft,
          departure_datetime.toISOString(),
          arrival_datetime.toISOString()
        );
      } else {
        return true;
      }
    }
  }

  isSaveValid() {
    let res = true;
    if (
      !isNullOrUndefined(this.formFlight.getRawValue().timebegin) &&
      !isNullOrUndefined(this.formFlight.getRawValue().timeend) &&
      !isNullOrUndefined(this.departure_airport_control.value) &&
      !isNullOrUndefined(this.departure_airport_control.value.id) &&
      !isNullOrUndefined(this.arrival_airport_control.value) &&
      !isNullOrUndefined(this.arrival_airport_control.value.id)
    ) {
      if (
        this.formFlight.getRawValue().timebegin.indexOf('_') === -1 &&
        this.formFlight.getRawValue().timeend.indexOf('_') === -1
      ) {
        let departure_datetime = this.timetablestoreService.dateFromIsoString(
          this.formFlight.getRawValue().datebegin
        );
        departure_datetime.setHours(
          this.formFlight.getRawValue().timebegin.split(':')[0]
        );
        departure_datetime.setMinutes(
          this.formFlight.getRawValue().timebegin.split(':')[1] -
            departure_datetime.getTimezoneOffset()
        );
        let arrival_datetime = this.timetablestoreService.dateFromIsoString(
          this.formFlight.getRawValue().dateend
        );
        arrival_datetime.setHours(
          this.formFlight.getRawValue().timeend.split(':')[0]
        );
        arrival_datetime.setMinutes(
          this.formFlight.getRawValue().timeend.split(':')[1] -
            arrival_datetime.getTimezoneOffset()
        );
        if (+departure_datetime >= +arrival_datetime) {
          res = false;
        }
      } else {
        res = false;
      }
    } else {
      res = false;
    }
    if (!this.formFlight.valid) {
      res = false;
    } else {
      this.formsExtraairports.map(formsExtraairport => {
        if (
          !isNullOrUndefined(formsExtraairport.value.timebegin) &&
          !isNullOrUndefined(formsExtraairport.value.timeend)
        ) {
          if (
            formsExtraairport.value.timebegin.indexOf('_') === -1 &&
            formsExtraairport.value.timeend.indexOf('_') === -1
          ) {
            let departure_datetime = this.timetablestoreService.dateFromIsoString(
              formsExtraairport.value.datebegin
            );
            departure_datetime.setHours(
              formsExtraairport.value.timebegin.split(':')[0]
            );
            departure_datetime.setMinutes(
              formsExtraairport.value.timebegin.split(':')[1] -
                departure_datetime.getTimezoneOffset()
            );
            let arrival_datetime = this.timetablestoreService.dateFromIsoString(
              formsExtraairport.value.dateend
            );
            arrival_datetime.setHours(
              formsExtraairport.value.timeend.split(':')[0]
            );
            arrival_datetime.setMinutes(
              formsExtraairport.value.timeend.split(':')[1] -
                arrival_datetime.getTimezoneOffset()
            );
            if (+departure_datetime <= +arrival_datetime) {
              res = false;
            }
          } else {
            res = false;
          }
        } else {
          res = false;
        }
        if (!formsExtraairport.valid) {
          res = false;
        }
        if (
          formsExtraairport.value.time === '---' ||
          isNullOrUndefined(formsExtraairport.value.time)
        ) {
          res = false;
        }
      });
      this.extra_airport_controls.map(extra_airport_control => {
        if (
          isNullOrUndefined(extra_airport_control.value) ||
          isNullOrUndefined(extra_airport_control.value.id)
        ) {
          res = false;
        }
      });
    }
    this.saveValid = res;
  }

  onClose() {
    this.dialogRef.close(false);
  }

  onCalcTime(b) {
    if (b === true) {
      if (true) {
        if (this.formsExtraairports.length === 0) {
          if (
            isNullOrUndefined(this.formFlight.getRawValue().timebegin) ||
            isNullOrUndefined(this.formFlight.getRawValue().timeend)
          ) {
            return;
          }
          if (
            this.formFlight.getRawValue().timebegin.length !== 5 ||
            this.formFlight.getRawValue().timebegin.length !== 5
          ) {
            return;
          }
          let begin;
          let end;
          if (
            new Date(this.formFlight.getRawValue().datebegin) instanceof Date &&
            !isNaN(new Date(this.formFlight.getRawValue().datebegin).valueOf())
          ) {
            begin = new Date(this.formFlight.getRawValue().datebegin);
            begin.setHours(
              this.formFlight.getRawValue().timebegin.split(':')[0]
            );
            begin.setMinutes(
              this.formFlight.getRawValue().timebegin.split(':')[1]
            );
          }
          if (
            new Date(this.formFlight.getRawValue().dateend) instanceof Date &&
            !isNaN(new Date(this.formFlight.getRawValue().dateend).valueOf())
          ) {
            end = new Date(this.formFlight.getRawValue().dateend);
            end.setHours(this.formFlight.getRawValue().timeend.split(':')[0]);
            end.setMinutes(this.formFlight.getRawValue().timeend.split(':')[1]);
          }
          let sum = (end - begin) / 60000;
          let sumStr = '';
          if (sum > 59) {
            if (Math.floor(sum / 60) < 10) {
              sumStr += '0';
            }
            sumStr += Math.floor(sum / 60);
          } else {
            sumStr += '00';
          }
          sumStr += ':';
          if (Math.floor(sum % 60) < 10) {
            sumStr += '0';
          }
          sumStr += Math.floor(sum % 60);
          this.formFlight.patchValue({
            time: sum > 0 && sum < 200000 ? sumStr : '---'
          });
        } else {
          for (let i = 0; i < this.formsExtraairports.length; i++) {
            if (i === 0) {
              if (
                isNullOrUndefined(this.formFlight.getRawValue().timebegin) ||
                isNullOrUndefined(
                  this.formsExtraairports[i].getRawValue().timeend
                )
              ) {
                return;
              }
              if (
                this.formFlight.getRawValue().timebegin.length !== 5 ||
                this.formsExtraairports[i].getRawValue().timeend.length !== 5
              ) {
                return;
              }
              let begin;
              let end;
              if (
                new Date(this.formFlight.getRawValue().datebegin) instanceof
                  Date &&
                !isNaN(
                  new Date(this.formFlight.getRawValue().datebegin).valueOf()
                )
              ) {
                begin = new Date(this.formFlight.getRawValue().datebegin);
                begin.setHours(
                  this.formFlight.getRawValue().timebegin.split(':')[0]
                );
                begin.setMinutes(
                  this.formFlight.getRawValue().timebegin.split(':')[1]
                );
              }
              if (
                new Date(
                  this.formsExtraairports[i].getRawValue().dateend
                ) instanceof Date &&
                !isNaN(
                  new Date(
                    this.formsExtraairports[i].getRawValue().dateend
                  ).valueOf()
                )
              ) {
                end = new Date(
                  this.formsExtraairports[i].getRawValue().dateend
                );
                end.setHours(
                  this.formsExtraairports[i].getRawValue().timeend.split(':')[0]
                );
                end.setMinutes(
                  this.formsExtraairports[i].getRawValue().timeend.split(':')[1]
                );
              }
              let sum = (end - begin) / 60000;
              let sumStr = '';
              if (sum > 59) {
                if (Math.floor(sum / 60) < 10) {
                  sumStr += '0';
                }
                sumStr += Math.floor(sum / 60);
              } else {
                sumStr += '00';
              }
              sumStr += ':';
              if (Math.floor(sum % 60) < 10) {
                sumStr += '0';
              }
              sumStr += Math.floor(sum % 60);
              this.formsExtraairports[i].patchValue({
                time: sum > 0 && sum < 200000 ? sumStr : '---'
              });
            }
            if (i === this.formsExtraairports.length - 1) {
              if (
                !isNullOrUndefined(
                  this.formsExtraairports[i].getRawValue().timebegin
                ) &&
                !isNullOrUndefined(this.formFlight.getRawValue().timeend) &&
                this.formsExtraairports[i].getRawValue().timebegin.length ===
                  5 &&
                this.formFlight.getRawValue().timeend.length === 5
              ) {
                let begin;
                let end;
                if (
                  new Date(
                    this.formsExtraairports[i].getRawValue().datebegin
                  ) instanceof Date &&
                  !isNaN(
                    new Date(
                      this.formsExtraairports[i].getRawValue().datebegin
                    ).valueOf()
                  )
                ) {
                  begin = new Date(
                    this.formsExtraairports[i].getRawValue().datebegin
                  );
                  begin.setHours(
                    this.formsExtraairports[i]
                      .getRawValue()
                      .timebegin.split(':')[0]
                  );
                  begin.setMinutes(
                    this.formsExtraairports[i]
                      .getRawValue()
                      .timebegin.split(':')[1]
                  );
                }
                if (
                  new Date(this.formFlight.getRawValue().dateend) instanceof
                    Date &&
                  !isNaN(
                    new Date(this.formFlight.getRawValue().dateend).valueOf()
                  )
                ) {
                  end = new Date(this.formFlight.getRawValue().dateend);
                  end.setHours(
                    this.formFlight.getRawValue().timeend.split(':')[0]
                  );
                  end.setMinutes(
                    this.formFlight.getRawValue().timeend.split(':')[1]
                  );
                }
                let sum = (end - begin) / 60000;
                let sumStr = '';
                if (sum > 59) {
                  if (Math.floor(sum / 60) < 10) {
                    sumStr += '0';
                  }
                  sumStr += Math.floor(sum / 60);
                } else {
                  sumStr += '00';
                }
                sumStr += ':';
                if (Math.floor(sum % 60) < 10) {
                  sumStr += '0';
                }
                sumStr += Math.floor(sum % 60);
                this.formFlight.patchValue({
                  time: sum > 0 && sum < 200000 ? sumStr : '---'
                });
              }
            }
            if (i !== 0) {
              if (
                !isNullOrUndefined(
                  this.formsExtraairports[i - 1].getRawValue().timebegin
                ) &&
                !isNullOrUndefined(
                  this.formsExtraairports[i].getRawValue().timeend
                ) &&
                this.formsExtraairports[i - 1].getRawValue().timebegin
                  .length === 5 &&
                this.formsExtraairports[i].getRawValue().timeend.length === 5
              ) {
                let begin;
                let end;
                if (
                  new Date(
                    this.formsExtraairports[i - 1].getRawValue().datebegin
                  ) instanceof Date &&
                  !isNaN(
                    new Date(
                      this.formsExtraairports[i - 1].getRawValue().datebegin
                    ).valueOf()
                  )
                ) {
                  begin = new Date(
                    this.formsExtraairports[i - 1].getRawValue().datebegin
                  );
                  begin.setHours(
                    this.formsExtraairports[i - 1]
                      .getRawValue()
                      .timebegin.split(':')[0]
                  );
                  begin.setMinutes(
                    this.formsExtraairports[i - 1]
                      .getRawValue()
                      .timebegin.split(':')[1]
                  );
                }
                if (
                  new Date(
                    this.formsExtraairports[i].getRawValue().dateend
                  ) instanceof Date &&
                  !isNaN(
                    new Date(
                      this.formsExtraairports[i].getRawValue().dateend
                    ).valueOf()
                  )
                ) {
                  end = new Date(
                    this.formsExtraairports[i].getRawValue().dateend
                  );
                  end.setHours(
                    this.formsExtraairports[i]
                      .getRawValue()
                      .timeend.split(':')[0]
                  );
                  end.setMinutes(
                    this.formsExtraairports[i]
                      .getRawValue()
                      .timeend.split(':')[1]
                  );
                }
                let sum = (end - begin) / 60000;
                let sumStr = '';
                if (sum > 59) {
                  if (Math.floor(sum / 60) < 10) {
                    sumStr += '0';
                  }
                  sumStr += Math.floor(sum / 60);
                } else {
                  sumStr += '00';
                }
                sumStr += ':';
                if (Math.floor(sum % 60) < 10) {
                  sumStr += '0';
                }
                sumStr += Math.floor(sum % 60);
                this.formsExtraairports[i].patchValue({
                  time: sum > 0 && sum < 200000 ? sumStr : '---'
                });
              }
            }
          }
        }
      } else if (false) {
      }
    } else if (b === false) {
      if (true) {
        if (this.formsExtraairports.length === 0) {
          if (
            !(
              new Date(this.formFlight.getRawValue().datebegin) instanceof Date
            ) &&
            isNaN(new Date(this.formFlight.getRawValue().datebegin).valueOf())
          ) {
            return;
          }
          if (
            isNullOrUndefined(this.formFlight.getRawValue().timebegin) ||
            isNullOrUndefined(this.formFlight.getRawValue().time)
          ) {
            return;
          }
          if (
            this.formFlight.getRawValue().timebegin.length !== 5 ||
            this.formFlight.getRawValue().time.length !== 5
          ) {
            return;
          }
          let begin = new Date(this.formFlight.getRawValue().datebegin);
          begin.setHours(this.formFlight.getRawValue().timebegin.split(':')[0]);
          begin.setMinutes(
            this.formFlight.getRawValue().timebegin.split(':')[1]
          );
          let _time = this.formFlight.getRawValue().time.toString();
          let time =
            _time.split(':')[0] * 60 * 60 * 1000 +
            _time.split(':')[1] * 60 * 1000;
          let sum = new Date(begin.getTime() + time);
          let _hours: number | string = sum.getHours();
          _hours = ('0' + _hours).slice(-2);
          let _minutes: number | string = sum.getMinutes();
          _minutes = ('0' + _minutes).slice(-2);
          this.formFlight.patchValue({ dateend: sum });
          this.formFlight.patchValue({ timeend: `${_hours}:${_minutes}` });
        } else {
          for (let i = 0; i < this.formsExtraairports.length; i++) {
            if (i === 0) {
              if (
                !(
                  new Date(this.formFlight.getRawValue().datebegin) instanceof
                  Date
                ) &&
                isNaN(
                  new Date(this.formFlight.getRawValue().datebegin).valueOf()
                )
              ) {
                return;
              }
              if (
                isNullOrUndefined(this.formFlight.getRawValue().timebegin) ||
                isNullOrUndefined(this.formsExtraairports[i].getRawValue().time)
              ) {
                return;
              }
              if (
                this.formFlight.getRawValue().timebegin.length !== 5 ||
                this.formsExtraairports[i].getRawValue().time.length !== 5
              ) {
                return;
              }
              let begin = new Date(this.formFlight.getRawValue().datebegin);
              begin.setHours(
                this.formFlight.getRawValue().timebegin.split(':')[0]
              );
              begin.setMinutes(
                this.formFlight.getRawValue().timebegin.split(':')[1]
              );
              let _time = this.formsExtraairports[i]
                .getRawValue()
                .time.toString();
              let time =
                _time.split(':')[0] * 60 * 60 * 1000 +
                _time.split(':')[1] * 60 * 1000;
              let sum = new Date(begin.getTime() + time);
              let _hours: number | string = sum.getHours();
              _hours = ('0' + _hours).slice(-2);
              let _minutes: number | string = sum.getMinutes();
              _minutes = ('0' + _minutes).slice(-2);
              this.formsExtraairports[i].patchValue({ dateend: sum });
              this.formsExtraairports[i].patchValue({
                timeend: `${_hours}:${_minutes}`
              });
            }
            if (i === this.formsExtraairports.length - 1) {
              if (
                new Date(
                  this.formsExtraairports[i].getRawValue().datebegin
                ) instanceof Date &&
                !isNaN(
                  new Date(
                    this.formsExtraairports[i].getRawValue().datebegin
                  ).valueOf()
                )
              ) {
                if (
                  !isNullOrUndefined(
                    this.formsExtraairports[i].getRawValue().timebegin
                  ) &&
                  !isNullOrUndefined(this.formFlight.getRawValue().time)
                ) {
                  if (
                    this.formsExtraairports[i].getRawValue().timebegin
                      .length === 5 &&
                    this.formFlight.getRawValue().time.length === 5
                  ) {
                    let begin = new Date(
                      this.formsExtraairports[i].getRawValue().datebegin
                    );
                    begin.setHours(
                      this.formsExtraairports[i]
                        .getRawValue()
                        .timebegin.split(':')[0]
                    );
                    begin.setMinutes(
                      this.formsExtraairports[i]
                        .getRawValue()
                        .timebegin.split(':')[1]
                    );
                    let _time = this.formFlight.getRawValue().time.toString();
                    let time =
                      _time.split(':')[0] * 60 * 60 * 1000 +
                      _time.split(':')[1] * 60 * 1000;
                    let sum = new Date(begin.getTime() + time);
                    let _hours: number | string = sum.getHours();
                    _hours = ('0' + _hours).slice(-2);
                    let _minutes: number | string = sum.getMinutes();
                    _minutes = ('0' + _minutes).slice(-2);
                    this.formFlight.patchValue({ dateend: sum });
                    this.formFlight.patchValue({
                      timeend: `${_hours}:${_minutes}`
                    });
                  }
                }
              }
            }
            if (i !== 0) {
              if (
                new Date(
                  this.formsExtraairports[i - 1].getRawValue().datebegin
                ) instanceof Date &&
                !isNaN(
                  new Date(
                    this.formsExtraairports[i - 1].getRawValue().datebegin
                  ).valueOf()
                )
              ) {
                if (
                  !isNullOrUndefined(
                    this.formsExtraairports[i - 1].getRawValue().timebegin
                  ) &&
                  !isNullOrUndefined(
                    this.formsExtraairports[i].getRawValue().time
                  )
                ) {
                  if (
                    this.formsExtraairports[i - 1].getRawValue().timebegin
                      .length === 5 &&
                    this.formsExtraairports[i].getRawValue().time.length === 5
                  ) {
                    let begin = new Date(
                      this.formsExtraairports[i - 1].getRawValue().datebegin
                    );
                    begin.setHours(
                      this.formsExtraairports[i - 1]
                        .getRawValue()
                        .timebegin.split(':')[0]
                    );
                    begin.setMinutes(
                      this.formsExtraairports[i - 1]
                        .getRawValue()
                        .timebegin.split(':')[1]
                    );
                    let _time = this.formsExtraairports[i]
                      .getRawValue()
                      .time.toString();
                    let time =
                      _time.split(':')[0] * 60 * 60 * 1000 +
                      _time.split(':')[1] * 60 * 1000;
                    let sum = new Date(begin.getTime() + time);
                    let _hours: number | string = sum.getHours();
                    _hours = ('0' + _hours).slice(-2);
                    let _minutes: number | string = sum.getMinutes();
                    _minutes = ('0' + _minutes).slice(-2);
                    this.formsExtraairports[i].patchValue({ dateend: sum });
                    this.formsExtraairports[i].patchValue({
                      timeend: `${_hours}:${_minutes}`
                    });
                  }
                }
              }
            }
          }
        }
      }
      this.onCalcTime(true);
    }
  }

  inputLink_departure() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.departure_airport_control.value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_departure_airport = this.departure_airport_control.valueChanges.pipe(
            startWith<any>(''),
            map(value =>
              typeof value === 'string'
                ? value
                : isNullOrUndefined(value)
                ? value
                : value.code
            ),
            map(code => (code ? this._filter(code) : this.airports.slice()))
          );
        } catch {}
      }
    }, 250);
  }

  inputLink_arrival() {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.arrival_airport_control.value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_arrival_airport = this.arrival_airport_control.valueChanges.pipe(
            startWith<string | any>(''),
            map(value =>
              typeof value === 'string'
                ? value
                : isNullOrUndefined(value)
                ? value
                : value.code
            ),
            map(code => (code ? this._filter(code) : this.airports.slice()))
          );
        } catch {}
      }
    }, 250);
  }

  inputLink_extra(idx) {
    clearTimeout(this.timerId);
    this.timerId = setTimeout(async () => {
      this.airports = await this.airportService.getWithCodefilterLimit(
        10,
        this.extra_airport_controls[idx].value
      );
      if (!isNullOrUndefined(this.airports)) {
        try {
          this.filtered_extra_airport = this.extra_airport_controls[
            idx
          ].valueChanges.pipe(
            startWith<string | any>(''),
            map(value => (typeof value === 'string' ? value : value.code)),
            map(code => (code ? this._filter(code) : this.airports.slice()))
          );
        } catch {}
      }
    }, 250);
  }

  private _filter(code: string): any[] {
    const filterValue = code.toLowerCase();
    return isNullOrUndefined(this.airports)
      ? null
      : this.airports.filter(
          airport => airport.code.toLowerCase().indexOf(filterValue) !== -1
        );
  }

  displayFn(airport?: any): string | undefined {
    return airport ? `${airport.code} ${airport.name_rus}` : undefined;
  }

  async getCreated() {
    if (this.data.type !== 'create') {
      return;
    }
    try {
      this.spinnerService.show();
      let fn = this.formFlight.value.flight_number;
      if (!isNullOrUndefined(fn) && fn.length > 0) {
        let flight = await this.flightService.getLastByNumber(fn);
        if (!isNullOrUndefined(flight) && !isNullOrUndefined(flight[0])) {
          let specificflights = await this.specificflightService._getByFlight(
            flight[0].id
          );
          if (
            !isNullOrUndefined(specificflights) &&
            !isNullOrUndefined(specificflights[0])
          ) {
            let flightpoints = await this.flightpointService.getBySpecificflight(
              specificflights[0].id
            );
            if (!isNullOrUndefined(flightpoints) && flightpoints.length === 2) {
              let datebegin_ = flightpoints[0].calc_departure_datatime.split(
                'T'
              );
              let datebegin = datebegin_[0]
                .split('-')
                .concat(datebegin_[1].split(':'));
              let dateend_ = flightpoints[1].calc_arrival_datatime.split('T');
              let dateend = dateend_[0]
                .split('-')
                .concat(dateend_[1].split(':')); // yyyy mm dd hh mm ss
              this.formFlight.patchValue({
                timebegin: `${datebegin[3]}:${datebegin[4]}`,
                timeend: `${dateend[3]}:${dateend[4]}`
              });
              this.onCalcTime(true);
              let departure_airport_id;
              let arrival_airport_id;
              try {
                departure_airport_id = this.airportService.getOneById(
                  flightpoints[0].airport_id
                );
                arrival_airport_id = this.airportService.getOneById(
                  flightpoints[1].airport_id
                );
              } catch (err) {
                console.error(err);
              }
              this.departure_airport_control.patchValue(
                await departure_airport_id
              );
              this.arrival_airport_control.patchValue(await arrival_airport_id);
            }
          }
        }
      }
    } catch (err) {
      this.globalsettingsService.exceptionHandling(err);
    } finally {
      this.spinnerService.hide();
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000
    });
  }
}
