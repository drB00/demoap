import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemComponent } from './system.component';
import { AuthGuard } from '../shared/services/auth.guard';
import { OrganisationModule } from './organisation/organisation.module';
import { TimetableModule } from './timetable/timetable.module';
// import { OrganizationModule } from './organization/organization.module';

const routes: Routes = [
  {
    path: 'sys',
    component: SystemComponent,
    data: { breadcrumb: '' },
    canActivate: [AuthGuard],
    children: [
      // { path: 'organization', loadChildren: () => OrganizationModule, },
      // { path: 'certification', loadChildren: () => CertificationModule, },
      // { path: 'timetable', loadChildren: () => TimetableModule, },
      // { path: 'docs', loadChildren: () => DocsModule, },
      {
        path: 'organisation',
        // loadChildren: './organisation/organisation.module#OrganisationModule',
        loadChildren: () =>
          import('./organisation/organisation.module').then(
            m => m.OrganisationModule
          ),
        // loadChildren: () => OrganisationModule,
        data: { breadcrumb: '' }
      },
      {
        path: 'timetable',
        // loadChildren: './timetable/timetable.module#TimetableModule',
        loadChildren: () =>
          import('./timetable/timetable.module').then(m => m.TimetableModule),
        // loadChildren: () => TimetableModule,
        data: { breadcrumb: '' }
      }
      // {
      //   path: 'certification',
      //   loadChildren: './certification/certification.module#CertificationModule',
      //   data: { breadcrumb: '' }
      // },
      // { path: 'timetable', loadChildren: './timetable/timetable.module#TimetableModule', },
      // {
      //   path: 'docs',
      //   loadChildren: './docs/docs.module#DocsModule',
      //   data: { breadcrumb: '' }
      // },
      // { path: 'pilot', loadChildren: './pilot/pilot.module#PilotModule', },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule {}
