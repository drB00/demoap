import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs';
import { IMessage, WebsocketService, WS_API } from './websocket';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  // messages$: Observable<IMessage[]>;
  // counter$: Observable<number>;
  // texts$: Observable<string[]>;

  public form: FormGroup;

  constructor(
    // private fb: FormBuilder,
    // private wsService: WebsocketService
  ) {
  }

  async ngOnInit() {
    // let arr = [1, 2, 3, 4];
    // let arr1 = [...arr, 5, 6];
    // this.form = this.fb.group({
    //   text: [null, [
    //     Validators.required
    //   ]]
    // });

    // this.messages$ = this.wsService.addEventListener(WS_API.EVENTS.MESSAGES);
    // this.messages$.subscribe(data => {
    //   console.log(data);
    // });

  }

  ngOnDestroy() {

  }

  // public sendText(): void {
    // if (this.form.valid) {
    //   this.wsService.sendMessage(WS_API.COMMANDS.SEND_TEXT, this.form.value.text);
    //   this.form.reset();
    // }
  // }

  // public removeText(index: number): void {
    // this.wsService.sendMessage(WS_API.COMMANDS.REMOVE_TEXT, index);
  // }

}
