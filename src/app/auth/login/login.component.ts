import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Message } from '../../shared/models/message.model';
import { UsersService } from '../../shared/services/users.service';
import { AuthService } from '../../shared/services/auth.service';
import { Router } from '@angular/router';
import { Airuser } from '../../shared/models/airuser.model';
import { isNullOrUndefined } from 'util';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  message: Message;

  constructor(
    private usersService: UsersService,
    private authService: AuthService,
    public snackBar: MatSnackBar,
    private router: Router
  ) { }

  async ngOnInit() {
    this.form = new FormGroup({
      'login': new FormControl(null, [Validators.required]),
      'password': new FormControl(null, [Validators.required]),
      'rmbMe': new FormControl(true)
    });
    let user: Airuser = JSON.parse(window.localStorage.getItem('user'));
    if (user) {
      let _userData = await this.usersService.getByLoginPwd(user, true);
      this.authService.login(_userData);
      // this.router.navigate(['/sys/organisation/subdivisions']);
      if (isNullOrUndefined(_userData.loggedInUserSubdivision.type)) {
        this.router.navigate(['/sys/organisation/subdivisions']);
      } else if (_userData.loggedInUserSubdivision.type === 1) {
        this.router.navigate(['/sys/timetable/fpgraphic/0']);
      } else {
        this.router.navigate(['/sys/organisation/subdivisions']);
      }
    }
  }

  async onSubmit() {
    if (this.form.invalid) {
      return;
    }
    let user = {
      login: this.form.value.login,
      password: this.form.value.password
    };
    try {
      let _userData = await this.usersService.getByLoginPwd(user, false);
      if (this.form.value.rmbMe) {
        window.localStorage.setItem('user', JSON.stringify(_userData.login));
      }
      this.authService.login(_userData);
      if (isNullOrUndefined(_userData.loggedInUserSubdivision.type)) {
        this.router.navigate(['/sys/organisation/subdivisions']);
      } else if (_userData.loggedInUserSubdivision.type === 1) {
        this.router.navigate(['/sys/timetable/fpgraphic/0']);
      } else {
        this.router.navigate(['/sys/organisation/subdivisions']);
      }
    } catch (error) {
      if (error.status === 401) {
        this.openSnackBar(`Логин или пароль не верны.`, 'Ok');
      } else {
        this.openSnackBar(`Ошибка соединения, попробуйте позже или обратитесь к системному администратору.`, 'Ok');
      }
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

}

