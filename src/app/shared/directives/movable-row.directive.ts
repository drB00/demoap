import { Directive, Input } from '@angular/core';

@Directive({
  selector: '[appMovableRow]'
})
export class MovableRowDirective {
  // tslint:disable-next-line:no-input-rename
  @Input('appRowPlaneType') appRowPlaneType = '';

  constructor() { }

}
