import {
  Directive,
  EventEmitter,
  HostListener,
  Input,
  Output,
  HostBinding
} from '@angular/core';

@Directive({
  selector: '[appFiledrop]'
})
export class FiledropDirective {

  @Input() allowed_extensions: Array<string> = [];
  @Output() filesChangeEmiter: EventEmitter<File[]> = new EventEmitter();
  @Output() filesInvalidEmiter: EventEmitter<File[]> = new EventEmitter();
  @HostBinding('style.background') background = '#fff';
  @HostBinding('style.color') color = '#000';
  @HostBinding('style.font-size') fontSize = '16px';

  constructor() { }

  @HostListener('dragover', ['$event']) public onDragOver(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#b9d5fd';
    // this.color = '#b9d5fd';
  }

  @HostListener('dragleave', ['$event']) public onDragLeave(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#fff';
    // this.color = '#000';
  }

  @HostListener('drop', ['$event']) public onDrop(evt) {
    evt.preventDefault();
    evt.stopPropagation();
    this.background = '#83b6ff';
    this.fontSize = '24px';
    // this.color = '#83b6ff';
    let files = evt.dataTransfer.files;
    let valid_files: Array<File> = [];
    let invalid_files: Array<File> = [];
    if (files.length > 0) {
      for (let i = 0; i < files.length; i++) {
        let ext = files[i].name.split('.')[files[i].name.split('.').length - 1];
        // if (this.allowed_extensions.lastIndexOf(ext) !== -1) {
        // 	valid_files.push(files[i]);
        // } else {
        // 	invalid_files.push(files[i]);
        // }
        valid_files.push(files[i]);
      }
      this.filesChangeEmiter.emit(valid_files);
      this.filesInvalidEmiter.emit(invalid_files);
    }
  }
}
