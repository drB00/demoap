import { Directive, HostBinding, HostListener, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appDraggable]'
})
export class DraggableDirective {
  // private dragging = false;
  @Output() dragStart = new EventEmitter<PointerEvent>();
  @Output() dragMove = new EventEmitter<PointerEvent>();
  @Output() dragStop = new EventEmitter<PointerEvent>();

  @HostBinding('class.draggable') draggable = true;
  @HostBinding('class.dragging') dragging = false;

  // @HostListener('pointerdown', [`$event`])
  @HostListener('mousedown', [`$event`]) onPointerDown(event: PointerEvent): void {
    this.dragging = true;
    event.stopPropagation();
    this.dragStart.emit(event);
  }
  // @HostListener('document:pointermove', ['$event'])
  @HostListener('document:mousemove', ['$event']) onPointerMove(event: PointerEvent): void {
    if (!this.dragging) {
      return;
    }
    this.dragMove.emit(event);
  }
  // @HostListener('document:pointercancel', ['$event'])
  // @HostListener('document:pointerup', ['$event'])
  @HostListener('document:mouseup', ['$event']) onPointerUp(event: PointerEvent): void {
    if (!this.dragging) {
      return;
    }
    this.dragging = false;
    this.dragStop.emit(event);
  }
}
