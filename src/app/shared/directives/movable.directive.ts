import {
  Directive,
  HostListener,
  HostBinding,
  Input,
  ElementRef,
  ContentChildren,
  QueryList,
  Output,
  EventEmitter,
  Renderer2
} from '@angular/core';
import { DraggableDirective } from './draggable.directive';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { MovableRowDirective } from './movable-row.directive';
import { isNullOrUndefined, isNull } from 'util';

interface Position {
  x: number;
  y: number;
}

@Directive({
  selector: '[appMovable]'
})
export class MovableDirective extends DraggableDirective {

  public position: Position = { x: 0, y: 0 };
  private startPosition: Position;
  private zeroPosition: Position;
  private mouseOverElement: Element;
  private rowOverElement: Element;
  private layerY: number;

  @Input() appMovableReset = false;
  @Input() aircraftmodel_id = '';
  @Output() startGrag = new EventEmitter<{}>();
  @Output() startTimePoint = new EventEmitter<number>();
  @Output() stopTimePoint = new EventEmitter<{}>();

  @HostBinding('style.transform') get transform(): SafeStyle {
    return this.sanitizer.bypassSecurityTrustStyle(
      `translateX(${this.position.x}px) translateY(${this.position.y}px)`
    );
  }

  @HostBinding('class.movable') movable = true;
  @HostBinding('class.enableToDrop') enableToDrop = false;

  constructor(private sanitizer: DomSanitizer, public element: ElementRef, private renderer: Renderer2) {
    super();
  }

  @HostListener('dragStart', ['$event']) onDragStart(event: PointerEvent) {
    this.startPosition = {
      x: event.clientX - this.position.x,
      y: event.clientY - this.position.y,
    };
    this.zeroPosition = this.position;
    this.mouseOverElement = document.elementFromPoint(event.clientX, event.clientY);
    this.renderer.setStyle(this.element.nativeElement, 'z-index', '2');
    // this.renderer.setStyle(this.element.nativeElement, 'opacity', '.99');
    this.layerY = event.layerY - this.position.y;
    this.startGrag.emit(null);
  }
  @HostListener('dragMove', ['$event']) onDragMove(event: PointerEvent) {
    this.position = {
      x: event.clientX - this.startPosition.x,
      y: event.clientY - this.startPosition.y,
    };
    // this.rowOverElement = document.elementFromPoint(this.mouseOverElement.getBoundingClientRect().left - 10,
    // 	(this.mouseOverElement.getBoundingClientRect().top + this.mouseOverElement.getBoundingClientRect().bottom) / 2);
    // if ((this.rowOverElement.id.toString() === this.aircraftmodel_id.toString()) || this.rowOverElement.id.toString() === 'all') {
    // 	this.enableToDrop = true;
    // } else {
    // 	this.enableToDrop = false;
    // }
    this.startTimePoint.emit(this.mouseOverElement.getBoundingClientRect().left);
    // console.log(this.rowOverElement.id.toString() === 'all');
    // console.log(this.aircraftmodel_id.toString());
  }
  @HostListener('dragStop', ['$event']) onDragStop(event: PointerEvent) {
    try {
      this.mouseOverElement = document.elementFromPoint(event.clientX, event.clientY);
      this.rowOverElement = document.elementFromPoint(this.mouseOverElement.getBoundingClientRect().left - 10,
        (this.mouseOverElement.getBoundingClientRect().top + this.mouseOverElement.getBoundingClientRect().bottom) / 2);
      // if (!isNullOrUndefined(this.rowOverElement) && this.rowOverElement.tagName === 'DIV' && !this.enableToDrop) {
      // 	this.position.y = ((this.rowOverElement.getBoundingClientRect().top))
      // 		- this.startPosition.y + this.layerY;
      // 	this.appMovableReset = false;
      // } else {
      // 	console.log(this.enableToDrop);
      // 	// this.position = {
      // 	// 	x: 0,
      // 	// 	y: 0,
      // 	// };
      // 	// this.position.x = this.startPosition.x + this.position.x - event.clientX ;
      // 	// this.position.y = this.startPosition.y + this.position.y - event.clientY;
      // 	this.position = this.zeroPosition;
      // 	this.enableToDrop = false;
      // }
      // console.log(this.enableToDrop);
      this.rowOverElement = document.elementFromPoint(this.mouseOverElement.getBoundingClientRect().left - 10,
        (this.mouseOverElement.getBoundingClientRect().top + this.mouseOverElement.getBoundingClientRect().bottom) / 2);
      if ((this.rowOverElement.id.toString() === this.aircraftmodel_id.toString()) || this.rowOverElement.id.toString() === 'all') {
        this.enableToDrop = true;
      } else {
        this.enableToDrop = false;
      }
      if (this.enableToDrop === true) {
        this.stopTimePoint.emit(
          { enableToDrop: this.enableToDrop, aircraft_id: this.rowOverElement.attributes.getNamedItem('aircraft_id').value }
        );
      } else {
        this.position = this.zeroPosition;
      }
      // this.enableToDrop = false;
      this.renderer.setStyle(this.element.nativeElement, 'z-index', '1');
      // this.renderer.setStyle(this.element.nativeElement, 'position', 'absolute');
    } catch (err) {
      console.error(err);
    }

  }

}
