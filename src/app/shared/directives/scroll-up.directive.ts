import { Directive, ElementRef, Renderer2, HostListener, OnInit } from '@angular/core';

@Directive({
  selector: '[appScrollUpp]'
})

export class ScrollUppDirective implements OnInit {
  ngOnInit(): void {
    this.renderer.setStyle(this.element.nativeElement, 'visibility', 'hidden');
  }

  constructor(private element: ElementRef, private renderer: Renderer2) { }

  @HostListener('window:scroll') scrollAppear() {
    let prop: string;
    const posY = (document.documentElement.scrollTop || document.body.scrollTop);
    prop = posY > 5 ? 'visible' : 'hidden';
    this.renderer.setStyle(this.element.nativeElement, 'visibility', prop);
  }

  @HostListener('click') scrollTop() {
    let posY = (document.documentElement.scrollTop || document.body.scrollTop);
    const interval = setInterval(() => {
      posY -= 40;
      // document.documentElement.scrollTo(0, posY);
      window.scroll(0, posY);
      if (posY <= 0) {
        clearInterval(interval);
      }
    });
  }
}
