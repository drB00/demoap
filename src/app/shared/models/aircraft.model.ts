/**Класс воздушного судна */
export class Aircraft {
  constructor(
    public id?: number,
    public flagarchive?: number,
    public code?: string,
    public aircraftmodel_id?: number,
    public flight_time?: number
  ) {
  }
}

/**Класс модели воздушного судна */
export class Aircraftmodel {
  constructor(
    public id?: number,
    public flagarchive?: number,
    public name?: string,
    public aircraftmodelgroup_id?: number,
  ) {
  }
}

/**Класс группы моделей воздушного судна */
export class Aircraftmodelgroup {
  constructor(
    public id?: number,
    public flagarchive?: number,
    public name?: string,
  ) {
  }
}
