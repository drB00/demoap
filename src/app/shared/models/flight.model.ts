/**Класс рейса */
export class Flight {
  constructor(
    public flight_number?: string,
    public scenario_id?: number,
    public flighttemplate_id?: number,
    public flagarchive?: number,
    public flight_direction?: number,
    public id?: number,
    public flight_type?: number
  ) {}
}
