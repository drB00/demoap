/**Класс аэропорта */
export class Airport {
  constructor(
    public id?: number,
    public code?: string,
    public name_rus?: string,
    public city_rus?: string,
    public city_id?: number,
    public country_rus?: string,
    public summer_timeshift?: number,
    public winter_timeshift?: number,
  ) {
  }
}
