/**Класс для типа сертификата */
export class Certificatetype {
  constructor(
    public id?: number,
    public type?: number,
    public active?: boolean,
    public name?: string
  ) {
  }
}
