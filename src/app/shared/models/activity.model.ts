import { Airuser } from './airuser.model';
import { Airport } from './airport.model';

/**Класс активности пользователя */
export class Activity {
  constructor(
    public id?: number,
    public scenario_id?: number,
    public activityname_id?: number,
    public airuser_id?: number,
    public airport?: Airport,
    public end_airport?: Airport,
    public last_end_airports?: Airport[],
    public bases?: Activity[],
    public airport_id?: number,
    public end_airport_id?: number,
    public flightpoint1_id?: number,
    public flightpointchain_id?: number,
    public _leftview?: number,
    public _left?: any,
    public _rest?: any,
    public _flightWidth?: any,
    public flightpoint2_id?: number,
    public flagarchive?: number,
    public begin_datetime?: Date | string,
    public end_datetime?: Date | string,
    public str_begin_datetime?: Date | string,
    public str_end_datetime?: Date | string,
    public address?: string,
    public flight_number?: string,
    public information?: string,
    public oneday?: boolean,
    public chipest?: any,
    public pilots?: Airuser[],
    public status?: number,
    public durationMin?: number,
    public durationStr?: string
  ) {}
}

/**Класс соответствия пользователя и активности */
export class Airuseractivity {
  constructor(
    public id?: number,
    public activity_id?: number,
    public airuser_id?: number
  ) {}
}
