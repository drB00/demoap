/**Класс шаблона рейса на временном отрезке */
export class Flighttemplate {
  constructor(
    public aircraftmodel_id?: number,
    public fitst_day?: Date,
    public last_day?: Date,
    public slot_adjustment?: string,
    public id?: number,
    public flagarchive?: number,
    public flight_id?: number,
    public flightlayout_id?: number
  ) {
  }
}
