/**Класс подразделения */
export class Subdivision {
  constructor(
    public id?: number,
    public disable?: boolean,
    public name?: string,
    public code?: string,
    public type?: number,
    public level?: number,
    public aircraftmodelgroup_id?: number,
    public parent_id?: number,
    public children?: Subdivision[],
    public countUsers?: number,
    public position?: number,
    public countOwnUsers?: number,
    public ownPositions?: Position[],
    public positions?: Position[]
  ) {
  }
}
