/**Класс для генерации рейса */
export class Flightlayout {
  constructor(
    public id?: number,
    public flight_id?: number,
    public type?: number,
    public name?: string,
    public flagarchive?: number,
    public layoutspecificflights?: Layoutspecificflight[]
  ) {
  }
}

/**Класс для генерации единичного рейса */
export class Layoutspecificflight {
  constructor(
    public aircraft_id?: number,
    public arrival_datetime?: Date,
    public departure_datetime?: Date,
    public arrival_airport_id?: number,
    public departure_airport_id?: number,
    public flightlayout_id?: number,
    public flagarchive?: number,
    public id?: number,
    public layoutflightpoints?: Layoutflightpoint[]
  ) {
  }
}

/**Класс для генерации промежуточной точки */
export class Layoutflightpoint {
  constructor(
    public departure_datetime?: Date,
    public airport_id?: number,
    public layoutspecificflight_id?: number,
    public flagarchive?: number,
    public id?: number,
    public arrival_datetime?: Date
  ) {
  }
}
