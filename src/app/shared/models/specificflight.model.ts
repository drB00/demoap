import { Flightpoint } from './flightpoint.model';

/**Класс единичного рейса */
export class Specificflight {
  constructor(
    public flight_id?: number,
    public flight_number?: string,
    public id?: number,
    public flagarchive?: number,
    public flighttemplate_id?: number,
    public aircraft_id?: number,
    public departure_airport_id?: number,
    public arrival_airport_id?: number,
    public departure_datetime?: string | Date,
    public arrival_datetime?: string | Date,
    public calc_departure_datatime?: string | Date,
    public calc_arrival_datatime?: string | Date,
    public flightpoints?: Flightpoint[],
    public aircraftmodel_id?: number,
    public flight_direction?: number,
    public _flightWidth?: any,
    public _left?: any,
  ) {
  }
}
