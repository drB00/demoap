/**Класс документа */
export class AirDocument {
  constructor(
    public id?: number,
    public name?: string,
    public docfile?,
    public loaddate?: Date,
    public airuser_id?: number,
    public subdivision_id?: number,
    public doctype_id?: number,
    public acquaintance?: boolean
  ) {
  }
}

/**Класс типа документа */
export class AirDocumentType {
  constructor(
    public id?: number,
    public original_id?: number,
    public name?: string,
    public dtype?: number,
  ) {
  }
}
