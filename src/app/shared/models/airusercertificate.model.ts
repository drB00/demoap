/**Класс сертификата сотрудника */
export class Airusercertificate {
  constructor(
    public id?: number,
    public certificatetype_id?: number,
    public aircraftmodelgroup_id?: number,
    public datebegin?: Date,
    public dateend?: Date,
    public date1?: Date,
    public text1?: string,
    public text2?: string,
    public text3?: string,
    public number1?: number,
    public number2?: number
  ) {
  }
}
