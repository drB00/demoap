import { Airport } from './airport.model';

/**Класс пользователя */
export class Airuser {
  constructor(
    public login?: number,
    public pilot_type?: number,
    public subdivision_id?: number,
    public position_id?: number,
    public password?: string,
    public salary?: number,
    public token?: string,
    public order_number?: string,
    public patronymic?: string,
    public id?: number,
    public name?: string,
    public surname?: string,
    public dt_of_birth?: string,
    public pass_seria?: string,
    public pass_number?: string,
    public pass_date?: string,
    public pass_code_dep?: string,
    public pass_kem_vydan?: string,
    public pass_birth_place?: string,
    public sex?: number,
    public location_index?: string,
    public location_city?: string,
    public location_street?: string,
    public location_house?: string,
    public location_flat?: string,
    public registration_index?: string,
    public registration_city?: string,
    public registration_street?: string,
    public registration_house?: string,
    public registration_flat?: string,
    public militaryduty_id?: number,
    public maritalstatus_id?: number,
    public emails?: any[],
    public phones?: any[],
    public activitys?: any[],
    public position_name?: string,
    public subdivision_name?: string,
    public pilotTypestr?: string,
    public flightTime?: any,
    public children?: Airuser[],
    public isadmin?: boolean,
    public home_airport_id?: number,
    public home_airport?: Airport,
    public accessLevel?: number
  ) {}
}

/**Класс образования пользователя */
export class Education {
  constructor(
    public id?: number,
    public institution_place?: string,
    public specialty?: string,
    public airuser_id?: number,
    public finish_year?: number,
    public diploma_number?: number
  ) {}
}

/**Класс членов семьи пользователя */
export class Familymember {
  constructor(
    public id?: number,
    public dt_of_birth?: Date,
    public name?: string,
    public surname?: string,
    public relationdegree_id?: number,
    public airuser_id?: number,
    public work_place?: number,
    public work_position?: number
  ) {}
}

/**Класс предыдущего места работы */
export class Previouswork {
  constructor(
    public id?: number,
    public airuser_id?: number,
    public work_place?: string,
    public work_position?: string,
    public dt_start?: Date,
    public dt_end?: Date
  ) {}
}

/**Класс логов пользователя */
export class AiruserLog {
  constructor(
    public login?: number,
    public subdivision_id?: number,
    public position_id?: number,
    public password?: string,
    public salary?: number,
    public token?: string,
    public order_number?: string,
    public patronymic?: string,
    public id?: number,
    public change_date_time?: string,
    public record_deleted_flag?: boolean,
    public changer_airuser_id?: number,
    public original_id?: number,
    public name?: string,
    public surname?: string,
    public dt_of_birth?: string,
    public pass_seria?: string,
    public pass_number?: string,
    public pass_date?: string,
    public pass_code_dep?: string,
    public pass_kem_vydan?: string,
    public pass_birth_place?: string,
    public sex?: number,
    public location_index?: string,
    public location_city?: string,
    public location_street?: string,
    public location_house?: string,
    public location_flat?: string,
    public registration_index?: string,
    public registration_city?: string,
    public registration_street?: string,
    public registration_house?: string,
    public registration_flat?: string,
    public militaryduty_id?: number,
    public maritalstatus_id?: number,
    public emails?: any[],
    public phones?: any[],
    public position_name?: string,
    public subdivision_name?: string,
    public children?: Airuser[]
  ) {}
}
