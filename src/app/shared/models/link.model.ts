export interface Link {
  modulLink: string;
  link: string;
  text: string;
  params?: any[];
  icon?: string;
  active?: boolean;
}
