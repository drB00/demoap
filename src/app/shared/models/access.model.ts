/**Класс прав доступа */
export class AccessRight {
  constructor(
    public resourcename?: string,
    public id?: number,
    public airuser_id?: number,
    public subdivision_id?: number,
    public get?: boolean,
    public post?: boolean,
    public _delete?: boolean,
    public put?: boolean
  ) {}
}
