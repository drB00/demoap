/**Класс задания на ознакомление с документом */
export class Acquaintance {
  constructor(
    public id?: number,
    public airuser_id?: number,
    public airdocument_id?: number,
    public maxdate?: string,
    public acquainted?: Boolean,
    public acqdate?: string,
    public downloaddate?: string,
    public availableToAcquaint?: boolean
  ) {
  }
}
