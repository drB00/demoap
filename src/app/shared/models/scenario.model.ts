/**Класс сценария для расписания и планирования */
export class Scenario {
  constructor(
    public name?: string,
    public status?: number, // 1 - основной, 2 - альтернативный
    public id?: number,
    public airuser_id?: number,
    public flagarchive?: number
  ) {
  }
}
