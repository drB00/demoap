/**Класс должности */
export class Position {
  constructor(
    public id?: number,
    public name?: string,
    public subdivision_id?: number,
    public parent_id?: number,
    public children?: Position[],
    public countUsers?: number,
    public salary?: number,
    public pilot_type?: number
  ) {
  }
}
