import { Activity } from './activity.model';

/**Класс дочки приземления */
export class Flightpoint {
  constructor(
    public departure_datetime?: string | Date,
    public left?: string,
    public right?: string,
    public airport_id?: number,
    public flightpointchain_id?: number,
    public specificflight_id?: number,
    public flagarchive?: number,
    public id?: number,
    public arrival_datetime?: string | Date,
    public calc_departure_datatime?: string | Date,
    public calc_arrival_datatime?: string | Date,
    public end_datatime?: string | Date,
    public flight_number?: string,
    public activitys?: Activity[],
    public pilots?: any[],
    public _widthview?: any,
    public _left?: any,
    public _leftview?: any,
    public _right?: any,
    public next_id?: number,
    public end_airport_id?: number,
  ) {
  }
}
