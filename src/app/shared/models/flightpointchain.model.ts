import { Flightpoint } from './flightpoint.model';

/**Класс цепочки легов */
export class Flightpointchain {
  constructor(
    public name?: string,
    public id?: number,
    public scenario_id?: number,
    public flightpoint_1?: Flightpoint,
    public flightpoint_2?: Flightpoint
  ) {
  }
}
