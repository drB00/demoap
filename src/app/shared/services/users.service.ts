import { Injectable } from '@angular/core';
import { BaseApi } from '../core/base-api';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Airuser } from '../models/airuser.model';
import { isNullOrUndefined } from 'util';
import { Subdivision } from '../models/subdivision.model';
import { Position } from '../models/position.model';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends BaseApi {

  constructor(public http: HttpClient) {
    super(http);
  }

  /**Функция получает логин пароль или токен из auth */
  async getByLoginPwd(user: Airuser, haveToken = false) {
    let login = (haveToken === false) ? await this.login('login', user).toPromise() : user;
    /**Получаем данные по сотруднику */
    let options = new HttpHeaders();
    options = options.set('Authorization', 'JWT ' + login.token);
    options = options.append('Content-Type', 'application/json');
    let _loggedInUser = this.get('airuser/' + login.id, options).toPromise();
    let _loggedInUserAccesses = this.get(`accessright/?airuser_id=${login.id}`, options).toPromise();
    let loggedInUser: Airuser = (isNullOrUndefined(await _loggedInUser)) ? null : await _loggedInUser;
    loggedInUser.token = login.token;
    let loggedInUserAccesses = (isNullOrUndefined(await _loggedInUserAccesses)) ? [] : await _loggedInUserAccesses;
    let loggedInUserPosition: Position;
    let loggedInUserSubdivision: Subdivision;
    if (isNullOrUndefined(loggedInUser.position_id)) {
      loggedInUserPosition = {};
      loggedInUserSubdivision = {};
    } else {
      let _loggedInUserPosition = this.get('position/' + loggedInUser.position_id, options).toPromise();
      loggedInUserPosition = (isNullOrUndefined(await _loggedInUserPosition)) ? null : await _loggedInUserPosition;
      if (isNullOrUndefined(loggedInUserPosition.subdivision_id)) {
        loggedInUserSubdivision = {};
      } else {
        loggedInUserSubdivision = await this.get('subdivision/' + loggedInUserPosition.subdivision_id, options).toPromise();
      }
    }
    let loggedInUserRole: number;
    if (loggedInUser.isadmin === true) {
      loggedInUserRole = 0;
    } else if (isNullOrUndefined(loggedInUserPosition.parent_id)) {
      loggedInUserRole = 1;
    } else {
      loggedInUserRole = 2;
    }
    return { login, loggedInUser, loggedInUserAccesses, loggedInUserPosition, loggedInUserSubdivision, loggedInUserRole };
  }

}
