import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Airuser } from '../models/airuser.model';
import { Subdivision } from '../models/subdivision.model';
import { Position } from '../models/position.model';
import { WebsocketService, WS_API, IMessage } from 'src/app/websocket';
import { Observable } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private isAuthenticated = false;
  private _loggedInUser: Airuser = null;

  private _loggedInUserPosition: Position;
  private _loggedInUserSubdivision: Subdivision;
  private _loggedInUserAccesses;
  private _loggedInUserRole;

  constructor(
    private router: Router
  ) { }

  login(userData) {
    this._loggedInUser = userData.loggedInUser;
    this._loggedInUserPosition = userData.loggedInUserPosition;
    this._loggedInUserSubdivision = userData.loggedInUserSubdivision;
    this._loggedInUserAccesses = userData.loggedInUserAccesses;
    this._loggedInUserRole = userData.loggedInUserRole;
    this.isAuthenticated = true;
  }

  logout() {
    this._loggedInUser = null;
    this._loggedInUserPosition = null;
    this._loggedInUserSubdivision = null;
    this._loggedInUserAccesses = null;
    window.localStorage.removeItem('user');
    this.isAuthenticated = false;
    this.router.navigate(['/login']);
    window.location.reload();
  }

  isLoggedIn(): boolean {
    return this.isAuthenticated;
  }

  get loggedInUser() {
    return this._loggedInUser;
  }

  get loggedInUserPosition() {
    return this._loggedInUserPosition;
  }

  get loggedInUserSubdivision() {
    return this._loggedInUserSubdivision;
  }

  get loggedInUserAccesses() {
    return this._loggedInUserAccesses;
  }

  get loggedInUserRole() {
    return this._loggedInUserRole;
  }

}
