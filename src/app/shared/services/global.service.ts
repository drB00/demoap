import { Injectable } from '@angular/core';
import { MatPaginatorIntl } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  constructor() { }

}

@Injectable() // перевод пагинатора
export class MatPaginatorIntlRus extends MatPaginatorIntl {
  itemsPerPageLabel = 'Строк на странице: ';
  nextPageLabel = 'Далее';
  previousPageLabel = 'Назад';

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    return ((page * pageSize) + 1) + ' - ' + ((page * pageSize) + pageSize) + ' из ' + length;
  }
}
