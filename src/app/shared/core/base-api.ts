import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

import { Airuser } from './../models/airuser.model';
import { environment } from 'src/environments/environment';


@Injectable()
export class BaseApi {

  private baseUrl = environment.baseUrl;

  constructor(public http: HttpClient) { }

  private getUrl(url: string = ''): string {
    return this.baseUrl + url;
  }

  public get(url: string = '', header: HttpHeaders): Observable<any> {
    const requestOptions = {
      headers: header
    };
    return this.http.get(this.getUrl(url), requestOptions);
  }

  // public getFile(url: string = '', header: HttpHeaders): Observable<any> {
  //   return this.http.get(this.getUrl(url), { headers: header, responseType: 'blob' });
  // }

  public getFile(url: string = '', header: HttpHeaders): Observable<any> {
    const req = new HttpRequest('GET', this.getUrl(url), {
      headers: header,
      responseType: 'blob',
      reportProgress: true,
    });
    return this.http.request(req);
  }

  public getFileBlob(url: string = '', header: HttpHeaders): Observable<any> {
    return this.http.get(this.getUrl(url), { headers: header, responseType: 'blob' });
  }

  public post(url: string = '', data, header: HttpHeaders): Observable<any> {
    const requestOptions = {
      headers: header
    };
    return this.http.post(this.getUrl(url), data, requestOptions);
  }

  public login(url: string, user: Airuser): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/x-www-form-urlencoded');
    const requestOptions = { headers };
    return this.http.post(this.getUrl(url), `login=${user.login}&password=${user.password}`, requestOptions);
  }

  public request(url: string = '', data: any = {}): Observable<any> {
    return this.http.request(this.getUrl(url), data);
  }

  public put(url: string = '', data: any = {}, header: HttpHeaders): Observable<any> {
    const requestOptions = {
      headers: header
    };
    return this.http.put(this.getUrl(url), data, requestOptions);
  }

  public delete(url: string = '', header: HttpHeaders): Observable<any> {
    const requestOptions = {
      headers: header
    };
    return this.http.delete(this.getUrl(url), requestOptions);
  }
}
