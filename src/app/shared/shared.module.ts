import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
  MatInputModule,
  MatButtonModule,
  MatCheckboxModule,
  MatSelectModule,
  MatDatepickerModule,
  MatTooltipModule,
  MatTabsModule,
  MatCardModule,
  MatAutocompleteModule,
  MatButtonToggleModule,
  MatSnackBarModule,
  MatExpansionModule,
  MatSortModule,
  MatDialogModule,
  MatPaginatorModule,
  MAT_DATE_LOCALE,
  DateAdapter,
  MatMenuModule,
  MAT_DATE_FORMATS,
  MatProgressBarModule
} from '@angular/material';
import { TextMaskModule } from 'angular2-text-mask';

import { HttpClientModule } from '@angular/common/http';
import { NgxGalleryModule } from 'ngx-gallery';
import { NgSelectModule } from '@ng-select/ng-select';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  MomentDateAdapter,
  MatMomentDateModule,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS
} from '@angular/material-moment-adapter';
import { ScrollUppDirective } from './directives/scroll-up.directive';
import { DisablecontrolDirective } from './directives/disablecontrol.directive';
import { DraggableDirective } from './directives/draggable.directive';
import { MovableDirective } from './directives/movable.directive';
import { MovableAreaDirective } from './directives/movable-area.directive';
import { MovableRowDirective } from './directives/movable-row.directive';
import { FiledropDirective } from './directives/filedrop.directive';
import { OnlyNumberDirective } from './directives/only-numbers.directive';
import { TranslateModule } from '@ngx-translate/core';
import { DropdownDirective } from './directives/dropdown.directive';
import { AircraftFilterPipe } from '../system/shared/pipes/aircraft-filter.pipe';
import { AircraftmodelFilterPipe } from '../system/shared/pipes/aircraftmodel-filter.pipe';
import { AircraftmodelgroupFilterPipe } from '../system/shared/pipes/aircraftmodelgroup-filter.pipe';
import { TooltipFlightDirective } from '../system/timetable/tooltip/tooltip-flight.directive';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { TooltipActivityDirective } from '../system/timetable/tooltip/tooltip-activity.directive';
import { NgxSpinnerModule } from 'ngx-spinner';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD.MM.YYYY'
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MM YYYY',
    fullPickerInput: 'DD.MM.YYYY',
    datePickerInput: 'DD.MM.YYYY'
  }
};

@NgModule({
  declarations: [
    DisablecontrolDirective,
    DropdownDirective,
    ScrollUppDirective,
    DraggableDirective,
    MovableDirective,
    MovableAreaDirective,
    MovableRowDirective,
    FiledropDirective,
    OnlyNumberDirective,
    TooltipFlightDirective,
    TooltipActivityDirective,
    AircraftFilterPipe,
    AircraftmodelFilterPipe,
    AircraftmodelgroupFilterPipe
  ],
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatSelectModule,
    MatCardModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatSortModule,
    MatAutocompleteModule,
    MatDialogModule,
    MatPaginatorModule,
    TextMaskModule,
    TranslateModule,
    MatProgressBarModule,
    MatMenuModule,
    NgxGalleryModule,
    NgSelectModule,
    ScrollDispatchModule,
    NgxSpinnerModule
  ],
  exports: [
    NgbModule,
    CommonModule,
    DisablecontrolDirective,
    DropdownDirective,
    ScrollUppDirective,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatSelectModule,
    MatCardModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatSortModule,
    MatAutocompleteModule,
    TextMaskModule,
    DraggableDirective,
    MovableDirective,
    MovableAreaDirective,
    MatDialogModule,
    MatPaginatorModule,
    MovableRowDirective,
    TooltipFlightDirective,
    TooltipActivityDirective,
    TranslateModule,
    MatProgressBarModule,
    MatMenuModule,
    NgxGalleryModule,
    AircraftFilterPipe,
    AircraftmodelFilterPipe,
    AircraftmodelgroupFilterPipe,
    NgSelectModule,
    ScrollDispatchModule,
    NgxSpinnerModule
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'ru' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE]
    },
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
  ]
})
export class SharedModule {}
